<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ config('app.name') }} | {{ $pageTitle }}</title>

    <!-- favicon -->
    <link rel="shortcut icon" href="{!! asset_url('favicon.ico') !!}" type="image/x-icon">
    <link rel="icon" href="{!! asset_url('favicon.ico') !!}" type="image/x-icon">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset_url('site_assets/css/font-awesome.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset_url('site_assets/css/landingpage.css') }}">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        #appLink{display:none;}
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.popup-close').click(function () {
                    $('.modal-mask').removeClass('show');
                });
            // $('.bg-mask').click(function () {
            //             $('.modal-mask').removeClass('show');
            //         });
                
            $('.click_here').click(function () {
                    $('#appLink').show();
                    $(this).parent().hide();
                });

            setTimeout(function() {        
                $('.modal-mask').addClass('show');	
            }, 5000);
        });	
    </script>
</head>

<body>
<section class="landingpage">
    <div class="landingwrp">
        <div class="container">
            <div class="ldcont">
                <div class="ldlogo">
                    <a href="#"><img src="{{ asset_url('site_assets/images/taxyapplogo.png') }}"
                                     class="img-fluid d-block" alt="icon"></a>
                </div>
                <div class="ldrightcont">
                    <img src="{{ asset_url('site_assets/images/ldphone.png') }}" class="img-fluid" alt="icon">
                </div>
                <div class="leftldcont">
                    <h1 class="ldtitle">Book a Car for  point to point travel</h1>
                    <p class="ldpara">Download the app or scan QR code to get started!</p>

                    <div class="applnk">
                        <a href="{{ config('constant.GOOGLE_PLAYSTORE_URL') }}">
                            <img src="{{ asset_url('site_assets/images/googleplay.png') }}"
                                 class="img-fluid d-block mx-auto" alt="icon">
                        </a>
                        <img src="{{ asset_url('site_assets/images/gbarcode.png') }}"
                             class="img-fluid d-block img-fluid mx-auto" alt="barcode">
                    </div>
                    <div class="applnk">
                        <a href="{{ config('constant.APPLE_APPSTORE_URL') }}">
                            <img src="{{ asset_url('site_assets/images/appstore.png') }}"
                                 class="img-fluid d-block mx-auto" alt="icon">
                        </a>
                        <img src="{{ asset_url('site_assets/images/appbarcode.png') }}"
                             class="img-fluid d-block img-fluid mx-auto"
                             alt="barcode">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>

<div class="modal-mask bg-mask dis-none anim">
    <div class="popup-container">
        <a href="javascript:;" class="popup-close">×</a>
        <div class="">
            <h1 class="ldtitle">Do you want <font>Drive</font>  & <span>Make Extra Money</span></h1>
			<div>Drivers who sign up now will pay only 10%</div>
			
            <div class="buttondiv"><button class="button click_here"  >Click Here</button></div>
            <div id="appLink">
                <div class="applnk">
                    <a href="https://play.google.com/store/apps/details?id=com.technoexponent.taxiappdriver">
                        <img src="https://destyapp.s3.amazonaws.com/site_assets/images/googleplay.png" class="img-fluid d-block mx-auto" alt="icon">
                    </a>
                    <img src="https://destyapp.s3.amazonaws.com/site_assets/images/driver-android-barcode.png" class="img-fluid d-block img-fluid mx-auto" alt="barcode">
                </div>
                <div class="applnk mr0">
                    <a href="https://itunes.apple.com/in/app/desty-driver/id1436283265">
                        <img src="https://destyapp.s3.amazonaws.com/site_assets/images/appstore.png" class="img-fluid d-block mx-auto" alt="icon">
                    </a>
                    <img src="https://destyapp.s3.amazonaws.com/site_assets/images/driver-ios-barcode.png" class="img-fluid d-block img-fluid mx-auto" alt="barcode">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

</body>
</html>