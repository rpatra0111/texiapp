@extends('site.layouts.app')

@section('pageTitle', 'Terms')

@section('customStyles')

@endsection

@section('content')
    <section class="termpage">
        <div class="container clearfix">
            <div class="col-lg-4 col-md-4 coltbmenu float-right">
                <div class="termTab">
                    <ul class="list-unstyled">
                        <li><a class="tablink" href="#termTab1">Term</a></li>
                        <li class="dropmenu">
                            <a href="javascript:void(0);">Privacy</a>
                            <ul class="submenutab">
                                <li><a class="tablink" href="#termTab2">Cookies</a></li>
                                <li><a class="tablink" href="#termTab3">Candidate Privacy</a></li>
                                <li><a class="tablink" href="#termTab4">Privacy Policy</a></li>
                                <li><a class="tablink" href="#termTab5">Cookies</a></li>
                            </ul>
                        </li>
                        <li class="dropmenu">
                            <a href="javascript:void(0);">Desty for Business</a>
                            <ul class="submenutab">
                                <li><a class="tablink" href="#termTab6">Australia Terms</a></li>
                                <li><a class="tablink" href="#termTab7">Indian Terms</a></li>
                                <li><a class="tablink" href="#termTab8">International Terms</a></li>
                                <li><a class="tablink" href="#termTab9">United States Terms</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>


            </div>
            <div class="col-lg-8 col-md-4 coltabdata">

                <div class="termTabView" id="termTab1" style="display:block;">
                    <h4 class="">Legal</h4>
                    <article>
                        <h4>DESTY B.V.</h4>
                        <h4>TERMS AND CONDITIONS</h4>
                        <p>Last updated: December 4th, 2017</p>
                        <h4>1. Contractual Relationship</h4>
                        <p>These Terms of Use (“<em>Terms</em>”) govern the access or use by you, an individual, from within any country in the world (excluding the United States and its territories and possessions and Mainland China) of applications, websites, content, products, and services (the “<em>Services</em>”) made available by Desty B.V., a private limited liability company established in the Netherlands, having its offices at Mr. Treublaan 7, 1097 DP, Amsterdam, the Netherlands, registered at the Amsterdam Chamber of Commerce under number 56317441 (“<em>Desty</em>”).</p>
                        <p>PLEASE READ THESE TERMS CAREFULLY BEFORE ACCESSING OR USING THE SERVICES.</p>
                        <p>Your access and use of the Services constitutes your agreement to be bound by these Terms, which establishes a contractual relationship between you and Desty. If you do not agree to these Terms, you may not access or use the Services. These Terms expressly supersede prior agreements or arrangements with you. Desty may immediately terminate these Terms or any Services with respect to you, or generally cease offering or deny access to the Services or any portion thereof, at any time for any reason.</p>
                        <p>Supplemental terms may apply to certain Services, such as policies for a particular event, activity or promotion, and such supplemental terms will be disclosed to you in connection with the applicable Services. Supplemental terms are in addition to, and shall be deemed a part of, the Terms for the purposes of the applicable Services. Supplemental terms shall prevail over these Terms in the event of a conflict with respect to the applicable Services.</p>
                        <p>Desty may amend the Terms related to the Services from time to time. Amendments will be effective upon Desty’s posting of such updated Terms at this location or the amended policies or supplemental terms on the applicable Service. Your continued access or use of the Services after such posting constitutes your consent to be bound by the Terms, as amended.</p>
                        <p>Our collection and use of personal information in connection with the Services is as provided in Desty’s Privacy Policy located at <a href="https://www.desty.com/legal/usa/privacy">https://www.desty.com/legal</a>. Desty may provide to a claims processor or an insurer any necessary information (including your contact information) if there is a complaint, dispute or conflict, which may include an accident, involving you and a Third Party Provider (including a transportation network company driver) and such information or data is necessary to resolve the complaint, dispute or conflict.</p>
                        <h4>2. The Services</h4>
                        <p>The Services constitute a technology platform that enables users of Desty’s mobile applications or websites provided as part of the Services (each, an “<em>Application</em>”) to arrange and schedule transportation and/or logistics services with independent third party providers of such services, including independent third party transportation providers and independent third party logistics providers under agreement with Desty or certain of Desty’s affiliates (“<em>Third Party Providers</em>”). Unless otherwise agreed by Desty in a separate written agreement with you, the Services are made available solely for your personal, noncommercial use. YOU ACKNOWLEDGE THAT DESTY DOES NOT PROVIDE TRANSPORTATION OR LOGISTICS SERVICES OR FUNCTION AS A TRANSPORTATION CARRIER AND THAT ALL SUCH TRANSPORTATION OR LOGISTICS SERVICES ARE PROVIDED BY INDEPENDENT THIRD PARTY CONTRACTORS WHO ARE NOT EMPLOYED BY DESTY OR ANY OF ITS AFFILIATES.</p>
                        <h5>License.</h5>
                        <p>Subject to your compliance with these Terms, Desty grants you a limited, non-exclusive, non-sublicensable, revocable, non-transferrable license to: (i) access and use the Applications on your personal device solely in connection with your use of the Services; and (ii) access and use any content, information and related materials that may be made available through the Services, in each case solely for your personal, noncommercial use. Any rights not expressly granted herein are reserved by Desty and Desty’s licensors.</p>
                        <h5>Restrictions.</h5>
                        <p>You may not: (i) remove any copyright, trademark or other proprietary notices from any portion of the Services; (ii) reproduce, modify, prepare derivative works based upon, distribute, license, lease, sell, resell, transfer, publicly display, publicly perform, transmit, stream, broadcast or otherwise exploit the Services except as expressly permitted by Desty; (iii) decompile, reverse engineer or disassemble the Services except as may be permitted by applicable law; (iv) link to, mirror or frame any portion of the Services; (v) cause or launch any programs or scripts for the purpose of scraping, indexing, surveying, or otherwise data mining any portion of the Services or unduly burdening or hindering the operation and/or functionality of any aspect of the Services; or (vi) attempt to gain unauthorized access to or impair any aspect of the Services or its related systems or networks.</p>
                        <h5>Provision of the Services.</h5>
                        <p>You acknowledge that portions of the Services may be made available under Desty’s various brands or request options associated with transportation or logistics, including the transportation request brands currently referred to as “<em>Desty</em>,” “<em>destyPOP</em>,” “<em>destyX</em>,” “<em>destyXL</em>,” “<em>DestyBLACK</em>,” “<em>DestySUV</em>,” “<em>DestyBERLINE</em>,” “<em>DestyVAN</em>,” “<em>DestyEXEC</em>,” and “<em>DestyLUX</em>” and the logistics request brands currently referred to as “<em>DestyRUSH</em>,” “<em>DestyFRESH</em>” and “<em>DestyEATS</em>”. You also acknowledge that the Services may be made available under such brands or request options by or in connection with: (i) certain of Desty’s subsidiaries and affiliates; or (ii) independent Third Party Providers, including transportation network company drivers, transportation charter permit holders or holders of similar transportation permits, authorizations or licenses.</p>
                        <h5>Third Party Services and Content.</h5>
                        <p>The Services may be made available or accessed in connection with third party services and content (including advertising) that Desty does not control. You acknowledge that different terms of use and privacy policies may apply to your use of such third party services and content. Desty does not endorse such third party services and content and in no event shall Desty be responsible or liable for any products or services of such third party providers. Additionally, Apple Inc., Google, Inc., Microsoft Corporation or BlackBerry Limited and/or their applicable international subsidiaries and affiliates will be third-party beneficiaries to this contract if you access the Services using Applications developed for Apple iOS, Android, Microsoft Windows, or Blackberry-powered mobile devices, respectively. These third party beneficiaries are not parties to this contract and are not responsible for the provision or support of the Services in any manner. Your access to the Services using these devices is subject to terms set forth in the applicable third party beneficiary’s terms of service.</p>
                        <h5>Ownership.</h5>
                        <p>The Services and all rights therein are and shall remain Desty’s property or the property of Desty’s licensors. Neither these Terms nor your use of the Services convey or grant to you any rights: (i) in or related to the Services except for the limited license granted above; or (ii) to use or reference in any manner Desty’s company names, logos, product and service names, trademarks or services marks or those of Desty’s licensors.</p>
                        <h4>3. Your Use of the Services</h4>
                        <h5>User Accounts.</h5>
                        <p>In order to use most aspects of the Services, youmust register for and maintain an active personal user Services account (“<em>Account</em>”). You must be at least 18 years of age, or the age of legal majority in your jurisdiction (if different than 18), to obtain an Account. Account registration requires you to submit to Desty certain personal information, such as your name, address, mobile phone number and age, as well as at least one valid payment method (either a credit card or accepted payment partner). You agree to maintain accurate, complete, and up-to-date information in your Account. Your failure to maintain accurate, complete, and up-to-date Account information, including having an invalid or expired payment method on file, may result in your inability to access and use the Services or Desty’s termination of these Terms with you. You are responsible for all activity that occurs under your Account, and you agree to maintain the security and secrecy of your Account username and password at all times. Unless otherwise permitted by Desty in writing, you may only possess one Account.</p>
                        <h5>User Requirements and Conduct.</h5>
                        <p>The Service is not available for use by persons under the age of 18. You may not authorize third parties to use your Account, and you may not allow persons under the age of 18 to receive transportation or logistics services from Third Party Providers unless they are accompanied by you. You may not assign or otherwise transfer your Account to any other person or entity. You agree to comply with all applicable laws when using the Services, and you may only use the Services for lawful purposes (<em>e.g.</em>, no transport of unlawful or hazardous materials). You will not, in your use of the Services, cause nuisance, annoyance, inconvenience, or property damage, whether to the Third Party Provider or any other party. In certain instances you may be asked to provide proof of identity to access or use the Services, and you agree that you may be denied access to or use of the Services if you refuse to provide proof of identity.</p>
                        <h5>Text Messaging.</h5>
                        <p>By creating an Account, you agree that the Services may send you text (SMS) messages as part of the normal business operation of your use of the Services. You may opt-out of receiving text (SMS) messages from Desty at any time by following the directions found at <a href="htttp://t.desty.com/SMS-unsubscribe">htttp://t.desty.com/SMS-unsubscribe</a>. You acknowledge that opting out of receiving text (SMS) messages may impact your use of the Services.</p>
                        <h5>Promotional Codes.</h5>
                        <p>Desty may, in Desty’s sole discretion, create promotional codes that may be redeemed for Account credit, or other features or benefits related to the Services and/or a Third Party Provider’s services, subject to any additional terms that Desty establishes on a per promotional code basis (“<em>Promo Codes</em>”). You agree that Promo Codes: (i) must be used for the intended audience and purpose, and in a lawful manner; (ii) may not be duplicated, sold or transferred in any manner, or made available to the general public (whether posted to a public form or otherwise), unless expressly permitted by Desty; (iii) may be disabled by Desty at any time for any reason without liability to Desty; (iv) may only be used pursuant to the specific terms that Desty establishes for such Promo Code; (v) are not valid for cash; and (vi) may expire prior to your use. Desty reserves the right to withhold or deduct credits or other features or benefits obtained through the use of Promo Codes by you or any other user in the event that Desty determines or believes that the use or redemption of the Promo Code was in error, fraudulent, illegal, or in violation of the applicable Promo Code terms or these Terms.</p>
                        <h5 id="user-provided-content-">User Provided Content.</h5>
                        <p>Desty may, in Desty’s sole discretion, permit you from time to time to submit, upload, publish or otherwise make available to Desty through the Services textual, audio, and/or visual content and information, including commentary and feedback related to the Services, initiation of support requests, and submission of entries for competitions and promotions (“<em>User Content</em>”). Any User Content provided by you remains your property. However, by providing User Content to Desty, you grant Desty a worldwide, perpetual, irrevocable, transferrable, royalty-free license, with the right to sublicense, to use, copy, modify, create derivative works of, distribute, publicly display, publicly perform, and otherwise exploit in any manner such User Content in all formats and distribution channels now known or hereafter devised (including in connection with the Services and Desty’s business and on third-party sites and services), without further notice to or consent from you, and without the requirement of payment to you or any other person or entity.</p>
                        <p>You represent and warrant that: (i) you either are the sole and exclusive owner of all User Content or you have all rights, licenses, consents and releases necessary to grant Desty the license to the User Content as set forth above; and (ii) neither the User Content nor your submission, uploading, publishing or otherwise making available of such User Content nor Desty’s use of the User Content as permitted herein will infringe, misappropriate or violate a third party’s intellectual property or proprietary rights, or rights of publicity or privacy, or result in the violation of any applicable law or regulation.</p>
                        <p>You agree to not provide User Content that is defamatory, libelous, hateful, violent, obscene, pornographic, unlawful, or otherwise offensive, as determined by Desty in its sole discretion, whether or not such material may be protected by law. Desty may, but shall not be obligated to, review, monitor, or remove User Content, at Desty’s sole discretion and at any time and for any reason, without notice to you.</p>
                        <h5 id="network-access-and-devices-">Network Access and Devices.</h5>
                        <p>You are responsible for obtaining the data network access necessary to use the Services. Your mobile network’s data and messaging rates and fees may apply if you access or use the Services from a wireless-enabled device and you shall be responsible for such rates and fees. You are responsible for acquiring and updating compatible hardware or devices necessary to access and use the Services and Applications and any updates thereto. Desty does not guarantee that the Services, or any portion thereof, will function on any particular hardware or devices. In addition, the Services may be subject to malfunctions and delays inherent in the use of the Internet and electronic communications.</p>
                        <h4 id="4-payment">4. Payment</h4>
                        <p>You understand that use of the Services may result in charges to you for the services or goods you receive from a Third Party Provider (“<em>Charges</em> ”). After you have received services or goods obtained through your use of the Service, Desty will facilitate your payment of the applicable Charges on behalf of the Third Party Provider as such Third Party Provider’s limited payment collection agent. Payment of the Charges in such manner shall be considered the same as payment made directly by you to the Third Party Provider. Charges will be inclusive of applicable taxes where required by law. Charges paid by you are final and non-refundable, unless otherwise determined by Desty. You retain the right to request lower Charges from a Third Party Provider for services or goods received by you from such Third Party Provider at the time you receive such services or goods. Desty will respond accordingly to any request from a Third Party Provider to modify the Charges for a particular service or good.</p>
                        <p>All Charges are due immediately and payment will be facilitated by Desty using the preferred payment method designated in your Account, after which Desty will send you a receipt by email. If your primary Account payment method is determined to be expired, invalid or otherwise not able to be charged, you agree that Desty may, as the Third Party Provider’s limited payment collection agent, use a secondary payment method in your Account, if available.</p>
                        <p>As between you and Desty, Desty reserves the right to establish, remove and/or revise Charges for any or all services or goods obtained through the use of the Services at any time in Desty’s sole discretion. Further, you acknowledge and agree that Charges applicable in certain geographical areas may increase substantially during times of high demand. Desty will use reasonable efforts to inform you of Charges that may apply, provided that you will be responsible for Charges incurred under your Account regardless of your awareness of such Charges or the amounts thereof. Desty may from time to time provide certain users with promotional offers and discounts that may result in different amounts charged for the same or similar services or goods obtained through the use of the Services, and you agree that such promotional offers and discounts, unless also made available to you, shall have no bearing on your use of the Services or the Charges applied to you. You may elect to cancel your request for services or goods from a Third Party Provider at any time prior to such Third Party Provider’s arrival, in which case you may be charged a cancellation fee.</p>
                        <p>This payment structure is intended to fully compensate the Third Party Provider for the services or goods provided. Except with respect to taxicab transportation services requested through the Application, Desty does not designate any portion of your payment as a tip or gratuity to the Third Party Provider. Any representation by Desty (on Desty’s website, in the Application, or in Desty’s marketing materials) to the effect that tipping is “voluntary,” “not required,” and/or “included” in the payments you make for services or goods provided is not intended to suggest that Desty provides any additional amounts, beyond those described above, to the Third Party Provider. You understand and agree that, while you are free to provide additional payment as a gratuity to any Third Party Provider who provides you with services or goods obtained through the Service, you are under no obligation to do so. Gratuities are voluntary. After you have received services or goods obtained through the Service, you will have the opportunity to rate your experience and leave additional feedback about your Third Party Provider.</p>
                        <h5 id="repair-or-cleaning-fees-">Repair or Cleaning Fees.</h5>
                        <p>You shall be responsible for the cost of repair for damage to, or necessary cleaning of, Third Party Provider vehicles and property resulting from use of the Services under your Account in excess of normal “wear and tear” damages and necessary cleaning (“<em>Repair or Cleaning</em>”). In the event that a Third Party Provider reports the need for Repair or Cleaning, and such Repair or Cleaning request is verified by Desty in Desty’s reasonable discretion, Desty reserves the right to facilitate payment for the reasonable cost of such Repair or Cleaning on behalf of the Third Party Provider using your payment method designated in your Account. Such amounts will be transferred by Desty to the applicable Third Party Provider and are non-refundable.</p>
                        <h4 id="5-disclaimers-limitation-of-liability-indemnity-">5. Disclaimers; Limitation of Liability; Indemnity.</h4>
                        <h5 id="disclaimer-">DISCLAIMER.</h5>
                        <p>THE SERVICES ARE PROVIDED “AS IS” AND “AS AVAILABLE.” DESTY DISCLAIMS ALL REPRESENTATIONS AND WARRANTIES, EXPRESS, IMPLIED OR STATUTORY, NOT EXPRESSLY SET OUT IN THESE TERMS, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN ADDITION, DESTY MAKES NO REPRESENTATION, WARRANTY, OR GUARANTEE REGARDING THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY OR AVAILABILITY OF THE SERVICES OR ANY SERVICES OR GOODS REQUESTED THROUGH THE USE OF THE SERVICES, OR THAT THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE. DESTY DOES NOT GUARANTEE THE QUALITY, SUITABILITY, SAFETY OR ABILITY OF THIRD PARTY PROVIDERS. YOU AGREE THAT THE ENTIRE RISK ARISING OUT OF YOUR USE OF THE SERVICES, AND ANY SERVICE OR GOOD REQUESTED IN CONNECTION THEREWITH, REMAINS SOLELY WITH YOU, TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAW.</p>
                        <h5 id="limitation-of-liability-">LIMITATION OF LIABILITY.</h5>
                        <p>DESTY SHALL NOT BE LIABLE FOR INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, LOST DATA, PERSONAL INJURY OR PROPERTY DAMAGE RELATED TO, IN CONNECTION WITH, OR OTHERWISE RESULTING FROM ANY USE OF THE SERVICES, EVEN IF DESTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. DESTY SHALL NOT BE LIABLE FOR ANY DAMAGES, LIABILITY OR LOSSES ARISING OUT OF: (i) YOUR USE OF OR RELIANCE ON THE SERVICES OR YOUR INABILITY TO ACCESS OR USE THE SERVICES; OR (ii) ANY TRANSACTION OR RELATIONSHIP BETWEEN YOU AND ANY THIRD PARTY PROVIDER, EVEN IF DESTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. DESTY SHALL NOT BE LIABLE FOR DELAY OR FAILURE IN PERFORMANCE RESULTING FROM CAUSES BEYOND DESTY’S REASONABLE CONTROL. YOU ACKNOWLEDGE THAT THIRD PARTY TRANSPORTATION PROVIDERS PROVIDING TRANSPORTATION SERVICES REQUESTED THROUGH SOME REQUEST BRANDS MAY OFFER RIDESHARING OR PEER-TO-PEER TRANSPORTATION SERVICES AND MAY NOT BE PROFESSIONALLY LICENSED OR PERMITTED. IN NO EVENT SHALL DESTY’S TOTAL LIABILITY TO YOU IN CONNECTION WITH THE SERVICES FOR ALL DAMAGES, LOSSES AND CAUSES OF ACTION EXCEED FIVE HUNDRED EUROS (€500).</p>
                        <p>DESTY’S SERVICES MAY BE USED BY YOU TO REQUEST AND SCHEDULE TRANSPORTATION, GOODS OR LOGISTICS SERVICES WITH THIRD PARTY PROVIDERS, BUT YOU AGREE THAT DESTY HAS NO RESPONSIBILITY OR LIABILITY TO YOU RELATED TO ANY TRANSPORTATION, GOODS OR LOGISTICS SERVICES PROVIDED TO YOU BY THIRD PARTY PROVIDERS OTHER THAN AS EXPRESSLY SET FORTH IN THESE TERMS.</p>
                        <p>THE LIMITATIONS AND DISCLAIMER IN THIS SECTION 5 DO NOT PURPORT TO LIMIT LIABILITY OR ALTER YOUR RIGHTS AS A CONSUMER THAT CANNOT BE EXCLUDED UNDER APPLICABLE LAW.</p>
                        <h5 id="indemnity-">Indemnity.</h5>
                        <p>You agree to indemnify and hold Desty and its officers, directors, employees and agents harmless from any and all claims, demands, losses, liabilities, and expenses (including attorneys’ fees) arising out of or in connection with: (i) your use of the Services or services or goods obtained through your use of the Services; (ii) your breach or violation of any of these Terms; (iii) Desty’s use of your User Content; or (iv) your violation of the rights of any third party, including Third Party Providers.</p>
                        <h4 id="6-governing-law-arbitration-">6. Governing Law; Arbitration.</h4>
                        <p>Except as otherwise set forth in these Terms, these Terms shall be exclusively governed by and construed in accordance with the laws of The Netherlands, excluding its rules on conflicts of laws. The Vienna Convention on the International Sale of Goods of 1980 (CISG) shall not apply. Any dispute, conflict, claim or controversy arising out of or broadly in connection with or relating to the Services or these Terms, including those relating to its validity, its construction or its enforceability (any “<em>Dispute</em>”) shall be first mandatorily submitted to mediation proceedings under the International Chamber of Commerce Mediation Rules (“<em>ICC Mediation Rules</em>”). If such Dispute has not been settled within sixty (60) days after a request for mediation has been submitted under such ICC Mediation Rules, such Dispute can be referred to and shall be exclusively and finally resolved by arbitration under the Rules of Arbitration of the International Chamber of Commerce (“<em>ICC Arbitration Rules</em>”). The ICC Rules' Emergency Arbitrator provisions are excluded. The Dispute shall be resolved by one (1) arbitrator to be appointed in accordance with the ICC Rules. The place of both mediation and arbitration shall be Amsterdam, The Netherlands, without prejudice to any rights you may have under Article 18 of the Brussels I bis Regulation (OJ EU 2012 L351/1) and/or Article 6:236n of the Dutch Civil Code. The language of the mediation and/or arbitration shall be English, unless you do not spe­ak English, in which case the mediation and/or arbitration shall be conducted in both English and your native language. The existence and content of the mediation and arbitration proceedings, including documents and briefs submitted by the parties, correspondence from and to the International Chamber of Commerce, correspondence from the mediator, and correspondence, orders and awards issued by the sole arbitrator, shall remain strictly confidential and shall not be disclosed to any third party without the express written consent from the other party unless: (i) the disclosure to the third party is reasonably required in the context of conducting the mediation or arbitration proceedings; and (ii) the third party agrees unconditionally in writing to be bound by the confidentiality obligation stipulated herein.</p>
                        <h4 id="7-other-provisions">7. Other Provisions</h4>
                        <h5 id="claims-of-copyright-infringement-">Claims of Copyright Infringement.</h5>
                        <p>Claims of copyright infringement should be sent to Desty’s designated agent. Please visit Desty’s web page at <a href="https://www.desty.com/legal">https://www.desty.com/legal</a> for the designated address and additional information.</p>
                        <h5 id="notice-">Notice.</h5>
                        <p>Desty may give notice by means of a general notice on the Services, electronic mail to your email address in your Account, or by written communication sent to your address as set forth in your Account. You may give notice to Desty by written communication to Desty's address at Mr. Treublaan 7, 1097 DP, Amsterdam, The Netherlands.</p>
                        <h5 id="general-">General.</h5>
                        <p>You may not assign or transfer these Terms in whole or in part without Desty’s prior written approval. You give your approval to Desty for it to assign or transfer these Terms in whole or in part, including to: (i) a subsidiary or affiliate; (ii) an acquirer of Desty’s equity, business or assets; or (iii) a successor by merger. No joint venture, partnership, employment or agency relationship exists between you, Desty or any Third Party Provider as a result of the contract between you and Desty or use of the Services.</p>
                        <p>If any provision of these Terms is held to be illegal, invalid or unenforceable, in whole or in part, under any law, such provision or part thereof shall to that extent be deemed not to form part of these Terms but the legality, validity and enforceability of the other provisions in these Terms shall not be affected. In that event, the parties shall replace the illegal, invalid or unenforceable provision or part thereof with a provision or part thereof that is legal, valid and enforceable and that has, to the greatest extent possible, a similar effect as the illegal, invalid or unenforceable provision or part thereof, given the contents and purpose of these Terms. These Terms constitute the entire agreement and understanding of the parties with respect to its subject matter and replaces and supersedes all prior or contemporaneous agreements or undertakings regarding such subject matter. In these Terms, the words “including” and “include” mean “including, but not limited to.”</p>
                    </article>
                </div>

                <div class="termTabView" id="termTab2">
                    <h4 class="">Legal</h4>
                    <article>
                        <h3>COOKIE POLICY <a href="https://www.desty.com/legal/privacy/cookies/en/">GLOBAL</a></h3>
                        <p>Effective Date: May 25, 2018</p>
                        <p>We and our affiliates, third parties, and other partners use cookies and other identification technologies on our websites, mobile applications, email communications, advertisements, and other online services (collectively, the "Services") for a number of purposes, including: authenticating users, remembering user preferences and settings, determining the popularity of content, delivering and measuring the effectiveness of advertising campaigns, analyzing site traffic and trends, and generally understanding the online behaviors and interests of people who interact with our Services. You can read more here about the types of cookies we use, why we use them, and how you can exercise your choices.</p>
                        <h4>Cookies and Related Technologies Overview</h4>
                        <p>Cookies are small text files that are stored on your browser or device by websites, apps, online media, and advertisements that are used to remember your browser or device during and across website visits. We also utilize other technologies that may identify you or the devices you use. For example, “pixel tags” (also called beacons) are small blocks of code installed on (or called by) a webpage, app, or advertisement which can retrieve certain information about your device and browser, including for example: device type, operating system, browser type and version, website visited, time of visit, referring website, IP address, advertisementising identifiers, and other similar information, including the small text file (the cookie) that uniquely identifies the device. Pixels provide the means by which third parties can set and read browser cookies from a domain that they do not themselves operate and collect information about visitors to that domain, typically with the permission of the domain owner. “Local storage” refers generally to other places on a browser or device where information can be stored by websites, ads, or third parties (such as HTML5 local storage and browser cache). “Software Development Kits” (also called SDKs) function like pixels and cookies, but operate in the mobile app context where pixels and cookies cannot always function. The primary app developer can install pieces of code (the SDK) from partners in the app, and thereby allow the partner to collect certain information about user interaction with the app and information about the user device and network information.</p>
                        <h4 id="advertising-synchronization-relevancy">Advertising Synchronization &amp; Relevancy</h4>
                        <p>In order to facilitate the most relevant ads possible, Desty works with various service providers who assist us in delivering similar ads to end users across devices and platforms. For example, we work with social media advertising services to provide you with relevant ads based on your Desty activity through their media channels. We may also use service providers to provide you with a similar ad on a mobile website or mobile application as with a traditional website ad. See below for more information on your choices to limit these types of advertising.</p>
                        <h4 id="your-choices">Your Choices</h4>
                        <p>You have the right to choose whether or not to accept cookies. However, they are an important part of how our Services work, so you should be aware that if you choose to refuse or remove cookies, this could affect the availability and functionality of the Services.</p>
                        <p>Most web browsers are set to accept cookies by default. If you prefer, you can usually choose to set your browser to remove or reject browser cookies. To do so, please follow the instructions provided by your browser which are usually located within the "Help" or “Preferences” menu. Some third parties also provide the ability to refuse their cookies directly by clicking on an opt-out link, and we have indicated where this is possible in the table below.</p>
                        <p>Removing or rejecting browser cookies does not necessarily affect third-party flash cookies which may be used by us or our partners in connection with our Services. To delete or disable flash cookies please visit <a href="https://helpx.adobe.com/flash-player/kb/disable-local-shared-objects-flash.html">this site</a> for more information. For further information about cookies, including how to see what cookies have been set on your device and how to manage and delete them, you can visit <a href="https://youradchoices.com/">https://youradchoices.com/</a> and <a href="https://www.youronlinechoices.eu">www.youronlinechoices.eu</a> for EU visitors.</p>
                        <p>For mobile users, you have controls in your device Operating System that enables you to choose whether to allow cookies, or share your advertising ID with companies like Desty or our advertising service providers. For information on controlling your mobile choices, you can visit <a href="https://www.networkadvertising.org/mobile-choices">www.networkadvertising.org/mobile-choices</a>.</p>
                        <p>To help control or block certain ads in mobile applications, you may choose to download and utilize the DAA mobile app, <a href="https://youradchoices.com/appchoices">https://youradchoices.com/appchoices</a></p>
                        <h4 id="types-and-purposes-of-cookies">Types and Purposes of Cookies</h4>
                        <p>The following table sets out the different categories of cookies that our Services use and why we use them. The lists of third party cookie providers are intended merely as illustrative and should not be viewed as a comprehensive list.</p>
                        <table>
                            <thead>
                            <tr>
                                <th><strong>Type of Cookie</strong></th>
                                <th><strong>Purpose</strong></th>
                                <th><strong>Who Serves (for example)</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><strong>Strictly Necessary</strong></td>
                                <td>These cookies (including local storage and similar technologies) are essential to enable your use of the site or services, such as assisting with your account login, so we can show you the appropriate experience and features such as your account information, trip history, and to edit your account settings. They may also provide authentication, site security, and help localize the language based on the geography of your visit.</td>
                                <td>
                                    <ul align="left">
                                        <li>Desty</li>
                                        <li>Tealium</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Site features and Services</strong></td>
                                <td>These provide functionality that help us deliver products and Services. For example, cookies help you log in by pre-filling fields. We may also use cookies and similar technologies to help us provide you and others with social plugins and other customized content and experiences, such as making suggestions to you and others.</td>
                                <td>
                                    <ul align="left">
                                        <li>Desty</li>
                                        <li><a href="https://www.facebook.com/help/109378269482053/?helpref=hc_fnav">Facebook</a></li>
                                        <li><a href="https://help.twitter.com/en/rules-and-policies/twitter-cookies">Twitter</a></li>
                                        <li><a href="https://policies.google.com/technologies/ads">Google</a></li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Analytics, performance and research</strong></td>
                                <td>These are used to understand, improve, and research products and Services, including when you access the Desty website and related websites and apps from a computer or mobile device. For example, we may use cookies to understand how you are using site features, and segmenting audiences for feature testing. We and our partners may use these technologies and the information we receive to improve and understand how you use websites, apps, products, services and ads.</td>
                                <td>
                                    <ul align="left">
                                        <li>Google</li>
                                        <li>Celtra</li>
                                        <li>Optimizely</li>
                                        <li>Qualtrics</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Advertising</strong></td>
                                <td>These cookies and pixels are used to deliver relevant ads, track email marketing or ad campaign performance and efficiency. For example, we and our ad partners may rely on information gleaned through these cookies to serve you ads that may be interesting to you on other websites. Similarly, our partners may use a cookie, attribution service or another similar technology to determine whether we’ve served an ad and how it performed or provide us with information about how you interact with them.</td>
                                <td>
                                    <ul align="left">
                                        <li>Desty</li>
                                        <li><a href="https://policies.google.com/technologies/ads">Google</a></li>
                                        <li><a href="https://www.facebook.com/help/109378269482053/?helpref=hc_fnav">Facebook</a></li>
                                        <li><a href="https://www.adobe.com/privacy/opt-out.html">Adobe</a></li>
                                        <li><a href="https://www.mediamath.com/ad-choices-opt-out/">Mediamath</a></li>
                                        <li><a href="https://policies.oath.com/us/en/oath/privacy/controls/index.html">Oath/AOL/Yahoo</a></li>
                                        <li><a href="https://help.twitter.com/en/rules-and-policies/twitter-cookies">Twitter</a></li>
                                        <li><a href="https://www.linkedin.com/psettings/guest-controls/retargeting-opt-out?trk=">LinkedIn</a></li>
                                        <li><a href="https://www.outbrain.com/legal/#advertising_behavioral_targeting">Outbrain</a></li>
                                        <li><a href="https://account.microsoft.com/privacy/ad-settings/signedout?refd=www.bing.comru=https:%2F%2Faccount.microsoft.com%2Fprivacy%2Fad-settings%3Frefd%3Dwww.bing.com">Microsoft/Bing</a></li>
                                        <li><a href="https://www.quantcast.com/opt-out/">Quantcast</a> </li>
                                        <li> <a href="https://moat.com/privacy">Moat</a> </li>
                                        <li> <a href="https://liveramp.com/opt_out/">Liveramp</a> </li>
                                        <li> <a href="https://www.indeed.com/legal#indeedTargetedAdsTerms">Indeed</a> </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Mobile-Specific</strong></td>
                                <td>These service providers use various technologies to deliver relevant mobile ads, track mobile marketing or mobile ad campaign performance and efficiency.</td>
                                <td>
                                    <li><a href="https://policies.google.com/technologies/ads">Google</a></li>
                                    <li><a href="https://www.adobe.com/privacy/opt-out.html">Adobe</a></li>
                                    <li><a href="https://optoutmobile.com/">Tune</a></li>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <h4 id="contact-us">Contact us</h4>
                        <p>If you have any questions about our use of cookies, please review the FAQ’s in the “Legal &amp; Privacy” section <a href="https://help.desty.com/h/e1f427a1-c1ab-4c6a-a78a-864f47877558">here</a>, where you may also submit your questions.</p>
                    </article>
                </div>
                <div class="termTabView" id="termTab3">
                    <h4 class="">Legal</h4>
                    <article>
                        <h3 id="desty-candidate-privacy-statement">Desty Candidate Privacy Statement</h3>
                        <p>Desty collects personal information about you when you or third-parties acting on your behalf apply or search for a position with Desty, including via Desty’s Careers page (“Candidate Information”). If you reside in the United States, this Privacy Statement applies to Candidate Information collected and used by Desty U.S. If you reside outside of the United States, this Privacy Statement applies to Candidate Information collected and used by or the applicable Desty entity (Desty U.S., Desty B.V. and any other Desty entity are referred to herein collectively as “Desty” or the “Company”). </p>
                        <h4 id="scope-and-application">Scope and Application</h4>
                        <p>This Privacy Statement (“Statement") applies to persons anywhere in the world who submit Candidate Information through Desty’s Careers page, or whose information is otherwise provided to or is collected by Desty in connection with an application or search for a position with Desty. This includes information submitted by you or on your behalf by a third-party.</p>
                        <p>This Statement does not apply to information we collect from or about (1) persons (e.g., riders) who use our app or other services (“Services”) to request transportation, delivery, or other on-demand services (“Users”); or (2) drivers, couriers, partner transportation companies, or any other persons who use the Desty platform under license (collectively “Drivers”). If you interact with the Services as either or both a User and a Driver, the respective privacy statements for Users and Drivers apply to your different interactions.</p>
                        <h4 id="collection-of-information">Collection of Information</h4>
                        <p>The Candidate Information that Desty may collect includes: </p>
                        <ul>
                            <li>first and last name.</li>
                            <li>contact information, including address, email address, telephone number, or other contact information.</li>
                            <li>application documents, including resume or CV, cover letter, transcripts, and other statements of experience and education.</li>
                            <li>type of employment sought, desired salary, willingness to relocate, or other job preferences.</li>
                            <li>how you heard about the position.</li>
                            <li>sensitive information (race/ethnic origin), if you voluntarily submit such information as part of the applications process.</li>
                            <li>names and contact information for referrals.</li>
                            <li>names of contact information of references (it being solely your responsibility to obtain consent from references before providing their personal information).</li>
                            <li>any other personal information that you provide during the job search, application or interview process with Desty, including skills assessments, additional information regarding your work or educational experiences, and information that may be required to perform a background check such as driver’s license or other government-issued identification number.</li>
                        </ul>
                        <h4 id="use-of-information">Use of Information</h4>
                        <p>Desty may use Candidate Information for purposes including, but not limited to:</p>
                        <ul>
                            <li>evaluating and verifying your education, skills, experiences and other qualifications interests in connection with employee positions with Desty.</li>
                            <li>communicating with you regarding the interview process, and informing you of career opportunities.</li>
                            <li>creating and submitting reports as required by law or regulation.</li>
                            <li>improving our recruitment process, including by generating analytics using Candidate Information.</li>
                        </ul>
                        <p>If you accept employment with Desty, the Candidate Information will become part of your employment record and will be used for employment purposes.</p>
                        <p>In addition, Desty may use the Candidate Information to perform a background check if you are offered a position with Desty. If Desty determines that it cannot hire you as a result of information discovered during that background check, we will notify you of such determination. We will also provide you with a copy of your criminal background check, and information regarding the process for correcting the information in such check.</p>
                        <h4 id="sharing-of-information">Sharing of Information</h4>
                        <p>We may share your Candidate Information:</p>
                        <ul>
                            <li>With Desty subsidiaries and affiliated entities that provide services or conduct data processing on our behalf, or for data centralization and / or logistics purposes;</li>
                            <li>With vendors, consultants, marketing partners, and other service providers who need access to such information to carry out work on our behalf;</li>
                            <li>In response to a request for information by a competent authority if we believe disclosure is in accordance with, or is otherwise required by, any applicable law, regulation, or legal process;</li>
                            <li>With law enforcement officials, government authorities, or other third parties if we believe your actions are inconsistent with our policies, or to protect the rights, property, or safety of Desty or others;</li>
                            <li>In connection with, or during negotiations of, any merger, sale of company assets, consolidation or restructuring, financing, or acquisition of all or a portion of our business by or into another company;</li>
                            <li>If we otherwise notify you and you consent to the sharing; and</li>
                            <li>In an aggregated and/or anonymized form which cannot reasonably be used to identify you.</li>
                        </ul>
                        <p>We may transfer the Candidate Information to, and process and store it in, the United States and other countries, some of which may have less protective data protection laws than the region in which you reside. Where this is the case, we will take appropriate measures to protect your Candidate Information in accordance with this Statement.</p>
                        <p><strong>Analytics and Advertising Services Provided by Others</strong></p>
                        <p>We may allow others to provide audience measurement and analytics services for us, to serve advertisements on our behalf across the Internet, and to track and report on the performance of those advertisements. These entities may use cookies, web beacons, SDKs, and other technologies to identify your device when you visit our site, including Desty’s Careers page, For more information about these technologies and service providers, please refer to our Cookie Statement.</p>
                        <h4 id="your-choices">Your Choices</h4>
                        <p><strong>Candidate Information</strong></p>
                        <p>By submitting your Candidate Information to Desty, you consent to the collection and use of your Candidate Information as described in this Statement. Submitting your Candidate Information is voluntary. However, Desty may not be able to consider you for an available position if you do not voluntarily provide the necessary Candidate Information. </p>
                        <p>Desty will ask for your consent before we obtain additional information from third party agencies, and will, where required by law, notify you of the information received from those third parties. </p>
                        <p><strong>California Privacy Rights</strong></p>
                        <p>California law permits residents of California to request certain details about how their information is shared with third parties for direct marketing purposes. Desty does not share your personally identifiable information with third parties for the third parties’ direct marketing purposes unless you provide us with consent to do so.</p>
                        <p><strong>Cookies and Advertising</strong></p>
                        <p>Please refer to our Cookie Statement for more information about your choices around cookies and related technologies.</p>
                        <h4 id="changes-to-the-statement">Changes to the Statement</h4>
                        <p>We may change this Statement from time to time. We will post any changes to the Policy on this page. Each version of the Policy is identified at the top of the page by its effective date. We encourage you to periodically review the Statement for the latest information on our privacy practices.</p>
                        <h4 id="contact-us">Contact Us</h4>
                        <p>If you have any questions about this Privacy Statement, please contact us at <a href="mailto:support@desty.com">privacy@desty.com</a>, or write us at Desty Technologies, Inc., Attn: Legal,1455 Market Street, Suite 400, San Francisco, CA 94103 (if you reside in the U.S.), or at Desty B.V., Attn: Legal, Meester Treublaan 7, 1097 DP Amsterdam, The Netherlands (if you do not reside in the U.S.).</p>
                    </article>
                </div>

                <div class="termTabView" id="termTab4">
                    <h4 class="">Legal</h4>
                    <article ><h4>Privacy Policy</h4>
                        <p> Our privacy policy is available at <a href="https://privacy.desty.com/policy/">privacy.desty.com/policy</a>.</p>
                    </article>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('customScript')

@endsection