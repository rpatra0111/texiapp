@extends('site.layouts.app')

@section('pageTitle', 'Home')

@section('customStyles')

@endsection

@section('content')
    <div class="topsec">
        <div class="Bannerightsec float-right">
            <h1>Get <span>There</span></h1>
            <p>Lorem Ipsum is simply dummy text of dummy text of printingthe printing.</p>
            <a href="#" class="btn btn-warning btncs">Take a drive</a>
        </div>
        <div class="clearfix"></div>
        <div class="secinfo clearfix">
            <ul class="list-unstyled clearfix">
                <li>
                    <div class="infobox">
                        <img src="{{ asset_url('site_assets/images/infoicon.png') }}" class="img-fluid mx-auto d-block"
                             alt="icon">
                        <h4>Share your car</h4>
                        <p>Lorem Ipsum imply dummy dummy printingthe printing.</p>
                    </div>
                </li>
                <li>
                    <div class="infobox">
                        <img src="{{ asset_url('site_assets/images/infoicon2.png') }}" class="img-fluid mx-auto d-block"
                             alt="icon">
                        <h4>Track your booking car</h4>
                        <p>Lorem Ipsum imply dummy dummy printingthe printing.</p>
                    </div>
                    <a href="#" class="btn btn-default btnride purplebtn">Reasons to ride <span class="iconn"><img
                                    src="{{ asset_url('site_assets/images/right-arrow.svg') }}"
                                    class="img-fluid -d-block"></span></a>
                </li>
                <li>
                    <div class="infobox">
                        <img src="{{ asset_url('site_assets/images/infoicon3.png') }}" class="img-fluid mx-auto d-block"
                             alt="icon">
                        <h4>Low cost to luxury</h4>
                        <p>Lorem Ipsum imply dummy dummy printingthe printing.</p>
                    </div>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
    <section class="secDrive">
        <div class="container">
            <div class="leftdCont float-left">
                <h4>Drive when you waant <span class="d-block"><strong
                                class="text-orange">Make what</strong> you need</span></h4>
                <p>Driving with {{ config('constant.APP_NAME') }} is flexible and rewarding, helping drivers meet their career and financial
                    goals.</p>
                <a class="btn btn-default btnride" href="#">Reasons to ride<span class="iconn"><img
                                src="{{ asset_url('site_assets/images/right-arrow.svg') }}" class="img-fluid -d-block"></span></a>
            </div>
            <div class="rightDcont float-left">
                <figure>
                    <img src="{{ asset_url('site_assets/images/drive-src.png') }}" class="img-fluid mx-auto d-block"
                         alt="image">
                </figure>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
    <section class="ourteam">
        <div class="container">
            <h4 class="mtteam">
                <small class="d-block">At the wheel</small>
                Meet our drivers
            </h4>
        </div>
        <div class="home-demo">
            <div class="row">
                <div class="large-12 columns">
                    <div id="owl-demo1" class="owl-carousel owl-theme1">
                        @foreach($reviews as $key => $val)
                            <div class="item">
                                <div class="teamBox">
                                    <figure>
                                        <img src="{{ asset_url('site_assets/images/driver_pic/'.$val->driver_pic) }}" class="img-fluid d-block"
                                             alt="..s">
                                    </figure>
                                    <div class="tDesk">
                                        <p>{{ $val->review }}</p>
                                        <h5 class="byt"><strong>{{ $val->driver_name }}</strong>, Sales and Chicago partner</h5>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('customScript')

@endsection