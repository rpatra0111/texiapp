<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name') }} | @yield('pageTitle')</title>
    <!-- favicon -->
    <link rel="shortcut icon" href="{!! asset_url('favicon.ico') !!}" type="image/x-icon">
    <link rel="icon" href="{!! asset_url('favicon.ico') !!}" type="image/x-icon">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset_url('site_assets/css/font-awesome.css') }}">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset_url('site_assets/plugins/owl-carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset_url('site_assets/plugins/owl-carousel/assets/owl.theme.default.min.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset_url('site_assets/css/custom.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+sha384-WskhaSGFgHYWDcbwN70:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    @yield('customStyles')
</head>
<body>
<div class="wrapper">
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light1">
            <div class="container">
                <a class="navbar-brand" href="#"><img src="{{ asset_url('site_assets/images/logo.png') }}" class="img-fluid mx-auto d-block"></a>
                <ul class="float-right nav rightmenu">
                    <li><a href="#">Help</a></li>
                    <li><a class="loginbtn btn btn-warning" href="#">Sign up</a></li>
                </ul>
            </div>
        </nav>
    </header>
    @yield('content')

    <footer>
        <img src="{{ asset_url('site_assets/images/footerbg.png') }}" class="img-fluid d-block fullsec" alt="photo">
        <div class="container">
            <div class="clearfix flexitem">
                <div class="col-lg-4 col-vxs-12 float-left">
                    <ul class="list-unstyled fmenu menuwgap">
                        <li><a href="#"><i class="fa fa-globe"></i>&nbsp;&nbsp;English</a></li>
                        <li><a href="#"><i class="fa fa-life-ring"></i>&nbsp;&nbsp;Help</a></li>
                    </ul>

                </div>
                <div class="col-lg-2 col-vxs-12 float-left">
                    <h4>About {{ config('constant.APP_NAME') }}</h4>
                    <ul class="list-unstyled fmenu">
                        <li><a href="#">How {{ config('constant.APP_NAME') }} Works</a></li>
                        <li><a href="#">Community</a></li>
                        <li><a href="#">Newsroom</a></li>
                        <li><a href="#">Blog</a></li>
                    </ul>

                </div>
                <div class="col-lg-2 col-vxs-12 float-left">
                    <h4>Ride</h4>
                    <ul class="list-unstyled fmenu">
                        <li><a href="#">Sign up to Ride</a></li>
                        <li><a href="#">Fare Estimate</a></li>
                        <li><a href="#">Cities</a></li>
                        <li><a href="#">Airports</a></li>
                        <li><a href="#">Gift Cards</a></li>
                    </ul>

                </div>
                <div class="col-lg-4 col-vxs-12 float-left">
                    <div class="fsocial">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-linkedin-square"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="appstores text-center clearfix">
                <a href="#"><img src="{{ asset_url('site_assets/images/app-store-apple.svg') }}" class="img-fluid mx-auto d-block"></a>
                <a href="#"><img src="{{ asset_url('site_assets/images/app-store-google.svg') }}" class="img-fluid mx-auto d-block"></a>
            </div>

            <div class="copyrsec clearfix">

                <div class="cmmenu float-right">
                    <a href="#">Privacy</a>
                    <a href="#">Accessibility</a>
                    <a href="#">Terms</a>
                </div>
                <p class="float-left">© {{ date('Y') }} {{ config('constant.APP_NAME') }}</p>
            </div>


        </div>
    </footer>
</div><!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!-- Popper.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<!-- Bootstrap 4.1.1 -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<!-- Owl Carousel -->
<script src="{{ asset_url('site_assets/plugins/owl-carousel/owl.carousel.js') }}"></script>
<script type="text/javascript">
</script>

<script type="text/javascript">
    let owl = $('.owl-carousel');
    owl.owlCarousel({
        margin: 10,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
</script>

@yield('customScript')

</body>
</html>
