<?php
$page_title = camelToSentence(ucfirst($resource)) . ' ' . $nat;
// Resolute the data
$data = ${$resource};
?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {!! $page_title !!}
                <small>{{$nat}} {!! ucwords($resource)!!}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{!! admin_url("$resource") !!}"><i
                                class="fa  fa-user"></i> {!! ucfirst($resource_pl) !!}</a></li>
                <li class="active">{{$nat}} {!! ucwords($resource)!!}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <style>
                .help-block {
                    color: #FF0000;
                }
            </style>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{$nat}} {!! ucwords($resource)!!}</h3>
                        </div>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{!! $error !!}</p>
                                @endforeach
                            </div>
                        @endif
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!! session('success') !!}
                            </div>
                        @endif
                        <form method="POST" action="{{ $saveLink }}" accept-charset="UTF-8" class="form-horizontal"
                              enctype="multipart/form-data">
                            <input name="_method" type="hidden" value={{$meth}}>
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="profilePicture" class="col-sm-2 control-label">Profile Picture</label>
                                    <div class="col-sm-6">
                                        @php
                                            $profilePicture=asset_url('default_images/avatar.jpg');
                                            if (isset($data->profilePicture)){
                                                $profilePicture = asset_url("uploads/profilePictures/$data->profilePicture");
                                            }
                                        @endphp
                                        <img id="sel_img" style="height: 100px; width: 100px;"
                                             src="{!! $profilePicture !!}">
                                        <input type="file" name="profilePicture" id="profilePicture">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="firstName" class="col-sm-2 control-label">First Name</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="firstName" placeholder="First Name" type="text"
                                               value="{{isset($data->firstName)?$data->firstName:""}}" id="firstName"
                                               required>
                                        @if ($errors->has('firstName'))
                                            <span class="help-block"><strong>{{ $errors->first('firstName') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastName" class="col-sm-2 control-label">Last Name</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="lastName" placeholder="Last Name" type="text"
                                               value="{{isset($data->lastName)?$data->lastName:""}}" id="lastName"
                                               required>
                                        @if ($errors->has('lastName'))
                                            <span class="help-block"><strong>{{ $errors->first('lastName') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="gender" class="col-sm-2 control-label">Gender</label>
                                    <div class="col-sm-6">
                                        <select id="gender" class="form-control" name="gender">
                                            <option @if(isset($data->gender)){{ $data->gender=="M"?"selected":"" }} @endif value="M">
                                                Male
                                            </option>
                                            <option @if(isset($data->gender)){{ $data->gender=="F"?"selected":"" }} @endif value="F">
                                                Female
                                            </option>
                                        </select>
                                        @if ($errors->has('gender'))
                                            <span class="help-block"><strong>{{ $errors->first('gender') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="mobile" class="col-sm-2 control-label">Mobile Number</label>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <select id="countryCode" class="form-control" name="countryCode"
                                                        required>
                                                    @foreach ($countries as $country)
                                                        <option @if(isset($data->countryCode)) {{$data->countryCode=="+".$country->phoneCode?"selected":""}} @endif value="{{'+'.$country->phoneCode}}">
                                                            {{$country->name.'(+'.$country->phoneCode.')'}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="mobile" placeholder="Mobile Number"
                                                       type="text" value="{{isset($data->mobile)?$data->mobile:""}}"
                                                       id="mobile" required>
                                            </div>
                                        </div>

                                        <br>

                                        @if ($errors->has('mobile'))
                                            <span class="help-block"><strong>{{ $errors->first('mobile') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">E-mail Address</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="email" placeholder="E-mail Address"
                                               type="text" value="{{isset($data->email)?$data->email:""}}" id="email"
                                               required>
                                        @if ($errors->has('email'))
                                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                @if($nat == 'Edit')
                                    <div class="form-group">
                                        <label for="password" class="col-sm-2 control-label">Password</label>
                                        <div class="col-sm-6">
                                            <input class="form-control" name="password" placeholder="Password"
                                                   type="password" value="" id="password">
                                            @if ($errors->has('password'))
                                                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="box-footer">
                                <a class="btn btn-default" href="{!! admin_url(strtolower($resource)) !!}">
                                    Back</a>
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection



@section('customScript')

    <script>
        // This example displays an address form, using the autocomplete feature
        // of the Google Places API to help users fill in the information.

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        var placeSearch, autocomplete;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };
        var elements = ['address', 'city', 'state', 'country', 'zipCode', 'lat', 'lng'];

        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('address')),
                {types: ['geocode']});

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            for (var j = 0; j < elements.length; j++) {
                console.log(elements[j]);
                document.getElementById(elements[j]).value = '';
                document.getElementById(elements[j]).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            document.getElementById('address').value = place.address_components[0][componentForm['street_number']] + " " + place.address_components[1][componentForm['route']];
            document.getElementById('city').value = place.address_components[2][componentForm['locality']];
            document.getElementById('state').value = place.address_components[5][componentForm['administrative_area_level_1']];
            document.getElementById('country').value = place.address_components[6][componentForm['country']];
            document.getElementById('zipCode').value = place.address_components[7][componentForm['postal_code']];
            document.getElementById('lat').value = place.geometry.location.lat();
            document.getElementById('lng').value = place.geometry.location.lng();
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDdnCDgASaKWcQI47ak0k1a5FgzvAEJ3ps&libraries=places&callback=initAutocomplete"
            async defer></script>
@endsection
