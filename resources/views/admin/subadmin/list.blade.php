@php
$page_title = ucfirst($resource_pl);
@endphp

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('customStyles')
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset_url('admin_assets/plugins/iCheck/all.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700" rel="stylesheet">

@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title }}
                {{--<small>List</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">List of All {{ $page_title }}</h3>

                            <div class="box-tools">
                                <form method="get" action="{!! url()->current() !!}">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input name="keyword" class="form-control pull-right" type="text" placeholder="Search" value="{{ (isset($keyword))?$keyword:"" }}">

                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>E-Mail</th>
                                    <th>Mobile</th>
                                    <th>Photo</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset(${$resource_pl}) && count(${$resource_pl})>0)
                                    @foreach(${$resource_pl} as $row)
                                        @php
                                            $menuPermits=[];
                                            if ($row->UserMenuPermits && !empty($row->UserMenuPermits)) {
                                                foreach ($row->UserMenuPermits as $permit) {
                                                    $menuPermits[] = $permit->menuId;
                                                }
                                            }
                                        @endphp
                                        <tr>
                                            <td>{!! $row->firstName !!} {!! $row->lastName !!}</td>
                                            <td>{!! $row->email  !!}</td>
                                            <td>{!! $row->mobile  !!}</td>
                                            <td>
                                                @if(!empty($row->profilePicture))
                                                    <img src="{{ asset_url('uploads/profilePictures'.'/'.$row->profilePicture) }}"
                                                         style="height:70px; width:70px;">
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{!! admin_url('subAdmin/'.$row->id.'/edit') !!}"
                                                   class="btn btn-sm btn-warning td-btn">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                                <a class="btn btn-sm btn-info td-btn permitButton" data-toggle="modal" data-target="#permissionModal" data-submiturl="{{ admin_url('subAdmin/updateMenuPermit/'.$row->id) }}" data-permits="{{ implode(',', $menuPermits) }}">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    Permissions
                                                </a>
                                                @if($row->status=='Y' || $row->status=='N')
                                                    <form method="POST"
                                                          action="{!! admin_url('subAdmin/'.$row->id.'/change-status') !!}"
                                                          onsubmit="return confirm('Are you sure to {{ $row->status=='Y'?'disable':'enable' }} {!! $row->email !!}?');">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="current_status"
                                                               value="{{ $row->status }}">
                                                        <button class="btn btn-sm btn-{{ $row->status == 'N'?'success':'danger' }} td-btn"
                                                                type="submit">
                                                            <i class="fa"
                                                               aria-hidden="true"></i> {!!$row->status=='N'?'Unblock':'Block'!!}
                                                        </button>
                                                    </form>
                                                @endif
                                                @if($row->status=='Y' || $row->status=='D')
                                                    <form method="POST"
                                                          action="{!! admin_url('subAdmin/'. @$row->id) !!}"
                                                          onsubmit="return confirm('Are you sure to {{ $row->status == 'D'?'restore':'delete' }} {!! $row->firstname !!}?');">
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        {{ csrf_field() }}
                                                        <button class="btn btn-sm btn-{{ $row->status == 'D'?'success':'danger' }} td-btn" type="submit">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                            {!!$row->status=='Y'?'Delete':'Restore'!!}
                                                        </button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" style="text-align: center;">No Data Found..</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>E-Mail</th>
                                    <th>Mobile</th>
                                    <th>Photo</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="paginationDiv">
                                {!! ${$resource_pl}->appends($_GET)->render() !!}
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->

    <div class="modal fade permissionModal" id="permissionModal" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Permissions</h4>
                </div>
                <div class="modal-body" id="permissionModalBody" style="height: auto;">
                    <div class="form-group">
                        <label>
                            <input type="checkbox" class="flat-red">
                            Flat green skin checkbox
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="$('#permissionModalBody form').submit();">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('customScript')
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset_url('admin_assets/plugins/iCheck/icheck.min.js') }}"></script>
    <script type="text/javascript">
        let menuData = JSON.parse('{!! json_encode(getAdminMenu()) !!}');

        //iCheck for checkbox and radio inputs
        function setupicheck () {
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
        }
        setupicheck ();

        $(document).ready(function () {
            $('.permitButton').click(function (event) {
                let submitURL = event.target.dataset.submiturl;
                let permitted = event.target.dataset.permits.split(',');
                permitted.forEach(function (elem, index) {
                    permitted[index] = parseInt(elem);
                });
                let htmlData = '<form class="permissionMenuForm" enctype="multipart/form-data" action="'+submitURL+'" method="POST">{{ csrf_field() }}<div class="row">';
                menuData.forEach(function (elem) {
                    if (elem.parent_id === 0) {
                        htmlData += '<div class="form-group col-sm-12 catemainbg"><label class="catehead"><input id="m'+elem.id+'" type="checkbox" name="menuItems[]" class="flat-red minimal m' + elem.parent_id + '" value="'+elem.id+'"'+(permitted.indexOf(parseInt(elem.id))!==-1?'checked':'')+' '+((parseInt(elem.id))===1?'disabled':'')+' style="margin-right: 10px;" data-level="1"> '+elem.name+'</label>';
                        let cMenu = [];
                        menuData.forEach(function (elm) {
                            if (elem.id === elm.parent_id) {
                                cMenu.push(elm);
                            }
                        });
                        if (cMenu.length > 0) {
                            htmlData += '<div class="childrenCate"><div class="row">';
                            cMenu.forEach(function (cElem) {
                                htmlData += '<div class="form-group col-sm-6"><label><input id="m'+cElem.id+'" type="checkbox" name="menuItems[]" class="flat-red minimal m' + cElem.parent_id + '" value="'+cElem.id+'"'+(permitted.indexOf(parseInt(elem.id))!==-1?'checked':'')+' style="margin-right: 10px;" data-level="2"> '+cElem.name+'</label>';
                                let gcMenu = [];
                                menuData.forEach(function (elm) {
                                    if (cElem.id === elm.parent_id) {
                                        gcMenu.push(elm);
                                    }
                                });
                                if (gcMenu.length > 0) {
                                    htmlData += '<div class="childrenCate"><div class="row">';
                                    gcMenu.forEach(function (gcElem) {
                                        htmlData += '<div class="form-group col-sm-6"><label><input id="m'+gcElem.id+'" type="checkbox" name="menuItems[]" class="flat-red minimal m' + gcElem.parent_id + '" value="'+gcElem.id+'"'+(permitted.indexOf(parseInt(elem.id))!==-1?'checked':'')+' style="margin-right: 10px;" data-level="3"> '+gcElem.name+'</label></div>';
                                    });
                                    htmlData += '</div></div>';
                                }
                                htmlData += '</div>';
                            });
                            htmlData += '</div></div>';
                        }
                        htmlData += '</div>';
                    }
                });
                htmlData += '</div></form>';
                $('#permissionModalBody').html(htmlData);
                setupicheck ();

                /*$('body').on('change', 'input[type="checkbox"]', function() {
                    let value = parseInt($(this).val());
                    let checkedStatus = $(this).prop('checked');
                    let cchild = $(this).parents('div.childrenCate');
                    let cgChild = $(this).parents('div.grand-children');
                    let pchild = $(this).children('div.childrenCate');
                    let pgChild = $(this).children('div.grand-children');
                    if (cchild.html()) {
                        let incchild = cchild.find('.minimal');
                        incchild.each(function (elem) {
                            console.log('Child: ', incchild[elem]);
                        });
                    }
                    if (cgChild.html()) {
                        let incgChild = cgChild.find('.minimal');
                        incgChild.each(function (elem) {
                            console.log('Grand Child: ', incgChild[elem]);
                        });
                    }
                    /!*menuData.forEach(function (elem) {
                        if (parseInt(elem.id) === value) {
                            $('#m' + parseInt(elem.parent_id)).prop('checked', checkedStatus);
                            if (parseInt(elem.parent_id) !== 0) {
                                menuData.forEach(function (elemnt) {
                                    if (parseInt(elemnt.id) === parseInt(elem.parent_id)) {
                                        $('#m' + parseInt(elemnt.parent_id)).prop('checked', checkedStatus);
                                    }
                                });
                            }
                        }
                        if (parseInt(elem.parent_id) === value) {
                            $('#m' + parseInt(elem.id)).prop('checked', checkedStatus);
                            menuData.forEach(function (elemnt) {
                                if (parseInt(elemnt.parent_id) === parseInt(elem.id)) {
                                    $('#m' + parseInt(elemnt.id)).prop('checked', checkedStatus);
                                }
                            });
                        }
                    });*!/
                });*/
                $('input[type="checkbox"]').on('ifChecked', handleCheckboxChange);
                $('input[type="checkbox"]').on('ifUnchecked', handleCheckboxChange);
                /*$('input[type="checkbox"]').on('ifChecked', function(event){
                    let value = parseInt($(this).val());
                    let checkedStatus = $(this).prop('checked');
                    let cchild = $(this).parents('div.childrenCate');
                    let cgChild = $(this).parents('div.grand-children');
                    let pchild = $(this).children('div.childrenCate');
                    let pgChild = $(this).children('div.grand-children');
                    switch (event.target.dataset.level) {
                        case 1:
                        case 2:
                        case 3:
                    }
                    if (cchild.html()) {
                        let incchild = cchild.find('.minimal');
                        incchild.each(function (elem) {
                            console.log('Child: ', incchild[elem]);
                        });
                    }
                    if (cgChild.html()) {
                        let incgChild = cgChild.find('.minimal');
                        incgChild.each(function (elem) {
                            console.log('Grand Child: ', incgChild[elem]);
                        });
                    }
                });
                $('input[type="checkbox"]').on('ifUnchecked', function(event){
                    let value = parseInt($(this).val());
                    let checkedStatus = $(this).prop('checked');
                    let cchild = $(this).parents('div.childrenCate');
                    let cgChild = $(this).parents('div.grand-children');
                    let pchild = $(this).children('div.childrenCate');
                    let pgChild = $(this).children('div.grand-children');
                    if (cchild.html()) {
                        let incchild = cchild.find('.minimal');
                        incchild.each(function (elem) {
                            console.log('Child: ', incchild[elem]);
                        });
                    }
                    if (cgChild.html()) {
                        let incgChild = cgChild.find('.minimal');
                        incgChild.each(function (elem) {
                            console.log('Grand Child: ', incgChild[elem]);
                        });
                    }
                });*/
            });
        });

        function handleCheckboxChange(event) {
            let currentElem = $('#'+event.currentTarget.id);
            let value = parseInt(currentElem.val());
            let checkedStatus = currentElem.prop('checked');
            menuData.forEach(function (elem) {
                if (parseInt(elem.id) === value) {
                    if (parseInt(elem.parent_id) !== 0) {
                        if (checkedStatus === true) {
                            $('#m' + parseInt(elem.parent_id)).prop('checked', checkedStatus);
                        }
                        else {
                            if (checkChildren(parseInt(elem.parent_id)) === false) {
                                $('#m' + parseInt(elem.parent_id)).prop('checked', checkedStatus);
                            }
                        }
                        menuData.forEach(function (elemnt) {
                            if (parseInt(elemnt.id) === parseInt(elem.parent_id)) {
                                if (checkedStatus === true) {
                                    $('#m' + parseInt(elemnt.parent_id)).prop('checked', checkedStatus);
                                }
                                else {
                                    if (checkChildren(parseInt(elemnt.parent_id)) === false) {
                                        $('#m' + parseInt(elemnt.parent_id)).prop('checked', checkedStatus);
                                    }
                                }
                            }
                        });
                    }
                }
                if (parseInt(elem.parent_id) === value) {
                    if (checkedStatus === true) {
                        $('#m' + parseInt(elem.id)).prop('checked', checkedStatus);
                    }
                    else {
                        if (checkChildren(parseInt(elem.id)) === false) {
                            $('#m' + parseInt(elem.id)).prop('checked', checkedStatus);
                        }
                    }
                    menuData.forEach(function (elemnt) {
                        if (parseInt(elemnt.parent_id) === parseInt(elem.id)) {
                            if (checkedStatus === true) {
                                $('#m' + parseInt(elemnt.id)).prop('checked', checkedStatus);
                            }
                            else {
                                if (checkChildren(parseInt(elemnt.id)) === false) {
                                    $('#m' + parseInt(elemnt.id)).prop('checked', checkedStatus);
                                }
                            }
                        }
                    });
                }
            });
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck('update');
        }

        function checkChildren (element)
        {
            let countChecked = 0;
            $('.m' + element).each(function () {
                    if ($(this).prop('checked') === true) {
                        countChecked++;
                    }
                });
                if (countChecked > 0) {
                    return true;
                } else {
                    return false;
                }
        }
    </script>
@endsection
