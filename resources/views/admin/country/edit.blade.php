<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */

$page_title = camelToSentence(ucfirst($resource)) . ' ' . $nat;
// Resolute the data
$data = ${$resource};

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {!! $page_title !!}
                <small>{{$nat}} {!! ucwords($resource)!!}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{!! admin_url("location/".strtolower($resource)) !!}"><i
                                class="fa  fa-user"></i> {!! ucfirst($resource_pl) !!}</a></li>
                <li class="active">{{$nat}} {!! ucwords($resource)!!}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <style>
                .help-block {
                    color: #FF0000;
                }
            </style>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{$nat}} {!! ucwords($resource)!!}</h3>
                        </div>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{!! $error !!}</p>
                                @endforeach
                            </div>
                        @endif
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!! session('success') !!}
                            </div>
                        @endif
                        <form method="POST" action="{{ $saveLink }}" accept-charset="UTF-8" class="form-horizontal"
                              enctype="multipart/form-data">
                            <input name="_method" type="hidden" value={{$meth}}> {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="name" placeholder="Doe" type="text"
                                               value="{{isset($data->name)?$data->name:""}}" id="lastName"
                                               required>
                                        @if ($errors->has('name'))
                                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ISO" class="col-sm-2 control-label">ISO</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="firstName" placeholder="John" type="text"
                                               value="{{isset($data->ISO)?$data->ISO:""}}" id="ISO"
                                               required>
                                        @if ($errors->has('ISO'))
                                            <span class="help-block"><strong>{{ $errors->first('ISO') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ISO3" class="col-sm-2 control-label">ISO3</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="ISO3" placeholder="" type="text"
                                               value="{{isset($data->ISO3)?$data->ISO3:""}}" id="ISO3">
                                        @if ($errors->has('ISO3'))
                                            <span class="help-block"><strong>{{ $errors->first('ISO3') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phoneCode" class="col-sm-2 control-label">Phone Code</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="phoneCode" placeholder="doe.john@abc.com"
                                               type="text" value="{{isset($data->phoneCode)?$data->phoneCode:""}}"
                                               id="phoneCode"
                                               required>
                                        @if ($errors->has('phoneCode'))
                                            <span class="help-block"><strong>{{ $errors->first('phoneCode') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="numCode" class="col-sm-2 control-label">Num Code</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="numCode" placeholder=""
                                               type="text" value="{{isset($data->numCode)?$data->numCode:""}}"
                                               id="numCode"
                                               required>
                                        @if ($errors->has('numCode'))
                                            <span class="help-block"><strong>{{ $errors->first('numCode') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <a class="btn btn-default" href="{!! admin_url("location/".strtolower($resource)) !!}">
                                    Back</a>
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection



@section('customScript')

@endsection
