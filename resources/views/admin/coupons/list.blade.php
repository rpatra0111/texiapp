<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */

$page_title = camelToSentence(ucfirst($resource_pl));

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title }}
                {{--<small>List</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">List of All {{ $page_title }}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Discount</th>
                                    <th>Start Time</th>
                                    <th>Expiry Time</th>
                                    <th>Status</th>
                                    <th>Created</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset(${$resource_pl}) && count(${$resource_pl})>0)
                                    @foreach(${$resource_pl} as $row)
                                        <tr>
                                            <td>{!! $row->code !!}</td>
                                            <td>{!! $row->title !!}</td>
                                            <td>{!! $row->description !!}</td>
                                            <td>{!! $row->discount !!}</td>
                                            <td>{!! $row->startTime !!}</td>
                                            <td>{!! $row->endTime !!}</td>
                                            <td>{!! $row->status !!}</td>
                                            <td>{!! $row->created_at !!}</td>
                                            <td>
                                                <a href="{!! admin_url(strtolower($resource).'/'.$row->id.'/edit') !!}"
                                                   class="btn btn-sm btn-warning td-btn">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </a>
                                                <form method="POST"
                                                      action="{!! admin_url('coupon/'.$row->id) !!}"
                                                      onsubmit="return confirm('Are you sure to @if($row->status == 'Y') deactivate @else reactivate @endif{!! $row->code !!}?');">
                                                    <input name="_method" type="hidden"
                                                           value="DELETE">{{ csrf_field() }}
                                                    @if($row->status == 'Y')
                                                        <button class="btn btn-sm btn-danger td-btn" type="submit">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                        </button>
                                                    @else
                                                        <button class="btn btn-sm btn-success td-btn" type="submit">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                        </button>
                                                    @endif
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9" style="text-align: center;">No Data Found..</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Code</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Discount</th>
                                    <th>Start Time</th>
                                    <th>Expiry Time</th>
                                    <th>Status</th>
                                    <th>Created</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="paginationDiv">
                                {!! ${$resource_pl}->appends($_GET)->render() !!}
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('customScript')
    <script type="text/javascript">

    </script>
@endsection
