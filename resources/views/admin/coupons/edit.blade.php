<?php
$headingName = camelToSentence(ucfirst($resource));
$page_title = $nat . ' ' . $headingName;
// Resolute the data
$data = ${$resource};

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {!! $page_title !!}
                {{--<small>{{$nat}} {!! ucwords($resource)!!}</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{!! admin_url(strtolower($resource)) !!}"><i
                                class="fa  fa-user"></i> {!! camelToSentence(ucfirst($resource_pl)) !!}</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <style>
                .help-block {
                    color: #FF0000;
                }
            </style>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ $page_title }}</h3>
                        </div>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{!! $error !!}</p>
                                @endforeach
                            </div>
                        @endif
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!! session('success') !!}
                            </div>
                        @endif
                        <form method="POST" action="{{ $saveLink }}" accept-charset="UTF-8" class="form-horizontal"
                              enctype="multipart/form-data">
                            <input name="_method" type="hidden" value={{$meth}}> {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="code" class="col-sm-2 control-label">Code</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="code" placeholder="Code" type="text"
                                               value="{{isset($data->code)?$data->code:""}}"
                                               id="code" required>
                                        @if ($errors->has('code'))
                                            <span class="help-block"><strong>{{ $errors->first('code') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-sm-2 control-label">Title</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="title" placeholder="Title" type="text"
                                               value="{{isset($data->title)?$data->title:""}}" id="title"
                                               required>
                                        @if ($errors->has('title'))
                                            <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-sm-2 control-label">Description</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="description" placeholder="Description" type="text"
                                               value="{{isset($data->description)?$data->description:""}}" id="description"
                                               required>
                                        @if ($errors->has('description'))
                                            <span class="help-block"><strong>{{ $errors->first('description') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="discount" class="col-sm-2 control-label">Discount</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="discount" placeholder="Discount" type="text"
                                               value="{{isset($data->discount)?$data->discount:""}}" id="discount"
                                               required>
                                        @if ($errors->has('discount'))
                                            <span class="help-block"><strong>{{ $errors->first('discount') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="timeSpan" class="col-sm-2 control-label">Time Span</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="timeSpan" placeholder="timeSpan" type="text" id="timeSpan" required>
                                        <input id="startTime" name="startTime" value="@php if(isset($data->startTime)) echo $data->startTime; else echo date('Y-m-d h:i:s'); @endphp" type="hidden" required>
                                        <input id="endTime" name="endTime" value="@php if(isset($data->endTime)) echo $data->endTime; else echo date('Y-m-d h:i:s'); @endphp" type="hidden" required>
                                        @if ($errors->has('timeSpan'))
                                            <span class="help-block"><strong>{{ $errors->first('timeSpan') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <a class="btn btn-default" href="{!! admin_url(strtolower($resource)) !!}">
                                    Back</a>
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection



@section('customScript')
    <!-- daterangepicker -->
    <script src="{!! asset_url('admin_assets/bower_components/moment/min/moment.min.js') !!}"></script>
    <script src="{!! asset_url('admin_assets/bower_components/bootstrap-daterangepicker/daterangepicker.js') !!}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#timeSpan').daterangepicker({
                timePicker: true,
                timePickerIncrement: 5,
                locale: {
                    format: 'MM/DD/YYYY h:mm A'
                },
                startDate: moment.utc($('#startTime').val()).local(),
                endDate  : moment.utc($('#endTime').val()).local()
            }, function (start, end) {
                $('#startTime').val(moment(start).utc().format('YYYY-MM-DD HH:mm:00'));
                $('#endTime').val(moment(end).utc().format('YYYY-MM-DD HH:mm:00'));
            });
        });
    </script>
@endsection
