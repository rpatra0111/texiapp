@extends('admin.layouts.app')

@section('pageTitle', 'Dashboard')

@section('customStyles')
    {{-- You can write styles or other scripts here, it will be added to <head> --}}
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Admin Panel</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active"><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{!! count_users('P') !!}</h3>

                            <p>Customers</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="{!! admin_url('passenger') !!}" class="small-box-footer">More info <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>{!! count_users('D') !!}</h3>

                            <p>Drivers</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="{!! admin_url('driver') !!}" class="small-box-footer">More info <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{!! count_rides() !!}</h3>

                            <p>Rides Today</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-map"></i>
                        </div>
                        <a href="{!! admin_url('rides/user-rides') !!}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>
                                <sup style="font-size: 20px">$</sup> {!! \Illuminate\Support\Facades\Auth::user()->balance !!}
                            </h3>

                            <p>Balance</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-cash"></i>
                        </div>
                        {{--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Earning in $</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                            class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="earnChart" style="height:230px"></canvas>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"># of Rides</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                            class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="ridesChart" style="height:230px"></canvas>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"># of new Customers</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                            class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="custRegChart" style="height:230px"></canvas>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"># of new Drivers</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                            class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="drivRegChart" style="height:230px"></canvas>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col (RIGHT) -->
            </div>
            <!-- Main row -->
            <div class="row">
                <section class="col-lg-12 connectedSortable">
                    <!-- quick email widget -->
                    <div class="box box-info">
                        <div class="box-header">
                            <i class="fa fa-envelope"></i>

                            <h3 class="box-title">Quick Email</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget="remove"
                                        data-toggle="tooltip"
                                        title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                            <!-- /. tools -->
                        </div>
                        <div class="box-body">
                            <form action="#" method="post">
                                <div class="form-group">
                                    <input type="email" class="form-control" name="emailto" placeholder="Email to:">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="subject" placeholder="Subject">
                                </div>
                                <div>
                                    <textarea class="textarea" placeholder="Message"
                                              style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="box-footer clearfix">
                            <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                                <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </div>
                </section><!-- right col -->
            </div><!-- /.row (main row) -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('customScript')
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{!! asset_url('admin_assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}"></script>
    <!-- Chart -->
    <script src="{!! asset_url('admin_assets/bower_components/chart.js/Chart.js') !!}"></script>
    <script>
        $(document).ready(function () {
            'use strict';
            // Make the dashboard widgets sortable Using jquery UI
            $('.connectedSortable').sortable({
                placeholder: 'sort-highlight',
                connectWith: '.connectedSortable',
                handle: '.box-header, .nav-tabs',
                forcePlaceholderSize: true,
                zIndex: 999999
            });

            // bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5();
        });
    </script>
    <script>
        $(function () {
            let chartCanvas = $('#earnChart');
            let previousYear = [];
            @foreach($earning[date("Y",strtotime("-1 year"))] as $key => $val)
                previousYear.push({{ $val }});
            @endforeach
            let currentYear = [];
            @foreach($earning[date("Y")] as $key => $val)
                currentYear.push({{ $val }});
            @endforeach
            let barChart = new Chart(chartCanvas, {
                type: 'bar',
                data: {
                    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    datasets: [
                        {
                            label: '{{ date("Y",strtotime("-1 year")) }}',
                            backgroundColor: 'rgba(255, 99, 132, 0.7)',
                            borderColor: 'rgba(255,99,132,1)',
                            data: previousYear
                        },
                        {
                            label: '{{ date('Y') }}',
                            backgroundColor: 'rgba(54, 162, 235, 0.7)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            data: currentYear
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        });
        $(function () {
            let chartCanvas = $('#ridesChart');
            let previousYear = [];
            @foreach($rides[date("Y",strtotime("-1 year"))] as $key => $val)
                previousYear.push({{ $val }});
            @endforeach
            let currentYear = [];
            @foreach($rides[date("Y")] as $key => $val)
                currentYear.push({{ $val }});
            @endforeach
            let barChart = new Chart(chartCanvas, {
                type: 'bar',
                data: {
                    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    datasets: [
                        {
                            label: '{{ date("Y",strtotime("-1 year")) }}',
                            backgroundColor: 'rgba(255, 206, 86, 0.7)',
                            borderColor: 'rgba(255, 206, 86,1)',
                            data: previousYear
                        },
                        {
                            label: '{{ date('Y') }}',
                            backgroundColor: 'rgba(75, 192, 192, 0.7)',
                            borderColor: 'rgba(75, 192, 192, 1)',
                            data: currentYear
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        });
        $(function () {
            let chartCanvas = $('#custRegChart');
            let previousYear = [];
            @foreach($customer[date("Y",strtotime("-1 year"))] as $key => $val)
                previousYear.push({{ $val }});
            @endforeach
            let currentYear = [];
            @foreach($customer[date("Y")] as $key => $val)
                currentYear.push({{ $val }});
            @endforeach
            let barChart = new Chart(chartCanvas, {
                type: 'bar',
                data: {
                    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    datasets: [
                        {
                            label: '{{ date("Y",strtotime("-1 year")) }}',
                            backgroundColor: 'rgba(153, 102, 255, 0.7)',
                            borderColor: 'rgba(153, 102, 255,1)',
                            data: previousYear
                        },
                        {
                            label: '{{ date('Y') }}',
                            backgroundColor: 'rgba(255, 159, 64, 0.7)',
                            borderColor: 'rgba(255, 159, 64, 1)',
                            data: currentYear
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        });
        $(function () {
            let chartCanvas = $('#drivRegChart');
            let previousYear = [];
            @foreach($driver[date("Y",strtotime("-1 year"))] as $key => $val)
                previousYear.push({{ $val }});
            @endforeach
            let currentYear = [];
            @foreach($driver[date("Y")] as $key => $val)
                currentYear.push({{ $val }});
            @endforeach
            let barChart = new Chart(chartCanvas, {
                type: 'bar',
                data: {
                    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    datasets: [
                        {
                            label: '{{ date("Y",strtotime("-1 year")) }}',
                            backgroundColor: 'rgba(54, 162, 235, 0.7)',
                            borderColor: 'rgba(54, 162, 235,1)',
                            data: previousYear
                        },
                        {
                            label: '{{ date('Y') }}',
                            backgroundColor: 'rgba(255, 206, 86, 0.7)',
                            borderColor: 'rgba(255, 206, 86, 1)',
                            data: currentYear
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        });
    </script>
@endsection
