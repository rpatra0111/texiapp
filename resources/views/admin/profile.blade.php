@extends('admin.layouts.app')

@section('pageTitle', 'Dashboard')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Settings
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Profile</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Update Profile</h3>
                        </div>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                @foreach($errors->all() as $error)
                                    <p>{!! $error !!}</p>
                                @endforeach
                            </div>
                        @endif
                        @if(session('success'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                {!! session('success') !!}
                            </div>
                    @endif
                    <!-- form start -->
                        <form class="form-horizontal" name="settings_form" action="{!! admin_url('profile') !!}"
                              method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="admin_email" class="col-sm-2 control-label">Admin Email</label>
                                    <div class="col-sm-6">
                                        <input type="email" class="form-control" id="admin_email" name="admin_email"
                                               value="{!! $admin_user->email !!}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="firstName" class="col-sm-2 control-label">Admin Password</label>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control" id="admin_pass" name="admin_pass"
                                               autocomplete="new-password"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="profilePicture" class="col-sm-2 control-label">Profile Picture</label>
                                    <div class="col-sm-6">
                                        <span class="btn btn-default btn-file">
                                            Browse <input type="file" id="profilePicture" name="profilePicture"/>
                                        </span>
                                    </div>
                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div><!-- /.box-footer -->
                        </form>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->

@endsection