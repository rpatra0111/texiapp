<?php
$headingName = camelToSentence(ucfirst($resource));
$page_title = $nat . ' ' . $headingName;
// Resolute the data
$data = ${$resource};

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {!! $page_title !!}
                {{--<small>{{$nat}} {!! ucwords($resource)!!}</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{!! admin_url('taxitype/rate') !!}"><i
                                class="fa  fa-user"></i> {!! camelToSentence(ucfirst($resource_pl)) !!}</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <style>
                .help-block {
                    color: #FF0000;
                }
            </style>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ $page_title }}</h3>
                        </div>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{!! $error !!}</p>
                                @endforeach
                            </div>
                        @endif
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!! session('success') !!}
                            </div>
                        @endif
                        <form method="POST" action="{{ $saveLink }}" accept-charset="UTF-8" class="form-horizontal"
                              enctype="multipart/form-data">
                            <input name="_method" type="hidden" value={{$meth}}> {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="taxiTypeId" class="col-sm-2 control-label">Taxi Type</label>
                                    <div class="col-sm-6">
                                        <select id="taxiTypeId" class="form-control" name="taxiTypeId" required>
                                            @foreach ($taxiType as $val)
                                                <option @if(isset($data->taxiTypeId)) {{ $data->taxiTypeId==$val->id?"selected":"" }} @endif value="{{ $val->id }}">
                                                    {{$val->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('taxiTypeId'))
                                            <span class="help-block"><strong>{{ $errors->first('taxiTypeId') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="state" class="col-sm-2 control-label">State</label>
                                    <div class="col-sm-6">
                                        <select id="state" class="form-control" name="state" required>
                                            @foreach ($state as $val)
                                                <option @if(isset($data->City->stateId)) {{ $data->City->stateId==$val->id?"selected":"" }} @endif value="{{ $val->id }}">
                                                    {{ $val->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('state'))
                                            <span class="help-block"><strong>{{ $errors->first('state') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="countryCityId" class="col-sm-2 control-label">City</label>
                                    <div class="col-sm-6">
                                        <select id="countryCityId" class="form-control" name="countryCityId" required>
                                            @foreach ($city as $val)
                                                <option @if(isset($data->countryCityId)) {{$data->countryCityId==$val->id?"selected":""}} @endif value="{{ $val->id }}">
                                                    {{ $val->cityName }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('countryCityId'))
                                            <span class="help-block"><strong>{{ $errors->first('countryCityId') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fenceDistance" class="col-sm-2 control-label">Base Distance(KM)</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="fenceDistance" placeholder="car" type="text"
                                               value="{{isset($data->fenceDistance)?$data->fenceDistance:""}}"
                                               id="fenceDistance" required>
                                        @if ($errors->has('fenceDistance'))
                                            <span class="help-block"><strong>{{ $errors->first('fenceDistance') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="baseCost" class="col-sm-2 control-label">Base Cost</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="baseCost" placeholder="car" type="text"
                                               value="{{isset($data->baseCost)?$data->baseCost:""}}" id="baseCost"
                                               required>
                                        @if ($errors->has('name'))
                                            <span class="help-block"><strong>{{ $errors->first('baseCost') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="perKmCost" class="col-sm-2 control-label">Cost(Per KM)</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="perKmCost" placeholder="car" type="text"
                                               value="{{isset($data->perKmCost)?$data->perKmCost:""}}" id="perKmCost"
                                               required>
                                        @if ($errors->has('perKmCost'))
                                            <span class="help-block"><strong>{{ $errors->first('perKmCost') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="timeCharge" class="col-sm-2 control-label">Cost(Per Min)</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="timeCharge" placeholder="car" type="text"
                                               value="{{isset($data->timeCharge)?$data->timeCharge:""}}" id="timeCharge"
                                               required>
                                        @if ($errors->has('timeCharge'))
                                            <span class="help-block"><strong>{{ $errors->first('timeCharge') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="cancelCharge" class="col-sm-2 control-label">Cancel Charge</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="cancelCharge" placeholder="car" type="text"
                                               value="{{isset($data->cancelCharge)?$data->cancelCharge:""}}"
                                               id="cancelCharge" required>
                                        @if ($errors->has('cancelCharge'))
                                            <span class="help-block"><strong>{{ $errors->first('cancelCharge') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="minCharge" class="col-sm-2 control-label">Minimum Charge</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="minCharge" placeholder="car" type="text"
                                               value="{{isset($data->minCharge)?$data->minCharge:""}}" id="minCharge"
                                               required>
                                        @if ($errors->has('minCharge'))
                                            <span class="help-block"><strong>{{ $errors->first('minCharge') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <a class="btn btn-default" href="{!! admin_url(strtolower($resource)) !!}">
                                    Back</a>
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection



@section('customScript')
    <script>
        $('#state').change(function () {
            let stateId = this.value;
            $('#countryCityId').attr('disabled', true);
            $.ajax({
                url: "{{ admin_url('getCities') }}/" + stateId,
                type: "GET",
                dataType: 'JSON',
                success: function (result) {
                    if (result.status.toString() === 'success') {
                        let html = "";
                        result.cities.forEach(function (elem, key) {
                            html += "<option value=" + elem.id + ">" + elem.cityName + "</option>";
                        });
                        $('#countryCityId').html(html);
                        $('#countryCityId').attr('disabled', false);
                    }
                }
            });
        });
    </script>
@endsection
