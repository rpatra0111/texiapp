@php
    $page_title = ucfirst($resource) . ' ' . $nat;
    // Resolute the data
    $data = ${$resource};
    if ($data != "")
        $bankTrans = collect($data)->toArray()['bank_translate'];
    $lang = array();
    foreach($languages as $language) {
        $lang[$language->locale]=$language->name;
    }
@endphp

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {!! $page_title !!}
                <small>{{$nat}} {!! ucwords($resource)!!}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{!! admin_url("$resource") !!}"><i
                                class="fa  fa-user"></i> {!! ucfirst($resource_pl) !!}</a></li>
                <li class="active">{{$nat}} {!! ucwords($resource)!!}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <style>
                .help-block {
                    color: #FF0000;
                }
            </style>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{$nat}} {!! ucwords($resource)!!}</h3>
                        </div>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{!! $error !!}</p>
                                @endforeach
                            </div>
                        @endif
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!! session('success') !!}
                            </div>
                        @endif
                        <form method="POST" action="{{ $saveLink }}" accept-charset="UTF-8" class="form-horizontal"
                              enctype="multipart/form-data">
                            <input name="_method" type="hidden" value={{$meth}}>
                            {{ csrf_field() }}
                            <div class="box-body">
                                @foreach($lang as $k => $v)
                                    @if(isset($bankTrans))
                                        @foreach($bankTrans as $ke => $va)
                                            @if($va['locale'] == $k)
                                                <div class="form-group">
                                                    <label for="{{ $k }}" class="col-sm-2 control-label">{{ $v }}
                                                        Name</label>
                                                    <div class="col-sm-6">
                                                        <input class="form-control" name="{{ $k }}" placeholder="car"
                                                               type="text"
                                                               value="{{$va['bank_name']}}" id="{{ $k }}" required>
                                                        @if ($errors->has($k))
                                                            <span class="help-block"><strong>{{ $errors->first($k) }}</strong></span>
                                                        @endif
                                                    </div>
                                                </div>
                                                @break
                                            @endif
                                        @endforeach
                                    @else
                                        <div class="form-group">
                                            <label for="{{ $k }}" class="col-sm-2 control-label">{{ $v }} Name</label>
                                            <div class="col-sm-6">
                                                <input class="form-control" name="{{ $k }}" placeholder="car"
                                                       type="text"
                                                       value="" id="{{ $k }}" required>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="form-group">
                                    <label for="code" class="col-sm-2 control-label">Code</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="code" placeholder="car" type="text"
                                               value="{{isset($data->code)?$data->code:""}}" id="code" required>
                                        @if ($errors->has('code'))
                                            <span class="help-block"><strong>{{ $errors->first('code') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-6">
                                        <select id="status" class="form-control" name="status" required>
                                            <option @if(isset($data->status)){{ $data->status==0?"selected":"" }}@endif value="0">
                                                Inactive
                                            </option>
                                            <option @if(isset($data->status)){{ $data->status==1?"selected":"" }}@endif value="1">
                                                Active
                                            </option>
                                            <option @if(isset($data->status)){{ $data->status==2?"selected":"" }}@endif value="2">
                                                Deleted
                                            </option>
                                        </select>
                                        @if ($errors->has('status'))
                                            <span class="help-block"><strong>{{ $errors->first('status') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <a class="btn btn-default" href="{!! admin_url($resource) !!}">
                                    Back</a>
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection



@section('customScript')

@endsection
