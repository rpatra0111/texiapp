<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */

$page_title = camelToSentence(ucfirst($resource)) . ' ' . $nat;
// Resolute the data
$data = ${$resource};

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {!! $page_title !!}
                <small>{{$nat}} {!! ucwords($resource)!!}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{!! admin_url("location/".strtolower($resource)) !!}"><i
                                class="fa  fa-user"></i> {!! ucfirst($resource_pl) !!}</a></li>
                <li class="active">{{$nat}} {!! ucwords($resource)!!}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <style>
                .help-block {
                    color: #FF0000;
                }
            </style>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{$nat}} {!! ucwords($resource)!!}</h3>
                        </div>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{!! $error !!}</p>
                                @endforeach
                            </div>
                        @endif
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!! session('success') !!}
                            </div>
                        @endif
                        <form method="POST" action="{{ $saveLink }}" accept-charset="UTF-8" class="form-horizontal"
                              enctype="multipart/form-data">
                            <input name="_method" type="hidden" value={{$meth}}>
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="countryId" class="col-sm-2 control-label">Country</label>
                                    <div class="col-sm-6">
                                        <select id="countryId" class="form-control" name="countryId" required>
                                            @foreach ($countries as $country)
                                                <option @if(isset($data->state->countryId)) {{$data->state->countryId==$country->id?"selected":""}} @endif value="{{ $country->id }}">
                                                    {{$country->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('countryId'))
                                            <span class="help-block"><strong>{{ $errors->first('countryId') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="stateId" class="col-sm-2 control-label">State</label>
                                    <div class="col-sm-6">
                                        <select id="stateId" class="form-control" name="stateId" required>
                                            @foreach ($states as $state)
                                                <option @if(isset($data->stateId)) {{$data->stateId==$state->id?"selected":""}} @endif value="{{ $state->id }}">
                                                    {{$state->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('stateId'))
                                            <span class="help-block"><strong>{{ $errors->first('stateId') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="cityName" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="cityName" placeholder="John" type="text"
                                               value="{{isset($data->cityName)?$data->cityName:""}}" id="cityName"
                                               required>
                                        @if ($errors->has('cityName'))
                                            <span class="help-block"><strong>{{ $errors->first('cityName') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="countyName" class="col-sm-2 control-label">County Name</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="countyName" placeholder="Doe" type="text"
                                               value="{{isset($data->countyName)?$data->countyName:""}}"
                                               id="countyName">
                                        @if ($errors->has('countyName'))
                                            <span class="help-block"><strong>{{ $errors->first('countyName') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="latitude" class="col-sm-2 control-label">Latitude</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="latitude" placeholder="" type="text"
                                               value="{{isset($data->latitude)?$data->latitude:0.0}}" id="latitude">
                                        @if ($errors->has('latitude'))
                                            <span class="help-block"><strong>{{ $errors->first('latitude') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="longitude" class="col-sm-2 control-label">Longitude</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="longitude" placeholder="" type="text"
                                               value="{{isset($data->longitude)?$data->longitude:0.0}}" id="longitude">
                                        @if ($errors->has('longitude'))
                                            <span class="help-block"><strong>{{ $errors->first('longitude') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <a class="btn btn-default" href="{!! admin_url("location/".strtolower($resource)) !!}">
                                    Back</a>
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection



@section('customScript')
    <script>
        $('#countryId').change(function () {
            let countryId = this.value;
            $('#stateId').attr('disabled', true);
            $.ajax({
                url: "{{ admin_url('getStates') }}/" + countryId,
                type: "GET",
                dataType: 'JSON',
                success: function (result) {
                    if (result.status.toString() === 'success') {
                        let html = "";
                        result.states.forEach(function (elem, key) {
                            html += "<option value=" + elem.id + ">" + elem.name + "</option>";
                        });
                        $('#stateId').html(html);
                        $('#stateId').attr('disabled', false);
                    }
                }
            });
        });
    </script>
@endsection
