<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */

$page_title = camelToSentence(ucfirst($resource)) . ' ' . $nat;
// Resolute the data
$data = ${$resource};

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {!! $page_title !!}
                <small>{{$nat}} {!! ucwords($resource)!!}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{!! admin_url("location/".strtolower($resource)) !!}"><i
                                class="fa  fa-user"></i> {!! ucfirst($resource_pl) !!}</a></li>
                <li class="active">{{$nat}} {!! ucwords($resource)!!}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <style>
                .help-block {
                    color: #FF0000;
                }
            </style>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{$nat}} {!! ucwords($resource)!!}</h3>
                        </div>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{!! $error !!}</p>
                                @endforeach
                            </div>
                        @endif
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!! session('success') !!}
                            </div>
                        @endif
                        <form method="POST" action="{{ $saveLink }}" accept-charset="UTF-8" class="form-horizontal"
                              enctype="multipart/form-data">
                            <input name="_method" type="hidden" value={{$meth}}>
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="countryId" class="col-sm-2 control-label">Country</label>
                                <div class="col-sm-6">
                                    <select id="countryId" class="form-control" name="countryId" required>
                                        @foreach ($countries as $country)
                                            <option @if(isset($data->countryId)) {{$data->countryId==$country->id?"selected":""}} @endif value="{{ $country->id }}">
                                                {{$country->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('countryId'))
                                        <span class="help-block"><strong>{{ $errors->first('countryId') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="name" placeholder="John" type="text"
                                               value="{{isset($data->name)?$data->name:""}}" id="name"
                                               required>
                                        @if ($errors->has('name'))
                                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="code" class="col-sm-2 control-label">Code</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="code" placeholder="Doe" type="text"
                                               value="{{isset($data->code)?$data->code:""}}" id="code"
                                               required>
                                        @if ($errors->has('code'))
                                            <span class="help-block"><strong>{{ $errors->first('code') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <a class="btn btn-default" href="{!! admin_url("location/".strtolower($resource)) !!}">
                                    Back</a>
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection



@section('customScript')

@endsection
