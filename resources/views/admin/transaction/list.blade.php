<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */

$page_title = camelToSentence(ucfirst($resource_pl));

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title }}
                {{--<small>List</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">List of All {{ $page_title }}</h3>

                            <div class="box-tools">
                                <form method="get" action="{!! url()->current() !!}">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <button class="btn btn-default pull-right" id="daterange-btn" type="button">
                                                    <span>
                                                      <i class="fa fa-calendar"></i> {{(isset($startDate) && isset($endDate))?$startDate." - ".$endDate:"Select Date Range"}}
                                                    </span>
                                            @if(isset($userId))
                                                <input type="hidden" name="id" value="{{(isset($userId))?$userId:""}}">
                                            @endif
                                            <input type="hidden" name="start" value="{{(isset($startDate) && isset($endDate))?$startDate:""}}">
                                            <input type="hidden" name="end" value="{{(isset($startDate) && isset($endDate))?$endDate:""}}">
                                            <i class="fa fa-caret-down"></i>
                                        </button>

                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                            <h4 class="text-right" style="margin-top: 0px; padding-top: 20px;">Total Earning: <strong>$ @if(isset($totalAmount)) {{ $totalAmount }} @else 0 @endif</strong></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Type</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset(${$resource_pl}) && count(${$resource_pl})>0)
                                        @foreach(${$resource_pl} as $row)
                                            <tr>
                                                <td>{!! adminDataListDateTime($row->created_at)  !!}</td>
                                                <td>@if($row->taxiRideId) Taxi Ride @else Other @endif</td>
                                                <td>{!! $row->description  !!}</td>
                                                @if($row->transactionType == 'C')
                                                    <td style="color: green">+ {!! $row->amount  !!}</td>
                                                @else
                                                    <td style="color: red">- {!! $row->amount  !!}</td>
                                                @endif
                                                <td>@if($row->paymentStatus == 'Y') Success @else Failed @endif</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5" style="text-align: center;">No Data Found..</td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Date</th>
                                        <th>Type</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="paginationDiv">
                                {!! ${$resource_pl}->appends($_GET)->render() !!}
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('customScript')
    <!-- daterangepicker -->
    <script src="{!! asset_url('admin_assets/bower_components/moment/min/moment.min.js') !!}"></script>
    <script src="{!! asset_url('admin_assets/bower_components/bootstrap-daterangepicker/daterangepicker.js') !!}"></script>
    <script type="text/javascript">
        $(function () {
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('l') + ' - ' + end.format('l'));
                    $('#daterange-btn input[name="start"]').val(start.format('l'));
                    $('#daterange-btn input[name="end"]').val(end.format('l'));
                }
            )
        });
    </script>
@endsection
