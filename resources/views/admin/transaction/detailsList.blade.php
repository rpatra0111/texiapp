<?php

$page_title = camelToSentence(ucfirst($resource_pl));
?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title }}
                {{--<small>List</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">List of {{ $page_title }}</h3>
                            <div class="clearfix"></div>
                            @if (!empty($lastAdjustment))
                            <h4 class="text-right" style="margin-top: 0px; padding-top: 20px;">Last Adjustment: <strong> @if(isset($lastAdjustment['description'])) {{ $lastAdjustment['description'] }} @else 0 @endif on {{ adminDataListDateTime($lastAdjustment->created_at) }} </strong></h4>
                            @endif
                            {{-- <h4 class="text-right" style="margin-top: 0px; padding-top: 20px;">Total Earning: <strong>$ @if(isset($totalAmount)) {{ $totalAmount }} @else 0 @endif</strong></h4> --}}
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Ride From Location</th>
                                        <th>Ride To Location</th>
                                        <th>Type</th>
                                        <th>Description</th>
                                        <th>Paymeny Type</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($transactionHistories) && count($transactionHistories)>0)
                                        @foreach($transactionHistories as $row)
                                            <tr @if (empty($row['TaxiRide'])) style="background-color: #ceede3;" @endif>
                                                <td>{!! adminDataListDateTime($row->created_at)  !!}</td>
                                                <td> {{ $row['TaxiRide']['fromAddress'] ? $row['TaxiRide']['fromAddress'] : "---" }} </td>
                                                <td> {{ $row['TaxiRide']['toAddress'] ? $row['TaxiRide']['toAddress'] : "---" }} </td>
                                                <td>@if($row->taxiRideId) Taxi Ride @else Other @endif</td>
                                                <td>{!! $row->description  !!}</td>
                                                <td>
                                                    @if ($row['TaxiRide']['paidBy'] == "R" && $row->description == "Received for the ride")
                                                        <i class="fa fa-credit-card" aria-hidden="true" style="font-size: 20px;"></i> Card
                                                    @elseif($row['TaxiRide']['paidBy'] == "S" && $row->description == "Received for the ride.")
                                                        <i class="fa fa-money" aria-hidden="true" style="color: green; font-size: 20px;"></i> Cash
                                                    @else 
                                                        --- 
                                                    @endif
                                                </td>
                                                @if($row->transactionType == 'C')
                                                    <td style="color: green">+ {!! $row->amount  !!}</td>
                                                @else
                                                    <td style="color: red">- {!! $row->amount  !!}</td>
                                                @endif
                                                <td>@if($row->paymentStatus == 'Y') Success @else Failed @endif</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8" style="text-align: center;">No Data Found..</td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="6" style="text-align-last: end;">Total By Card <i class="fa fa-credit-card" aria-hidden="true"  style="font-size: 20px;"></i></th>
                                        <th colspan="2"> {{ $byCard }} $</th>
                                    </tr>
                                    <tr>
                                        <th colspan="6" style="text-align-last: end;">Total By Cash <i class="fa fa-money" aria-hidden="true" style="color: green; font-size: 20px;"></i></th>
                                        <th colspan="2"> {{ $byCash }} $</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="paginationDiv">
                                {!! $transactionHistories->appends($_GET)->render() !!}
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('customScript')

@endsection
