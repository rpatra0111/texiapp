<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            @if(\Illuminate\Support\Facades\Auth::user()->userType=='SU')
                <img src="{!! (\Illuminate\Support\Facades\Auth::user()->profilePicture)?asset_url('admin_assets/dist/img/'.\Illuminate\Support\Facades\Auth::user()->profilePicture):asset_url('default_images/avatar.jpg') !!}" class="img-circle" alt="User Image">
            @else
                <img src="{!! (\Illuminate\Support\Facades\Auth::user()->profilePicture)?asset_url('uploads/profilePictures/'.\Illuminate\Support\Facades\Auth::user()->profilePicture):asset_url('default_images/avatar.jpg') !!}" class="img-circle" alt="User Image">
            @endif
        </div>
        <div class="pull-left info">
            <p>{{ \Illuminate\Support\Facades\Auth::user()->firstName }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <ul class="sidebar-menu tree" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        @php
            $menubar = getAdminMenu();
        @endphp
        @foreach($menubar as $value)
            @if($value['parent_id'] == 0)
                @php
                    $cArr = [];
                    foreach ($menubar as $pkey => $pvalue) {
                        if ($pvalue['parent_id'] == $value['id']) {
                            $cArr[]=$pvalue;
                        }
                    }
                @endphp
                <li @if(count($cArr)>0) class="{{ (Request::segment(2) == $value['link'])?'treeview active':'treeview' }}" @else class="{{ (Request::segment(2) == $value['link'])?'active':'' }}"@endif>
                    <a href={{ (count($cArr)==0)?admin_url($value['link']):'#' }}>
                        <i class="fa {{ $value['fa_icon'] }}"></i>
                        <span>{{ $value['name'] }}</span>
                        @if(count($cArr)>0)
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        @endif
                    </a>
                    @if(count($cArr)>0)
                        <ul class="treeview-menu">
                            @foreach($cArr as $valueOne)
                                @php
                                    $gcArr = [];
                                    foreach ($menubar as $pkey => $pvalue) {
                                        if ($pvalue['parent_id'] == $valueOne['id']) {
                                            $gcArr[]=$pvalue;
                                        }
                                    }
                                @endphp
                                <li @if(count($gcArr)>0) class="{{ (Request::segment(2) == $value['link'] && Request::segment(3) == $valueOne['link'])?'treeview active':'treeview' }}" @else class="{{ (Request::segment(2) == $value['link'] && Request::segment(3) == $valueOne['link'])?'active':'' }}"@endif>
                                    <a href={{ (count($gcArr)==0)?admin_url($value['link'].'/'.$valueOne['link']):'#' }}>
                                        <i class="fa {{ $valueOne['fa_icon'] }}"></i>
                                        <span>{{ $valueOne['name'] }}</span>
                                        @if(count($gcArr)>0)
                                            <span class="pull-right-container">
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                        @endif
                                    </a>
                                    @if(count($gcArr)>0)
                                        <ul class="treeview-menu">
                                            @foreach($gcArr as $valueTwo)
                                                <li class="{{ (Request::segment(3) == $valueOne['link'] && Request::segment(4) == $valueTwo['link'])?'active':'' }}">
                                                    <a href="{{ admin_url($value['link'].'/'.$valueOne['link'].'/'.$valueTwo['link']) }}"><i class="fa {{ $valueTwo['fa_icon'] }}"></i> {{ $valueTwo['name'] }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endif
        @endforeach
        {{--<li>
            <a href="{{ admin_url('dashboard') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
        <li class="{{ (Request::segment(2) == 'site')?'treeview active':'treeview' }}">
            <a href="#">
                <i class="fa fa-bookmark"></i>
                <span>Site Customizations</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ (Request::segment(2) == 'site' && Request::segment(3) == 'settings')?'active':'' }}">
                    <a href="{{ admin_url('site/settings') }}">
                        <i class="fa fa-cog"></i> <span>Site Settings</span>
                    </a>
                </li>
                <li class="{{ (Request::segment(2) == 'site' && Request::segment(3) == 'review')?'treeview active':'treeview' }}">
                    <a href="#">
                        <i class="fa fa-book"></i>
                        <span>Driver Reviews</span>
                        <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ (Request::segment(3) == 'review' && Request::segment(4) == 'create')?'active':'' }}">
                            <a href="{{ admin_url('site/review/create') }}"><i class="fa fa-circle-o"></i> Add</a>
                        </li>
                        <li class="{{ (Request::segment(3) == 'review' && empty(Request::segment(4)))?'active':'' }}">
                            <a href="{{ admin_url('site/review') }}"><i class="fa fa-circle-o"></i> List</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="{{ Request::segment(2) == 'driver'?'treeview active':'treeview' }}">
            <a href="#">
                <i class="fa fa-id-card"></i>
                <span>Drivers</span>
                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ Request::segment(2) == 'driver' && Request::segment(3) == 'create'?'active':'' }}">
                    <a href="{{ admin_url('driver/create') }}"><i class="fa fa-circle-o"></i> Add</a>
                </li>
                <li class="{{ Request::segment(2) == 'driver' && empty(Request::segment(3))?'active':'' }}">
                    <a href="{{ admin_url('driver') }}"><i class="fa fa-circle-o"></i> List</a>
                </li>
                <li class="{{ Request::segment(2) == 'driver' && Request::segment(3) == 'document'?'active':'' }}">
                    <a href="{{ admin_url('driver/document') }}"><i class="fa fa-circle-o"></i>
                        Verification</a>
                </li>
                <li class="{{ Request::segment(2) == 'driver' && Request::segment(3) == 'withdraw'?'active':'' }}">
                    <a href="{{ admin_url('driver/withdrawal') }}"><i class="fa fa-circle-o"></i>
                        Withdrawal</a>
                </li>
                <li class="{{ Request::segment(2) == 'driver' && Request::segment(3) == 'walletadjust'?'active':'' }}">
                    <a href="{{ admin_url('driver/walletadjust') }}"><i class="fa fa-circle-o"></i>
                        Driver Wallet</a>
                </li>
            </ul>
        </li>
        <li class="{{ Request::segment(2) == 'passenger'?'treeview active':'treeview' }}">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Passengers</span>
                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ Request::segment(2) == 'passenger' && Request::segment(3) == 'create'?'active':'' }}">
                    <a href="{{ admin_url('passenger/create') }}"><i class="fa fa-circle-o"></i> Add</a>
                </li>
                <li class="{{ Request::segment(2) == 'passenger' && empty(Request::segment(3))?'active':'' }}">
                    <a href="{{ admin_url('passenger') }}"><i class="fa fa-circle-o"></i> List</a>
                </li>
            </ul>
        </li>
        <li class="{{ Request::segment(2) == 'subAdmin'?'treeview active':'treeview' }}">
            <a href="#">
                <i class="fa fa-graduation-cap"></i>
                <span>Admins</span>
                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ Request::segment(2) == 'subAdmin' && Request::segment(3) == 'create'?'active':'' }}">
                    <a href="{{ admin_url('subAdmin/create') }}"><i class="fa fa-circle-o"></i> Add</a>
                </li>
                <li class="{{ Request::segment(2) == 'subAdmin' && empty(Request::segment(3))?'active':'' }}">
                    <a href="{{ admin_url('subAdmin') }}"><i class="fa fa-circle-o"></i> List</a>
                </li>
            </ul>
        </li>
        <li class="{{ (Request::segment(2) == 'taxitype' || Request::segment(2) == 'taxityperate')?'treeview active':'treeview' }}">
            <a href="#">
                <i class="fa fa-car"></i>
                <span>Taxi Type</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ (Request::segment(2) == 'taxitype' && Request::segment(3) == 'create')?'active':'' }}">
                    <a href="{{ admin_url('taxitype/create') }}"><i class="fa fa-circle-o"></i> Add</a>
                </li>
                <li class="{{ Request::segment(2) == 'taxitype' && empty(Request::segment(3))?'active':'' }}">
                    <a href="{{ admin_url('taxitype') }}"><i class="fa fa-circle-o"></i> List</a>
                </li>
                <li class="{{ Request::segment(2) == 'taxityperate'?'treeview active':'treeview' }}">
                    <a href="#">
                        <i class="fa fa-map-marker"></i>
                        <span>City Specific Pricing</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ Request::segment(2) == 'taxityperate' && Request::segment(3) == 'create'?'active':'' }}">
                            <a href="{{ admin_url('taxityperate/create') }}"><i class="fa fa-circle-o"></i> Add</a>
                        </li>
                        <li class="{{ Request::segment(2) == 'taxityperate' && empty(Request::segment(3))?'active':'' }}">
                            <a href="{{ admin_url('taxityperate') }}"><i class="fa fa-circle-o"></i> List</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="{{ (Request::segment(2) == 'location')?'treeview active':'treeview' }}">
            <a href="#">
                <i class="fa fa-globe"></i>
                <span>Location</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ Request::segment(3) == 'country'?'treeview active':'treeview' }}">
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span>Country</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ Request::segment(3) == 'country' && Request::segment(4) == 'create'?'active':'' }}">
                            <a href="{{ admin_url('location/country/create') }}"><i class="fa fa-circle-o"></i> Add</a>
                        </li>
                        <li class="{{ Request::segment(3) == 'country' && empty(Request::segment(4))?'active':'' }}">
                            <a href="{{ admin_url('location/country') }}"><i class="fa fa-circle-o"></i> List</a>
                        </li>
                    </ul>
                </li>
                <li class="{{ Request::segment(3) == 'state'?'treeview active':'treeview' }}">
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span>State</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ Request::segment(3) == 'state' && Request::segment(4) == 'create'?'active':'' }}">
                            <a href="{{ admin_url('location/state/create') }}"><i class="fa fa-circle-o"></i> Add</a>
                        </li>
                        <li class="{{ Request::segment(3) == 'state' && empty(Request::segment(4))?'active':'' }}">
                            <a href="{{ admin_url('location/state') }}"><i class="fa fa-circle-o"></i> List</a>
                        </li>
                    </ul>
                </li>
                <li class="{{ Request::segment(3) == 'city'?'treeview active':'treeview' }}">
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span>City</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ Request::segment(3) == 'city' && Request::segment(4) == 'create'?'active':'' }}">
                            <a href="{{ admin_url('location/city/create') }}"><i class="fa fa-circle-o"></i> Add</a>
                        </li>
                        <li class="{{ Request::segment(3) == 'city' && empty(Request::segment(4))?'active':'' }}">
                            <a href="{{ admin_url('location/city') }}"><i class="fa fa-circle-o"></i> List</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="{{ (Request::segment(2) == 'email')?'treeview active':'treeview' }}">
            <a href="#">
                <i class="fa fa-envelope"></i>
                <span>Email Templates</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ (Request::segment(2) == 'email' && Request::segment(3) == 'create')?'active':'' }}">
                    <a href="{{ admin_url('email/create') }}"><i class="fa fa-circle-o"></i> Add</a>
                </li>
                <li class="{{ (Request::segment(2) == 'email' && empty(Request::segment(3)))?'active':'' }}">
                    <a href="{{ admin_url('email') }}"><i class="fa fa-circle-o"></i> List</a>
                </li>
            </ul>
        </li>
        <li class="{{ (Request::segment(2) == 'helptopic')?'treeview active':'treeview' }}">
            <a href="#">
                <i class="fa fa-question-circle"></i>
                <span>Help Topics</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ (Request::segment(2) == 'helptopic' && Request::segment(3) == 'create')?'active':'' }}">
                    <a href="{{ admin_url('helptopic/create') }}"><i class="fa fa-circle-o"></i> Add</a>
                </li>
                <li class="{{ (Request::segment(2) == 'helptopic' && empty(Request::segment(3)))?'active':'' }}">
                    <a href="{{ admin_url('helptopic') }}"><i class="fa fa-circle-o"></i> List</a>
                </li>
            </ul>
        </li>
        <li class="{{ (Request::segment(2) == 'coupon')?'treeview active':'treeview' }}">
            <a href="#">
                <i class="fa fa-ticket"></i>
                <span>Coupons</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ (Request::segment(2) == 'coupon' && Request::segment(3) == 'create')?'active':'' }}">
                    <a href="{{ admin_url('coupon/create') }}"><i class="fa fa-circle-o"></i> Add</a>
                </li>
                <li class="{{ (Request::segment(2) == 'coupon' && empty(Request::segment(3)))?'active':'' }}">
                    <a href="{{ admin_url('coupon') }}"><i class="fa fa-circle-o"></i> List</a>
                </li>
            </ul>
        </li>
        <li class="{{ (Request::segment(2) == 'transaction')?'treeview active':'treeview' }}">
            <a href="#">
                <i class="fa fa-exchange"></i>
                <span>Transactions</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ (Request::segment(2) == 'transaction' && Request::segment(3) == 'administrator')?'active':'' }}">
                    <a href="{{ admin_url('transaction/administrator') }}"><i class="fa fa-circle-o"></i> Administrator</a>
                </li>
                <li class="{{ (Request::segment(2) == 'transaction' && Request::segment(3) == 'company')?'active':'' }}">
                    <a href="{{ admin_url('transaction/company') }}"><i class="fa fa-circle-o"></i> Company</a>
                </li>
            </ul>
        </li>--}}
    </ul>
</section>
