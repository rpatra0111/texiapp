<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin | @yield('pageTitle')</title>
    <!-- favicon -->
    <link rel="shortcut icon" href="{!! asset_url('favicon.ico') !!}" type="image/x-icon">
    <link rel="icon" href="{!! asset_url('favicon.ico') !!}" type="image/x-icon">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet"
          href="{!! asset_url('admin_assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}">
    <!-- Font Awesome -->
    <link rel="stylesheet"
          href="{!! asset_url('admin_assets/bower_components/font-awesome/css/font-awesome.min.css') !!}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{!! asset_url('admin_assets/bower_components/Ionicons/css/ionicons.min.css') !!}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{!! asset_url('admin_assets/dist/css/AdminLTE.min.css') !!}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{!! asset_url('admin_assets/dist/css/skins/skin-yellow.min.css') !!}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{!! asset_url('admin_assets/plugins/iCheck/flat/yellow.css') !!}">
    <!-- Daterange picker -->
    <link rel="stylesheet"
          href="{!! asset_url('admin_assets/bower_components/bootstrap-daterangepicker/daterangepicker.css') !!}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet"
          href="{!! asset_url('admin_assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{!! asset_url('admin_assets/plugins/select2/css/select2.min.css') !!}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{!! asset_url('admin_assets/dist/css/style.css') !!}">
    <style>
        .sidebar{padding-bottom: 0 !important;}
        .sidebar-menu{overflow: auto !important;}
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @yield('customStyles')
</head>
<body class="hold-transition skin-yellow sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="{!! admin_url('dashboard') !!}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">
                @php
                    $site_logo = asset_url('uploads/site_logo')."/".get_general_settings('logo');
                @endphp
                <img src="{{ $site_logo }}" class="img-responsive" alt="Site Logo" style="height: 50px; width: auto;"/>
            </span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">
                <b>Admin</b>
                </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            @if(\Illuminate\Support\Facades\Auth::user()->userType=='SU')
                                <img src="{!! \Illuminate\Support\Facades\Auth::user()->profilePicture?asset_url('admin_assets/dist/img/'.\Illuminate\Support\Facades\Auth::user()->profilePicture):asset_url('default_images/avatar.jpg') !!}" class="user-image" alt="User Image">
                            @else
                                <img src="{!! \Illuminate\Support\Facades\Auth::user()->profilePicture?asset_url('uploads/profilePictures/'.\Illuminate\Support\Facades\Auth::user()->profilePicture):asset_url('default_images/avatar.jpg') !!}" class="user-image" alt="User Image">
                            @endif
                            <span class="hidden-xs">{!! \Illuminate\Support\Facades\Auth::user()->firstName !!}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                @if(\Illuminate\Support\Facades\Auth::user()->userType=='SU')
                                    <img src="{!! \Illuminate\Support\Facades\Auth::user()->profilePicture?asset_url('admin_assets/dist/img/'.\Illuminate\Support\Facades\Auth::user()->profilePicture):asset_url('default_images/avatar.jpg') !!}" class="user-image" alt="User Image">
                                @else
                                    <img src="{!! \Illuminate\Support\Facades\Auth::user()->profilePicture?asset_url('uploads/profilePictures/'.\Illuminate\Support\Facades\Auth::user()->profilePicture):asset_url('default_images/avatar.jpg') !!}" class="user-image" alt="User Image">
                                @endif
                                <p>{!! \Illuminate\Support\Facades\Auth::user()->firstName !!}</p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="{!! admin_url('logout') !!}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                                <div class="pull-left">
                                    <a class="btn btn-default btn-flat" href="{!! admin_url('profile') !!}">Profile</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        @include('admin.layouts.sidebar')
    </aside>

    @yield('content')

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 6.5.4
        </div>
        <strong>Copyright &copy; {{ date("Y") }}
            <a href="https://www.technoexponent.com">Techno Exponent</a>.
        </strong> All rights reserved.
    </footer>
</div><!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{!! asset_url('admin_assets/bower_components/jquery/dist/jquery.min.js') !!}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{!! asset_url('admin_assets/bower_components/jquery-ui/jquery-ui.min.js') !!}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{!! asset_url('admin_assets/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}"></script>
<!-- Select2 -->
<script src="{!! asset_url('admin_assets/plugins/select2/js/select2.min.js') !!}"></script>
<!-- Slimscroll -->
<script src="{!! asset_url('admin_assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') !!}"></script>
<!-- FastClick -->
<script src="{!! asset_url('admin_assets/bower_components/fastclick/lib/fastclick.js') !!}"></script>
<!-- AdminLTE App -->
<script src="{!! asset_url('admin_assets/dist/js/adminlte.min.js') !!}"></script>

<script type="text/javascript">
    window.setTimeout(function () {
        $(".alert-success").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 3000);
    $(".select2").select2();

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    $(document).ready(function(){
        setTimeout(function () {
            //var screenH = $(window).height();
            // var slidebargap = $('.main-header').innerHeight() + $('.sidebar .user-panel').innerHeight();
            var sidebarH = ($('.content-wrapper').innerHeight() - $('.sidebar .user-panel').innerHeight()) + $('.main-footer').innerHeight();
            $('.sidebar-menu').css({'max-height': sidebarH});
        },2000);
    });
</script>

@yield('customScript')

</body>
</html>
