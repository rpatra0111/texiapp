@php
    $page_title = ucfirst($resource_pl);
    $lang = array();
    foreach($languages as $language) {
        $lang[$language->locale]=$language->name;
    }
@endphp

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title }}
                {{--<small>List</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">List of All {{ $page_title }}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    @foreach($lang as $key => $values)
                                        <th>{{ $values }}</th>
                                    @endforeach
                                    <th>Status</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset(${$resource_pl}) && count(${$resource_pl})>0)
                                    @foreach(${$resource_pl} as $row)
                                        @php
                                            $t = $row->toArray()['hqt_translations'];
                                        @endphp
                                        <tr>
                                            @foreach($t as $k => $v)
                                                @foreach($lang as $key => $values)
                                                    @if($key == $v['locale'])
                                                        <th>{{ $v['topic_name'] }}</th>
                                                        @break
                                                    @endif
                                                @endforeach
                                            @endforeach
                                            <td>{!! $row->status !!}</td>
                                            <td>
                                                <a href="{!! admin_url(strtolower($resource).'/'.$row->id.'/edit') !!}"
                                                   class="btn btn-sm btn-warning td-btn">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                                <form method="POST"
                                                      action="{!! admin_url(strtolower($resource).'/'.$row->id) !!}"
                                                      onsubmit="return confirm('Are you sure to remove {!! $row->name !!}?');">
                                                    <input name="_method" type="hidden"
                                                           value="DELETE">{{ csrf_field() }}
                                                    <button class="btn btn-sm btn-danger td-btn" type="submit">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                        Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="{{ count($lang)+2 }}" style="text-align: center;">No Data Found..</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    @foreach($lang as $key => $values)
                                        <th>{{ $values }}</th>
                                    @endforeach
                                    <th>Status</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="paginationDiv">
                                {!! ${$resource_pl}->appends($_GET)->render() !!}
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('customScript')
    <script type="text/javascript">
    </script>
@endsection
