@extends('admin.layouts.app')

@section('pageTitle', 'Dashboard')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Settings
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Settings</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Update Settings</h3>
                        </div>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                @foreach($errors->all() as $error)
                                    <p>{!! $error !!}</p>
                                @endforeach
                            </div>
                        @endif
                        @if(session('success'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                {!! session('success') !!}
                            </div>
                    @endif
                    <!-- form start -->
                        <form class="form-horizontal" name="settings_form" action="{!! admin_url('site/settings') !!}"
                              method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="siteTitle" class="col-sm-2 control-label">Site Title</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="siteTitle" name="siteTitle"
                                               value="{!! $settings->siteTitle !!}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="contactEmail" class="col-sm-2 control-label">Contact Email</label>
                                    <div class="col-sm-6">
                                        <input type="email" class="form-control" id="contactEmail" name="contactEmail"
                                               value="{!! $settings->contactEmail !!}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="contactName" class="col-sm-2 control-label">Contact Email Name</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="contactName" name="contactName"
                                               value="{!! $settings->contactName !!}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="contactPhone" class="col-sm-2 control-label">Contact Phone</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="contactPhone" name="contactPhone"
                                               value="{!! $settings->contactPhone !!}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="driverRange" class="col-sm-2 control-label">Show driver in range</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="driverRange" name="driverRange"
                                               value="{!! $settings->driverRange !!}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bookingRange" class="col-sm-2 control-label">Max range for
                                        booking</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="bookingRange" name="bookingRange"
                                               value="{!! $settings->bookingRange !!}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="commission" class="col-sm-2 control-label">Admin Commission</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="commission" name="commission"
                                               value="{!! $settings->commission !!}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ref_reword_point" class="col-sm-2 control-label">Reword Point Per Reference</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="ref_reword_point" name="ref_reword_point"
                                               value="{!! $settings->ref_reword_point !!}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="contactAdminText" class="col-sm-2 control-label">Contact Admin Message</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="contactAdminText" name="contactAdminText"
                                               value="{!! $settings->contactAdminText !!}"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="logo" class="col-sm-2 control-label">Logo Image</label>
                                    <div class="col-sm-6">
								<span class="btn btn-default btn-file">
									Browse <input type="file" id="logo" name="logo"/>
								</span>
                                        <p class="help-block" id="thumb_image_help">Current Site Logo</p>
                                        <img class="list_table_img"
                                             src="{!! asset_url('uploads/site_logo/'.$settings->logo) !!}"
                                             alt="No Logo">
                                    </div>
                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div><!-- /.box-footer -->
                        </form>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->

@endsection