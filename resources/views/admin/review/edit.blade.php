@php
    $page_title = camelToSentence(ucfirst($resource)) . ' ' . $nat;
    // Resolute the data
    $data = ${$resource};
@endphp

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {!! $page_title !!}
                <small>{{$nat}} {!! ucwords($resource)!!}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{!! admin_url('site/'.strtolower($resource)) !!}"><i
                                class="fa  fa-user"></i> {!! ucfirst($resource_pl) !!}</a></li>
                <li class="active">{{$nat}} {!! ucwords($resource)!!}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <style>
                .help-block {
                    color: #FF0000;
                }
            </style>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{$nat}} {!! ucwords($resource)!!}</h3>
                        </div>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{!! $error !!}</p>
                                @endforeach
                            </div>
                        @endif
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!! session('success') !!}
                            </div>
                        @endif
                        <form method="POST" action="{{ $saveLink }}" accept-charset="UTF-8" class="form-horizontal"
                              enctype="multipart/form-data">
                            <input name="_method" type="hidden" value={{$meth}}>
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="driver_pic" class="col-sm-2 control-label">Driver Picture</label>
                                    <div class="col-sm-6">
                                        @php
                                            $driver_pic=asset_url('default_images/avatar.jpg');
                                            if (isset($data->driver_pic)){
                                                $driver_pic = asset_url("site_assets/images/driver_pic/$data->driver_pic");
                                            }
                                        @endphp
                                        <img id="sel_img" style="height: 100px; width: 100px;"
                                             src="{!! $driver_pic !!}">
                                        <input type="file" name="driver_pic" id="driver_pic">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="driver_name" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" name="driver_name" placeholder="Name" type="text" value="{{isset($data->driver_name)?$data->driver_name:""}}" id="driver_name" required>
                                        @if ($errors->has('driver_name'))
                                            <span class="help-block"><strong>{{ $errors->first('driver_name') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="review" class="col-sm-2 control-label">Review</label>
                                    <div class="col-sm-6">
                                        <textarea class="form-control" name="review" placeholder="Review" id="review" required>{{isset($data->review)?$data->review:""}}</textarea>
                                        @if ($errors->has('review'))
                                            <span class="help-block"><strong>{{ $errors->first('review') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <a class="btn btn-default" href="{!! admin_url(strtolower('site/'.$resource)) !!}">
                                    Back</a>
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection



@section('customScript')

@endsection
