@php
$page_title = ucfirst($resource_pl);
@endphp

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title }}
                {{--<small>List</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">List of All {{ $page_title }}</h3>

                            <div class="box-tools">
                                <form method="get" action="{!! url()->current() !!}">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input name="keyword" class="form-control pull-right" type="text" placeholder="Search" value="{{ (isset($keyword))?$keyword:"" }}">

                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Review</th>
                                    <th>Created On</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset(${$resource_pl}) && count(${$resource_pl})>0)
                                    @foreach(${$resource_pl} as $row)
                                        <tr>
                                            <td>
                                                @if(!empty($row->driver_pic))
                                                    <img src="{{ asset_url('site_assets/images/driver_pic/'.$row->driver_pic) }}"
                                                         style="height:70px; width:70px;">
                                                @endif
                                            </td>
                                            <td>{!! $row->driver_name  !!}</td>
                                            <td>{!! $row->review  !!}</td>
                                            <td>{!! adminDataListDateTime($row->created_at)  !!}</td>
                                            <td>
                                                <a href="{!! admin_url('site/'.strtolower($resource).'/'.$row->id.'/edit') !!}"
                                                   class="btn btn-sm btn-warning td-btn">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                                <form method="POST"
                                                      action="{!! admin_url('site/' .strtolower($resource). '/' . @$row->id) !!}"
                                                      onsubmit="return confirm('Are you sure to remove {!! $row->driver_name !!}?');">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    {{ csrf_field() }}
                                                    <button class="btn btn-sm btn-danger td-btn" type="submit">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                        Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" style="text-align: center;">No Data Found..</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Review</th>
                                    <th>Created On</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="paginationDiv">
                                {!! ${$resource_pl}->appends($_GET)->render() !!}
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('customScript')
    <script type="text/javascript">

    </script>
@endsection
