<?php

$page_title = 'Rejected '.ucfirst($resource_pl);

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title }}
                {{--<small>List</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">List of Rejected Rides</h3>

                            <div class="box-tools">
                                <form method="get" action="{!! url()->current() !!}">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <button class="btn btn-default pull-right" id="daterange-btn" type="button">
                                            <span>
                                              <i class="fa fa-calendar"></i> {{(isset($startDate) && isset($endDate))?$startDate." - ".$endDate:"Select Date Range"}}
                                            </span>
                                            <input type="hidden" name="start" value="{{(isset($startDate) && isset($endDate))?$startDate:""}}">
                                            <input type="hidden" name="end" value="{{(isset($startDate) && isset($endDate))?$endDate:""}}">
                                            <i class="fa fa-caret-down"></i>
                                        </button>

                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Passenger Name</th>
                                    {{--<th>Driver Name</th>--}}
                                    <th>Source</th>
                                    <th>Destination</th>
                                    <th>Distance (KM)</th>
                                    <th>Regular Price</th>
                                    <th>Amount Offered</th>
                                    <th>Status</th>
                                    {{--<th>Payment Status</th>--}}
                                    <th>Payment Method</th>
                                    <th>Coupon</th>
                                    <th>Started On (UTC)</th>
                                    <th>Finished On (UTC)</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset(${$resource_pl}) && count(${$resource_pl})>0)
                                    @foreach(${$resource_pl} as $row)
                                        <tr>
                                            <td>
                                                <a href="{!! admin_url('passenger/'.$row->passenger->id.'/edit') !!}">
                                                    {!! $row->passenger->firstName !!} {!! $row->passenger->lastName !!}
                                                </a>
                                            </td>
                                            {{--<td>--}}
                                                {{--<a href="{!! admin_url('driver/'.$row->usertaxi->driver->id.'/edit') !!}">--}}
                                                    {{--{!! $row->usertaxi->driver->firstName !!} {!! $row->usertaxi->driver->lastName !!}--}}
                                                {{--</a>--}}
                                            {{--</td>--}}
                                            <td>{!! $row->fromAddress !!}</td>
                                            <td>{!! $row->toAddress !!}</td>
                                            <td>{!! $row->estimatedDistance/1000 !!}</td>
                                            <td>{!! $row->estimatedFare  !!}</td>
                                            <td>{!! $row->fare  !!}</td>
                                            <td>
                                                @if($row->cancel == 'N')
                                                    @if($row->status == 'S')
                                                        Ride Started
                                                    @elseif($row->status == 'A')
                                                        Ride Active
                                                    @elseif($row->status == 'R')
                                                        Ride Completed, Awaiting Payment
                                                    @elseif($row->status == 'Y')
                                                        Ride Successfully Completed
                                                    @elseif($row->status == 'N')
                                                        Ride Not Completed
                                                    @endif
                                                @elseif($row->cancel == 'P')
                                                    Ride Cancelled By Passenger
                                                @else
                                                    Ride Cancelled By Driver
                                                @endif
                                            </td>
                                            {{--<td>--}}
                                                {{--@if($row->paidStatus == 'N')--}}
                                                    {{--Not Paid--}}
                                                {{--@elseif($row->paidStatus == 'P')--}}
                                                    {{--Payment Pending--}}
                                                {{--@else--}}
                                                    {{--Paid--}}
                                                {{--@endif--}}
                                            {{--</td>--}}
                                            <td>
                                                @if($row->paidBy == 'R')
                                                    Card
                                                @else
                                                    Cash
                                                @endif
                                            </td>
                                            <td>@if($row->coupon){!! $row->coupon->code  !!}@endif</td>
                                            <td>{!! adminDataListDateTime($row->created_at)  !!}</td>
                                            <td>@if($row->cancel != 'N' || $row->status == 'Y'){!! adminDataListDateTime($row->updated_at)  !!}@endif</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9" style="text-align: center;">No Data Found..</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Passenger Name</th>
                                    {{--<th>Driver Name</th>--}}
                                    <th>Source</th>
                                    <th>Destination</th>
                                    <th>Distance (KM)</th>
                                    <th>Regular Price</th>
                                    <th>Amount Offered</th>
                                    <th>Status</th>
                                    {{--<th>Payment Status</th>--}}
                                    <th>Payment Method</th>
                                    <th>Coupon</th>
                                    <th>Started On (UTC)</th>
                                    <th>Finished On (UTC)</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="paginationDiv">
                                {!! ${$resource_pl}->appends($_GET)->render() !!}
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('customScript')
    <!-- daterangepicker -->
    <script src="{!! asset_url('admin_assets/bower_components/moment/min/moment.min.js') !!}"></script>
    <script src="{!! asset_url('admin_assets/bower_components/bootstrap-daterangepicker/daterangepicker.js') !!}"></script>
    <script type="text/javascript">
        $(function () {
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('l') + ' - ' + end.format('l'));
                    $('#daterange-btn input[name="start"]').val(start.format('l'));
                    $('#daterange-btn input[name="end"]').val(end.format('l'));
                }
            )
        });
    </script>
@endsection
