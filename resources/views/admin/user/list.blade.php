@php
    $page_title = ucfirst($resource_pl);
@endphp

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
<style type="text/css">
    .left{
        float: left;
    }
</style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title }}
                {{--<small>List</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">List of All {{ $page_title }}</h3>

                            <div class="box-tools">
                                <form method="get" id="searchForm" action="{!! url()->current() !!}">
                                    
                                    <div class="row">
                                        <!-- @if($page_title == 'Drivers')
                                        <div class="col-sm-3"><input name="curr_latitude" class="form-control pull-right" type="text"
                                            placeholder="latitude" value="{{(isset($currlatitude))? $currlatitude:''}}" /></div>
                                            
                                        <div class="col-sm-3"><input name="curr_longitude" class="form-control pull-right" type="text"
                                            placeholder="longitude" value="{{(isset($currlongitude))? $currlongitude:''}}" /></div>

                                        <div class="col-sm-3">
                                            <select name="distance" class="form-control pull-right" placeholder="distance">
                                            @for($i=1;$i<=10;$i++)
                                                <option {{ (isset($distance) && $distance == $i)?"selected":""}} value="{{$i}}">{{$i}} km</option>
                                            @endfor
                                            </select>
                                        </div>
                                        @endif -->
                                        <div class="col-sm-3 pull-right">
                                            <div class="input-group input-group-sm">
                                                <input name="keyword" class="form-control pull-right" type="text"
                                                placeholder="Search" value="{{ (isset($keyword))?$keyword:"" }}" />
                                                <input name="statussort" class="form-control pull-right" type="hidden"
                                                value="{{ (isset($statussort)) ? $statussort : "" }}" />
                                                <input name="carsort" class="form-control pull-right" type="hidden"
                                                value="{{ (isset($carsort)) ? $carsort : "" }}" />
                                                <input name="losort" class="form-control pull-right" type="hidden"
                                                value="{{ (isset($losort)) ? $losort : "" }}" />
                                                <div class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Sl#</th>
                                        @if($page_title == 'Drivers')
                                            <th>SSN</th>
                                            <th>
                                                <div class="dropdown" style="white-space: nowrap;">
                                                    <a style="color: #000;" href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Last Online</a>
                                                    <span class="glyphicon {!! ($losort=='desc')?'glyphicon-collapse-up':'glyphicon-collapse-down' !!} dropdown-toggle" data-toggle="dropdown"></span>
                                                    <ul class="dropdown-menu">
    													<li><a href="javascript:void(0);" onclick="sortBy('losort', 'asc')">Ascending</a></li>
    													<li><a href="javascript:void(0);" onclick="sortBy('losort', 'desc')">Descending</a></li>
    												</ul>
                                                </div>
                                            </th>
                                        @endif
                                        @if($page_title == 'Passengers')
                                            <th>Current Location</th>
                                        @endif
                                        <th>Name</th>
                                        <th>E-Mail</th>
                                        <th>Mobile</th>
                                        <th>Balance</th>
                                        <th>Reword point</th>
                                        <th>Photo</th>
                                        @if($page_title == 'Drivers')
                                            <th>
                                            	<div class="dropdown" style="white-space: nowrap;">
                                                    <a style="color: #000;" href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Cars</a>
                                                    <span class="glyphicon {!! ($carsort=='desc')?'glyphicon-collapse-up':'glyphicon-collapse-down' !!} dropdown-toggle" data-toggle="dropdown"></span>
                                                    <ul class="dropdown-menu">
    													<li><a href="javascript:void(0);" onclick="sortBy('carsort', 'asc')">No Car</a></li>
    													<li><a href="javascript:void(0);" onclick="sortBy('carsort', 'desc')">Have Car</a></li>
    												</ul>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="dropdown" style="white-space: nowrap;">
                                                    <a style="color: #000;" href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Status</a>
                                                    <span class="glyphicon {!! ($statussort=='desc')?'glyphicon-collapse-up':'glyphicon-collapse-down' !!} dropdown-toggle" data-toggle="dropdown"></span>
                                                    <ul class="dropdown-menu">
    													<li><a href="javascript:void(0);" onclick="sortBy('statussort', 'asc')">Ascending</a></li>
    													<li><a href="javascript:void(0);" onclick="sortBy('statussort', 'desc')">Descending</a></li>
    												</ul>
                                                </div>
                                            </th>
                                        @endif
                                        <th style="min-width: 100px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $sl = ($$resource_pl->currentPage() * $$resource_pl->perPage()) - $$resource_pl->perPage();
                                    @endphp
                                    @if(isset(${$resource_pl}) && count(${$resource_pl})>0)
                                        @foreach(${$resource_pl} as $row)
                                            @php
                                                $sl++;
                                                $userCars = collect($row->UserCars)->toArray();
                                            @endphp
                                            <tr>
                                                <th>{{ $sl }}</th>
                                                @if($page_title == 'Drivers')
                                                    <td>@php echo (!$row->ssn)?" ":$row->ssn; @endphp</td>
                                                    <td title="{{ $row->is_online == 'Y'?'Online':'Offline' }}" class="btn btn-{{ $row->is_online == 'Y'?'success':'secondary' }}">{{adminDataListDateTime($row->updated_at)}}</td>
                                                @endif
                                                @if($page_title == 'Passengers')
                                                    <td>
                                                        {{$row->curr_lat? $row->curr_lat.','.$row->curr_lng.', '.$row->location :''}}
                                                        </td>
                                                @endif
                                                <td>{!! $row->firstName !!} {!! $row->lastName !!}</td>
                                                <td>{!! $row->email  !!}</td>
                                                <td>{!! $row->mobile  !!}</td>
                                                <td>{!! $row->balance  !!}</td>
                                                <td>{!! $row->reword_point  !!}</td>
                                                <td>
                                                    @if(!empty($row->profilePicture))
                                                        <img src="{{ asset_url('uploads/profilePictures/'.$row->profilePicture) }}"
                                                             style="height:70px; width:70px;">
                                                    @endif
                                                </td>
                                                @if($page_title == 'Drivers')
                                                    <td data-toggle="modal" data-target="#carDetails" data-content="{{ json_encode($userCars) }}" style="cursor: pointer;" data-id="{{ $row->id }}">{!! count($userCars)  !!}</td>
                                                    <td>{!! ($row->driverApproval=='Y' && $row->completion=='Y')?'<small class="label bg-green">'.$row->driver_rides_count.'</small>':'<small class="label bg-red">'.$row->driver_rides_count.'</small>'  !!}</td>
                                                @endif
                                                <td>
                                                    <a href="{!! admin_url(''.strtolower($resource).'/rides/'.$row->id) !!}"
                                                       class="btn btn-sm btn-info td-btn">
                                                        <i class="fa fa-map" aria-hidden="true"></i>
                                                        Rides
                                                    </a>
                                                    <a href="{!! admin_url(''.strtolower($resource).'/transaction?id='.$row->id) !!}"
                                                       class="btn btn-sm btn-info td-btn">
                                                        <i class="fa fa-exchange" aria-hidden="true"></i>
                                                        Transactions
                                                    </a>
                                                    @if($row->userType == 'D')
                                                        <a href="{!! admin_url(''.strtolower($resource).'/'.$row->id.'/document') !!}"
                                                           class="btn btn-sm btn-info td-btn">
                                                            <i class="fa fa-file" aria-hidden="true"></i>
                                                            Documents
                                                        </a>
                                                        <a href="{!! admin_url(''.strtolower($resource).'/'.$row->id.'/bank-details') !!}"
                                                            class="btn btn-sm btn-info td-btn">
                                                                <i class="fa fa-university" aria-hidden="true"></i>
                                                                Bank Details
                                                        </a>
                                                    @endif
                                                    <a href="{!! admin_url(''.strtolower($resource).'/'.$row->id.'/edit') !!}"
                                                       class="btn btn-sm btn-warning td-btn">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                    <form method="POST"
                                                          action="{!! admin_url(''.strtolower($resource).'/'.$row->id.'/change-status') !!}"
                                                          onsubmit="return confirm('Are you sure to {{ $row->status=='Y'?'disable':'enable' }} {!! $row->email !!}?');">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="current_status"
                                                               value="{{ $row->status }}">
                                                        <button class="btn btn-sm btn-{{ $row->status == 'N'?'success':'danger' }} td-btn"
                                                                type="submit">
                                                            <i class="fa"
                                                               aria-hidden="true"></i> {!!$row->status=='N'?'Unblock':'Block'!!}
                                                        </button>
                                                    </form>
                                                    @if ($row->status=='D')
                                                        <form method="POST"
                                                              action="{!! admin_url('' .strtolower($resource). '/' . @$row->id) !!}"
                                                              onsubmit="return confirm('Are you sure to remove {!! $row->email !!}?');">
                                                            <input name="_method" type="hidden" value="DELETE">
                                                            {{ csrf_field() }}
                                                            <button class="btn btn-sm btn-danger td-btn" type="submit">
                                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                                                Delete
                                                            </button>
                                                        </form>
                                                    @endif
                                                    @if ($row->completion=='P')
                                                        <form method="POST"
                                                              action="{!! admin_url(''.strtolower($resource).'/'.$row->id.'/change-ssn-status') !!}"
                                                              onsubmit="return confirm('Are you sure to approve?');">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="current_status" value="Y">
                                                            <button class="btn btn-sm btn-success td-btn" type="submit">
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                                Approve SSN
                                                            </button>
                                                        </form>
                                                        <form method="POST"
                                                              action="{!! admin_url(''.strtolower($resource).'/'.$row->id.'/change-ssn-status') !!}"
                                                              onsubmit="return confirm('Are you sure to reject?');">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="current_status" value="N">
                                                            <button class="btn btn-sm btn-danger td-btn" type="submit">
                                                                <i class="fa fa-close" aria-hidden="true"></i>
                                                                Reject SSN
                                                            </button>
                                                        </form>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6" style="text-align: center;">No Data Found..</td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Sl#</th>
                                        @if($page_title == 'Drivers')
                                            <th>SSN</th>
                                            <th>
                                                <div class="dropdown" style="white-space: nowrap;">
                                                    <a style="color: #000;" href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Last Online</a>
                                                    <span class="glyphicon {!! ($losort=='desc')?'glyphicon-collapse-up':'glyphicon-collapse-down' !!} dropdown-toggle" data-toggle="dropdown"></span>
                                                    <ul class="dropdown-menu">
    													<li><a href="javascript:void(0);" onclick="sortBy('losort', 'asc')">Ascending</a></li>
    													<li><a href="javascript:void(0);" onclick="sortBy('losort', 'desc')">Descending</a></li>
    												</ul>
                                                </div>
                                            </th>
                                        @endif
                                        @if($page_title == 'Passengers')
                                            <th>Current Location</th>
                                        @endif
                                        <th>Name</th>
                                        <th>E-Mail</th>
                                        <th>Mobile</th>
                                        <th>Balance</th>
                                        <th>Reword point</th>
                                        <th>Photo</th>
                                        @if($page_title == 'Drivers')
                                            <th>
                                            	<div class="dropdown" style="white-space: nowrap;">
                                                    <a style="color: #000;" href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Cars</a>
                                                    <span class="glyphicon {!! ($carsort=='desc')?'glyphicon-collapse-up':'glyphicon-collapse-down' !!} dropdown-toggle" data-toggle="dropdown"></span>
                                                    <ul class="dropdown-menu">
    													<li><a href="javascript:void(0);" onclick="sortBy('carsort', 'asc')">No Car</a></li>
    													<li><a href="javascript:void(0);" onclick="sortBy('carsort', 'desc')">Have Car</a></li>
    												</ul>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="dropdown" style="white-space: nowrap;">
                                                    <a style="color: #000;" href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Status</a>
                                                    <span class="glyphicon {!! ($statussort=='desc')?'glyphicon-collapse-up':'glyphicon-collapse-down' !!} dropdown-toggle" data-toggle="dropdown"></span>
                                                    <ul class="dropdown-menu">
    													<li><a href="javascript:void(0);" onclick="sortBy('statussort', 'asc')">Ascending</a></li>
    													<li><a href="javascript:void(0);" onclick="sortBy('statussort', 'desc')">Descending</a></li>
    												</ul>
                                                </div>
                                            </th>
                                        @endif
                                        <th style="min-width: 100px;">Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="paginationDiv">
                                {!! ${$resource_pl}->appends($_GET)->render() !!}
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
    <div class="modal fade" id="carDetails" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">
                        <div class="left">Car Details</div><div class="actionBtnArea"></div>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>Modal body…</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="addEditCarModal" style="display: none;overflow-x: hidden;overflow-y: auto;">
        <div class="modal-dialog" style="">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Car Details</h4>
                </div>
                <div class="modal-body">
                    <p>Modal body…</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('customScript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#carDetails').on('show.bs.modal', function (event) {
            let carDetails = JSON.parse(event.relatedTarget.dataset.content);
            let userId = JSON.parse(event.relatedTarget.dataset.id);
            //console.log(userId);
            
            let actionBtn = '<a href="javascript:void(0);" style="margin-left: 5px;" class="btn btn-sm btn-warning acBtn" onclick="actionOnCar('+userId+')">';
            if(carDetails.length == 0)
            {
                actionBtn += '<i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add';
            }
            else
            {
                actionBtn += '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit';
            }
            actionBtn += '</a>';
            //actionBtn = '';
            //console.log(actionBtn);
            $(".actionBtnArea").html(actionBtn);

            let taxiType = [];
            let html = '<div class="box-body table-responsive">' +
                '                             <table class="table table-bordered table-hover">\n' +
                '                                <thead>\n' +
                '                                <tr>\n' +
                '                                    <th>Number Plate</th>\n' +
                '                                    <th>Make</th>\n' +
                '                                    <th>Model</th>\n' +
                '                                    <th>Colour</th>\n' +
                '                                    <th>Year</th>\n' +
                '                                    <th>Taxi Types</th>\n' +
                '                                </tr>\n' +
                '                                </thead>\n' +
                '                                <tbody>';
            carDetails.forEach(function(elem) {
                html += '                                <tr>\n' +
                    '                                    <td>' + elem.numberPlate + '</td>\n' +
                    '                                    <td>' + elem.make + '</td>\n' +
                    '                                    <td>' + elem.model + '</td>\n' +
                    '                                    <td>' + elem.colour + '</td>\n' +
                    '                                    <td>' + elem.year + '</td>\n';
                elem.taxi_types.forEach(function (elemnt) {
                    taxiType.push(elemnt.name);
                });
                html += '                                    <td>' + taxiType.join(', ') + '</td>\n';
                html += '                                </tr>\n';
            });
            html += '                                </tbody>'+
                '                                <tfoot>\n' +
                '                                <tr>\n' +
                '                                    <th>Number Plate</th>\n' +
                '                                    <th>Make</th>\n' +
                '                                    <th>Model</th>\n' +
                '                                    <th>Colour</th>\n' +
                '                                    <th>Year</th>\n' +
                '                                    <th>Taxi Types</th>\n' +
                '                                </tr>\n' +
                '                                </tfoot>\n'+
                '                            </table>\n' +
                '                        </div>';
            $(this).find('.modal-body').html(html);
        });

        $('#carDetails').on('hidden.bs.modal', function (event) {
            $(this).find('.modal-body').html('<p>Modal body…</p>');
        });
    });

    function sortBy(fieldname, sortType){
    	$('input[name='+fieldname+']').val(sortType);
    	$('#searchForm').submit();
    }

    function actionOnCar(userId){
        // console.log(userId);
        // let carDet = JSON.parse($(".carDet"+userId).attr('data-content'));
        // console.log(carDet);
        $.ajax({
            url:"{{ admin_url('get-taxi-details') }}",
            type:"post",
            data:{
                user_id:userId
            }, success: function(response){
                if(response.has_error == 0){
                    $("#carDetails").modal('hide');
                    $('.modal-body').html(response.form);
                    $("#addEditCarModal").modal('show');
                }
            },
            error: function(textStatus, errorThrown){

            }
        });
    }
</script>
@endsection
