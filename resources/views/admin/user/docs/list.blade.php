@php
    $page_title = ucfirst($resource_pl);
@endphp

@extends('admin.layouts.app')

@section('pageTitle', $page_title)
@section('customStyles')
    <style>
        .not-active {
            pointer-events: none;
            cursor: default;
        }
    </style>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title }}
                {{--<small>List</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    @php $i=1 @endphp
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">List of All {{ $page_title }}</h3>

                            @if(isset($keybox) && $keybox == true)
                                <div class="box-tools">
                                    <form method="get" action="{!! url()->current() !!}">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input name="keyword" class="form-control pull-right"
                                                   type="text" placeholder="Search"
                                                   value="{{ (isset($keyword))?$keyword:"" }}">

                                            <div class="input-group-btn">
                                                <button class="btn btn-default" type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Driver</th>
                                    <th>Document Type</th>
                                    <th>Document</th>
                                    <th>Verify status</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset(${$resource_pl}) && count(${$resource_pl})>0)
                                    @foreach(${$resource_pl} as $row)
                                        <tr>
                                            <td>{!! $row->user['firstName'] !!} {!! $row->user['lastName'] !!}</td>
                                            <td>
                                                {!! $row->docs->name !!}
                                            </td>
                                            <td>
                                                @if(!empty($row->userDocument))
                                                    @if (isset($row->typeImage) && $row->typeImage == "Y")
                                                        <a href="#" onclick="showModal({{$i}})"><img
                                                        id="imageresource{{$i}}"
                                                        src="{{ asset_url('uploads/driverVerification'.'/'.$row->userDocument) }}"
                                                        style="height:70px; width:70px;"></a>
                                                    @else
                                                        <a target="_blank" href="{{ asset_url('uploads/driverVerification'.'/'.$row->userDocument) }}">
                                                        <i class="fa fa-file" aria-hidden="true" style="font-size: 83px;"></i>
                                                        </a>
                                                    @endif
                                                @endif
                                            </td>
                                            <td>
                                                @if($row->verifyStatus=="P"){!! 'Pending' !!} @elseif($row->verifyStatus=="N"){!! 'Rejected' !!} @else{!! 'Accepted' !!} @endif
                                            </td>
                                            <td>
                                                @if($row->verifyStatus=="P")
                                                    <form method="POST"
                                                          action="{!! admin_url('driver/'.strtolower($resource).'/'.$row->id.'/update') !!}"
                                                          onsubmit="return confirm('Are you sure to approve?');">
                                                        {{ csrf_field() }}
                                                        <input name="verifyStatus" type="hidden" value="Y">
                                                        <button class="btn btn-sm btn-success td-btn" type="submit">
                                                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                                            Approve
                                                        </button>
                                                    </form>
                                                    <form method="POST"
                                                          action="{!! admin_url('driver/'.strtolower($resource).'/'.$row->id.'/update') !!}"
                                                          onsubmit="return confirm('Are you sure to reject?');">
                                                        {{ csrf_field() }}
                                                        <input name="verifyStatus" type="hidden" value="N">
                                                        <button class="btn btn-sm btn-danger td-btn" type="submit">
                                                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                                                            Reject
                                                        </button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">
															<span aria-hidden="true">&times;</span>
															<span class="sr-only">Close</span>
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel">Image preview</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="" id="imagepreview" style="width: 480px; height: 720px;">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @php $i++ @endphp
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" style="text-align: center;">No Data Found..</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Driver</th>
                                    <th>Document Type</th>
                                    <th>Document</th>
                                    <th>Verify status</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="paginationDiv">
                                {!! ${$resource_pl}->appends($_GET)->render() !!}
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        @if(isset($keybox) && $keybox == false)
            <section class="content" style="min-height: auto !important;">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-warning">
                            <div class="box-header">
                                <h3 class="box-title">Add New {{ $page_title }}</h3>
                            </div>
                            <div class="box-body">
                                <form role="form" method="post" action="{{ admin_url('driver/document/' . $id . '/upsert') }}" enctype="multipart/form-data">
                                    <div class="box-body">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="docType">Document Type</label>
                                            <select id="docType" class="form-control" name="docType">
                                                @foreach($verificationDocs as $docs)
                                                    <option value="{{ $docs->id }}">{{ $docs->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="verifyDoc">Document</label>
                                            <input id="verifyDoc" type="file" name="verifyDoc">
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        @endif
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('customScript')
    <script type="text/javascript">
        function showModal(i) {
            $('#imagepreview').attr('src', $('#imageresource' + i).attr('src')); // here asign the image to the modal when the user click the enlarge link
            $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
        }
    </script>
@endsection
