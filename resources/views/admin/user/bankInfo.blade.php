@php
    $page_title = ucfirst($resource_pl);
@endphp

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            {{ $userDetails['firstName'] }} {{ $userDetails['lastName'] }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">{{ $page_title }}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">List of Banks</h3>
                        <div class="box-tools">
                            <form method="get" id="searchForm">
                                <div class="row">
                                    <div class="col-sm-3 pull-right">
                                        <div class="input-group input-group-sm">
                                            <input name="keyword" class="form-control pull-right" type="text" placeholder="Search" value="{{ (isset($keyword))? $keyword : "" }}" />
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="box-body table-responsive">
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!! session('success') !!}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Sl#</th>
                                    <th>Bank Name</th>
                                    <th>Username as Bank Ac.</th>
                                    <th>Address 1</th>
                                    <th>Address 2</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                    <th>PinZip</th>
                                    <th>Account Number</th>
                                    <th>IFSC Code</th>
                                    <th>Default</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $sl = ($userBankAccounts->currentPage() * $userBankAccounts->perPage()) - $userBankAccounts->perPage();
                                @endphp
                                @if(!empty($userBankAccounts) && $userBankAccounts->count() > 0)
                                    @foreach ($userBankAccounts as $list) @php $list = $list->toArray(); $sl++; @endphp
                                    <tr>
                                        <th> {{ $sl }} </th>
                                        <td> {{ $list['bank_name'] ? $list['bank_name'] : "---" }} </td>
                                        <td> {{ $list['user_name_same_as_bank_ac'] ? $list['user_name_same_as_bank_ac'] : "---"  }} </td>
                                        <td> {{ $list['address1'] ? $list['address1'] : "---" }} </td>
                                        <td> {{ $list['address2'] ? $list['address2'] : "---" }} </td>
                                        <td> {{ $list['city'] ? $list['city'] : "---" }} </td>
                                        <td> {{ $list['state_details'] ? $list['state_details']['name'] : "---" }} </td>
                                        <td> {{ $list['country_details'] ? $list['country_details']['name'] : "---" }} </td>
                                        <td> {{ $list['pin_zip'] ? $list['pin_zip'] : "---" }} </td>
                                        <td> {{ $list['account_number'] ? $list['account_number'] : "---" }} </td>
                                        <td> {{ $list['ifsc_code'] ? $list['ifsc_code'] : "---" }} </td>
                                        <td style="color:green;">
                                            @if ($list['is_default'] == '1')
                                                <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="12" style="text-align: center;">No Data Found..</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <div class="paginationDiv">
                            {!! $userBankAccounts->appends($_GET)->render() !!}
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('customScript')

@endsection
