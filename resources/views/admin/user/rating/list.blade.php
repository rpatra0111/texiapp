@php
    $page_title = ucfirst($resource_pl);
@endphp

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('customStyles')
    <!-- Bootstrap Rating -->
    <link rel="stylesheet" href="{!! asset_url('admin_assets/plugins/bootstrap-rating/bootstrap-rating.css') !!}">
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title }}
                {{--<small>List</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">List of All {{ $page_title }}</h3>

                            <div class="box-tools">
                                <form method="get" action="{!! url()->current() !!}">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input name="keyword" class="form-control pull-right" type="text"
                                               placeholder="Search" value="{{ (isset($keyword))?$keyword:"" }}">

                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th>Total Rating</th>
                                    <th>Average Driver Rating</th>
                                    <th>Average Car Rating</th>
                                    <th>Average Driving Rating</th>
                                    <th>Average Rating</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset(${$resource_pl}) && count(${$resource_pl})>0)
                                    @foreach(${$resource_pl} as $row)
                                        <tr>
                                            <td>
                                                @if(!empty($row->Ratee->profilePicture))
                                                    <img src="{{ asset_url('uploads/profilePictures/'.$row->Ratee->profilePicture) }}"
                                                         style="height:70px; width:70px;">
                                                @endif
                                            </td>
                                            <td>{!! $row->Ratee->firstName !!} {!! $row->Ratee->lastName !!}</td>
                                            <td>{!! $row->totalRating  !!}</td>
                                            <td>
                                                <input type="hidden" class="rating" data-readonly value="{!! round($row->avg_driverRating, 1)  !!}"/>
                                            </td>
                                            <td>
                                                <input type="hidden" class="rating" data-readonly value="{!! round($row->avg_carRating, 1)  !!}"/>
                                            </td>
                                            <td>
                                                <input type="hidden" class="rating" data-readonly value="{!! round($row->avg_drivingRating, 1)  !!}"/>
                                            </td>
                                            <td>
                                                <input type="hidden" class="rating" data-readonly value="{!! round($row->avg_rating, 1)  !!}"/>
                                            </td>
                                            <td>
                                                <form method="POST"
                                                      action="{!! admin_url(''.strtolower($resource).'/'.$row->id.'/change-status') !!}"
                                                      onsubmit="return confirm('Are you sure to {{ $row->Ratee->status=='Y'?'disable':'enable' }} {!! $row->Ratee->firstName !!}?');">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="current_status"
                                                           value="{{ $row->status }}">
                                                    <button class="btn btn-sm btn-{{ $row->Ratee->status == 'N'?'success':'danger' }} td-btn"
                                                            type="submit">
                                                        <i class="fa"
                                                           aria-hidden="true"></i> {!!$row->Ratee->status=='N'?'Unblock':'Block'!!}
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" style="text-align: center;">No Data Found..</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th>Total Rating</th>
                                    <th>Average Driver Rating</th>
                                    <th>Average Car Rating</th>
                                    <th>Average Driving Rating</th>
                                    <th>Average Rating</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="paginationDiv">
                                {!! ${$resource_pl}->appends($_GET)->render() !!}
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('customScript')
    <!-- Bootstrap Rating -->
    <script src="{!! asset_url('admin_assets/plugins/bootstrap-rating/bootstrap-rating.js') !!}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.rating-tooltip').rating({
                extendSymbol: function (rate) {
                    $(this).tooltip({
                        container: 'body',
                        placement: 'bottom',
                        title: 'Rate ' + rate
                    });
                }
            });
            $('.rating').each(function () {
                $('<span class="label label-default"></span>')
                    .text($(this).val() || ' ')
                    .insertAfter(this);
            });
        });
    </script>
@endsection
