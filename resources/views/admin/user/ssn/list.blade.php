@php
    $page_title = ucfirst($resource_pl);
@endphp

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title }}
                {{--<small>List</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">List of All {{ $page_title }}</h3>

                            <div class="box-tools">
                                <form method="get" action="{!! url()->current() !!}">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input name="keyword" class="form-control pull-right" type="text"
                                               placeholder="Search" value="{{ (isset($keyword))?$keyword:"" }}">

                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>E-Mail</th>
                                    <th>Mobile</th>
                                    <th>SSN</th>
                                    <th>Photo</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset(${$resource_pl}) && count(${$resource_pl})>0)
                                    @foreach(${$resource_pl} as $row)
                                        <tr>
                                            <td>{!! $row->firstName !!} {!! $row->lastName !!}</td>
                                            <td>{!! $row->email  !!}</td>
                                            <td>{!! $row->mobile  !!}</td>
                                            <td>@php echo (!$row->ssn)?" ":$row->ssn; @endphp</td>
                                            <td>
                                                @if(!empty($row->profilePicture))
                                                    <img src="{{ asset_url('uploads/profilePictures/'.$row->profilePicture) }}"
                                                         style="height:70px; width:70px;">
                                                @endif
                                            </td>
                                            <td>
                                                @if ($row->completion=='P')
                                                    <form method="POST"
                                                          action="{!! admin_url(''.strtolower($resource).'/'.$row->id.'/change-ssn-status') !!}"
                                                          onsubmit="return confirm('Are you sure to approve?');">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="current_status" value="Y">
                                                        <button class="btn btn-sm btn-success td-btn" type="submit">
                                                            <i class="fa fa-check" aria-hidden="true"></i>
                                                        </button>
                                                    </form>
                                                    <form method="POST"
                                                          action="{!! admin_url(''.strtolower($resource).'/'.$row->id.'/change-ssn-status') !!}"
                                                          onsubmit="return confirm('Are you sure to reject?');">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="current_status" value="N">
                                                        <button class="btn btn-sm btn-danger td-btn" type="submit">
                                                            <i class="fa fa-close" aria-hidden="true"></i>
                                                        </button>
                                                    </form>
                                                @endif
                                                <form method="POST"
                                                      action="{!! admin_url(''.strtolower($resource).'/'.$row->id.'/change-status') !!}"
                                                      onsubmit="return confirm('Are you sure to {{ $row->status=='Y'?'disable':'enable' }} {!! $row->email !!}?');">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="current_status"
                                                           value="{{ $row->status }}">
                                                    <button class="btn btn-sm btn-{{ $row->status == 'N'?'success':'danger' }} td-btn"
                                                            type="submit">
                                                        <i class="fa"
                                                           aria-hidden="true"></i> {!!$row->status=='N'?'Unblock':'Block'!!}
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" style="text-align: center;">No Data Found..</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>E-Mail</th>
                                    <th>Mobile</th>
                                    <th>SSN</th>
                                    <th>Photo</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="paginationDiv">
                                {!! ${$resource_pl}->appends($_GET)->render() !!}
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('customScript')
    <script type="text/javascript">

    </script>
@endsection
