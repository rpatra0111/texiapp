@php
$user = $user ? $user->toArray() : [];
$carDetails = !empty($user['user_cars'][0]) ? $user['user_cars'][0] : [];
$taxyTypeOfCar = isset($carDetails['taxi_types']) ? $carDetails['taxi_types'] : [];
$taxyTypeids = [];
foreach($taxyTypeOfCar as $eachTypeOfCar)
{
    $taxyTypeids[] = $eachTypeOfCar['id'];
}
//dd($taxyTypeOfCar);
@endphp
<div class="msg_area" style="display: none;"></div>
<form id="addEditCar" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="numberPlate">Number Plate</label>
        <input type="text" required class="form-control" id="numberPlate" name="numberPlate" value="{!! isset($carDetails['numberPlate']) ? $carDetails['numberPlate'] : '' !!}">
    </div>
    <div class="form-group">
        <label for="make">Make</label>
        <input type="text" required class="form-control" id="make" name="make" value="{!! isset($carDetails['make']) ? $carDetails['make'] : '' !!}">
    </div>
    <div class="form-group">
        <label for="model">Model</label>
        <input type="text" required class="form-control" id="model" name="model" value="{!! isset($carDetails['model']) ? $carDetails['model'] : '' !!}">
    </div>
    <div class="form-group">
        <label for="colour">Colour</label>
        <select required class="form-control" id="colour" name="colour">
            <option> -Select- </option>
            @if($taxiColors)
                @foreach($taxiColors as $eachColors)
                    <option value="{{ $eachColors->id }}" {!! isset($carDetails['colour']) && $carDetails['colour']==$eachColors->id ? 'selected' : '' !!}> {{ $eachColors->colour }} </option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="form-group">
        <label for="year">Year</label>
        <input type="text" required class="form-control" id="year" name="year" value="{!! isset($carDetails['year']) ? $carDetails['year'] : '' !!}">
    </div>
    <div class="form-group">
         <label for="">Taxi Types</label>
       <div class="row">
            @if($taxiTypes)
            @foreach($taxiTypes as $eachTaxiTypes)
                <div class="col-sm-4">
                    <div class="checkbox">
                        <label for="invalidCheck{{ $eachTaxiTypes->id }}">
                            <input type="checkbox" name="taxiTypes[]" value="{{ $eachTaxiTypes->id }}" id="invalidCheck{{ $eachTaxiTypes->id }}" {!! in_array($eachTaxiTypes->id, $taxyTypeids) ? 'checked' : '' !!}>
                            {{ $eachTaxiTypes->name }}
                        </label>
                    </div>
                </div>
            @endforeach
        @endif
       </div>
        
    </div>
    
    <div class="form-group">
        <button type="button" class="btn btn-info saveTaxy" name="">Save</button>
        <input type="hidden" name="carId" value="{!! isset($carDetails['id']) ? $carDetails['id'] : '' !!}">
        <input type="hidden" name="userId" value="{!! isset($user['id']) ? $user['id'] : '' !!}">
    </div>
</form>

<script type="text/javascript">
$(document).ready(function(){
    var max = 2;
    var checkboxes = $('input[name="taxiTypes"]');
                       
    checkboxes.change(function(){
        var current = checkboxes.filter(':checked').length;
        checkboxes.filter(':not(:checked)').prop('disabled', current >= max);
    });

    $(".saveTaxy").on('click', function(event) {

        $(".error_area").css('display', 'none');
        $('.saveTaxy').attr("disabled", "disabled");

        let formData = $("#addEditCar").serialize();
        let taxiTypeIdsCheckedCount = $("input[name=taxiTypes]").val();
        console.log(taxiTypeIdsCheckedCount);
        
        $.ajax({
            type:"post",
            url:"{{ admin_url('user/add-edit-car') }}",
            data:formData,
            success: function(response){
                if(response.has_error == 0) {
                    $('.msg_area').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Successfully saved!</div>');
                    $(".msg_area").css("display", "block");
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                } else {
                    $('.saveTaxy').removeAttr("disabled");
                    var error_msg = "";
                    $.each(response.errors, function(index, value){
                        error_msg += error_msg ? "<br>"+value[0] : value[0];
                    });
                    $('.msg_area').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+error_msg+'</div>');
                    $(".msg_area").css("display", "block");
                }
            },
            error: function(jqXHR, exception){
                $('.saveTaxy').removeAttr("disabled");
            }
        });
    });
});
</script>