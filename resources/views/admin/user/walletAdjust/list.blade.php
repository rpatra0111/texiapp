<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */

$page_title = "Negative Wallet Balance";

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)
@section('customStyles')
    <style>
        .not-active {
            pointer-events: none;
            cursor: default;
        }
    </style>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title }}
                {{--<small>List</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!! admin_url('dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">{{ $page_title }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    @php $i=1 @endphp
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">List of All {{ $page_title }}</h3>

                            <div class="box-tools">
                                <form method="get" action="{!! url()->current() !!}">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input name="keyword" class="form-control pull-right" type="text" placeholder="Search" value="{{ (isset($keyword))?$keyword:"" }}">

                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            @if(session('success'))
                                <div class="alert alert-success">
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Driver</th>
                                    <th>Total Amount by Cash</th>
                                    <th>Total Amount by Card</th>
                                    {{-- <th>Adjustment</th> --}}
                                    <th>Amount to Pay</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset(${$resource_pl}) && count(${$resource_pl})>0)
                                    @foreach(${$resource_pl} as $row)
                                        <tr>
                                            <td>{!! $row->firstName !!} {!! $row->lastName !!}</td>
                                            <td>{!! $row->totalAmountByCash ? $row->totalAmountByCash : "0.00" !!}</td>
                                            <td>{!! $row->totalAmountByCard ? $row->totalAmountByCard : "0.00" !!}</td>
                                            {{-- <td>{!! $row->adjustmanet ? $row->adjustmanet : "0.00" !!}</td> --}}
                                            <td>{!! $row->balance !!}</td>
                                            <td>
                                                <a href="{!! admin_url('driver/'.strtolower($resource).'/'.$row->id.'/reset') !!}"
                                                   class="btn btn-sm btn-success td-btn">
                                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                                    Reset
                                                </a>
                                                <a target="_blank" href="{!! admin_url(''.strtolower($resource).'/transaction?id='.$row->id) !!}"
                                                    class="btn btn-sm btn-info td-btn">
                                                        <i class="fa fa-exchange" aria-hidden="true"></i>
                                                        Transactions
                                                </a>
                                                <a target="_blank" href="{!! admin_url(''.strtolower($resource).'/ride-transaction?id='.$row->id) !!}"
                                                    class="btn btn-sm btn-info td-btn">
                                                        <i class="fa fa-info" aria-hidden="true"></i>
                                                        view Details
                                                </a>
                                                <form method="POST"
                                                      action="{!! admin_url('driver/'.$row->id.'/change-status') !!}"
                                                      onsubmit="return confirm('Are you sure to {{ $row->status=='Y'?'disable':'enable' }} {!! $row->email !!}?');">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="current_status"
                                                           value="{{ $row->status }}">
                                                    <button class="btn btn-sm btn-{{ $row->status == 'N'?'success':'danger' }} td-btn"
                                                            type="submit">
                                                        <i class="fa"
                                                           aria-hidden="true"></i> {!!$row->status=='N'?'Unblock':'Block'!!}
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @php $i++ @endphp
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" style="text-align: center;">No Data Found..</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Driver</th>
                                    <th>Total Amount by Cash</th>
                                    <th>Total Amount by Card</th>
                                    {{-- <th>Adjustment</th> --}}
                                    <th>Amount to Pay</th>
                                    <th style="min-width: 100px;">Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="paginationDiv">
                                {!! ${$resource_pl}->appends($_GET)->render() !!}
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('customScript')
    <script type="text/javascript">
        function showModal(i) {
            $('#imagepreview').attr('src', $('#imageresource' + i).attr('src')); // here asign the image to the modal when the user click the enlarge link
            $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
        }
    </script>
@endsection
