@component('mail::message')
    # Hi {{ $user->firstName }}!

    Thanks for signing up!

    @component('mail::button', ['url' => $confirm_link])
        Confirm Link
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
