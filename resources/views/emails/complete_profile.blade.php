<table style="width: 520px;margin: 0 auto;box-shadow: 0 0 10px 2px #ccc;background: #fff;border-collapse: collapse;font-family: 'Roboto', sans-serif;">
    <tr>
        <td style="padding: 0;">
            <table style="border-collapse: collapse;font-family: 'Roboto', sans-serif;width: 100%;border-bottom: 1px solid #ccc;">
                <tr>
                    <td align="center">
                        <table width="75" align="center" cellpadding="15">
                            <tr>
                                <td>
                                    <img src="{{ asset_url('default_images/destydriverlogo.png') }}" alt="" width="70">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="30" style="padding: 0;"></td>
    </tr>
    <tr>
        <td style="padding: 0;">
            <table style="border-collapse: collapse;font-family: 'Roboto', sans-serif;">
                <tr>
                    <td style="padding: 0 15px;">
                        <p>Hello {{ $driverName }},</p>
                        <p>
                            We have seen you tried to register as Desty Driver but you stopped in the middle, Now you
                            are only one click behind to make money while driving your car.
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="20" style="padding: 0;"></td>
    </tr>
    <tr>
        <td align="center" style="padding: 0;">
            <!-- <a href="javascript:void(0);" style="display: inline-block;line-height: 46px;background: #f37401;padding: 0 30px;color: #fff;font-weight: 500;text-decoration: none;border-radius: 10px;">Click Here to complete register</a> -->
            <table width="300" align="center">
                <tr>
                    <td>
                        <a href="{{ (string)env('GOOGLE_PLAYSTORE_URL') }}" style="display: block;text-decoration: none;"><img
                                    src="{{ asset_url('site_assets/images/googleplay.png') }}" alt=""
                                    style="width: 120px;" width="120"></a>
                    </td>
                    <td width="20"></td>
                    <td>
                        <a href="{{ (string)env('APPLE_APPSTORE_URL') }}" style="display: block;text-decoration: none;"><img
                                    src="{{ asset_url('site_assets/images/appstore.png') }}" alt=""
                                    style="width: 120px;" width="120"></a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="30" style="padding: 0;"></td>
    </tr>
    <tr>
        <td>
            <table style="border-collapse: collapse;font-family: 'Roboto', sans-serif;background-color: #000000;width: 100%;">
                <tr>
                    <td height="10" style="padding: 0;"></td>
                </tr>
                <tr>
                    <td style="padding: 0;">
                        <table width="308" align="center">
                            <tr>
                                <td>
                                    <a href="javascript:void(0);"
                                       style="display: block;text-decoration: none;color: #fff;line-height: 30px;font-size: 14px;padding: 0 15px;">Terms
                                        &amp; Conditions</a>
                                </td>
                                <td>
                                    <a href="javascript:void(0);"
                                       style="display: block;text-decoration: none;color: #fff;line-height: 30px;font-size: 14px;padding: 0 15px;">Privacy
                                        Policy</a>
                                </td>
                                <td>
                                    <a href="{{ $unsubscribe }}"
                                       style="display: block;text-decoration: none;color: #fff;line-height: 30px;font-size: 14px;padding: 0 15px;">Unsubscribe</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="20"></td>
                </tr>
                <tr>
                    <td style="padding: 0;">
                        <table width="220" align="center">
                            <tr>
                                <td><a href="javascript:void(0);"><img
                                                src="{{ asset_url('default_images/facebook.png') }}" alt=""
                                                style="width: 24px;" width="24"></a></td>
                                <td><a href="javascript:void(0);"><img
                                                src="{{ asset_url('default_images/twitter.png') }}" alt=""
                                                style="width: 24px;" width="24"></a></td>
                                <td><a href="javascript:void(0);"><img
                                                src="{{ asset_url('default_images/instagram.png') }}" alt=""
                                                style="width: 24px;" width="24"></a></td>
                                <td><a href="javascript:void(0);"><img
                                                src="{{ asset_url('default_images/google-plus.png') }}" alt=""
                                                style="width: 24px;" width="24"></a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- <tr>
                    <td style="padding: 0;">
                        <ul style="padding: 0;margin: 20px 0 0 0;display: block;text-align: center;">
                            <li style="display: inline-block;list-style: none;padding: 0 10px;"></li>
                            <li style="display: inline-block;list-style: none;padding: 0 10px;"></li>
                        </ul>
                    </td>
                </tr> -->
                <tr>
                    <td height="10" style="padding: 0;"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>