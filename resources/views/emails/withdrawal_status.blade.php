@component('mail::message')
    # Hi {{ $body->firstName }}!

    {{ $body->description }}

    Thanks,
    {{ config('app.name') }}
@endcomponent