<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminPanelMenu extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id', 'name', 'link', 'fa_icon', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function SubMenu()
    {
        return $this->hasMany('App\AdminPanelMenu', 'parent_id', 'id');
    }

    public function UserMenu()
    {
        return $this->hasMany('App\UserMenuPermit', 'menuId', 'id');
    }
}
