<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverReview extends Model
{
    protected $fillable = ['driver_pic', 'driver_name', 'review'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [];
}
