<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryCity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['stateId', 'cityName', 'countyName', 'latitude', 'longitude'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function State()
    {
        return $this->belongsTo('App\CountryState', 'stateId', 'id');
    }


    public function RideRates()
    {
        return $this->hasMany('App\TaxiTypeRate', 'countryCityId', 'id');
    }
}
