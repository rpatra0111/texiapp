<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxiTypeRate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'taxiTypeId', 'countryCityId', 'fenceDistance',
        'baseCost', 'perKmCost', 'cancelCharge', 'timeCharge',
        'minCharge'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function City()
    {
        return $this->belongsTo('App\CountryCity', 'countryCityId', 'id');
    }

    public function TaxiType()
    {
        return $this->belongsTo('App\TaxiType', 'taxiTypeId', 'id');
    }
}
