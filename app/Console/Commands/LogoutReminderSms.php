<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

use \App\User;

class LogoutReminderSms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drivers:lm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Logout reminder to drivers via SMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        date_default_timezone_set('America/New_York');
        $currTime = time();
        $newDateTime = date('A', $currTime);
        $drivers = User::with('UserTokens')->where(['driverApproval'=>'Y','status'=>'Y','userType'=>'D','is_online'=>'N'])->where('updated_at', '<=', date('Y-m-d H:i:s', ($currTime - (3600*8))))->get();
        // 'mobile'=>'9804881521',
        foreach ($drivers as $k => $user) {
            if($user->UserTokens()->count()==0){                
                sendSms($user->countryCode . $user->mobile, 'Hi '.ucfirst($user->firstName).', Your app is logged out, please log in to your app to make sure you get ride request');
            }
        }

        // DB::table('users')->where('email','pabitra@technoexponent.com')
        // ->update([
        //     'zipcode' => rand()
        // ]);
    }
}
