<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use \App\User;

class IdleDriverPushNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drivers:idle-push';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push Notify Inactive Drivers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        date_default_timezone_set('America/New_York');
        $currTime = time();
        $newDateTime = date('A', $currTime);
        $drivers = User::with('UserTokens')->where(['driverApproval'=>'Y','status'=>'Y','userType'=>'D','is_online'=>'N'])->where('updated_at', '<=', date('Y-m-d H:i:s', ($currTime - 1800)))->get();
              
        // ->whereIn('email', ['sabyasachi@yopmail.com', 'pabitra@technoexponent.com'])
        $fcmTokensI = [];
        $fcmTokensA = [];
        foreach ($drivers as $k => $val) {
            if (isset($val->UserTokens) && $val->UserTokens!=null)
            {
              foreach ($val->UserTokens as $key => $values) {
                    if ($values->flag == 'A')
                    {   // $fcmTokensA[] = $values['fcmToken'];
                        sendNotification([$values->fcmToken], ['dataContent' => ['notiType' => 'IDLE', 'title' => config('constant.APP_NAME'), 'body' => 'Hello '.ucfirst($val->firstName).' '.($newDateTime=='AM'?'good morning':'good afternoon').' remember to open the DestyDriver app so can accept rides as they come in']]);
                    }
                    if ($values->flag == 'I'){
                        // $fcmTokensI[] = $values['fcmToken'];

                        sendNotification([$values->fcmToken], ['dataContent' => ['notiType' => 'IDLE']], ['title' => config('constant.APP_NAME'), 'sound' =>'default', 'body' => 'Hello '.ucfirst($val->firstName).' '.($newDateTime=='AM'?'good morning':'good afternoon').' remember to open the DestyDriver app so can accept rides as they come in']);
                    }
                }
            }
        }
        
        // DB::table('users')->where('email','pabitra@technoexponent.com')
        // ->update([
        //     'zipcode' =>  rand()
        // ]);
        
    }
}
