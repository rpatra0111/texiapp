<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class ResetInactiveDrivers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drivers:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resetting Inactive Drivers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('user_taxis')->where('updated_at', '<=', date('Y-m-d h:i:s', (time() - 3600)))
        ->where(function ($query) {
            $query->where('available', '=', 'Y')
                ->orWhere('forHire', '=', 'Y')
                ->orWhere('requested', '=', 'Y');
        })->update([
            'available' => 'N',
            'forHire' => 'N',
            'requested' => 'N'
        ]);
    }
}
