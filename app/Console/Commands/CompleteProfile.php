<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CompleteProfile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'driver:register';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Driver Registration Completion Reminder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userData = collect(DB::table('users')->select('id', 'firstName', 'email')
            ->where(['userType' => 'D', 'subscribed' => 'Y'])
            ->where(function ($query) {
                $query->where('driverApproval', 'N')
                    ->orWhere('completion', 'N');
            })->get())->toArray();
        foreach ($userData as $user) {
            Mail::send('emails.complete_profile', ['driverName' => $user->firstName, 'unsubscribe' => url('unsubscribe/' . encrypt($user->id))], function ($message) use ($user) {
                $message->to($user->email);
                $message->subject('Complete Your Registration');
            });
        }
    }
}
