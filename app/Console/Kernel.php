<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        '\App\Console\Commands\ResetInactiveDrivers',
        '\App\Console\Commands\CompleteProfile'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('drivers:lm')->timezone('America/New_York')->weeklyOn(3, '8:00');
        
        $schedule->command('drivers:idle-push')->timezone('America/New_York')->dailyAt('08:00');
        $schedule->command('drivers:idle-push')->timezone('America/New_York')->dailyAt('16:00');
        // $schedule->command('drivers:reset')->hourly();
        // $schedule->command('drivers:register')->weekly();
        // $schedule->command('drivers:idle-push')->everyMinute();
        // $schedule->command('drivers:logout-reminder')->dailyAt('14:36');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
