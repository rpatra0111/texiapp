<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'taxiRideId', 'userId', 'notiType',
        'message', 'readStatus'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function TaxiRide()
    {
        return $this->belongsTo('App\TaxiRide', 'taxiRideId', 'id');
    }

    public function User()
    {
        return $this->belongsTo('App\User', 'userId', 'id');
    }
}
