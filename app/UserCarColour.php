<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCarColour extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'colour', 'hexCode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function UserCars()
    {
        return $this->hasMany('App\UserCar', 'colour', 'id');
    }
}
