<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefererUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'referer_user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function User()
    {
        return $this->belongsTo('App\User', 'user_d', 'id');
    }
}
