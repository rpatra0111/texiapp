<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTaxi extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId', 'carId', 'taxiTypeId', 'currLat', 'currLng',
        'currLocality', 'forHire', 'available', 'socketId',
        'requested'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function Driver()
    {
        return $this->belongsTo('App\User', 'userId', 'id');
    }

    public function UserCar()
    {
        return $this->belongsTo('App\UserCar', 'carId', 'id');
    }

    public function TaxiType()
    {
        return $this->belongsTo('App\TaxiType', 'taxiTypeId', 'id');
    }

    public function TaxiRide()
    {
        return $this->hasMany('App\TaxiRide', 'userTaxiId', 'id');
    }
}
