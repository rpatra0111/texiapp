<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMenuPermit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['userId', 'menuId'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function User()
    {
        return $this->belongsTo('App\User', 'userId', 'id');
    }

    public function Menu()
    {
        return $this->belongsTo('App\AdminPanelMenu', 'menuId', 'id');
    }
}
