<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCashout extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId', 'currencyCode', 'amount', 'paidStatus'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function User()
    {
        return $this->belongsTo('App\User', 'userId', 'id');
    }
}
