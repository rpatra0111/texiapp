<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralSetting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'siteTitle', 'logo', 'contactName', 'contactPhone',
        'contactEmail', 'paypalEmail', 'siteUrl', 'siteFbLink',
        'siteTwitterLink', 'siteGplusLink', 'siteLinkedinLink',
        'sitePinterestLink', 'bookingRange', 'driverRange',
        'commission', 'contactAdminText', 'ref_reword_point'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function UserTaxies()
    {
        return $this->hasMany('App\UserTaxi', 'taxiTypeId', 'id');
    }
}
