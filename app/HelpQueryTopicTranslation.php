<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelpQueryTopicTranslation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'help_query_topic_id', 'topic_name', 'locale'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function HelpQueryTopic()
    {
        return $this->belongsTo('App\HelpQueryTopic', 'help_query_topic_id', 'id');
    }
}
