<?php

use App\GeneralSetting;
use App\User;
use App\TaxiRide;
use App\AdminPanelMenu;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;
use \Illuminate\Support\Facades\Storage;
use \Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\DB;

if (!function_exists('split_name')) {
    /**
     * Spilt first name and last name.
     *
     * @param $name
     * @return array
     */
    function split_name($name)
    {
        $name = rtrim($name);
        $parts = explode(" ", $name);
        if (count($parts) === 1) {
            return [
                'first_name' => $name,
                'last_name' => $name
            ];
        } else {
            $last_name = array_pop($parts);
            $first_name = implode(' ', $parts);
            return [
                'first_name' => $first_name,
                'last_name' => $last_name
            ];
        }
    }
}

if (!function_exists('create_api_token')) {
    function create_api_token($user)
    {
        return base64_encode($user->id . '=' . str_random(40));
    }
}

if (!function_exists('asset_url')) {
    /**
     * Generate a url for the application.
     * @param string $path
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    function asset_url($path = null)
    {
        if (is_null($path)) {
            return Storage::disk('s3')->url();
        }
        return Storage::disk('s3')->url($path);
    }
}

if (!function_exists('admin_url')) {
    /**
     * Generate a url for the application.
     * @param string $path
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    function admin_url($path = null)
    {
        $admin_path = config('constant.ADMIN_ROUTE_PREFIX') . '/';
        if (is_null($path)) {
            return url($admin_path);
        }
        return url($admin_path . $path);
    }
}

if (!function_exists('generate_file_name')) {
    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return string
     */
    function generate_file_name(\Symfony\Component\HttpFoundation\File\UploadedFile $file)
    {
        $file_ext = strtolower(trim($file->getClientOriginalExtension()));

        $original_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);;
        $original_name = str_replace(' ', '-', substr($original_name, 0, 75));
        return time() . str_random(10) . '.' . $original_name . '.' . $file_ext;
    }
}

if (!function_exists('sendNotification')) {
    function sendNotification($tokens, $message, $noti = null)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        if ($noti) {
            //For ios
            $fields = array(
                'notification' => $noti,
                'registration_ids' => $tokens,
                'priority' => 'high',
                'data' => $message,
                'sound' => 'default'
            );
        } else {
            //For android
            $fields = array(
                'registration_ids' => $tokens,
                'priority' => 'high',
                'data' => $message,
                'sound' => 'default'
            );
        }
        $headers = array(
            'Authorization:key =' . config('constant.FCM_KEY'),
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            $result = 'Curl failed: ' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }
}

if (!function_exists('getBigSocialImage')) {
    function getBigSocialImage($imgUrl, $socialType)
    {
        $imageurl = "";
        $arr = explode('?', $imgUrl);
        if ($socialType == 'fb') {
            $imageurl = $arr[0] . "?type=large";
        } else {
            $imageurl = $arr[0] . "?sz=1000";
        }
        return $imageurl;
    }
}

if (!function_exists('count_users')) {
    function count_users($user_type)
    {
        return User::where('userType', $user_type)
            ->where('status', 'Y')
            ->count();
    }
}

if (!function_exists('count_rides')) {
    function count_rides()
    {
        return TaxiRide::whereNotIn('status', ['P', 'V'])
            ->where(DB::raw('DATE(created_at)'), date('Y-m-d'))->count();
    }
}

if (!function_exists('get_general_settings')) {
    function get_general_settings($key)
    {
        $setting = GeneralSetting::find(1);
        return $setting->$key;
    }
}

if (!function_exists('sendSms')) {
    function sendSms($to, $message)
    {
        $accountSid = config('constant.TWILIO_ACCOUNT_SID');
        $authToken = config('constant.TWILIO_AUTH_TOKEN');
        $twilioNumber = config('constant.TWILIO_NUMBER');

        $client = new Client($accountSid, $authToken);

        try {
            $client->messages->create(
                $to,
                [
                    "body" => $message,
                    "from" => $twilioNumber
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );
            return ['status' => 'success'];
        } catch (TwilioException $e) {
            return [
                'status' => 'fail',
                'exception' => $e->getMessage()
            ];
        }
    }
}

if (!function_exists('dayToDates')) {
    function dayToDates($day, $month, $year)
    {
        $dates = [];
        $daysMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $flag = 0;
        $i = 1;
        while ($i <= $daysMonth) {
            if ($flag == 0) {
                if (date('D', strtotime($year . '-' . $month . '-' . $i)) == $day) {
                    $dates[] = ($year . '-' . $month . '-' . $i);
                    $flag = 1;
                    $i += 7;
                } else {
                    $i++;
                }
            } else {
                $dates[] = ($year . '-' . $month . '-' . $i);
                $i += 7;
            }
        }
        return $dates;
    }
}

if (!function_exists('getStaticMap')) {
    function getStaticMap($fromLatLng, $toLatLng)
    {
        $client = new Aws\S3\S3Client([
            'key'    => config('constant.AWS_ACCESS_KEY_ID'),
            'secret' => config('constant.AWS_SECRET_ACCESS_KEY'),
            'region' => config('constant.AWS_DEFAULT_REGION'),
            'version' => config('constant.AWS_S3_VERSION')
        ]);
        $client->registerStreamWrapper();
        $startMarker = "https://goo.gl/142nZz";
        $endMarker = "https://goo.gl/BRN8Ac";
        $qp = [
            'key' => config('constant.GOOGLE_MAPS_KEY'),
            'origin' => $fromLatLng,
            'destination' => $toLatLng,
            'mode' => 'driving'
        ];
        $guzzleClient = new \GuzzleHttp\Client();
        $guzzleResponse = $guzzleClient->request('GET', "https://maps.googleapis.com/maps/api/directions/json", [
            'query' => $qp
        ]);
        $path = 'uploads/rideHistory/';
        $buffer = json_decode($guzzleResponse->getBody());
        $filename = str_random(30) . ".jpg";
        $file = null;
        if($buffer->routes!=null){            
            $imgUrl = "https://maps.googleapis.com/maps/api/staticmap?mode=driving&size=600x400&markers=anchor:top%7Cicon:" . $startMarker . "%7C" . $fromLatLng . "&markers=anchor:top%7Cicon:" . $endMarker . "%7C" . $toLatLng . "&path=weight:4|color:0x000000ff|enc:" . $buffer->routes[0]->overview_polyline->points . "&key=" . config('constant.GOOGLE_MAPS_KEY');
            $file = file_get_contents($imgUrl);
        }
        if ($file) {
            Storage::disk('s3')->put($path . $filename, $file);
            return $filename;
        } else {
            return false;
        }
        // return $qp;
    }
}

if (!function_exists('camelToSentence')) {
    function camelToSentence($camelStr)
    {
        $intermediate = preg_replace('/(?!^)([[:upper:]][[:lower:]]+)/',
            ' $0',
            $camelStr);
        $titleStr = preg_replace('/(?!^)([[:lower:]])([[:upper:]])/',
            '$1 $2',
            $intermediate);
        return $titleStr;
    }
}

if (!function_exists('getAdminMenu')) {
    function getAdminMenu()
    {
        $sideMenuBar = [];
        if (Auth::user()->userType == 'A') {
            $sideMenuBar = collect(AdminPanelMenu::where('type', 'M')
                ->whereHas('UserMenu', function ($query) {
                    $query->where('userId', Auth::id());
                })->get())->toArray();
        }
        if (Auth::user()->userType == 'SU') {
            $sideMenuBar = collect(AdminPanelMenu::where('type', 'M')->get())->toArray();
        }
        return $sideMenuBar;
    }
}

if (!function_exists('adminDataListDateTime')) {
    function adminDataListDateTime($datetime=null)
    {
		if($datetime)
			return date('d M, Y h:i A', strtotime($datetime));
		else
			return '';
    }
}

if (!function_exists('generateReferenceCode')) {
    function generateReferenceCode()
    {
        return strtoupper(str_random(3)).mt_rand(100,999);
    }
}

if (!function_exists('getExtension')) {
    function getExtension($fileName)
    {
        $file_ext = explode(".",$fileName);
        if (isset($file_ext[1])) {
            return $file_ext[1];
        }
        return $fileName;
    }
}