<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankNameTranslation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bank_name_id', 'bank_name', 'locale'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function banks()
    {
        return $this->belongsTo('App\BankName', 'bank_name_id', 'id');
    }
}
