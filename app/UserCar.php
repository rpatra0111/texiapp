<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCar extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId', 'numberPlate', 'make',
        'model', 'colour', 'year'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function User()
    {
        return $this->belongsTo('App\User', 'userId', 'id');
    }

    public function UserCarColour()
    {
        return $this->belongsTo('App\UserCarColour', 'colour', 'id');
    }

    public function UserTaxis()
    {
        return $this->hasMany('App\UserTaxi', 'carId', 'id');
    }

    public function TaxiTypes()
    {
        return $this->hasManyThrough('App\TaxiType', 'App\UserTaxi', 'carId', 'id', 'id', 'taxiTypeId');
    }
}
