<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBankAccount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'bank_name', 'user_name_same_as_bank_ac',
        'address1', 'address2', 'city', 'state', 'country',
        'pin_zip', 'latitude', 'longitude', 'account_number',
        'ifsc_code', 'is_default', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function Bank()
    {
        return $this->belongsTo('App\BankName', 'bank_id', 'id');
    }

    public function countryDetails()
    {
        return $this->hasOne('App\Country', 'id' , 'country');
    }

    public function stateDetails()
    {
        return $this->hasOne('App\CountryState', 'id' , 'state');
    }
}
