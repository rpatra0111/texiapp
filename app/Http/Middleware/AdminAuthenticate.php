<?php

namespace App\Http\Middleware;

use App\AdminPanelMenu;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->isAdmin()) {
            if (Auth::user()->userType == 'A') {
                $menu = AdminPanelMenu::where('link', $request->segment(2))->whereHas('UserMenu', function ($query) {
                    $query->where('userId', Auth::id());
                })->first();
                if ($menu) {
                    return $next($request);
                } else {
                    return redirect('admin');
                }
            } else {
                return $next($request);
            }
        }
        return redirect('admin/login');
    }
}
