<?php

namespace App\Http\Controllers\Api;

use App\Events\CancelRide;
use App\Events\CashPaid;
use App\Events\DriverRequest;
use App\Events\DriverResponse;
use App\Events\FinishRide;
use App\Events\ReleaseDrivers;
use App\Events\StartRide;
use App\Http\Controllers\Controller;
use App\Coupon;
use App\Notification;
use App\TaxiRide;
use App\RejectedRide;
use App\TaxiType;
use App\TaxiTypeRate;
use App\User;
use App\UserRating;
use App\UserTaxi;
use App\UserTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Stripe\Charge;
use Stripe\Error\ApiConnection;
use Stripe\Error\Base;
use Stripe\Error\Card;
use Stripe\Error\InvalidRequest;
use Stripe\Error\RateLimit;
use Stripe\Stripe;
use Validator;

class RideController extends Controller
{
    public function __construct()
    {
        Stripe::setApiKey(config('constant.STRIPE_SECRET_KEY'));
    }

    public function driverRequest()
    {
        $user = Auth::user();
        $input = request()->all();
        $rules = [
            'fromAddress' => 'required',
            'fromLat' => 'required',
            'fromLng' => 'required',
            'toAddress' => 'required',
            'toLat' => 'required',
            'toLng' => 'required',
            'taxiType' => 'required',
            'estimatedDistance' => 'required',
            'estimatedTime' => 'required',
            'fare' => 'required',
            'couponId' => 'nullable',
            'paidBy' => 'required',
            'estimatedFare' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ], 400);
        }
        if ($user->userType == 'P') {
            $ride = TaxiRide::where(['passengerId' => Auth::id()])
                ->where(function ($qwery) {
                    $qwery->whereIn('status', ['S', 'A', 'R']);
                })->count();
            if ($ride == 0) {
                $updateData = [
                    'fromAddress' => $input['fromAddress'],
                    'fromLat' => $input['fromLat'],
                    'fromLng' => $input['fromLng'],
                    'toAddress' => $input['toAddress'],
                    'toLat' => $input['toLat'],
                    'toLng' => $input['toLng'],
                    'estimatedDistance' => $input['estimatedDistance'],
                    'estimatedTime' => $input['estimatedTime'],
                    'estimatedFare' => $input['estimatedFare'],
                    'fare' => $input['fare'],
                    'paidBy' => $input['paidBy'],
                    'taxiTypeId' => $input['taxiType']
                ];
                $discountedPrice = $input['fare'];
                if (isset($input['couponId'])) {
                    $coupon = Coupon::where('id', '=', $input['couponId'])->first();
                    if ($coupon) {
                        $updateData['couponId'] = $input['couponId'];
                        $discountedPrice *= (1 - ($coupon->discount / 100));
                    } else {
                        return response()->json([
                            'status' => 'fail',
                            'errors' => "Invalid Coupon"
                        ], 400);
                    }
                }
                $updateData['discounted'] = $discountedPrice;
                /*if ($input['paidBy'] == 'R') {
                    if ($user->balance < $discountedPrice) {
                        return response()->json([
                            'status' => 'fail',
                            'errors' => "Insufficient Balance"
                        ], 400);
                    }
                }*/
                $taxiRide = TaxiRide::updateOrCreate([
                    'passengerId' => Auth::id(),
                    'status' => 'P'
                ], $updateData);
                $input['discounted'] = (string)$discountedPrice;

                event(new DriverRequest([
                    'userId' => Auth::id(),
                    'taxiRideId' => $taxiRide->id,
                    'requestData' => $input
                ]));
                $inputData = $input;
                $inputData['passengerId'] = Auth::user()->id;
                $inputData['passengerFirstName'] = Auth::user()->firstName;
                $inputData['passengerLastName'] = Auth::user()->lastName;
                $inputData['passengerProfilePicture'] = Auth::user()->profilePicture;
                $inputData['passengerCountryCode'] = Auth::user()->countryCode;
                $inputData['passengerMobile'] = Auth::user()->mobile;
                event(new DriverRequest([
                    'userType' => 'D',
                    'taxiRideId' => $taxiRide->id,
                    'requestData' => $inputData
                ]));
                return response()->json([
                    'status' => 'success',
                    'timer' => 180,
                    'taxiRide' => $taxiRide->id
                ], 200);
            } else {
                return response()->json([
                    'status' => 'fail',
                    'errors' => "Already On Ride"
                ], 400);
            }
        } else {
            return response()->json([
                'status' => 'fail',
                'errors' => "Invalid User"
            ], 400);
        }
    }

    public function driverResponse()
    {
        $user = Auth::user();
        $image = null;
        $input = request()->all();
        $rules = [
            'taxiRequestId' => 'required',
            'driverResponse' => [
                'required',
                Rule::in(["Y", "N"])
            ]
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ], 400);
        }
        $taxiRide = TaxiRide::where(['id' => $input['taxiRequestId'], 'status' => 'P'])->first();
        if ($taxiRide) {
            if ($user->userType == 'D') {
                $passengerId = $taxiRide->passengerId;
                $passenger = User::find($passengerId);
                switch ($input['driverResponse']) {
                    case 'Y':
                        $driver = [
                            'id' => $user->id,
                            'firstName' => $user->firstName,
                            'lastName' => $user->lastName,
                            'gender' => $user->gender,
                            'countryCode' => $user->countryCode,
                            'mobile' => $user->mobile,
                            'profilePicture' => $user->profilePicture
                        ];
                        $user->UserTaxis()->update(['forHire' => 'N']);
                        $userTaxi = $user->UserTaxis()->get();
                        $userTaxiTypeId = 0;
                        foreach ($userTaxi as $value) {
                            if ($value->taxiTypeId == $taxiRide->taxiTypeId) {
                                $userTaxiTypeId = $value->id;
                            }
                        }
                        $taxiRide->userTaxiId = $userTaxiTypeId;
                        $taxiRide->status = 'S';
                        $taxiRide->save();
                        $image = getStaticMap($taxiRide->fromLat . ',' . $taxiRide->fromLng, $taxiRide->toLat . ',' . $taxiRide->toLng);
                        if ($image) {
                            $taxiRide->polylineImage = $image;
                        }
                        $taxiRide->save();
                        event(new DriverResponse([
                            'userId' => Auth::id(),
                            'taxiRequestId' => $input['taxiRequestId'],
                            'passengerId' => $passengerId,
                            'driver' => $driver,
                            'dResponse' => $input['driverResponse']
                        ]));
                        event(new DriverResponse([
                            'userId' => $passengerId,
                            'driver' => $driver,
                            'dResponse' => $input['driverResponse']
                        ]));
                        event(new ReleaseDrivers([
                            'userType' => 'D',
                            'taxiRequestId' => $input['taxiRequestId']
                        ]));
                        $tokens = collect($passenger->UserTokens()->get())->toArray();
                        $fcmTokensI = [];
                        $fcmTokensA = [];
                        foreach ($tokens as $key => $values) {
                            if ($values['flag'] == 'A')
                                $fcmTokensA[] = $values['fcmToken'];
                            if ($values['flag'] == 'I')
                                $fcmTokensI[] = $values['fcmToken'];
                        }
                        if (!empty($fcmTokensA))
                            sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'TRA', 'taxiRequestId' => $input['taxiRequestId'], 'driver' => $driver, 'title' => config('constant.APP_NAME'), 'body' => "Driver " . $user->firstName . ' has accepted your request']]);
                        if (!empty($fcmTokensI))
                            sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'TRA', 'taxiRequestId' => $input['taxiRequestId'], 'driver' => $driver]], ['title' => config('constant.APP_NAME'), 'body' => "Driver " . $user->firstName . ' has accepted your request']);
                        $message = "Ride Accepted Successfully";
                        Notification::create([
                            'notiType' => 'TRA',
                            'userId' => $passengerId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'message' => $message]);
                        Notification::create([
                            'notiType' => 'TRA',
                            'userId' => $user->id,
                            'taxiRideId' => $input['taxiRequestId'],
                            'message' => 'You accepted ' . $passenger->firstName . "'s request for $" . $taxiRide->fare]);
                        event(new DriverResponse([
                            'userId' => $passengerId,
                            'dResponse' => $input['driverResponse']
                        ]));
                        break;
                    case 'N':
                        event(new DriverResponse([
                            'userId' => $passengerId,
                            'dResponse' => $input['driverResponse']
                        ]));
                        $message = "Ride Rejected Successfully";
                        break;
                }
                return response()->json([
                    'status' => 'success',
                    'message' => $message,
                    'image' => $image
                ], 200);
            } else {
                return response()->json([
                    'status' => 'fail',
                    'errors' => "Invalid User"
                ], 400);
            }
        } else {
            $user->UserTaxis()->update(['requested' => 'N']);
            return response()->json([
                'status' => 'fail',
                'errors' => "Request has timed out"
            ], 400);
        }
    }

    public function startRide()
    {
        $user = Auth::user();
        $input = request()->all();
        $rules = [
            'taxiRequestId' => 'required|exists:taxi_rides,id'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ], 400);
        }
        $taxiRide = TaxiRide::where(['id' => $input['taxiRequestId'], 'status' => 'S'])->first();
        if ($taxiRide) {
            $driverId = collect($taxiRide->UserTaxi()->first())->toArray()['userId'];
            $passengerId = $taxiRide->passengerId;
            if ($user->userType == 'D') {
                if (Auth::id() == $driverId) {
                    $taxiRide->status = 'A';
                    $taxiRide->save();
                    event(new StartRide([
                        'userId' => Auth::id(),
                        'taxiRequestId' => $input['taxiRequestId']
                    ]));
                    return response()->json([
                        'status' => 'success'
                    ], 200);
                } else {
                    return response()->json([
                        'status' => 'fail',
                        'errors' => "Invalid Driver"
                    ], 400);
                }
            } else {
                return response()->json([
                    'status' => 'fail',
                    'errors' => "Invalid User"
                ], 400);
            }
        } else {
            return response()->json([
                'status' => 'fail',
                'errors' => "Invalid Request"
            ], 400);
        }
    }

    public function finishRide()
    {
        $user = Auth::user();
        $input = request()->all();
        $rules = [
            'taxiRequestId' => 'required|exists:taxi_rides,id'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ], 400);
        }
        $taxiRide = TaxiRide::where(['id' => $input['taxiRequestId'], 'status' => 'A'])->first();
        if ($taxiRide) {
            $fare = (float)($taxiRide->discounted);
            $driverId = collect($taxiRide->UserTaxi()->first())->toArray()['userId'];
            $passengerId = $taxiRide->passengerId;
            $passenger = User::find($passengerId);
            if ($user->userType == 'D') {
                if (Auth::id() == $driverId) {
                    $driver = [
                        'id' => $user->id,
                        'firstName' => $user->firstName,
                        'lastName' => $user->lastName,
                        'gender' => $user->gender,
                        'countryCode' => $user->countryCode,
                        'mobile' => $user->mobile,
                        'profilePicture' => $user->profilePicture
                    ];
                    if ($taxiRide->paidBy == 'R') {
                        $passengerTransaction = UserTransaction::where(['userId' => $passengerId, 'transactionType' => 'D', 'paymentStatus' => 'P', 'amount' => $taxiRide->discounted])
                            ->whereNotNull('gatewayTransactionId')->first();
                        if (!$passengerTransaction) {
                            return response()->json([
                                'status' => 'fail',
                                'errors' => "Transaction Failed!"
                            ], 400);
                        }
                        $stripeCharge = $this->stripeCharge($passengerTransaction->gatewayTransactionId, $taxiRide->discounted);
                        if ($stripeCharge['status'] == 'fail') {
                            return response()->json([
                                'status' => 'fail',
                                'errors' => $stripeCharge['errors']
                            ], 400);
                        }
                        $charge = $stripeCharge['chargeData'];
                        $passenger->Transactions()->create([
                            'gatewayTransactionId' => $charge['id'],
                            'gatewayResponse' => $charge,
                            'amount' => $taxiRide->discounted,
                            'paidUsing' => 'S',
                            'transactionType' => 'C',
                            'description' => "Paid for Ride id: " . $taxiRide->id
                        ]);
                        $taxiRide->paidStatus = 'Y';
                        $taxiRide->status = 'Y';
                        $taxiRide->save();
                        $commission = ($taxiRide->fare * (float)((get_general_settings('commission')) / 100));
                        event(new FinishRide([
                            'userId' => Auth::id(),
                            'taxiRequestId' => $input['taxiRequestId'],
                            'passengerId' => $passengerId,
                            'fare' => $taxiRide->discounted,
                            'driver' => $driver,
                            'paidBy' => $taxiRide->paidBy,
                            'finishedOn' => $taxiRide->updated_at
                        ]));
                        $passengerTransaction->amount = $taxiRide->discounted;
                        $passengerTransaction->taxiRideId = $input['taxiRequestId'];
                        $passengerTransaction->description = "Paid for the ride";
                        $passengerTransaction->paymentStatus = "Y";
                        $passengerTransaction->save();
                        /*UserTransaction::create([
                            'userId' => $passengerId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'amount' => $taxiRide->discounted,
                            'transactionType' => 'D',
                            'description' => "Paid for the ride"
                        ]);*/
                        UserTransaction::create([
                            'userId' => $driverId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'amount' => $taxiRide->fare,
                            'transactionType' => 'C',
                            'description' => "Received for the ride"
                        ]);
                        UserTransaction::create([
                            'userId' => $driverId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'amount' => $commission,
                            'transactionType' => 'D',
                            'description' => config('constant.APP_NAME') . "'s Commission for the ride."
                        ]);
                        UserTransaction::create([
                            'userId' => 1,
                            'taxiRideId' => $input['taxiRequestId'],
                            'amount' => $commission,
                            'transactionType' => 'C',
                            'description' => "Commission for Ride"
                        ]);
                        if ($taxiRide->discounted != $taxiRide->fare) {
                            UserTransaction::create([
                                'userId' => 1,
                                'taxiRideId' => $input['taxiRequestId'],
                                'amount' => ($taxiRide->fare - $taxiRide->discounted),
                                'transactionType' => 'D',
                                'description' => "Discount adjustment for Ride"
                            ]);
                        }
                        User::where('id', '=', 1)->increment('balance', $commission);
                        if ($taxiRide->discounted != $taxiRide->fare) {
                            User::where('id', '=', 1)->decrement('balance', ($taxiRide->fare - $taxiRide->discounted));
                        }
                        User::where('id', '=', $driverId)->increment('balance', ($taxiRide->fare - $commission));
                        // User::where('id', '=', $passengerId)->decrement('balance', $taxiRide->discounted);
                        Notification::create([
                            'notiType' => 'TRC',
                            'userId' => $driverId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'message' => "The trip has been completed & $" . $taxiRide->fare . " collected"
                        ]);
                        Notification::create([
                            'notiType' => 'TRC',
                            'userId' => $passengerId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'message' => "The trip has been completed & $" . $taxiRide->fare . " collected"
                        ]);
                        UserTaxi::where('userId', $driverId)->update(['forHire' => 'Y']);
                        $tokens = collect($passenger->UserTokens()->get())->toArray();
                        $fcmTokensI = [];
                        $fcmTokensA = [];
                        foreach ($tokens as $key => $values) {
                            if ($values['flag'] == 'A')
                                $fcmTokensA[] = $values['fcmToken'];
                            if ($values['flag'] == 'I')
                                $fcmTokensI[] = $values['fcmToken'];
                        }
                        if (!empty($fcmTokensA))
                            sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'TRC', 'taxiRequestId' => $input['taxiRequestId'], 'driver' => $driver, 'finishedOn' => $taxiRide->updated_at, 'title' => config('constant.APP_NAME'), 'body' => 'Driver ' . $user->firstName . ' has completed the trip']]);
                        if (!empty($fcmTokensI))
                            sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'TRC', 'taxiRequestId' => $input['taxiRequestId'], 'driver' => $driver, 'finishedOn' => $taxiRide->updated_at]], ['title' => config('constant.APP_NAME'), 'body' => 'Driver ' . $user->firstName . ' has completed the trip']);
                    }
                    if ($taxiRide->paidBy == 'S') {
                        $taxiRide->status = 'R';
                        $taxiRide->save();
                        if ((float)($passenger->balance) < 0) {
                            $fare = (float)($taxiRide->discounted) - (float)($passenger->balance);
                        }
                        event(new FinishRide([
                            'userId' => Auth::id(),
                            'taxiRequestId' => $input['taxiRequestId'],
                            'driver' => $driver,
                            'fare' => $fare,
                            'paidBy' => $taxiRide->paidBy,
                            'finishedOn' => $taxiRide->updated_at
                        ]));
                        $tokens = collect($passenger->UserTokens()->get())->toArray();
                        $fcmTokensI = [];
                        $fcmTokensA = [];
                        foreach ($tokens as $key => $values) {
                            if ($values['flag'] == 'A')
                                $fcmTokensA[] = $values['fcmToken'];
                            if ($values['flag'] == 'I')
                                $fcmTokensI[] = $values['fcmToken'];
                        }
                        if (!empty($fcmTokensA))
                            sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'TRF', 'taxiRequestId' => $input['taxiRequestId'], 'driver' => $driver, 'finishedOn' => $taxiRide->updated_at, 'title' => config('constant.APP_NAME'), 'body' => 'Driver ' . $user->firstName . ' has completed the trip']]);
                        if (!empty($fcmTokensI))
                            sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'TRF', 'taxiRequestId' => $input['taxiRequestId'], 'driver' => $driver, 'finishedOn' => $taxiRide->updated_at]], ['title' => config('constant.APP_NAME'), 'body' => 'Driver ' . $user->firstName . ' has completed the trip']);
                    }
                    return response()->json([
                        'status' => 'success',
                        'paidBy' => $taxiRide->paidBy,
                        'fare' => $fare,
                        'finishedOn' => $taxiRide->updated_at,
                        'passengerId' => $passengerId
                    ], 200);
                } else {
                    return response()->json([
                        'status' => 'fail',
                        'errors' => "Invalid Driver"
                    ], 400);
                }
            } else {
                return response()->json([
                    'status' => 'fail',
                    'errors' => "Invalid User"
                ], 400);
            }
        } else {
            return response()->json([
                'status' => 'fail',
                'errors' => "Invalid Request"
            ], 400);
        }
    }

    protected function stripeCharge($chargeId, $amount)
    {
        try {
            $charge = Charge::retrieve($chargeId);
            $charge->capture([
                'amount' => $amount * 100,
                'expand' => array('balance_transaction')
            ]);
            return [
                'status' => 'success',
                'chargeData' => $charge
            ];
        } catch (Card $e) {
            $body = $e->getJsonBody();
            return [
                'status' => 'fail',
                'errors' => $body['error']['message']
            ];
        } catch (RateLimit $e) {
            $body = $e->getJsonBody();
            return [
                'status' => 'fail',
                'errors' => $body['error']['message']
            ];
        } catch (InvalidRequest $e) {
            $body = $e->getJsonBody();
            return [
                'status' => 'fail',
                'errors' => $body['error']['message']
            ];
        } catch (ApiConnection $e) {
            $body = $e->getJsonBody();
            return [
                'status' => 'fail',
                'errors' => $body['error']['message']
            ];
        } catch (Base $e) {
            $body = $e->getJsonBody();
            return [
                'status' => 'fail',
                'errors' => $body['error']['message']
            ];
        }
    }

    public function cashPaid()
    {
        $user = Auth::user();
        $input = request()->all();
        $rules = [
            'taxiRequestId' => 'required|exists:taxi_rides,id',
            'paidStatus' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ], 400);
        }
        $taxiRide = TaxiRide::where(['id' => $input['taxiRequestId'], 'status' => 'R', 'paidBy' => 'S'])->first();
        if ($taxiRide) {
            $driverId = collect($taxiRide->UserTaxi()->first())->toArray()['userId'];
            $passengerId = $taxiRide->passengerId;
            $passenger = User::find($passengerId);
            if ($user->userType == 'D') {
                if (Auth::id() == $driverId) {
                    $driver = [
                        'id' => $user->id,
                        'firstName' => $user->firstName,
                        'lastName' => $user->lastName,
                        'gender' => $user->gender,
                        'countryCode' => $user->countryCode,
                        'mobile' => $user->mobile,
                        'profilePicture' => $user->profilePicture
                    ];
                    if ($input['paidStatus'] == 'Y') {
                        if ($taxiRide->cancel == 'N') {
                            $taxiRide->status = 'Y';
                        } else {
                            $taxiRide->status = 'N';
                        }
                        $taxiRide->paidStatus = 'Y';
                        $taxiRide->save();
                        $commission = ($taxiRide->fare * ((float)(get_general_settings('commission')) / 100));
                        event(new CashPaid([
                            'userId' => Auth::id(),
                            'taxiRequestId' => $input['taxiRequestId'],
                            'passengerId' => $passengerId,
                            'driver' => $driver
                        ]));
                        $dBalDed = $commission;
                        UserTransaction::create([
                            'userId' => $driverId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'amount' => $taxiRide->discounted,
                            'transactionType' => 'C',
                            'description' => "Received for the ride."
                        ]);
                        UserTransaction::create([
                            'userId' => $driverId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'amount' => $commission,
                            'transactionType' => 'D',
                            'description' => config('constant.APP_NAME') . "'s Commission for the ride."
                        ]);
                        UserTransaction::create([
                            'userId' => $passengerId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'amount' => $taxiRide->discounted,
                            'transactionType' => 'D',
                            'description' => "Paid for the ride."
                        ]);
                        if ((float)($passenger->balance) < 0) {
                            UserTransaction::create([
                                'userId' => $passengerId,
                                'taxiRideId' => $input['taxiRequestId'],
                                'amount' => ((float)($passenger->balance) * (-1)),
                                'transactionType' => 'C',
                                'description' => "Adjustment of due balance in the wallet."
                            ]);
                            UserTransaction::create([
                                'userId' => $driverId,
                                'taxiRideId' => $input['taxiRequestId'],
                                'amount' => ((float)($passenger->balance) * (-1)),
                                'transactionType' => 'D',
                                'description' => "Adjustment of passenger's due balance."
                            ]);
                            $dBalDed -= (float)($passenger->balance);
                        }
                        UserTransaction::create([
                            'userId' => 1,
                            'taxiRideId' => $input['taxiRequestId'],
                            'amount' => $commission,
                            'transactionType' => 'C',
                            'description' => "Commission for Ride."
                        ]);
                        if ($taxiRide->discounted != $taxiRide->fare) {
                            UserTransaction::create([
                                'userId' => 1,
                                'taxiRideId' => $input['taxiRequestId'],
                                'amount' => ($taxiRide->fare - $taxiRide->discounted),
                                'transactionType' => 'D',
                                'description' => "Discount adjustment for Ride"
                            ]);
                            UserTransaction::create([
                                'userId' => $driverId,
                                'taxiRideId' => $input['taxiRequestId'],
                                'amount' => ($taxiRide->fare - $taxiRide->discounted),
                                'transactionType' => 'C',
                                'description' => "Discount adjustment for Ride"
                            ]);
                        }
                        User::where('id', '=', 1)->increment('balance', $commission);
                        if ($taxiRide->discounted != $taxiRide->fare) {
                            User::where('id', '=', 1)->decrement('balance', ($taxiRide->fare - $taxiRide->discounted));
                            User::where('id', '=', $driverId)->increment('balance', ($taxiRide->fare - $taxiRide->discounted));
                        }
                        User::where('id', '=', $driverId)->decrement('balance', $dBalDed);
                        UserTaxi::find($taxiRide->userTaxiId)->update(['forHire' => 'Y']);
                        $m = "The trip has been " . (($taxiRide->cancel == 'N') ? "completed" : "cancelled") . " & $" . $taxiRide->fare . " collected";
                        $tokens = collect($passenger->UserTokens()->get())->toArray();
                        $fcmTokensI = [];
                        $fcmTokensA = [];
                        foreach ($tokens as $key => $values) {
                            if ($values['flag'] == 'A')
                                $fcmTokensA[] = $values['fcmToken'];
                            if ($values['flag'] == 'I')
                                $fcmTokensI[] = $values['fcmToken'];
                        }
                        if (!empty($fcmTokensA))
                            sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'TRC', 'taxiRequestId' => $input['taxiRequestId'], 'driver' => $driver, 'title' => config('constant.APP_NAME'), 'body' => $m]]);
                        if (!empty($fcmTokensI))
                            sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'TRC', 'taxiRequestId' => $input['taxiRequestId'], 'driver' => $driver]], ['title' => config('constant.APP_NAME'), 'body' => $m]);
                        if ((float)($passenger->balance) < 0) {
                            $passenger->balance = 0.00;
                            $passenger->save();
                        }
                        Notification::create([
                            'notiType' => 'TRC',
                            'userId' => $passengerId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'message' => $m
                        ]);
                        Notification::create([
                            'notiType' => 'TRC',
                            'userId' => $user->id,
                            'taxiRideId' => $input['taxiRequestId'],
                            'message' => $m
                        ]);
                        return response()->json([
                            'status' => 'success',
                            'passengerId' => $passengerId
                        ], 200);
                    } elseif ($input['paidStatus'] == 'N') {
                        $tokens = collect($passenger->UserTokens()->get())->toArray();
                        $fcmTokensI = [];
                        $fcmTokensA = [];
                        foreach ($tokens as $key => $values) {
                            if ($values['flag'] == 'A')
                                $fcmTokensA[] = $values['fcmToken'];
                            if ($values['flag'] == 'I')
                                $fcmTokensI[] = $values['fcmToken'];
                        }
                        if (!empty($fcmTokensA))
                            sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'TRC', 'taxiRequestId' => $input['taxiRequestId'], 'driver' => $driver, 'title' => config('constant.APP_NAME'), 'body' => 'Unpaid Amount will be deducted from your balance']]);
                        if (!empty($fcmTokensI))
                            sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'TRC', 'taxiRequestId' => $input['taxiRequestId'], 'driver' => $driver]], ['title' => config('constant.APP_NAME'), 'body' => 'Unpaid Amount will be deducted from your balance']);
                        return response()->json([
                            'status' => 'success',
                            'message' => 'Unpaid amount will be deducted from the passenger balance'
                        ], 200);
                    } else {
                        return response()->json([
                            'status' => 'fail',
                            'errors' => "Invalid Request"
                        ], 400);
                    }
                } else {
                    return response()->json([
                        'status' => 'fail',
                        'errors' => "Invalid Driver"
                    ], 400);
                }
            } else {
                return response()->json([
                    'status' => 'fail',
                    'errors' => "Invalid User"
                ], 400);
            }
        } else {
            return response()->json([
                'status' => 'fail',
                'errors' => "Invalid Request"
            ], 400);
        }
    }

    public function cancelRide(Request $request)
    {
        $user = Auth::user();
        $input = $request->all();
        $rules = [
            'taxiRequestId' => 'required|exists:taxi_rides,id'
        ];
        if ($user->userType == 'D') {
            $rules['cancelReason'] = 'required';
        }
        if ($user->userType == 'P') {
            $rules['locality'] = 'required';
        }
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ], 400);
        }
        $taxiRide = TaxiRide::where('id', '=', $input['taxiRequestId'])
            ->whereIn('status', ['S', 'A'])->first();
        if ($taxiRide) {
            $driverId = collect($taxiRide->UserTaxi()->first())->toArray()['userId'];
            $passengerId = $taxiRide->passengerId;
            $taxiType = collect($taxiRide->UserTaxi()->first())->toArray()['taxiTypeId'];
            if ($user->userType == 'D') {
                $driver = [
                    'id' => $user->id,
                    'firstName' => $user->firstName,
                    'lastName' => $user->lastName,
                    'gender' => $user->gender,
                    'countryCode' => $user->countryCode,
                    'mobile' => $user->mobile,
                    'profilePicture' => $user->profilePicture
                ];
                $passenger = User::find($passengerId);
                $taxiRide->status = 'N';
                $taxiRide->cancel = $user->userType;
                if ($request->has('cancelReason')) {
                    $taxiRide->cancelReason = $input['cancelReason'];
                }
                $taxiRide->save();
                UserTaxi::find($taxiRide->userTaxiId)->update(['forHire' => 'Y']);
                Notification::create([
                    'notiType' => 'TRCN',
                    'userId' => $passengerId,
                    'taxiRideId' => $input['taxiRequestId'],
                    'message' => "The trip was cancelled by you"
                ]);
                Notification::create([
                    'notiType' => 'TRCN',
                    'userId' => $user->id,
                    'taxiRideId' => $input['taxiRequestId'],
                    'message' => "The trip was cancelled by driver"
                ]);
                event(new CancelRide([
                    'userId' => Auth::id(),
                    'taxiRequestId' => $input['taxiRequestId'],
                    'passengerId' => $passengerId
                ]));
                $tokens = collect($passenger->UserTokens()->get())->toArray();
                $fcmTokensI = [];
                $fcmTokensA = [];
                foreach ($tokens as $key => $values) {
                    if ($values['flag'] == 'A')
                        $fcmTokensA[] = $values['fcmToken'];
                    if ($values['flag'] == 'I')
                        $fcmTokensI[] = $values['fcmToken'];
                }
                if (!empty($fcmTokensA))
                    sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'TRCN', 'taxiRequestId' => $input['taxiRequestId'], 'driver' => $driver, 'title' => config('constant.APP_NAME'), 'body' => 'Driver ' . $user->firstName . ' has cancelled the trip']]);
                if (!empty($fcmTokensI))
                    sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'TRCN', 'taxiRequestId' => $input['taxiRequestId'], 'driver' => $driver]], ['title' => config('constant.APP_NAME'), 'body' => 'Driver ' . $user->firstName . ' has cancelled the trip']);
                return response()->json([
                    'status' => 'success',
                    'message' => "Ride Cancelled Successfully"
                ], 200);
            }
            if ($user->userType == 'P') {
                $tmp = collect(TaxiTypeRate::whereHas('City', function ($query) use ($input) {
                    $query->where('cityName', '=', $input['locality']);
                })->where(['taxiTypeId' => $taxiType])->first())->toArray();
                if (!empty($tmp)) {
                    $cancelCharge = $tmp['cancelCharge'];
                } else {
                    $cancelCharge = collect(TaxiType::find($taxiType))->toArray()['cancelCharge'];
                }
                $driver = User::find($driverId);
                $passenger = [
                    'id' => $user->id,
                    'firstName' => $user->firstName,
                    'lastName' => $user->lastName,
                    'gender' => $user->gender,
                    'profilePicture' => $user->profilePicture
                ];
                $tokens = collect($driver->UserTokens()->get())->toArray();
                $fcmTokensI = [];
                $fcmTokensA = [];
                foreach ($tokens as $key => $values) {
                    if ($values['flag'] == 'A')
                        $fcmTokensA[] = $values['fcmToken'];
                    if ($values['flag'] == 'I')
                        $fcmTokensI[] = $values['fcmToken'];
                }
                if (!empty($fcmTokensA))
                    sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'TRCN', 'taxiRequestId' => $input['taxiRequestId'], 'passenger' => $passenger, 'title' => config('constant.APP_NAME'), 'body' => 'Passenger ' . $user->firstName . ' has cancelled the trip']]);
                if (!empty($fcmTokensI))
                    sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'TRCN', 'taxiRequestId' => $input['taxiRequestId'], 'passenger' => $passenger]], ['title' => config('constant.APP_NAME'), 'body' => 'Passenger ' . $user->firstName . ' has cancelled the trip']);
                if ($taxiRide->status == 'S') {
                    event(new CancelRide([
                        'userId' => Auth::id(),
                        'taxiRequestId' => $input['taxiRequestId'],
                        'driverId' => $driverId,
                        'paidBy' => 'R',
                        'fare' => $cancelCharge
                    ]));
                    if ($taxiRide->paidBy == 'S') {
                        User::where('id', '=', Auth::id())->decrement('balance', $cancelCharge);
                        UserTransaction::create([
                            'userId' => Auth::id(),
                            'taxiRideId' => $input['taxiRequestId'],
                            'amount' => $cancelCharge,
                            'transactionType' => 'D',
                            'description' => "Charges for Cancellation of Ride"
                        ]);
                        Notification::create([
                            'notiType' => 'TRCN',
                            'userId' => $passengerId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'message' => "The trip was cancelled by you!"
                        ]);
                    }
                    if ($taxiRide->paidBy == 'R') {
                        $passengerTransaction = UserTransaction::where(['userId' => $passengerId, 'transactionType' => 'D', 'paymentStatus' => 'P'])
                            ->where('amount', '>=', $taxiRide->discounted)
                            ->whereNotNull('gatewayTransactionId')->first();
                        if (!$passengerTransaction) {
                            return response()->json([
                                'status' => 'fail',
                                'errors' => "Transaction Failed!"
                            ], 400);
                        }
                        $cancelCharge = ($cancelCharge>$passengerTransaction->amount)?$passengerTransaction->amount:$cancelCharge;
                        $stripeCharge = $this->stripeCharge($passengerTransaction->gatewayTransactionId, $cancelCharge);
                        if ($stripeCharge['status'] == 'fail') {
                            return response()->json([
                                'status' => 'fail',
                                'errors' => $stripeCharge['errors']
                            ], 400);
                        }
                        $passengerTransaction->gatewayResponse = $stripeCharge['chargeData'];
                        $passengerTransaction->amount = $cancelCharge;
                        $passengerTransaction->taxiRideId = $input['taxiRequestId'];
                        $passengerTransaction->description = "Charges for Cancellation of Ride";
                        $passengerTransaction->paymentStatus = "Y";
                        $passengerTransaction->save();
                        Notification::create([
                            'notiType' => 'TRCN',
                            'userId' => $passengerId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'message' => "The trip was cancelled by you! A charge of $" . $cancelCharge . " was deducted as penalty."
                        ]);
                    }
                    User::where('id', '=', $driverId)->increment('balance', $cancelCharge);
                    UserTransaction::create([
                        'userId' => $driverId,
                        'taxiRideId' => $input['taxiRequestId'],
                        'amount' => $cancelCharge,
                        'transactionType' => 'C',
                        'description' => "Compensation for Ride Cancellation"
                    ]);
                    $taxiRide->fare = $cancelCharge;
                    $taxiRide->status = 'N';
                    $taxiRide->paidStatus = 'Y';
                    Notification::create([
                        'notiType' => 'TRCN',
                        'userId' => $user->id,
                        'taxiRideId' => $input['taxiRequestId'],
                        'message' => "The trip was cancelled by passenger"
                    ]);
                }
                elseif ($taxiRide->status == 'A') {
                    $commission = ($taxiRide->fare * (float)((get_general_settings('commission')) / 100));
                    if ($taxiRide->paidBy == 'S') {
                        $fare = (float)($taxiRide->discounted) - (float)($user->balance);
                        event(new CancelRide([
                            'userId' => Auth::id(),
                            'taxiRequestId' => $input['taxiRequestId'],
                            'driverId' => $driverId,
                            'paidBy' => 'S',
                            'fare' => $fare
                        ]));
                        $taxiRide->status = 'R';
                    }
                    if ($taxiRide->paidBy == 'R') {
                        $passengerTransaction = UserTransaction::where(['userId' => $passengerId, 'transactionType' => 'D', 'paymentStatus' => 'P'])
                            ->where('amount', '>=', $taxiRide->discounted)
                            ->whereNotNull('gatewayTransactionId')->first();
                        if (!$passengerTransaction) {
                            return response()->json([
                                'status' => 'fail',
                                'errors' => "Transaction Failed!"
                            ], 400);
                        }
                        $stripeCharge = $this->stripeCharge($passengerTransaction->gatewayTransactionId, $taxiRide->discounted);
                        if ($stripeCharge['status'] == 'fail') {
                            return response()->json([
                                'status' => 'fail',
                                'errors' => $stripeCharge['errors']
                            ], 400);
                        }
                        $charge = $stripeCharge['chargeData'];
                        Auth::user()->Transactions()->create([
                            'gatewayTransactionId' => $charge['id'],
                            'gatewayResponse' => $charge,
                            'amount' => $taxiRide->discounted,
                            'paidUsing' => 'S',
                            'transactionType' => 'C',
                            'description' => "Paid for Ride id: " . $taxiRide->id
                        ]);
                        $passengerTransaction->amount = $taxiRide->discounted;
                        $passengerTransaction->taxiRideId = $input['taxiRequestId'];
                        $passengerTransaction->description = "Paid for the ride";
                        $passengerTransaction->paymentStatus = "Y";
                        $passengerTransaction->save();
                        /*UserTransaction::create([
                            'userId' => $passengerId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'amount' => $taxiRide->discounted,
                            'transactionType' => 'D',
                            'description' => "Paid for Ride"
                        ]);*/
                        UserTransaction::create([
                            'userId' => $driverId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'amount' => $taxiRide->fare,
                            'transactionType' => 'C',
                            'description' => "Received for Ride"
                        ]);
                        UserTransaction::create([
                            'userId' => $driverId,
                            'taxiRideId' => $input['taxiRequestId'],
                            'amount' => $commission,
                            'transactionType' => 'D',
                            'description' => config('constant.APP_NAME') . "'s Commission for the ride."
                        ]);
                        UserTransaction::create([
                            'userId' => 1,
                            'taxiRideId' => $input['taxiRequestId'],
                            'amount' => $commission,
                            'transactionType' => 'C',
                            'description' => "Commission for Ride"
                        ]);
                        if ($taxiRide->discounted != $taxiRide->fare) {
                            UserTransaction::create([
                                'userId' => 1,
                                'taxiRideId' => $input['taxiRequestId'],
                                'amount' => ($taxiRide->fare - $taxiRide->discounted),
                                'transactionType' => 'D',
                                'description' => "Discount adjustment for Ride"
                            ]);
                            User::where('id', '=', 1)->decrement('balance', ($taxiRide->fare - $taxiRide->discounted));
                        }
                        User::where('id', '=', 1)->increment('balance', $commission);
                        User::where('id', '=', $driverId)->increment('balance', ($taxiRide->fare - $commission));
                        // User::where('id', '=', $passengerId)->decrement('balance', $taxiRide->fare);
                        UserTaxi::find($taxiRide->userTaxiId)->update(['forHire' => 'Y']);
                        event(new CancelRide([
                            'userId' => Auth::id(),
                            'taxiRequestId' => $input['taxiRequestId'],
                            'driverId' => $driverId,
                            'paidBy' => 'R',
                            'fare' => $taxiRide->discounted
                        ]));
                        $taxiRide->status = 'N';
                        $taxiRide->paidStatus = 'Y';
                    }
                }
                $taxiRide->cancel = $user->userType;
                $taxiRide->save();
                UserTaxi::find($taxiRide->userTaxiId)->update(['forHire' => 'Y']);
                return response()->json([
                    'status' => 'success',
                    'message' => "Ride Cancelled Successfully"
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'fail',
                'errors' => 'Invalid Request'
            ], 400);
        }
    }

    public function rateUser()
    {
        $user = Auth::user();
        $input = request()->all();
        $rules = [
            'rateeId' => 'required',
            'taxiRideId' => 'required',
            'rating' => 'required',
            'driverRating' => 'nullable',
            'carRating' => 'nullable',
            'drivingRating' => 'nullable',
            'message' => 'nullable|string'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ], 400);
        }
        $updateData = [
            'rating' => $input['rating'],
            'message' => $input['message']
        ];
        $updateData['driverRating'] = (isset($input['driverRating'])) ? $input['driverRating'] : 0;
        $updateData['carRating'] = (isset($input['carRating'])) ? $input['carRating'] : 0;
        $updateData['drivingRating'] = (isset($input['drivingRating'])) ? $input['drivingRating'] : 0;
        UserRating::updateOrCreate([
            'raterId' => $user->id,
            'rateeId' => $input['rateeId'],
            'taxiRideId' => $input['taxiRideId']
        ], $updateData);
        Notification::create([
            'notiType' => 'TRR',
            'userId' => $user->id,
            'taxiRideId' => $input['taxiRideId'],
            'message' => $user->firstName . " rated you " . $input['rating'] . (($input['rating'] > 1) ? " stars" : " star")]);
        $msg = "Successfully rated the ";
        if ($user->userType == 'D')
            $msg .= "passenger.";
        elseif ($user->userType == 'P')
            $msg .= "driver.";
        return response()->json([
            'status' => 'success',
            'message' => $msg,
            'rating' => $input['rating']
        ], 200);
    }

    public function rideHistory()
    {
        $user = Auth::user();
        if ($user->userType == 'P') {
            $rides = collect($user->TaxiRides()->with(['UserTaxi.Driver' => function ($query) {
                $query->select('id', 'firstName', 'lastName', 'profilePicture');
            }])->orderBy('created_at', 'desc')->get())->toArray();
            return response()->json([
                'status' => 'success',
                'rides' => $rides,
                'polylineImage' => asset_url("uploads/rideHistory") . "/",
                'profilePicture' => asset_url("uploads/profilePicture") . "/"
            ], 200);
        }
        if ($user->userType == 'D') {
            $rides = collect(TaxiRide::whereHas('UserTaxi', function ($query) use ($user) {
                $query->where('userId', '=', $user->id);
            })->with(['Passenger' => function ($query) {
                $query->select('id', 'firstName', 'lastName', 'profilePicture');
            }])->orderBy('created_at', 'desc')->get())->toArray();
            return response()->json([
                'status' => 'success',
                'rides' => $rides,
                'polylineImage' => config('constant.APP_ASSET_URL') . "uploads/rideHistory/",
                'profilePicture' => config('constant.APP_ASSET_URL') . "uploads/profilePicture/"
            ], 200);
        }
    }

    public function onRideData()
    {
        $user = Auth::user();
        $rides = collect(TaxiRide::whereIn('status', ['S', 'A', 'R'])
            ->where(function ($query) {
                $query->where('passengerId', '=', Auth::id())
                    ->orWhere(function ($qwery) {
                        $qwery->whereHas('UserTaxi', function ($query2) {
                            $query2->where('userId', '=', Auth::id());
                        });
                    });
            })->with(['Passenger' => function ($query) {
                $query->select('id', 'firstName', 'lastName', 'profilePicture', 'balance', 'countryCode', 'mobile');
            }, 'UserTaxi.Driver' => function ($query) {
                $query->select('id', 'firstName', 'lastName', 'profilePicture', 'balance', 'countryCode', 'mobile');
            }, 'UserTaxi.UserCar.UserCarColour'])->get())->toArray();
        if (count($rides) == 0) {
            return response()->json([
                'status' => 'fail',
                'message' => "Not On Ride"
            ], 400);
        }
        return response()->json([
            'status' => 'success',
            'rides' => $rides,
            'polylineImage' => asset_url("uploads/rideHistory") . "/"
        ], 200);
    }
}
