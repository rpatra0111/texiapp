<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['otpReset']);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    /*public function reset(Request $request)
    {
        $user = User::where(['reset_token' => request('token')])->first();
        if ($user != null) {
            $request->merge([
                'email' => $user->email
            ]);
            $user->reset_token = null;
            $user->save();
        }
        //dd($request);

        $this->validate($request, $this->rules(), $this->validationErrorMessages());
        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
            $this->resetPassword($user, $password);
        });
        if ($response == Password::PASSWORD_RESET) {
            return response()->json(['status' => 'success', 'error' => trans('passwords.reset')], 200);
        } else {
            return response()->json(['status' => 'fail', 'error' => trans($response)], 400);
        }
    }*/
	public function reset(Request $request)
    {
		$validator = Validator::make($request->all(), [
			'token' => 'required',
			'password' => 'required|string|min:6|confirmed'
		], [
			'token|required' => 'User not found!'
		]);
		
		if ($validator->fails()) {
            return response()->json(['status' => 'fail',
                'error' => $validator->errors()], 400);
        }
		//dd($request->all());
		$token = $request->input('token');
		$password = $request->input('password');
		
		$findUser = User::where('token_to_reset_password', $token)->first();
		if ($findUser) {
			$findUser->token_to_reset_password = null;
			$findUser->password = Hash::make($password);
			$findUser->save();
            return response()->json([
				'status' => 'success',
                'message' => "Password Changed Successfully"
			], 200);
        } else {
            return response()->json(['status' => 'fail',
                'error' => 'Failed to update password'], 400);
        }
    }

    /**
     * Validate OTP.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function validateOTP(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, ['countryCode' => 'required', 'mobile' => 'required|exists:users,mobile', 'mtoken' => 'required'], ['exists' => 'Invalid :attribute']);
        if ($validator->fails()) {
            return response()->json(['status' => 'fail',
                'error' => $validator->errors()], 400);
        }
        $vt = DB::table(config('auth.passwords.users.table'))->where([
            'countryCode' => $input['countryCode'],
            'mobile' => $input['mobile'],
            'mtoken' => $input['mtoken']
        ])->first();
        if ($vt) {
            $user = User::where([
                'countryCode' => $input['countryCode'],
                'mobile' => $input['mobile']
            ])->first();
            DB::table(config('auth.passwords.users.table'))->where(['countryCode' => $input['countryCode'], 'mobile' => $input['mobile'], 'mtoken' => $input['mtoken']])->delete();
            $accessToken = $user->createToken('TaxiApp')->accessToken;
            return response()->json(['status' => 'success',
                'Authorization' => 'Bearer ' . $accessToken], 200);
        } else {
            return response()->json(['status' => 'fail',
                'error' => "Invalid OTP"], 400);
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function otpReset(Request $request)
    {
        $input = $request->all();
        $user = Auth::user();
        $validator = Validator::make($input, ['password' => 'required|confirmed|min:6'], ['exists' => 'Invalid :attribute']);
        if ($validator->fails()) {
            return response()->json(['status' => 'fail',
                'error' => $validator->errors()], 400);
        }
        $user->password = Hash::make($input['password']);
        $user->setRememberToken(Str::random(60));
        $user->save();
        return response()->json(['status' => 'success', 'message' => trans('passwords.reset')], 200);
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string|null $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token=null)
    {
        /*if (strstr(request()->server('HTTP_USER_AGENT'), 'iPhone') ||
            strstr(request()->server('HTTP_USER_AGENT'), 'iPad') ||
            strstr(request()->server('HTTP_USER_AGENT'), 'Android')
        ) {
            $token = substr(request()->fullUrl(), (strrpos (request()->fullUrl(), '/')+1));
			//dd('location: '.strtoupper(str_replace(' ', '', config('app.name'))).'://password/reset?token=' . $token);
            header('location: '.strtoupper(str_replace(' ', '', config('app.name'))).'://password/reset?token=' . $token);
            exit();
        } else {
            echo 'Action only possible through app!';
            exit();
        }*/
		
		if ( strstr(request()->server('HTTP_USER_AGENT'), 'iPhone') || strstr(request()->server('HTTP_USER_AGENT'), 'iPad') ) {
            $token = substr(request()->fullUrl(), (strrpos (request()->fullUrl(), '/')+1));
			//dd('location: '.strtoupper(str_replace(' ', '', config('app.name'))).'://password/reset?token=' . $token);
            header('location: '.strtoupper(str_replace(' ', '', config('app.name'))).'://password/reset?token=' . $token);
            exit();
        } elseif ( strstr(request()->server('HTTP_USER_AGENT'), 'Android') ) {
			//dd('location: https://www.destyapp.com/api/password/reset/' . $token);
			//header('location: https://www.destyapp.com/api/password/reset/' . $token);
			//echo "<script>window.location.href='https://www.destyapp.com/api/password/reset/".$token."'</script>";
			header('location: https://www.destyapp.com/api/password/reset/' . $token);
			exit();
		} else {
            echo 'Action only possible through app!';
            exit();
        }
    }
}
