<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Validator;
use App\Notifications\MailResetPasswordOTP;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Validate the email for the given request.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $input = $request->all();
        return Validator::make($input, ['email' => 'required|email|exists:users,email'], ['exists' => 'Invalid :attribute']);
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $validator = $this->validateEmail($request);
        if ($validator->fails()) {
            return response()->json(['status' => 'fail',
                'error' => $validator->errors()], 400);
        }
		$email = $request->input('email');

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        /*$response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        if ($isOTPSentToEmail == Password::RESET_LINK_SENT) {
            return response()->json(['status' => 'success',
                'message' => "Message Sent Successfully"], 200);
        } else {
            return response()->json(['status' => 'fail',
                'error' => 'Message Sending Failed'], 400);
        }*/
		
		$user = User::where('email', $email)->first();
		$user->otp_to_reset_password = rand(11, 99).mt_rand(1111, 9999);
		$user->token_to_reset_password = md5($user->id.$email.$user->otp_to_reset_password.env('ENCRYPTION_KEY'));
		$user->save();
		$user->notify(new MailResetPasswordOTP($user));
		return response()->json([
				'status' => 'success',
                'message' => "Message Sent Successfully",
				'vtoken'=>$user->token_to_reset_password
			], 200);
    }
	
	public function verifyOTP(Request $request)
    {
		$validator = Validator::make($request->all(), [
			'token' => 'required',
			'otp' => 'required'
		], [
			'token|required' => 'User not found!'
		]);
		
		if ($validator->fails()) {
            return response()->json(['status' => 'fail',
                'error' => $validator->errors()], 400);
        }
		$token = $request->input('token');
		$otp = $request->input('otp');
		
		$findUser = User::where('token_to_reset_password', $token)->where('otp_to_reset_password', $otp)->first();
		if ($findUser) {
			$findUser->token_to_reset_password = md5($findUser->id.time().env('ENCRYPTION_KEY'));
			$findUser->otp_to_reset_password = null;
			$findUser->save();
            return response()->json([
				'status' => 'success',
                'message' => "OTP Verified Successfully",
				'vtoken' => $findUser->token_to_reset_password
			], 200);
        } else {
            return response()->json(['status' => 'fail',
                'error' => 'Invalid OTP Provided'], 400);
        }
    }

    /**
     * Validate the email for the given request.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateMobile(Request $request)
    {
        $input = $request->all();
        return Validator::make($input, ['countryCode' => 'required', 'mobile' => 'required|exists:users,mobile'], ['exists' => 'Invalid :attribute']);
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendResetOTP(Request $request)
    {
        $validator = $this->validateMobile($request);
        if ($validator->fails()) {
            return response()->json(['status' => 'fail',
                'error' => $validator->errors()], 400);
        }
        $input = $request->all();
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $user = User::where(['countryCode' => $input['countryCode'], 'mobile' => $input['mobile']])->first();
        if ($user) {
            $mt = random_int(1000, 9999);
            DB::table(config('auth.passwords.users.table'))->where(['countryCode' => $input['countryCode'], 'mobile' => $input['mobile']])->delete();
            DB::table(config('auth.passwords.users.table'))->insert([
                'countryCode' => $input['countryCode'],
                'mobile' => $input['mobile'],
                'mtoken' => $mt
            ]);
            sendSms($input['countryCode'] . $input['mobile'], 'Your OTP is: ' . $mt . " It is valid for 30 minutes.");
            $accessToken = $user->createToken(config('constant.PASSPORT_TOKEN_KEY'))->accessToken;
            return response()->json([
                'status' => 'success',
                'Authorization' => 'Bearer ' . $accessToken,
                'message' => "Message Sent Successfully"], 200);
        } else {
            return response()->json([
                'status' => 'fail',
                'message' => "Invalid Mobile Number"], 400);
        }
    }
}
