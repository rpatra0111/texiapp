<?php

namespace App\Http\Controllers\Api\Auth;

use App\Mail\ConfirmationMail;
use App\User;
use App\Http\Controllers\Controller;
use App\UserVerification;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Stripe\Customer;
use Stripe\Stripe;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['verifyPhone', 'resendOTP']);
    }

    /**
     * Handles incoming Driver Registration Requests
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function register()
    {
        $input = request()->all();
        $rules = [
            'firstName' => 'required|string',
            'lastName' => 'required|string',
            'gender' => [
                'nullable',
                Rule::in(["M", "F"])
            ],
            'addressLine1' => 'string',
            'addressLine2' => '',
            'city' => 'string',
            'state' => 'string',
            'zipcode' => 'numeric',
            'email' => 'required|string|email',
            'countryCode' => 'string',
            'mobile' => 'required|numeric',
            'password' => 'required|string|min:6',
            'userType' => [
                'required',
                Rule::in(["D"])
            ],
            'fcmToken' => 'required',
            'deviceId' => 'required',
            'deviceType' => 'required|in:I,A'
        ];
        $messages = [
            'unique' => "This :attribute is already registered"
        ];
        $validator = Validator::make($input, $rules, $messages);
        $validator->after(function ($validator) use ($input) {
            if (User::where(['email' => $input['email'], 'userType' => $input['userType']])->whereIn('status', ['Y', 'P', 'MP', 'EP'])->count() > 0) {
                $validator->errors()->add('email', 'This email is already registered');
            }
            if (User::where(['countryCode' => $input['countryCode'], 'mobile' => $input['mobile'], 'userType' => $input['userType']])->whereIn('status', ['Y', 'P', 'MP', 'EP'])->count() > 0) {
                $validator->errors()->add('mobile', 'This mobile number is already registered');
            }
        });
        if ($validator->fails()) {
            $response = [
                'status' => 'fail',
                'errors' => $validator->errors()
            ];
            return response()->json($response, 400);
        }
        $input['password'] = Hash::make($input['password']);
        $input['userType'] = 'D';
        $input['status'] = 'Y';
        $input['completion'] = 'N';
        $input['driverApproval'] = 'N';
        $user = User::create($input);
        $accessToken = $user->createToken(config('constant.PASSPORT_TOKEN_KEY'))->accessToken;
        $user->UserTokens()->updateOrCreate([
            'deviceId' => $input['deviceId'],
            'flag' => $input['deviceType']
        ], [
            'fcmToken' => $input['fcmToken'],
        ]);
        /*$mt=random_int (1000,9999);
        $et=str_random(30);
        $user->UserVerify()->create([
            'eVerifyCode' => $et,
            'mVerifyCode' => $mt
        ]);
        Mail::to($user->email)->send(new ConfirmationMail($user, $confirm_link = url('/confirm/'.$user->id.'/'.$et)));
        sendSms($user->countryCode.$user->mobile,'Your OTP is: '.$mt." It is valid for 30 minutes.");*/
        return response()->json([
            'status' => 'success',
            'message' => 'Registration Successful',
            'Authorization' => 'Bearer ' . $accessToken
        ], 200);
    }

    /**
     * Handles incoming Passenger Registration Requests
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function passengerRegister()
    {
        $user = Auth::user();
        $input = request()->all();
        $rules = [
            'firstName' => 'required|string',
            'lastName' => 'required|string',
            'email' => 'required|string|email',
            'password' => 'required|string|min:6',
            'fcmToken' => 'required',
            'deviceId' => 'required',
            'deviceType' => 'required|in:I,A'
        ];
        $messages = [
            'unique' => "This :attribute is already registered"
        ];
        $validator = Validator::make($input, $rules, $messages);
        $validator->after(function ($validator) use ($input) {
            if (User::where(['email' => $input['email'], 'userType' => 'P'])->whereIn('status', ['Y', 'P', 'MP', 'EP'])->count() > 0) {
                $validator->errors()->add('email', 'This email is already registered');
            }
        });
        if ($validator->fails()) {
            $response = [
                'status' => 'fail',
                'errors' => $validator->errors()
            ];
            return response()->json($response, 400);
        }
        $user->firstName = $input['firstName'];
        $user->lastName = $input['lastName'];
        $user->email = $input['email'];
        $user->password = Hash::make($input['password']);
        $user->completion = 'Y';
        $user->save();
        $user->UserTokens()->updateOrCreate([
            'deviceId' => $input['deviceId'],
            'flag' => $input['deviceType']
        ], [
            'fcmToken' => $input['fcmToken'],
        ]);
        /*$mt=random_int (1000,9999);
        $et=str_random(30);
        $user->UserVerify()->create([
            'eVerifyCode' => $et,
            'mVerifyCode' => $mt
        ]);
        Mail::to($user->email)->send(new ConfirmationMail($user, $confirm_link = url('/confirm/'.$user->id.'/'.$et)));
        sendSms($user->countryCode.$user->mobile,'Your OTP is: '.$mt." It is valid for 30 minutes.");*/
        return response()->json([
            'status' => 'success',
            'message' => 'Registration Successful'
        ], 200);
    }

    /**
     * Send OTP to Given Phone Number
     *
     * @return \App\User
     */
    public function sendOTP()
    {
        $input = request()->all();
        $rules = [
            'countryCode' => 'required|string|max:5',
            'mobile' => 'required|numeric'
        ];
        $messages = [
            'unique' => "This :attribute is already registered"
        ];
        $validator = Validator::make($input, $rules, $messages);
        $validator->after(function ($validator) use ($input) {
            if (User::where(['countryCode' => $input['countryCode'], 'mobile' => $input['mobile'], 'userType' => 'P', 'status' => 'Y', 'completion' => 'Y'])->count() > 0) {
                $validator->errors()->add('mobile', 'This mobile number is already registered');
            }
        });
        if ($validator->fails()) {
            $response = [
                'status' => 'fail',
                'errors' => $validator->errors()
            ];
            return response()->json($response, 400);
        }
        Stripe::setApiKey(config('constant.STRIPE_SECRET_KEY'));
        $inDat = $input;
        $inDat['userType'] = 'P';
        $inDat['status'] = 'P';
        $inDat['completion'] = 'N';
        $inDat['stripeCustomerId'] = Customer::create(array())->id;
        $user = User::updateOrCreate(['countryCode' => $input['countryCode'], 'mobile' => $input['mobile'], 'userType' => 'P'], $inDat);
        $mt = random_int(1000, 9999);
        $et = str_random(30);
        $user->UserVerify()->delete();
        $user->UserVerify()->create([
            'eVerifyCode' => $et,
            'mVerifyCode' => $mt
        ]);
        $smsRes = sendSms($input['countryCode'] . $input['mobile'], 'Your OTP is: ' . $mt . " It is valid for 30 minutes.");
        if ($smsRes['status'] == 'fail') {
            $user->UserVerify()->delete();
            $user->delete();
            return response()->json([
                'status' => 'fail',
                'errors' => 'The number ' . $input['countryCode'] . $input['mobile'] . ' is not a valid phone number'
            ], 400);
        }
        $accessToken = $user->createToken(config('constant.PASSPORT_TOKEN_KEY'))->accessToken;
        return response()->json([
            'status' => 'success',
            'message' => 'An OTP has been sent to your number.',
            'Authorization' => 'Bearer ' . $accessToken,
        ], 200);
    }

    /**
     * Verify Phone Number
     *
     * @return \App\User
     */
    public function verifyPhone()
    {
        $input = request()->all();
        $rules = [
            'mVerifyCode' => 'required|numeric'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $response = [
                'status' => 'fail',
                'errors' => $validator->errors()
            ];
            return response()->json($response, 400);
        }
        $user = Auth::user();
        $v = $user->UserVerify()->first();
        if ($v->mVerifyCode == $input['mVerifyCode']) {
            $user->status = 'Y';
            $user->save();
            $user->UserVerify()->delete();
//            $user->UserVerify()->update(['mVerifyCode' => null]);
            return response()->json([
                'status' => 'success',
                'message' => "Successfully Verified"
            ], 200);
        }
        return response()->json([
            'status' => 'fail',
            'message' => "Invalid Code"
        ], 400);
    }

    /**
     * Verify Phone Number
     *
     * @return \App\User
     */
    public function resendOTP()
    {
        $user = Auth::user();
        // if ($user->status == 'Y') 
        // {
        //     return response()->json([
        //         'status' => 'fail',
        //         'message' => "Account Already Activated"
        //     ], 400);
        // }
        // $v = $user->UserVerify()->first();
        $mt = random_int(1000, 9999);
        
            DB::table(config('auth.passwords.users.table'))->where(['countryCode' => $user->countryCode, 'mobile' => $user->mobile])->delete();
            DB::table(config('auth.passwords.users.table'))->insert([
                'countryCode' => $user->countryCode,
                'mobile' => $user->mobile,
                'mtoken' => $mt
            ]);
        // $v->mVerifyCode
        sendSms($user->countryCode . $user->mobile, 'Your OTP is: ' . $mt . " It is valid for 30 minutes.");
        $accessToken = $user->createToken(config('constant.PASSPORT_TOKEN_KEY'))->accessToken;
        return response()->json([
            'status' => 'success',
            'Authorization' => 'Bearer ' . $accessToken,
            'message' => 'An OTP has been sent to your number.',
            'OTP' => $mt
        ], 200);
    }
}
