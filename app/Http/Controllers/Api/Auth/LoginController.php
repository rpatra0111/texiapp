<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login()
    {
        $input = request()->all();

        $rules = [
            'userLoginId' => 'required',
            'userType' => 'required',
            'password' => 'required',
            'fcmToken' => 'required',
            'deviceId' => 'required',
            'deviceType' => 'required|in:I,A'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $response = [
                'status' => 'fail',
                'errors' => $validator->errors()
            ];
            return response()->json($response, 400);
        }
        $user = User::where('userType', '=', $input['userType'])
            ->where(function ($q) use ($input) {
                $q->where(function ($query) use ($input) {
                    $query->where('email', '=', $input['userLoginId'])
                        ->where('email', '!=', null);
                })
                    ->orWhere(function ($query) use ($input) {
                        $query->where('mobile', '=', $input['userLoginId'])
                            ->where('mobile', '!=', 0);
                    });
            })->first();
        if ($user !== null) {
            if (Hash::check($input['password'], $user->password)) {
                if ($user->status == 'N') {
                    $response = [
                        'status' => 'fail',
                        'errors' => 'Account is blocked.'
                    ];
                    return response()->json($response, 400);
                }
                if ($user->status != 'Y') {
                    $response = [
                        'status' => 'fail',
                        'errors' => 'Account is not activated.'
                    ];
                    return response()->json($response, 400);
                }
                if ($user->userType != $input['userType']) {
                    $response = [
                        'status' => 'fail',
                        'errors' => 'Invalid Login'
                    ];
                    return response()->json($response, 400);
                }
                $dat = collect($user->UserTokens()->where('deviceId', '<>', $input['deviceId'])->get())->toArray();
                $fcmTokensI = [];
                $fcmTokensA = [];
                foreach ($dat as $key => $values) {
                    if ($values['flag'] == 'A')
                        $fcmTokensA[] = $values['fcmToken'];
                    if ($values['flag'] == 'I')
                        $fcmTokensI[] = $values['fcmToken'];
                }
                if (!empty($fcmTokensA))
                    sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'logout', 'userType' => $user->userType, 'title' => config('constant.APP_NAME'), 'body' => 'Logging Out']]);
                if (!empty($fcmTokensI))
                    sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'logout', 'userType' => $user->userType]], ['title' => config('constant.APP_NAME'), 'body' => 'Logging Out']);
                $user->UserTokens()->where('deviceId', '<>', $input['deviceId'])->delete();
                $user->UserTokens()->updateOrCreate([
                    'deviceId' => $input['deviceId'],
                    'flag' => $input['deviceType']
                ], [
                    'fcmToken' => $input['fcmToken'],
                ]);
                $accessToken = $user->createToken(config('constant.PASSPORT_TOKEN_KEY'))->accessToken;
                $val = User::where('id', $user->id)->with(['UserCars' => function ($query) {
                    $query->with([
                        'UserCarColour' => function ($query) {
                            $query->select('id', 'colour', 'hexCode');
                        },
                        'UserTaxis.taxiType'
                    ]);
                }])->first();
                return response()->json([
                    'status' => 'success',
                    'user' => $val,
                    'Authorization' => 'Bearer ' . $accessToken,
                    'profile_img_link' => asset_url("uploads/profilePictures/")
                ], 200);
            } else {
                $response = [
                    'status' => 'fail',
                    'errors' => 'The Password doesn\'t match'
                ];
                return response()->json($response, 400);
            }
        } else {
            $response = [
                'status' => 'fail',
                'errors' => 'No user is registered with these credentials'
            ];
            return response()->json($response, 400);
        }
    }

    public function logout()
    {
        $input = request()->all();
        $rules = [
            'deviceId' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $response = [
                'status' => 'fail',
                'errors' => $validator->errors()
            ];
            return response()->json($response, 400);
        }
        $user = Auth::user();
        $user->UserTokens()->where(['deviceId' => $input['deviceId']])->delete();
        $user->UserTaxis()->update(['forHire' => 'N', 'available' => 'N']);
        /*$value = request()->bearerToken();00
        $id= (new Parser())->parse($value)->getHeader('jti');
        $token=  DB::table('oauth_access_tokens')
            ->where('id', '=', $id)
            ->update(['revoked' => true]);*/

        request()->user()->token()->revoke();
//        Auth::guard('api')->logout();
        return response()->json([
            'status' => 'success',
            'message' => "Logged Out Successfully"
        ], 200);
    }
}
