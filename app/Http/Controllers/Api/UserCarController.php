<?php

namespace App\Http\Controllers\Api;

use App\UserCar;
use App\UserTaxi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserCarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $input_data = $request->all();
        $rules = [
            'taxiType' => 'required',
            'numberPlate' => 'required',
            'make' => 'required',
            'model' => 'required',
            'colour' => 'required',
            'year' => 'required'
        ];
        $validator = Validator::make($input_data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ]);
        }
        $userCar = $user->UserCars()->updateOrCreate([
            'numberPlate' => $input_data['numberPlate'],
            'make' => $input_data['make'],
            'model' => $input_data['model'],
            'colour' => $input_data['colour'],
            'year' => $input_data['year']
        ]);
        UserTaxi::where('carId', $userCar->id)->orWhere('userId', $user->id)->delete();
        if (is_array($input_data['taxiType'])) {
            foreach ($input_data['taxiType'] as $key => $value) {
                $user->UserTaxis()->create([
                    'taxiTypeId' => $value,
                    'carId' => $userCar->id
                ]);
            }
        } else {
            $user->UserTaxis()->create([
                'taxiTypeId' => $input_data['taxiType'],
                'carId' => $userCar->id
            ]);
        }
        return response()->json([
            'status' => 'success',
            'message' => "Car Details Added Successfully"
        ]);
    }

    /**
     * Display the specified resource.
     *
     */
    public function show()
    {
        return response()->json([
            'status' => 'success',
            'car' => Auth::user()->UserCars()->with([
                'UserCarColour' => function ($query) {
                    $query->select('id', 'colour', 'hexCode');
                },
                'UserTaxis.taxiType'
            ])->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserCar $userCar
     * @return \Illuminate\Http\Response
     */
    public function edit(UserCar $userCar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\UserCar $userCar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        $input_data = $request->all();
        $rules = [
            'carId' => 'required|exists:user_cars,id',
            'taxiType' => 'required',
            'numberPlate' => 'required',
            'make' => 'required',
            'model' => 'required',
            'colour' => 'required',
            'year' => 'required'
        ];
        $validator = Validator::make($input_data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ]);
        }
        $car = UserCar::find($input_data['carId']);
        if ($car) {
            $car->numberPlate = $input_data['numberPlate'];
            $car->make = $input_data['make'];
            $car->model = $input_data['model'];
            $car->colour = $input_data['colour'];
            $car->year = $input_data['year'];
            $car->save();
            UserTaxi::where('carId', $car->id)->orWhere('userId', $user->id)->delete();
            if (is_array($input_data['taxiType'])) {
                foreach ($input_data['taxiType'] as $key => $value) {
                    $user->UserTaxis()->create([
                        'taxiTypeId' => $value,
                        'carId' => $car->id
                    ]);
                }
            } else {
                $user->UserTaxis()->create([
                    'taxiTypeId' => $input_data['taxiType'],
                    'carId' => $car->id
                ]);
            }
            return response()->json([
                'status' => 'success',
                'message' => "Car Details Updated Successfully"
            ]);
        } else {
            return response()->json([
                'status' => 'fail',
                'message' => "Invalid Car Id"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserCar $userCar
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserCar $userCar)
    {
        //
    }
}
