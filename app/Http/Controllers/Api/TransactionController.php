<?php

namespace App\Http\Controllers\Api;

use App\TaxiRide;
use App\Transaction;
use App\User;
use App\UserTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Error\ApiConnection;
use Stripe\Error\Base;
use Stripe\Error\Card;
use Stripe\Error\InvalidRequest;
use Stripe\Error\RateLimit;
use Stripe\Refund;
use Stripe\Stripe;
use Stripe\Token;
use Validator;

class TransactionController extends Controller
{
    public function __construct()
    {
        Stripe::setApiKey(config('constant.STRIPE_SECRET_KEY'));
    }

    public function stripePay()
    {
        $user = Auth::user();
        $input = request()->all();
        $rules = [
            'stripeToken' => 'nullable',
            'stripeCardId' => 'nullable',
            'amount' => 'required',
            'description' => 'nullable|string'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ], 400);
        }
        $customerId = $user->stripeCustomerId;
        if (isset($input['stripeToken'])) {
            $card = Token::retrieve($input['stripeToken'])->card;
            $cardId = $card->id;
            if ($user->stripeCustomerId == null) {
                $customer = Customer::create(array(
                    'source' => $input['stripeToken']
                ));
                $customerId = $customer->id;
                $user->stripeCustomerId = $customer->id;
                $user->save();
            } else {
                $customer = Customer::retrieve($customerId);
                $cards = collect($customer->sources->all(array(
                    "object" => "card"
                )))->toArray()['data'];
                $flag = false;
                foreach($cards as $cardData)
                {
                    if ($cardData['fingerprint'] == $card->fingerprint) {
                        $flag = true;
                        $cardId = $cardData['id'];
                    }
                }
                if (!$flag) {
                    $customer->sources->create(array("source" => $input['stripeToken']));
                }
            }
        }
        elseif(isset($input['stripeCardId'])) {
            $cardId = $input['stripeCardId'];
        }
        else {
            return response()->json([
                'status' => 'fail',
                'errors' => "Invalid Request"
            ], 400);
        }
        try {
            $oldCharges = $user->UserTransactions()->where('paymentStatus', 'P')->whereNotNull('gatewayTransactionId')->get();
            if (!empty($oldCharges)) {
                foreach($oldCharges as $oldCharge) {
                    Refund::create([
                        "charge" => $oldCharge->gatewayTransactionId
                    ]);
                }
                $user->UserTransactions()->where('paymentStatus', 'P')->whereNotNull('gatewayTransactionId')->delete();
            }
            // Charge the Customer instead of the card
            $charge = Charge::create(array(
                    'customer' => $customerId,
                    'source' => $cardId,
                    'currency' => 'USD',
                    'amount' => (int)($input['amount']*100),
                    'description' => 'Recharge Wallet',
                    'capture' => false)
            );
            if ($charge['status'] == 'succeeded') {
                $user->UserTransactions()->create([
                    'amount' => $input['amount'],
                    'gatewayTransactionId' => $charge['id'],
                    'transactionType' => 'D',
                    'description' => "Charging for the ongoing ride",
                    'paymentStatus' => 'P'
                ]);
                return response()->json([
                    'status' => 'success',
                    'message' => 'Charge Created Successfully!',
                    'balance' => User::find(Auth::id())->balance
                ], 200);
                /*$description = "Successfully Recharged Wallet with $" . $input['amount'] . ", transaction ref number is " . $charge['id'];
                $user->Transactions()->create([
                    'gatewayTransactionId' => $charge['id'],
                    'gatewayResponse' => $charge,
                    'amount' => $input['amount'],
                    'paidUsing' => 'S',
                    'transactionType' => 'C',
                    'description' => $description
                ]);
                $user->UserTransactions()->create([
                    'amount' => $input['amount'],
                    'transactionType' => 'C',
                    'description' => $description
                ]);
                User::where('id', '=', $user->id)
                    ->increment('balance', $input['amount']);
                return response()->json([
                    'status' => 'success',
                    'message' => 'Recharged Successfully',
                    'balance' => User::find(Auth::id())->balance
                ], 200);*/

            } else {
                return response()->json([
                    'status' => 'fail',
                    'errors' => "Transaction Failed"
                ], 400);
            }
        } catch (Card $e) {
            $body = $e->getJsonBody();
            return response()->json([
                'status' => 'fail',
                'errors' => $body['error']['message']
            ], 400);
        } catch (RateLimit $e) {
            $body = $e->getJsonBody();
            return response()->json([
                'status' => 'fail',
                'errors' => $body['error']['message']
            ], 400);
        } catch (InvalidRequest $e) {
            $body = $e->getJsonBody();
            return response()->json([
                'status' => 'fail',
                'errors' => $body['error']['message']
            ], 400);
        } catch (ApiConnection $e) {
            $body = $e->getJsonBody();
            return response()->json([
                'status' => 'fail',
                'errors' => $body['error']['message']
            ], 400);
        } catch (Base $e) {
            $body = $e->getJsonBody();
            return response()->json([
                'status' => 'fail',
                'errors' => $body['error']['message']
            ], 400);
        }
    }

    public function transactionHistory()
    {
        $user = Auth::user();
        return response()->json([
            'status' => 'success',
            'transactions' => collect($user->UserTransactions()->orderBy('created_at', 'desc')->get())->toArray(),
            'balance' => $user->balance
        ], 200);
    }

    public function cashOut()
    {
        $user = Auth::user();
        if ($user->balance > 0) {
            $cashouts = collect($user->UserCashouts()->where(['paidStatus' => 'P'])->get())->toArray();
            if (count($cashouts) == 0) {
                $user->UserCashouts()->create([
                    'userId' => $user->id,
                    'currencyCode' => '$',
                    'amount' => $user->balance
                ]);
                $user->balance = 0.00;
                $user->save();
                return response()->json([
                    'status' => 'success',
                    'message' => "CashOut Requested Successfully"
                ], 200);
            } else {
                return response()->json([
                    'status' => 'fail',
                    'errors' => "A CashOut Request is already pending"
                ], 400);
            }
        } else {
            return response()->json([
                'status' => 'fail',
                'errors' => "Insufficient balance to request cashout"
            ], 400);
        }
    }

    public function payTips()
    {
        $passenger = Auth::user();
        $input = request()->all();
        $rules = [
            'stripeToken' => 'nullable',
            'stripeCardId' => 'nullable',
            'description' => 'nullable|string',
            'taxiRideId' => 'required',
            'tipAmount' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ], 400);
        }
        $taxiRide = collect(TaxiRide::where('id', '=', $input['taxiRideId'])->with('UserTaxi')->first())->toArray();
        $passengerId = $taxiRide['passengerId'];
        $driverId = $taxiRide['user_taxi']['userId'];
        $customerId = $passenger->stripeCustomerId;
        if (isset($input['stripeToken'])) {
            $card = Token::retrieve($input['stripeToken'])->card;
            $cardId = $card->id;
            if ($passenger->stripeCustomerId == null) {
                $customer = Customer::create(array(
                    'source' => $input['stripeToken']
                ));
                $customerId = $customer->id;
                $passenger->stripeCustomerId = $customer->id;
                $passenger->save();
            } else {
                $customer = Customer::retrieve($customerId);
                $cards = collect($customer->sources->all(array(
                    "object" => "card"
                )))->toArray()['data'];
                $flag = false;
                foreach($cards as $cardData)
                {
                    if ($cardData['fingerprint'] == $card->fingerprint) {
                        $flag = true;
                        $cardId = $cardData['id'];
                    }
                }
                if (!$flag) {
                    $customer->sources->create(array("source" => $input['stripeToken']));
                }
            }
        }
        elseif(isset($input['stripeCardId'])) {
            $cardId = $input['stripeCardId'];
        }
        else {
            return response()->json([
                'status' => 'fail',
                'errors' => "Invalid Request"
            ], 400);
        }
        try {
            // Charge the Customer instead of the card
            $charge = Charge::create(array(
                    'customer' => $customerId,
                    'source' => $cardId,
                    'currency' => 'USD',
                    'amount' => (int)($input['tipAmount']*100),
                    'description' => 'Paid For Tip',
                    'expand' => array('balance_transaction'))
            );
            if ($charge['status'] == 'succeeded') {
                UserTransaction::create([
                    'userId' => $driverId,
                    'taxiRideId' => $input['taxiRideId'],
                    'amount' => $charge['balance_transaction']['net']/100,
                    'transactionType' => 'C',
                    'description' => "Received as tip for the ride inclusive of standard charges."
                ]);
                UserTransaction::create([
                    'userId' => $passengerId,
                    'taxiRideId' => $input['taxiRideId'],
                    'amount' => $input['tipAmount'],
                    'transactionType' => 'D',
                    'gatewayResponse' => $charge,
                    'description' => "Paid as tip for the ride."
                ]);
                User::where('id', '=', $driverId)->increment('balance', $charge['balance_transaction']['net']/100);
                TaxiRide::where('id', '=', $input['taxiRideId'])->update(array('tips' =>$charge['balance_transaction']['net']/100));
                return response()->json([
                    'status' => 'success',
                    'message' => "Tipped the Driver Successfully!"
                ], 200);

            } else {
                return response()->json([
                    'status' => 'fail',
                    'errors' => "Transaction Failed"
                ], 400);
            }
        } catch (Card $e) {
            $body = $e->getJsonBody();
            return response()->json([
                'status' => 'fail',
                'errors' => $body['error']['message']
            ], 400);
        } catch (RateLimit $e) {
            $body = $e->getJsonBody();
            return response()->json([
                'status' => 'fail',
                'errors' => $body['error']['message']
            ], 400);
        } catch (InvalidRequest $e) {
            $body = $e->getJsonBody();
            return response()->json([
                'status' => 'fail',
                'errors' => $body['error']['message']
            ], 400);
        } catch (ApiConnection $e) {
            $body = $e->getJsonBody();
            return response()->json([
                'status' => 'fail',
                'errors' => $body['error']['message']
            ], 400);
        } catch (Base $e) {
            $body = $e->getJsonBody();
            return response()->json([
                'status' => 'fail',
                'errors' => $body['error']['message']
            ], 400);
        }
        /*if ($user->balance > 0 && $user->balance >= $input['tipAmount']) {
            $taxiRide = collect(TaxiRide::where('id', '=', $input['taxiRideId'])->with('UserTaxi')->first())->toArray();
            $passengerId = $taxiRide['passengerId'];
            $driverId = $taxiRide['user_taxi']['userId'];
            UserTransaction::create([
                'userId' => $driverId,
                'taxiRideId' => $input['taxiRideId'],
                'amount' => $input['tipAmount'],
                'transactionType' => 'C',
                'description' => "Received as tip for the ride."
            ]);
            UserTransaction::create([
                'userId' => $passengerId,
                'taxiRideId' => $input['taxiRideId'],
                'amount' => $input['tipAmount'],
                'transactionType' => 'D',
                'description' => "Paid as tip for the ride."
            ]);
            User::where('id', '=', $driverId)->increment('balance', $input['tipAmount']);
            User::where('id', '=', $passengerId)->decrement('balance', $input['tipAmount']);
            TaxiRide::where('id', '=', $input['taxiRideId'])->update(array('tips' => $input['tipAmount']));
            return response()->json([
                'status' => 'success',
                'message' => "Tipped the Driver Successfully!"
            ], 200);
        }
        else {
            return response()->json([
                'status' => 'fail',
                'balnce' => $user->balance,
                'errors' => "Insufficient balance to pay tips, please recharge your account"
            ], 400);
        }*/
    }

    public function cardListing()
    {
        $user = Auth::user(); $cards = null;
        $customerId = $user->stripeCustomerId;
        if ($user->stripeCustomerId == null) {
            $customer = Customer::create();
            $customerId = $customer->id;
            $user->stripeCustomerId = $customer->id;
            $user->save();
        }
        $customer = Customer::retrieve($customerId);
        if($customer){
            $cards = collect($customer->sources->all(array(
                "object" => "card"
            )))->toArray()['data'];
        }
        
        return response()->json([
            'status' => 'success',
            'cards' => $cards
        ], 200);
    }
}
