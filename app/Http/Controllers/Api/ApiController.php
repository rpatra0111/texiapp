<?php

namespace App\Http\Controllers\Api;

use App;
use App\Coupon;
use App\Language;
use App\TaxiType;
use App\VerificationDocument;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BankName;
use App\TaxiTypeRate;
use App\UserCarColour;
use App\HelpQueryTopic;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except(['recommendedPrice', 'couponValidate']);
    }

    public function taxiTypeList()
    {
        return response()->json([
            'status' => 'success',
            'taxiType' => collect(TaxiType::all())->toArray(),
            'taxi_img_link' => asset_url("uploads/taxiPictures/")
        ], 201);
    }

    public function languageList()
    {
        return response()->json([
            'status' => 'success',
            'languages' => collect(Language::all())->toArray()
        ], 201);
    }

    public function docTypeList()
    {
        return response()->json([
            'status' => 'success',
            'docType' => collect(VerificationDocument::all())->toArray()
        ], 201);
    }

    public function carColourList()
    {
        $selectedId = config('constant.SELECTED_COLOR');
        return response()->json([
            'status' => 'success',
            'colours' => collect(UserCarColour::whereIn('id', $selectedId)->get())->toArray()
        ], 201);
    }

    public function recommendedPrice(Request $request)
    {
        $user = Auth::user();
        $input = $request->all();
        $rules = [
            'distance' => 'required',
            'time' => 'required',
            'city' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $response = [
                'status' => 'fail',
                'errors' => $validator->errors()
            ];
            return response()->json($response);
        }
        $time = $input['time'] / 60;
        $distance = $input['distance'] / 1000;
        $taxiType = collect(TaxiType::all())->toArray();
        $cost = collect(TaxiTypeRate::whereHas('City', function ($query) use ($input) {
            $query->where('cityName', 'LIKE', $input['city']);
        })->get())->toArray();
        $rp = [];
        foreach ($taxiType as $key => $value) {
            $p = $value;
            if (count($cost) > 0) {
                foreach ($cost as $k => $v) {
                    if ($value['id'] == $v['taxiTypeId']) {
                        $p['fenceDistance'] = $v['fenceDistance'];
                        $p['baseCost'] = $v['baseCost'];
                        $p['perKmCost'] = $v['perKmCost'];
                        $p['timeCharge'] = $v['timeCharge'];
                        $p['minCharge'] = $v['minCharge'];
                        break;
                    }
                }
            }
            if ($distance > $p['fenceDistance']) {
                $t = round(($p['baseCost'] + ((($distance - $p['fenceDistance']) / $p['fenceDistance']) * $p['perKmCost']) + $time * $p['timeCharge']));
                if ($t < $p['minCharge']) {
                    $t = $p['minCharge'];
                }
                $rp[$p['id']] = [
                    'price' => (float)$t,
                    'name' => $p['name']
                ];
            }
            if ($distance <= $p['fenceDistance']) {
                $t = round($p['baseCost'] + ($time * $p['timeCharge']));
                if ($t < $p['minCharge']) {
                    $t = $p['minCharge'];
                }
                $rp[$p['id']] = [
                    'price' => (float)$t,
                    'name' => $p['name']
                ];
            }
        }
        $ob = 0;
        if ($user->balance < 0.00)
            $ob = $user->balance;
        return response()->json([
            'status' => 'success',
            'price' => $rp,
            'time' => $time,
            'balanceDue' => $ob,
            'balance' => $user->balance
        ]);
    }

    public function bankList(Request $request)
    {
        $locale = $request->input('locale') ? $request->input('locale') : config('app.DEFAULT_LANGUAGE_CODE');
        $bankList = BankName::whereNotIn('status', ['2'])
            ->translatedIn($locale)->get();
        $bankListArray = array();
        $counter = 0;
        if (count($bankList) > 0) {
            foreach ($bankList as $key => $value) {
                $bankListArray[$counter]['id'] = $value->id;
                $bankListArray[$counter]['name'] = $value->bank_name;
                $bankListArray[$counter]['code'] = $value->code;
                $counter++;
            }
        }
        $response['bankList'] = $bankListArray;
        return response()->json($response);
    }

    public function topicList(Request $request)
    {
        $locale = $request->input('locale') ? $request->input('locale') : config('app.DEFAULT_LANGUAGE_CODE');
        $topicList = HelpQueryTopic::whereNotIn('status', ['2'])->translatedIn($locale)->get();
        $topicListArray = array();
        $counter = 0;
        if (count($topicList) > 0) {
            foreach ($topicList as $key => $value) {
                $topicListArray[$counter]['id'] = $value->id;
                $topicListArray[$counter]['name'] = $value->topic_name;
                $topicListArray[$counter]['description'] = $value->description;
                $counter++;
            }
        }
        $response['topicList'] = $topicListArray;
        return response()->json($response);
    }

    public function couponList()
    {
        $coupons = collect(Coupon::where('status', '=', 'Y')->where('startTime', '<=', DB::raw('UTC_TIMESTAMP()'))->where('endTime', '>=', DB::raw('UTC_TIMESTAMP()'))->get())->toArray();
        return response()->json([
            'status' => 'success',
            'coupons' => $coupons
        ], 201);
    }

    public function couponValidate(Request $request)
    {
        $input = $request->all();
        $rules = [
            'couponCode' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ], 400);
        }
        $coupon = collect(Coupon::where('code', 'LIKE', $input['couponCode'])->where('status', '=', 'Y')->where('startTime', '<=', DB::raw('UTC_TIMESTAMP()'))->where('endTime', '>=', DB::raw('UTC_TIMESTAMP()'))->first())->toArray();
        if ($coupon && count($coupon) > 0) {
            return response()->json([
                'status' => 'success',
                'message' => "Coupon is Valid!",
                'coupon' => $coupon
            ], 201);
        } else {
            return response()->json([
                'status' => 'success',
                'message' => "Coupon Expired!"
            ], 201);
        }
    }

    public function appDeepLink()
    {
		if(strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPad'))
		{
			header('location: https://apps.apple.com/in/app/desty/id1436302195');
			// header('location: fuloop://resetpass?token='.$token);
        }
        else if(strstr($_SERVER['HTTP_USER_AGENT'],'Android'))
		{
            header('location: https://play.google.com/store/apps/details?id=com.technoexponent.taxiapp.passenger   ');
		}
		else{
			echo "Web work is under process. Please check in mobile.";
		}
		
	}

    public function test()
    {
        echo getStaticMap('22.569307342715355,88.43576185405254', '22.5691124,88.4309165');
    }
}
