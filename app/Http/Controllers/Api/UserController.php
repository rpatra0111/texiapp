<?php

namespace App\Http\Controllers\Api;

use App;
use App\UserBankAccount;
use App\UserQuery;
use App\UserRating;
use App\Notification;
use App\EmailTemplate;
use App\TaxiRide;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Mail\DocumentVerification;
use Validator;
use File;

class UserController extends Controller
{
    public function __construct()
    {
        //
    }

    public function editProfile(Request $request)
    {
        $user = Auth::user();
        $input = $request->all();
        $rules = [
            'firstName' => 'string|max:255',
            'lastName' => 'string|max:255',
            'email' => 'string|email',
            'password' => 'nullable|string|min:6',
            'addressLine1' => 'nullable|string',
            'addressLine2' => 'nullable|string',
            'city' => 'nullable|string',
            'state' => 'nullable|string',
            'zipcode' => 'nullable|string',
            'countryCode' => 'nullable',
            'mobile' => 'nullable|numeric',
            'gender' => [
                'nullable',
                Rule::in(["M", "F"])
            ],
            'taxiType' => 'nullable|exists:taxi_types,id',
            'language_id' => 'nullable|exists:languages,id',
            'ssn' => 'nullable'
        ];
        $validator = Validator::make($input, $rules);
        $validator->after(function ($validator) use ($input, $user) {
            if (isset($input['email'])) {
                if ($user->email != $input['email']) {
                    if (User::where(['email' => $input['email'], 'userType' => $user->userType])->whereIn('status', ['Y', 'P', 'MP', 'EP'])->count() > 0) {
                        $validator->errors()->add('email', 'This email is already registered');
                    }
                }
            }
            if (isset($input['countryCode']) && isset($input['mobile'])) {
                if ($user->countryCode != $input['countryCode'] && $user->mobile != $input['mobile']) {
                    if (User::where(['countryCode' => $input['countryCode'], 'mobile' => $input['mobile'], 'userType' => $user->userType])->whereIn('status', ['Y', 'P', 'MP', 'EP'])->count() > 0) {
                        $validator->errors()->add('mobile', 'This mobile number is already registered');
                    }
                }
            }
            if (isset($input['ssn'])) {
                if ($user->ssn != $input['ssn']) {
                    // $USValidator = new \Validate_US();
                    // if (!$USValidator->ssn((string)$input['ssn']))
                    //     $validator->errors()->add('ssn', 'This ssn is invalid');
                    // else
                        if (User::where(['ssn' => $input['ssn'], 'userType' => $user->userType])->count() > 0)
                            $validator->errors()->add('ssn', 'This ssn is already in use');
                }
            }
            
            if (isset($input['user_reference_code']) && $input['user_reference_code']!='') {
                if (User::where(['reference_code' => trim($input['user_reference_code'])])->count() == 0) {
                    $validator->errors()->add('user_reference_code', 'Invalid reference code');
                }
            }
        });
        if ($validator->fails()) {
            $response = [
                'status' => 'fail',
                'errors' => $validator->errors()
            ];
            return response()->json($response, 400);
        }

        if (isset($input['firstName']))
            $user->firstName = $input['firstName'];
        if (isset($input['lastName']))
            $user->lastName = $input['lastName'];
        if (isset($input['password']))
            $user->password = bcrypt($input['password']);
        if (isset($input['email']))
            $user->email = $input['email'];
        if (isset($input['addressLine1']))
            $user->addressLine1 = $input['addressLine1'];
        if (isset($input['addressLine2']))
            $user->addressLine2 = $input['addressLine2'];
        if (isset($input['city']))
            $user->city = $input['city'];
        if (isset($input['state']))
            $user->state = $input['state'];
        if (isset($input['zipcode']))
            $user->zipcode = $input['zipcode'];
        if (isset($input['countryCode']))
            $user->countryCode = $input['countryCode'];
        if (isset($input['mobile']))
            $user->mobile = $input['mobile'];
        if (isset($input['gender']))
            $user->gender = $input['gender'];
        if (isset($input['taxiType'])) {
            $user->UserTaxis()->delete();
            if (is_array($input['taxiType'])) {
                foreach ($input['taxiType'] as $key => $value) {
                    $user->UserTaxis()->create([
                        'taxiTypeId' => $value
                    ]);
                }
            } else {
                $user->UserTaxis()->create([
                    'taxiTypeId' => $input['taxiType']
                ]);
            }
        }
        if (isset($input['language_id']))
            $user->language_id = $input['language_id'];
        if (isset($input['ssn'])) {
            $user->ssn = $input['ssn'];
            if ($user->userType == 'D')
                $user->completion = 'P';
        }
        if ($user->userType == 'P')
            $user->completion = 'Y';
        $user->save();
        
        $generalSetting = App\GeneralSetting::find(1);
        
        if (isset($input['user_reference_code']) && $input['user_reference_code']!=''){
             $ref_user = User::where('reference_code',trim($input['user_reference_code']))->first();
             if($ref_user!=null){
                $ref_user->reword_point =  $ref_user->reword_point + $generalSetting->ref_reword_point;
                $ref_user->save();
                $ref_user->referenceUsers()->create(['referer_user_id'=>$user->id]);
             }
             unset($input['user_reference_code']);
        }

        if($user->reference_code == null){
            $user->reference_code = generateReferenceCode();
            $user->save();
        }
        return response()->json([
            'status' => 'success',
            'user' => collect(User::where('id', $user->id)->with(['UserTaxis.taxiType'])->first())->toArray(),
            'profile_img_link' => config('constant.APP_ASSET_URL')."uploads/profilePictures/"
        ], 201);
    }

    public function saveProfilePicture(Request $request)
    {
        $user = Auth::user();
        $input = $request->all();
        $rules = [
            'profilePicture' => 'required|mimes:jpeg,png,jpg,gif,svg|max:10000'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ]);
        }

        if ($request->hasFile('profilePicture')) {
            if ($request->file('profilePicture')->isValid()) {
                $path = 'uploads/profilePictures/';
                if ($user->profilePicture != null) {
                    if (Storage::disk('s3')->exists($path . $user->profilePicture)) {
                        Storage::disk('s3')->delete($path . $user->profilePicture);
                    }
                }
                $filePath = $request->file('profilePicture')->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $imageName = $imagePath[count($imagePath)-1];
                $user->profilePicture = $imageName;
                $user->save();
                return response()->json([
                    'status' => 'success',
                    'message' => "Profile Picture Changed Successfully",
                    'user' => collect(User::find(Auth::id())),
                    'profile_img_link' => config('constant.APP_ASSET_URL')."uploads/profilePictures/"
                ]);
            }
        }
        return response()->json([
            'status' => 'fail',
            'errors' => "Unable to update profile picture"
        ]);
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();
        $input = $request->all();
        $rules = [
            'oldPassword' => 'required',
            'password' => 'required|confirmed|min:6'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $response = [
                'status' => 'fail',
                'errors' => $validator->errors()
            ];
            return response()->json($response);
        }
        if (Hash::check($input['oldPassword'], $user->password)) {
            $user->password = Hash::make($input['password']);
            $user->save();
            return response()->json([
                'status' => 'success',
                'message' => "Password Changed Successfully"
            ]);
        } else {
            $response = [
                'status' => 'fail',
                'errors' => "Wrong Password"
            ];
            return response()->json($response);
        }
    }

    public function userDetails(Request $request)
    {
        $user = Auth::user();
        $val = User::where('id', $user->id)->with(['UserCars' => function ($query) {
            $query->with([
                'UserCarColour' => function ($query) {
                    $query->select('id', 'colour', 'hexCode');
                },
                'UserTaxis.taxiType'
            ]);
        }])->first();
        
        $generalSetting = App\GeneralSetting::find(1);

        return response()->json([
            'status' => 'success',
            'user' => $val,
            'Setting' => $generalSetting,
            'profile_img_link' => asset_url("uploads/profilePictures/"),
            
        ]);
    }

    public function verifyDocs(Request $request)
    {
        $user = Auth::user();
        if ($request->method() == 'POST') {
            $input = $request->all();
            $rules = [
                'verifyDoc' => 'required',
                'docType' => 'required|exists:verification_documents,id'
            ];
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => $validator->errors()
                ]);
            }
            $userDoc = $user->UserDocs()->where(['docType' => $input['docType']])->first();
            if ($request->file('verifyDoc')->isValid()) {
                $path = 'uploads/driverVerification/';
                if ($userDoc) {
                    if ($userDoc->userDocument != null) {
                        if (Storage::disk('s3')->exists($path . $userDoc->userDocument)) {
                            Storage::disk('s3')->delete($path . $userDoc->userDocument);
                        }
                    }
                }
                $filePath = $request->file('verifyDoc')->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $fileName = $imagePath[count($imagePath)-1];
            }
            else {
                return response()->json([
                    'status' => 'fail',
                    'errors' => "Invalid File"
                ]);
            }
            $user->UserDocs()->updateOrCreate(['docType' => $input['docType']], ['userDocument' => $fileName, 'docType' => $input['docType'], 'verifyStatus' => 'P']);
            if ($user->UserDocs()->count() == 3) {
                $user->driverApproval = 'P';
                $user->save();
                $body = EmailTemplate::find(9);
                $body->firstName = 'Admin';
                Mail::to('admin@destyapp.com')->send(new DocumentVerification($body));
            }
            return response()->json([
                'status' => 'success',
                'message' => "Uploaded Successfully"
            ]);
        }
        return response()->json([
            'status' => 'success',
            'userDocs' => $user->UserDocs()->with('docs')->get()
        ]);
    }

    public function bankAccountDetails(Request $request, $id = 0)
    {
        $user = Auth::user();
        $response['status'] = 'fail';
        if ($user) {
            $response['status'] = 'success';
            $bankDetails = array();
            if ($id) {
                $bankAccountDetails = UserBankAccount::where(['user_id' => Auth::id()])
                    ->where(['id' => $id])
                    ->whereNotIn('status', ['2'])
                    ->get();
            } else {
                $bankAccountDetails = UserBankAccount::where(['user_id' => Auth::id()])
                    ->whereNotIn('status', ['2'])
                    ->orderBy('is_default', 'DESC')
                    ->get();
            }
            $counter = 0;
            if (count($bankAccountDetails) > 0) {
                foreach ($bankAccountDetails as $key => $value) {
                    $bankDetails[$counter]['id'] = $value->id;
                    $bankDetails[$counter]['user_id'] = $value->user_id;
                    $bankDetails[$counter]['bank_name'] = $value->bank_name;
                    $bankDetails[$counter]['user_name_same_as_bank_ac'] = $value->user_name_same_as_bank_ac;
                    $bankDetails[$counter]['account_number'] = $value->account_number;
                    $bankDetails[$counter]['ifsc_code'] = $value->ifsc_code;
                    $bankDetails[$counter]['is_default'] = $value->is_default;
                    $bankDetails[$counter]['status'] = $value->status;
                    $bankDetails[$counter]['created_on'] = date('d-m-Y H:i a', strtotime($value->created_at));
                    $bankDetails[$counter]['last_updated_on'] = date('d-m-Y H:i a', strtotime($value->updated_at));
                    $counter++;
                }
            }
            $response['bankDetails'] = $bankDetails;
        }

        return response()->json($response);
    }

    public function updateBankAccount(Request $request)
    {
        $user = Auth::user();
        $response['status'] = 'fail';
        if ($user) {
            $authUserId = Auth::id();
            $input_data = $request->all();
            $rules = [
                'bank_name' => 'required',
                'user_name_same_as_bank_ac' => 'required|string',
                'account_number' => 'required',
                'ifsc_code' => 'required'
            ];
            $validator = Validator::make($input_data, $rules);
            if ($validator->fails()) {
                $response['errors'] = $validator->errors();
                //return response()->json($response);
            } else {
                $response['status'] = 'success';
                if ($request->input('id')) {
                    $userBankAccount = UserBankAccount::find($request->input('id'));
                    if (count($userBankAccount) > 0) {
                        UserBankAccount::where(['user_id' => $authUserId])->update(['is_default' => '0']);
                        $userBankAccount->bank_name = $request->input('bank_name');
                        $userBankAccount->user_name_same_as_bank_ac = $request->input('user_name_same_as_bank_ac');
                        $userBankAccount->account_number = $request->input('account_number');
                        $userBankAccount->ifsc_code = $request->input('ifsc_code');
                        $userBankAccount->updated_at = date('Y-m-d H:i:s');
                        $userBankAccount->is_default = '1';
                        $userBankAccount->save();
                        $response['message'] = 'Updated Successfully';
                    }
                } else {
                    UserBankAccount::where(['user_id' => $authUserId])->update(['is_default' => '0']);

                    $userBankAccount = new UserBankAccount;
                    $userBankAccount->user_id = $authUserId;
                    $userBankAccount->bank_name = $request->input('bank_name');
                    $userBankAccount->user_name_same_as_bank_ac = $request->input('user_name_same_as_bank_ac');
                    $userBankAccount->account_number = $request->input('account_number');
                    $userBankAccount->ifsc_code = $request->input('ifsc_code');
                    $userBankAccount->created_at = date('Y-m-d H:i:s');
                    $userBankAccount->is_default = '1';
                    $userBankAccount->save();
                    $response['message'] = 'Added Successfully';
                }
            }
        }

        return response()->json($response);
    }

    public function queryForHelp(Request $request)
    {
        $user = Auth::user();
        $response['status'] = 'fail';
        if ($user) {
            //echo "123"; exit;
            $authUserId = Auth::id();
            $input_data = $request->all();
            $rules = [
                'topic_id' => 'required',
                'message' => 'required|string'
            ];
            $validator = Validator::make($input_data, $rules);
            if ($validator->fails()) {
                //echo $validator->fails(); exit;
                $response['errors'] = $validator->errors();
                //return response()->json($response);
            } else {
                $response['status'] = 'success';

                $userQuery = new UserQuery;
                $userQuery->user_id = $authUserId;
                $userQuery->topic_id = $request->input('topic_id');
                $userQuery->message = $request->input('message');
                $userQuery->created_at = date('Y-m-d H:i:s');
                $userQuery->save();
            }
        }

        return response()->json($response);
    }

    public function userRating(Request $request)
    {
        $user = Auth::user();
        $input_data = $request->all();
        $rules = [
            'userId' => 'nullable'
        ];
        $validator = Validator::make($input_data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ]);
        }
        if (isset($input_data['userId'])) {
            $avgRating = UserRating::where('rateeId', '=', $input_data['userId'])->avg('rating');
        } else {
            $avgRating = UserRating::where('rateeId', '=', $user->id)->avg('rating');
        }
        return response()->json([
            'status' => 'success',
            'rating' => $avgRating
        ]);
    }

    public function notificationList()
    {
        $user = Auth::user();
        $notify = collect($user->Notifications()->orderBy('created_at', 'desc')->get())->toArray();
        foreach ($notify as $key => $values) {
            if ($values['notiType'] == 'TRC') {
                if (UserRating::where(['raterId' => $user->id, 'taxiRideId' => $values['taxiRideId']])->count() > 0) {
                    $notify[$key]['rated'] = true;
                } else {
                    $notify[$key]['rated'] = false;
                    $notify[$key]['taxiRide'] = collect(TaxiRide::where('id', '=', $values['taxiRideId'])
                        ->with(['Passenger' => function ($query) {
                            $query->select('id', 'firstName', 'lastName', 'profilePicture', 'balance');
                        }, 'UserTaxi.Driver' => function ($query) {
                            $query->select('id', 'firstName', 'lastName', 'profilePicture', 'balance');
                        }])->get())->toArray();
                }
            }
        }
        return response()->json([
            'status' => 'success',
            'notifications' => $notify
        ], 200);
    }

    public function keepLoggedIn(Request $request)
    {
        $user = Auth::user();
        $input_data = $request->all();
        $rules = [
            'deviceId' => 'required'
        ];
        $validator = Validator::make($input_data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->errors()
            ]);
        }
        if(App\UserToken::where(['userId' => Auth::id(), 'deviceId' => $input_data['deviceId']])->count() > 0) {
            return response()->json([
                'status' => 'fail',
                'keepLoggedIn' => true
            ]);
        }
        else {
            return response()->json([
                'status' => 'fail',
                'keepLoggedIn' => false
            ]);
        }
    }
}
