<?php

namespace App\Http\Controllers\Admin;

use App\AdminPanelMenu;
use App\Country;
use App\EmailTemplate;
use App\Mail\RegistrationNotification;
use App\User;
use App\UserMenuPermit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class AdminAccess extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword =  $request->input('keyword');
        $resource = 'Admin';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $$resource_pl = User::where(['userType' => 'A'])
            ->where(function ($qwery) use ($keyword) {
                if ($keyword != '') {
                    $qwery->orWhere(function ($query) use ($keyword) {
                        $query->where('firstName', 'like', '%' . $keyword . '%')
                            ->where('firstName', '!=', null);
                    })->orWhere(function ($query) use ($keyword) {
                        $query->where('lastName', 'like', '%' . $keyword . '%')
                            ->where('lastName', '!=', null);
                    })->orWhere(function ($query) use ($keyword) {
                        $query->where('email', 'like', '%' . $keyword . '%')
                            ->where('email', '!=', null);
                    });
                }
            })->with(['UserMenuPermits'])->orderBy('id', 'desc')->paginate($per_page);
        return view('admin.subadmin.list', compact($resource_pl, 'resource', 'resource_pl', 'keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $type
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $resource = 'Admin';
        $nat = 'Register';
        $meth = 'POST';
        $saveLink = admin_url('subAdmin');
        $resource_pl = str_plural($resource);
        $countries = Country::orderBy('name', 'asc')->get();
        $$resource = '';
        return view('admin.subadmin.edit', compact($resource, 'resource', 'resource_pl',
            'countries', 'saveLink', 'nat', 'meth'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  $type
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $rules = [
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'mobile' => 'required|numeric',
            'addressLine1' => 'nullable',
            'addressLine2' => 'nullable',
            'city' => 'nullable',
            'state' => 'nullable',
            'zipCode' => 'nullable',
            'profilePicture' => 'mimes:jpeg,png,jpg,gif,svg|max:10240'
        ];
        $this->validate($request, $rules, [
            'profilePicture.mimes' => 'Please upload a picture of type jpeg, png, jpg, gif, svg',
            'profilePicture.max' => 'Maximum of 10 MB is allowed for profile picture',
            'unique' => 'This :attribute is already registered'
        ]);
        if (User::where('userType', '=', 'A')
                ->where(function ($query) use ($input) {
                    $query->where('email', '=', $input['email'])
                        ->orWhere('mobile', '=', $input['mobile']);
                })->where('status', 'Y')->count() > 0) {
            return redirect()->back()->with('error', 'Email/Mobile already in use!');
        }
        /* profile picture Image upload */
        if ($request->hasFile('profilePicture')) {
            if ($request->file('profilePicture')->isValid()) {
                $path = 'uploads/profilePictures/';
                $filePath = $request->profilePicture->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $imageName = $imagePath[count($imagePath)-1];
                $input['profilePicture'] = $imageName;
            }
        }
        $pass = str_random(6);
        $input['password'] = Hash::make($pass);
        $input['userType'] = 'A';
        $input['status'] = 'Y';
        $user = User::create($input);
        $user->UserMenuPermits()->create([
            'menuId' => 1
        ]);
        $user->UserMenuPermits()->create([
            'menuId' => 48
        ]);
        $body = EmailTemplate::where(['id' => 10])->first();
        $body->description = str_replace('{EMAIL}', $user->email, $body->description);
        $body->description = str_replace('{MOBILE}', $user->mobileNumber, $body->description);
        $body->description = str_replace('{PASSWORD}', $pass, $body->description);
        $body->firstName = $user->firstName;
        Mail::to($user->email)->send(new RegistrationNotification($body));
        return redirect()->back()->with('success', 'Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $resource = 'Admin';
        $nat = 'Edit';
        $meth = 'PATCH';
        $saveLink = admin_url('subAdmin/'.$id);
        $resource_pl = str_plural($resource);
        $$resource = $user;
        $countries = Country::orderBy('name', 'asc')->get();
        return view('admin.subadmin.edit', compact($resource, 'resource', 'resource_pl',
            'countries', 'saveLink', 'nat', 'meth'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $user = User::find($id);
        $rules = [
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'profilePicture' => 'mimes:jpeg,png,jpg,gif,svg|max:10240'
        ];
        if ($user->email != $input['email']) {
            $rules['email'] = 'required|string|email|max:255|unique:users';
        } else {
            $rules['email'] = 'required|string|email|max:255';
        }
        if ($user->mobile != $input['mobile']) {
            $rules['mobile'] = 'required|numeric|unique:users';
        } else {
            $rules['mobile'] = 'required|numeric';
        }
        $this->validate($request, $rules, [
            'profilePicture.mimes' => 'Please upload a picture of type jpeg, png, jpg, gif, svg',
            'profilePicture.max' => 'Maximum of 10 MB is allowed for profile picture',
        ]);
        /* profile picture Image upload */
        if ($request->hasFile('profilePicture')) {
            if ($request->file('profilePicture')->isValid()) {
                $path = 'uploads/profilePictures/';
                if ($user->logo != null) {
                    if (Storage::disk('s3')->exists($path . $user->profilePicture)) {
                        Storage::disk('s3')->delete($path . $user->profilePicture);
                    }
                }
                $filePath = $request->profilePicture->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $imageName = $imagePath[count($imagePath)-1];
                $user->profilePicture = $imageName;
            }
        }
        $user->firstName = isset($input['firstName']) ? $input['firstName'] : "";
        $user->lastName = isset($input['lastName']) ? $input['lastName'] : "";
        $user->gender = isset($input['gender']) ? $input['gender'] : "";
        $user->mobile = isset($input['mobile']) ? $input['mobile'] : "";
        $user->email = isset($input['email']) ? $input['email'] : "";
        $user->password = isset($input['password']) ? Hash::make($input['password']) : "";
        $user->save();
        return redirect()->back()->with('success', 'Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        /*if ($user->profilePicture) {
            $path = 'uploads/profilePictures/';
            if (Storage::disk('s3')->exists($path . $user->profilePicture)) {
                Storage::disk('s3')->delete($path . $user->profilePicture);
            }
        }
        User::destroy($id);*/
        if ($user->status == 'D') {
            $user->status = 'Y';
        } else {
            $user->status = 'D';
        }
        $user->save();
        return redirect()->back()->with('success', 'Admin successfully removed!');
    }

    public function changeStatus(Request $request, $id)
    {
        $type = $request->segment(2);
        if (!$request->has('current_status')) {
            return redirect()->back()->with('error', 'Sorry! something went wrong.');
        }

        if ($request->input('current_status') == 'Y') {
            $is_active = 'N';
            $msg = 'disabled';
        } else {
            $is_active = 'Y';
            $msg = 'enabled';
        }

        User::where('id', $id)->update(['status' => $is_active]);
        return redirect()->back()->with('success', 'Account is ' . $msg . '!');
    }

    public function updatePermission(Request $request, $id)
    {
        $input = $request->all();
        $user = User::find($id);
        $rules = [
            'menuItems' => 'required|array'
        ];
        $this->validate($request, $rules, []);
        $menuItems = [];
        foreach ($input['menuItems'] as $val) {
            $menuItems[] = [
                'userId' => $user->id,
                'menuId' => $val,
                'created_at' => gmdate('Y-m-d H:i:s'),
                'updated_at' => gmdate('Y-m-d H:i:s')
            ];
        }
        $user->UserMenuPermits()->delete();
        UserMenuPermit::insert($menuItems);
        return redirect()->back()->with('success', ucwords($user->firstName).' menu access successfully updated!');
    }
}
