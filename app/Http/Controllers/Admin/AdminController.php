<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index() {
        if (Auth::check() && Auth::user()->isAdmin()) {
            return redirect(url('admin/dashboard'));
        } else {
            return redirect(url('admin/login'));
        }
    }
}
