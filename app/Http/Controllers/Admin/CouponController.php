<?php

namespace App\Http\Controllers\Admin;

use App\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resource = 'coupon';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $$resource_pl = Coupon::orderBy('created_at', 'desc')->paginate($per_page);
        return view('admin.coupons.list', compact($resource_pl, 'resource', 'resource_pl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nat = 'Add';
        $meth = 'POST';
        $resource = 'coupon';
        $saveLink = admin_url('coupon');
        $resource_pl = str_plural($resource);
        $$resource = '';
        return view('admin.coupons.edit', compact($resource, 'resource', 'resource_pl', 'saveLink', 'nat', 'meth'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $rules = [
            'code' => 'required|unique:coupons,code',
            'title' => 'required',
            'description' => 'required',
            'discount' => 'required',
            'startTime' => 'required',
            'endTime' => 'required'
        ];
        $this->validate($request, $rules, [
            'unique' => 'Invalid %s'
        ]);
        $coupon = Coupon::create($input);
        return redirect()->back()->with('success', 'Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nat = 'Edit';
        $meth = 'PATCH';
        $resource = 'coupon';
        $saveLink = admin_url('coupon/' . $id);
        $resource_pl = str_plural($resource);
        $$resource = Coupon::find($id);
        return view('admin.coupons.edit', compact($resource, 'resource', 'resource_pl', 'saveLink', 'nat', 'meth'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $coupon = Coupon::find($id);
        $rules = [
            'code' => 'required',
            'title' => 'required',
            'description' => 'required',
            'discount' => 'required',
            'startTime' => 'required',
            'endTime' => 'required'
        ];
        $this->validate($request, $rules, [
            'exists' => 'Invalid :attribute'
        ]);
        $coupon->code = isset($input['code']) ? $input['code'] : "";
        $coupon->title = isset($input['title']) ? $input['title'] : "";
        $coupon->description = isset($input['description']) ? $input['description'] : "";
        $coupon->discount = isset($input['discount']) ? $input['discount'] : "";
        $coupon->startTime = isset($input['startTime']) ? $input['startTime'] : "";
        $coupon->endTime = isset($input['endTime']) ? $input['endTime'] : "";
        $coupon->save();
        return redirect()->back()->with('success', 'Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coupon = Coupon::find($id);
        if ($coupon->status == 'N') {
            $coupon->status = 'Y';
            $message = "Reactivated successfully.";
        }
        else {
            $coupon->status = 'N';
            $message = "Deactivated successfully.";
        }
        $coupon->save();
        return redirect()->back()->with('success', $message);
    }
}
