<?php

namespace App\Http\Controllers\Admin;

use App\BankName;
use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword =  $request->input('keyword');
        $languages = Language::all();
        $resource = 'bank';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $$resource_pl = BankName::whereHas('BankTranslate', function ($query) use ($keyword){
            $query->where('bank_name', 'like', '%' . $keyword . '%');
        })->orderBy('id', 'asc')->with(['BankTranslate'])->paginate($per_page);
        return view('admin.bank.list', compact($resource_pl, 'resource', 'resource_pl', 'languages', 'keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nat = 'Add';
        $meth = 'POST';
        $languages = Language::all();
        $resource = 'bank';
        $saveLink = admin_url('bank');
        $resource_pl = str_plural($resource);
        $$resource = '';
        return view('admin.bank.edit', compact($resource, 'resource', 'resource_pl',
            'saveLink', 'nat', 'meth', 'languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $lang = collect(Language::all()->pluck('locale'))->toArray();
        $rules = [
            'code' => 'required',
            'status' => 'required'
        ];
        foreach ($lang as $key => $val) {
            if ($val == app()->getLocale())
                $rules[$val] = "required";
            $rules[$val] = "";
        }
        $this->validate($request, $rules, []);
        $bank = BankName::create([
            'code' => $input['code'],
            'status' => $input['status']
        ]);
        foreach ($lang as $key => $val) {
            if ($input[$val] == '' || $input[$val] == null) {
                $bank->BankTranslate()->create([
                    'bank_name_id' => $bank->id,
                    'bank_name' => $input[app()->getLocale()],
                    'locale' => $val
                ]);
            } else {
                $bank->BankTranslate()->create([
                    'bank_name_id' => $bank->id,
                    'bank_name' => $input[$val],
                    'locale' => $val
                ]);
            }
        }
        return redirect()->back()->with('success', 'Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bank = BankName::with(['BankTranslate'])->find($id);
        $nat = 'Edit';
        $meth = 'PATCH';
        $languages = Language::all();
        $resource = 'bank';
        $saveLink = admin_url('bank/' . $id);
        $resource_pl = str_plural($resource);
        $$resource = $bank;
        return view('admin.bank.edit', compact($resource, 'resource', 'resource_pl',
            'saveLink', 'nat', 'meth', 'languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $lang = collect(Language::all()->pluck('locale'))->toArray();
        $bank = BankName::find($id);
        $bank->BankTranslate()->delete();
        $rules = [
            'code' => 'required',
            'status' => 'required'
        ];
        foreach ($lang as $key => $val) {
            if ($val == app()->getLocale())
                $rules[$val] = "required";
            $rules[$val] = "";
        }

        $this->validate($request, $rules, []);

        $bank->code = isset($input['code']) ? $input['code'] : "";
        $bank->status = isset($input['status']) ? $input['status'] : "";
        $bank->save();
        foreach ($lang as $key => $val) {
            if ($input[$val] == '') {
                $bank->BankTranslate()->create([
                    'bank_name_id' => $id,
                    'bank_name' => $input[app()->getLocale()],
                    'locale' => $val
                ]);
            } else {
                $bank->BankTranslate()->create([
                    'bank_name_id' => $id,
                    'bank_name' => $input[$val],
                    'locale' => $val
                ]);
            }
        }
        return redirect()->back()->with('success', 'Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BankName::find($id)->BankTranslate()->delete();
        BankName::destroy($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
}
