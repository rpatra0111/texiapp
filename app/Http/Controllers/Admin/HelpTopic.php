<?php

namespace App\Http\Controllers\Admin;

use App\HelpQueryTopic;
use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HelpTopic extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = Language::all();
        $resource = 'helpTopic';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $$resource_pl = HelpQueryTopic::orderBy('id', 'asc')->with(['hqtTranslations'])->paginate($per_page);
        return view('admin.helpTopics.list', compact($resource_pl, 'resource', 'resource_pl', 'languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nat = 'Add';
        $meth = 'POST';
        $languages = Language::all();
        $resource = 'helpTopic';
        $saveLink = admin_url('helptopic');
        $resource_pl = str_plural($resource);
        $$resource = '';
        return view('admin.helpTopics.edit', compact($resource, 'resource', 'resource_pl',
            'saveLink', 'nat', 'meth', 'languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $lang = collect(Language::all()->pluck('locale'))->toArray();
        $rules = [
            'status' => 'required'
        ];
        foreach ($lang as $key => $val) {
            if ($val == app()->getLocale())
                $rules[$val] = "required";
            $rules[$val] = "";
        }
        $this->validate($request, $rules, []);
        $hqt = HelpQueryTopic::create([
            'status' => $input['status']
        ]);
        foreach ($lang as $key => $val) {
            if ($input[$val] == '' || $input[$val] == null) {
                $hqt->hqtTranslations()->create([
                    'help_query_topic_id' => $hqt->id,
                    'topic_name' => $input[app()->getLocale()],
                    'locale' => $val
                ]);
            } else {
                $hqt->hqtTranslations()->create([
                    'help_query_topic_id' => $hqt->id,
                    'topic_name' => $input[$val],
                    'locale' => $val
                ]);
            }
        }
        return redirect()->back()->with('success', 'Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $helpTopic = HelpQueryTopic::with(['hqtTranslations'])->find($id);
        $nat = 'Edit';
        $meth = 'PATCH';
        $languages = Language::all();
        $resource = 'helpTopic';
        $saveLink = admin_url('helptopic/' . $id);
        $resource_pl = str_plural($resource);
        $$resource = $helpTopic;
        return view('admin.helpTopics.edit', compact($resource, 'resource', 'resource_pl',
            'saveLink', 'nat', 'meth', 'languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $lang = collect(Language::all()->pluck('locale'))->toArray();
        $hqt = HelpQueryTopic::find($id);
        $hqt->hqtTranslations()->delete();
        $rules = [
            'status' => 'required'
        ];
        foreach ($lang as $key => $val) {
            if ($val == app()->getLocale())
                $rules[$val] = "required";
            $rules[$val] = "";
        }

        $this->validate($request, $rules, []);

        $hqt->status = isset($input['status']) ? $input['status'] : "";
        $hqt->save();
        foreach ($lang as $key => $val) {
            if ($input[$val] == '') {
                $hqt->hqtTranslations()->create([
                    'help_query_topic_id' => $id,
                    'topic_name' => $input[app()->getLocale()],
                    'locale' => $val
                ]);
            } else {
                $hqt->hqtTranslations()->create([
                    'help_query_topic_id' => $id,
                    'topic_name' => $input[$val],
                    'locale' => $val
                ]);
            }
        }
        return redirect()->back()->with('success', 'Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        HelpQueryTopic::find($id)->hqtTranslations()->delete();
        HelpQueryTopic::destroy($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
}
