<?php

namespace App\Http\Controllers\Admin;

use App\CountryCity;
use App\CountryState;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Ajax extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }


    public function fetchAllStates($id)
    {
        $states = collect(CountryState::where('countryId', '=', $id)->get())->toArray();
        return response()->json([
            'status' => 'success',
            'states' => $states
        ]);
    }

    public function fetchAllCities($id)
    {
        $cities = collect(CountryCity::where('stateId', '=', $id)->get())->toArray();
        return response()->json([
            'status' => 'success',
            'cities' => $cities
        ]);
    }
}
