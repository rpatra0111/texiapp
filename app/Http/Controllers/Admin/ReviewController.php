<?php

namespace App\Http\Controllers\Admin;

use App\DriverReview;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ReviewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resource = 'review';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $$resource_pl = DriverReview::orderBy('id', 'asc')->paginate($per_page);
        return view('admin.review.list', compact($resource_pl, 'resource', 'resource_pl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nat = 'Add';
        $meth = 'POST';
        $resource = 'review';
        $saveLink = admin_url('site/review');
        $resource_pl = str_plural($resource);
        $$resource = '';
        return view('admin.review.edit', compact($resource, 'resource', 'resource_pl', 'saveLink', 'nat', 'meth'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $rules = [
            'driver_name' => 'required|string|max:255',
            'review' => 'required|string|max:500',
            'driver_pic' => 'mimes:jpeg,png,jpg,gif,svg|max:10240'
        ];
        $this->validate($request, $rules, [
            'driver_pic.mimes' => 'Please upload a picture of type jpeg, png, jpg, gif, svg',
            'driver_pic.max' => 'Maximum of 10 MB is allowed for profile picture',
            'unique' => 'This :attribute is already registered'
        ]);
        /* profile picture Image upload */
        if ($request->hasFile('driver_pic')) {
            if ($request->file('driver_pic')->isValid()) {
                $path = 'site_assets/images/driver_pic/';
                $filePath = $request->driver_pic->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $imageName = $imagePath[count($imagePath)-1];
                $input['driver_pic'] = $imageName;
            }
        }
        $input['status'] = 'Y';
        $driverReview = DriverReview::create($input);
        return redirect()->back()->with('success', 'Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DriverReview  $driverReview
     * @return \Illuminate\Http\Response
     */
    public function show(DriverReview $driverReview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DriverReview  $driverReview
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $review = DriverReview::find($id);
        $resource = 'review';
        $nat = 'Edit';
        $meth = 'PATCH';
        $saveLink = admin_url('site/review/' . $id);
        $resource_pl = str_plural($resource);
        $$resource = $review;
        return view('admin.review.edit', compact($resource, 'resource', 'resource_pl', 'saveLink', 'nat', 'meth'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DriverReview  $driverReview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $review = DriverReview::find($id);
        $rules = [
            'driver_name' => 'required|string|max:255',
            'review' => 'required|string|max:500'
        ];
        $this->validate($request, $rules, []);
        /* profile picture Image upload */
        if ($request->hasFile('driver_pic')) {
            if ($request->file('driver_pic')->isValid()) {
                $path = 'site_assets/images/driver_pic';
                if ($review->driver_pic != null) {
                    if (Storage::disk('s3')->exists($path . $review->driver_pic)) {
                        Storage::disk('s3')->delete($path . $review->driver_pic);
                    }
                }
                $filePath = $request->driver_pic->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $imageName = $imagePath[count($imagePath)-1];
                $review->driver_pic = $imageName;
            }
        }
        $review->driver_name = isset($input['driver_name']) ? $input['driver_name'] : "";
        $review->review = isset($input['review']) ? $input['review'] : "";
        $review->save();
        return redirect()->back()->with('success', 'Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DriverReview  $driverReview
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $input = $request->all();
        $review = DriverReview::find($input['id']);
        if ($review->driver_pic != null) {
            $path = 'site_assets/images/driver_pic';
            if (Storage::disk('s3')->exists($path . $review->driver_pic)) {
                Storage::disk('s3')->delete($path . $review->driver_pic);
            }
        }
        DriverReview::find($input['id'])->delete();
        return redirect()->back()->with('success', 'Deleted successfully.');
    }
}
