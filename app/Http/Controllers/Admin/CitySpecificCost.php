<?php

namespace App\Http\Controllers\Admin;

use App\CountryCity;
use App\CountryState;
use App\TaxiType;
use App\TaxiTypeRate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CitySpecificCost extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resource = 'taxiTypeRate';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $$resource_pl = TaxiTypeRate::with(['City', 'TaxiType'])->orderBy('taxiTypeId', 'asc')->paginate($per_page);
        return view('admin.taxiType.citySpecific.list', compact($resource_pl, 'resource', 'resource_pl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nat = 'Add';
        $meth = 'POST';
        $resource = 'taxiTypeRate';
        $saveLink = admin_url('taxitype/rate');
        $resource_pl = str_plural($resource);
        $$resource = '';
        $taxiType = TaxiType::all();
        $state = CountryState::all();
        $city = CountryCity::where('stateId', '=', $state[0]->id)->get();
        return view('admin.taxiType.citySpecific.edit', compact($resource, 'resource', 'resource_pl',
            'saveLink', 'nat', 'meth', 'taxiType', 'state', 'city'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $rules = [
            'taxiTypeId' => 'required|exists:taxi_types,id',
            'countryCityId' => 'required|exists:country_cities,id',
            'fenceDistance' => 'required',
            'baseCost' => 'required',
            'perKmCost' => 'required',
            'cancelCharge' => 'required',
            'timeCharge' => 'required',
            'minCharge' => 'required'
        ];
        $this->validate($request, $rules, [
            'exists' => 'Invalid %s'
        ]);
        $taxi = TaxiTypeRate::create($input);
        return redirect()->back()->with('success', 'Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $taxi = TaxiTypeRate::find($id);
        $nat = 'Edit';
        $meth = 'PATCH';
        $resource = 'taxi';
        $saveLink = admin_url('taxitype/rate/' . $id);
        $resource_pl = str_plural($resource);
        $$resource = $taxi;
        $taxiType = TaxiType::all();
        $state = CountryState::all();
        $currCityId = CountryCity::find($taxi->countryCityId);
        $city = CountryCity::where('stateId', '=', $currCityId->stateId)->get();
        return view('admin.taxiType.citySpecific.edit', compact($resource, 'resource', 'resource_pl',
            'saveLink', 'nat', 'meth', 'taxiType', 'state', 'city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $taxi = TaxiTypeRate::find($id);
        $rules = [
            'taxiTypeId' => 'required|exists:taxi_types,id',
            'countryCityId' => 'required|exists:country_cities,id',
            'fenceDistance' => 'required',
            'baseCost' => 'required',
            'perKmCost' => 'required',
            'cancelCharge' => 'required',
            'timeCharge' => 'required',
            'minCharge' => 'required'
        ];

        $this->validate($request, $rules, [
            'exists' => 'Invalid %s'
        ]);

        $taxi->taxiTypeId = isset($input['taxiTypeId']) ? $input['taxiTypeId'] : "";
        $taxi->countryCityId = isset($input['countryCityId']) ? $input['countryCityId'] : "";
        $taxi->fenceDistance = isset($input['fenceDistance']) ? $input['fenceDistance'] : "";
        $taxi->baseCost = isset($input['baseCost']) ? $input['baseCost'] : "";
        $taxi->perKmCost = isset($input['perKmCost']) ? $input['perKmCost'] : "";
        $taxi->cancelCharge = isset($input['cancelCharge']) ? $input['cancelCharge'] : "";
        $taxi->timeCharge = isset($input['timeCharge']) ? $input['timeCharge'] : "";
        $taxi->minCharge = isset($input['minCharge']) ? $input['minCharge'] : "";
        $taxi->save();
        return redirect()->back()->with('success', 'Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TaxiTypeRate::destroy($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
}
