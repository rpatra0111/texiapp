<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\GeneralSetting;
use App\TaxiRide;
use App\User;
use App\UserTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index()
    {
        $years[] = date("Y",strtotime("-1 year"));
        $years[] = date('Y');
        $months = array_fill(0, 12, 0);
        // Rides Count
        $rides = TaxiRide::whereIn(DB::raw('YEAR(created_at)'), $years)
            ->select(DB::raw('YEAR(created_at) AS year'), DB::raw('MONTH(created_at) AS month'), DB::raw('COUNT(*) AS rideCount'))
            ->groupBy(DB::raw('YEAR(created_at)'), DB::raw('MONTH(created_at)'))
            ->orderBy('month', 'asc')->get();
        $temp[$years[0]] = $months;
        $temp[$years[1]] = $months;
        foreach ($rides as $key => $values) {
            $temp[$values->year][$values->month-1] = $values->rideCount;
        }
        $rides = $temp;
        // Customer Count
        $customer = User::where('userType', '=', 'P')->whereIn(DB::raw('YEAR(created_at)'), $years)
            ->select(DB::raw('YEAR(created_at) AS year'), DB::raw('MONTH(created_at) AS month'), DB::raw('COUNT(*) AS custCount'))
            ->groupBy(DB::raw('YEAR(created_at)'), DB::raw('MONTH(created_at)'))
            ->orderBy('month', 'asc')->get();
        $temp[$years[0]] = $months;
        $temp[$years[1]] = $months;
        foreach ($customer as $key => $values) {
            $temp[$values->year][$values->month-1] = $values->custCount;
        }
        $customer = $temp;
        // Driver Count
        $driver = User::where('userType', '=', 'D')->whereIn(DB::raw('YEAR(created_at)'), $years)
            ->select(DB::raw('YEAR(created_at) AS year'), DB::raw('MONTH(created_at) AS month'), DB::raw('COUNT(*) AS driverCount'))
            ->groupBy(DB::raw('YEAR(created_at)'), DB::raw('MONTH(created_at)'))
            ->orderBy('month', 'asc')->get();
        $temp[$years[0]] = $months;
        $temp[$years[1]] = $months;
        foreach ($driver as $key => $values) {
            $temp[$values->year][$values->month-1] = $values->driverCount;
        }
        $driver = $temp;
        // Earning
        $earning = UserTransaction::where('userId', '=', Auth::id())->whereIn(DB::raw('YEAR(created_at)'), $years)
            ->select(DB::raw('YEAR(created_at) AS year'), DB::raw('MONTH(created_at) AS month'), DB::raw('SUM(amount) AS totalAmount'))
            ->groupBy(DB::raw('YEAR(created_at)'), DB::raw('MONTH(created_at)'))
            ->orderBy('month', 'asc')->get();
        $temp[$years[0]] = $months;
        $temp[$years[1]] = $months;
        foreach ($earning as $key => $values) {
            $temp[$values->year][$values->month-1] = $values->totalAmount;
        }
        $earning = $temp;
        return view('admin.dashboard', compact('rides', 'customer', 'driver', 'earning'));
    }

    public function profile()
    {
        $admin_user = Auth::user();
        return view('admin.profile', compact('admin_user'));
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'admin_email' => 'required|email|max:255',
            'profilePicture' => 'mimes:jpg,jpeg,png,bmp|max:10000',
            'admin_password' => ''
        ],
            [
                'profilePicture.mimes' => 'Please upload :attribute of type jpg,jpeg,png,bmp',
                'profilePicture.max' => 'Maximum of 10 MB is allowed for :attribute'
            ]);
        $input = $request->all();
        $admin_user = Auth::user();
        if ($admin_user->email != $input['admin_email']) {
            if (User::where(['email' => $input['admin_email'], 'userType' => 'SU'])->whereIn('status', ['Y', 'P', 'MP', 'EP'])->count() > 0) {
                return redirect()->back()->with('fail', 'The Provided email is already in use.');
            } else {
                $admin_user->email = $input['admin_email'];
            }
        }
        if ($request->hasFile('profilePicture')) {
            if ($request->file('profilePicture')->isValid()) {
                $path = 'admin_assets/dist/img/';
                if ($admin_user->profilePicture != null) {
                    if (Storage::disk('s3')->exists($path . $admin_user->profilePicture)) {
                        Storage::disk('s3')->delete($path . $admin_user->profilePicture);
                    }
                }
                $filePath = $request->profilePicture->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $imageName = $imagePath[count($imagePath)-1];
                $admin_user->profilePicture = $imageName;
            }
        }
        if ($input['admin_pass'] != '') {
            $admin_user->password = bcrypt($input['admin_pass']);
        }
        $admin_user->save();
        return redirect()->back()->with('success', 'Profile updated successfully.');
    }
}
