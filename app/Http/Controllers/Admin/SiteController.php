<?php

namespace App\Http\Controllers\Admin;

use App\GeneralSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function settings()
    {
        $settings = GeneralSetting::find(1);
        return view('admin.settings', compact('settings'));
    }

    public function updateSettings(Request $request)
    {
        $this->validate($request, [
            'siteTitle' => 'required',
            'contactEmail' => 'required|email',
            'contactName' => 'required',
            'contactPhone' => 'required',
            'driverRange' => 'required',
            'bookingRange' => 'required',
            'logo' => 'mimes:jpg,jpeg,png,bmp|max:10000',
        ],
            [
                'mimes' => 'Please upload :attribute of type jpg,jpeg,png,bmp',
                'max' => 'Maximum of 10 MB is allowed for :attribute'
            ]);
        $input = $request->all();
        $settings = GeneralSetting::find(1);
        $settings->siteTitle = $input['siteTitle'];
        $settings->contactEmail = $input['contactEmail'];
        $settings->contactName = $input['contactName'];
        $settings->contactPhone = $input['contactPhone'];
        $settings->driverRange = $input['driverRange'];
        $settings->bookingRange = $input['bookingRange'];
        $settings->commission = $input['commission'];
        $settings->ref_reword_point = $input['ref_reword_point'];
        $settings->contactAdminText = $input['contactAdminText'];

        /* Logo Image upload */
        if ($request->hasFile('logo')) {
            if ($request->file('logo')->isValid()) {
                $path = 'uploads/site_logo/';
                if ($settings->logo != null) {
                    if (Storage::disk('s3')->exists($path . $settings->logo)) {
                        Storage::disk('s3')->delete($path . $settings->logo);
                    }
                }
                $filePath = $request->logo->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $imageName = $imagePath[count($imagePath)-1];
                $settings->logo = $imageName;
            }
        }
        $settings->save();
        return redirect()->back()->with('success', 'Settings Updated Successfully.');
    }
}
