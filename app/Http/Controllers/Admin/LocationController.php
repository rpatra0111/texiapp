<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\CountryCity;
use App\CountryState;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request, $type)
    {
        $keyword =  $request->input('keyword');
        $per_page = config('constant.ADMIN_PER_PAGE');
        switch ($type) {
            case 'country':
                $resource = 'country';
                $resource_pl = str_plural($resource);
                $$resource_pl = Country::where('name', 'like', '%' . $keyword . '%')->orderBy('id', 'asc')->paginate($per_page);
                return view('admin.country.list', compact($resource_pl, 'resource', 'resource_pl', 'keyword'));
                break;
            case 'state':
                $resource = 'state';
                $resource_pl = str_plural($resource);
                $$resource_pl = CountryState::where('name', 'like', '%' . $keyword . '%')->orderBy('id', 'asc')->with(['Country'])->paginate($per_page);
                return view('admin.countryState.list', compact($resource_pl, 'resource', 'resource_pl', 'keyword'));
                break;
            case 'city':
                $resource = 'city';
                $resource_pl = str_plural($resource);
                $$resource_pl = CountryCity::where('cityName', 'like', '%' . $keyword . '%')->orderBy('id', 'asc')->with(['State.Country'])->paginate($per_page);
                return view('admin.countryCity.list', compact($resource_pl, 'resource', 'resource_pl', 'keyword'));
                break;
            default:
                abort(404);
                break;
        }
    }

    public function create($type)
    {
        $nat = 'Add';
        $meth = 'POST';
        $saveLink = admin_url('location/' . $type);
        switch ($type) {
            case 'country':
                $resource = 'country';
                $resource_pl = str_plural($resource);
                $$resource = '';
                return view('admin.country.edit', compact($resource, 'resource', 'resource_pl',
                    'saveLink', 'nat', 'meth'));
                break;
            case 'state':
                $resource = 'state';
                $countries = Country::all();
                $resource_pl = str_plural($resource);
                $$resource = '';
                return view('admin.countryState.edit', compact($resource, 'resource', 'resource_pl',
                    'saveLink', 'nat', 'meth', 'countries'));
                break;
            case 'city':
                $resource = 'city';
                $countries = Country::all();
                $states = CountryState::where('countryId', '=', $countries[0]->id)->get();
                $resource_pl = str_plural($resource);
                $$resource = '';
                return view('admin.countryCity.edit', compact($resource, 'resource', 'resource_pl',
                    'saveLink', 'nat', 'meth', 'countries', 'states'));
                break;
            default:
                abort(404);
                break;
        }
    }

    public function store(Request $request, $type)
    {
        $input = $request->all();
        switch ($type) {
            case 'country':
                $rules = [
                    'ISO' => 'required',
                    'name' => 'required',
                    'ISO3' => 'required',
                    'numCode' => 'required',
                    'phoneCode' => 'required'
                ];
                $this->validate($request, $rules, []);
                $input['cap_name'] = strtoupper($input['name']);
                Country::create($input);
                break;
            case 'state':
                $rules = [
                    'countryId' => 'required|exists:countries,id',
                    'code' => 'required',
                    'name' => 'required'
                ];
                $this->validate($request, $rules, [
                    'exists' => 'This :attribute is invalid'
                ]);
                CountryState::create($input);
                break;
            case 'city':
                $rules = [
                    'stateId' => 'required|exists:country_states,id',
                    'cityName' => 'required',
                    'countyName' => '',
                    'latitude' => 'required',
                    'longitude' => 'required'
                ];
                $this->validate($request, $rules, [
                    'exists' => 'This :attribute is invalid'
                ]);
                CountryCity::create($input);
                break;
            default:
                abort(404);
                break;
        }
        return redirect()->back()->with('success', 'Created successfully!');
    }

    public function edit($type, $id)
    {
        $nat = 'Edit';
        $meth = 'PATCH';
        $saveLink = admin_url('location/' . $type . '/' . $id);
        switch ($type) {
            case 'country':
                $country = Country::find($id);
                $resource = 'country';
                $resource_pl = str_plural($resource);
                $$resource = $country;
                return view('admin.country.edit', compact($resource, 'resource', 'resource_pl',
                    'saveLink', 'nat', 'meth'));
                break;
            case 'state':
                $countries = Country::all();
                $state = CountryState::find($id);
                $resource = 'state';
                $resource_pl = str_plural($resource);
                $$resource = $state;
                return view('admin.countryState.edit', compact($resource, 'resource', 'resource_pl',
                    'saveLink', 'nat', 'meth', 'countries'));
                break;
            case 'city':
                $city = CountryCity::with(['State'])->find($id);
                $countries = Country::all();
                $states = CountryState::where('countryId', '=', $city->state->countryId)->get();
                $resource = 'city';
                $resource_pl = str_plural($resource);
                $$resource = $city;
                return view('admin.countryCity.edit', compact($resource, 'resource', 'resource_pl',
                    'saveLink', 'nat', 'meth', 'countries', 'states'));
                break;
            default:
                abort(404);
                break;
        }
    }

    public function update(Request $request, $type, $id)
    {
        $input = $request->all();
        switch ($type) {
            case 'country':
                $country = Country::find($id);
                $rules = [
                    'ISO' => 'required',
                    'name' => 'required',
                    'ISO3' => 'required',
                    'numCode' => 'required',
                    'phoneCode' => 'required'
                ];
                $this->validate($request, $rules, []);
                $country->name = isset($input['name']) ? $input['name'] : "";
                $country->ISO = isset($input['ISO']) ? $input['ISO'] : "";
                $country->ISO3 = isset($input['ISO3']) ? $input['ISO3'] : "";
                $country->numCode = isset($input['numCode']) ? $input['numCode'] : "";
                $country->phoneCode = isset($input['phoneCode']) ? $input['phoneCode'] : "";
                $country->save();
                break;
            case 'state':
                $state = CountryState::find($id);
                $rules = [
                    'countryId' => 'required|exists:countries,id',
                    'code' => 'required',
                    'name' => 'required'
                ];
                $this->validate($request, $rules, [
                    'exists' => 'This :attribute is invalid'
                ]);
                $state->name = isset($input['name']) ? $input['name'] : "";
                $state->countryId = isset($input['countryId']) ? $input['countryId'] : "";
                $state->code = isset($input['code']) ? $input['code'] : "";
                $state->save();
                break;
            case 'city':
                $city = CountryCity::find($id);
                $rules = [
                    'stateId' => 'required|exists:country_states,id',
                    'cityName' => 'required',
                    'countyName' => '',
                    'latitude' => 'required',
                    'longitude' => 'required'
                ];
                $this->validate($request, $rules, [
                    'exists' => 'This :attribute is invalid'
                ]);
                $city->stateId = isset($input['stateId']) ? $input['stateId'] : "";
                $city->cityName = isset($input['cityName']) ? $input['cityName'] : "";
                $city->countyName = isset($input['countyName']) ? $input['countyName'] : "";
                $city->latitude = isset($input['latitude']) ? $input['latitude'] : "";
                $city->longitude = isset($input['longitude']) ? $input['longitude'] : "";
                $city->save();
                break;
            default:
                abort(404);
                break;
        }
        return redirect()->back()->with('success', 'Updated successfully.');
    }

    public function destroy($type, $id)
    {
        $input = ['id' => $id];
        switch ($type) {
            case 'country':
                $rules = [
                    'id' => 'required|exists:country,id'
                ];
                $this->validate($input, $rules, []);
                Country::destroy($id);
                break;
            case 'state':
                $rules = [
                    'id' => 'required|exists:country_states,id'
                ];
                $this->validate($input, $rules, []);
                CountryState::destroy($id);
                break;
            case 'city':
                $rules = [
                    'id' => 'required|exists:country_cities,id'
                ];
                $this->validate($input, $rules, []);
                CountryCity::destroy($id);
                break;
            default:
                abort(404);
                break;
        }
        return redirect()->back()->with('success', 'Deleted successfully');
    }
}
