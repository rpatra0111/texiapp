<?php

namespace App\Http\Controllers\Admin;

use App\TaxiRide;
use App\RejectedRide;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RideController extends Controller
{
    public function index (Request $request)
    {
        $startDate = $request->input('start');
        $endDate = $request->input('end');
        $resource = 'ride';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $$resource_pl = TaxiRide::whereNotIn('status', ['P', 'V'])
            ->where(function ($query) use ($startDate, $endDate) {
            if (isset($startDate) && isset($endDate)) {
                $query->where('created_at', '>=', date('Y-m-d H:i:s', strtotime($startDate)))
                    ->where('created_at', '<=', date('Y-m-d H:i:s', strtotime($endDate . " 23:59:59")));
            }
        })->with(['Passenger' => function ($query) {
            $query->select('id', 'firstName', 'lastName', 'profilePicture', 'balance');
        }, 'UserTaxi.Driver' => function ($query) {
            $query->select('id', 'firstName', 'lastName', 'profilePicture', 'balance');
        }, 'Coupon'])
            ->orderBy('id', 'desc')
            ->paginate($per_page);
        return view('admin.rides.list', compact($resource_pl, 'resource', 'resource_pl', 'startDate', 'endDate'));
    }

    public function rejectedRides (Request $request)
    {
        $startDate = $request->input('start');
        $endDate = $request->input('end');
        $resource = 'ride';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $$resource_pl = RejectedRide::whereIn('status', ['P', 'V'])
            ->where(function ($query) use ($startDate, $endDate) {
                if (isset($startDate) && isset($endDate)) {
                    $query->where('created_at', '>=', date('Y-m-d H:i:s', strtotime($startDate)))
                        ->where('created_at', '<=', date('Y-m-d H:i:s', strtotime($endDate . " 23:59:59")));
                }
            })->with(['Passenger' => function ($query) {
                $query->select('id', 'firstName', 'lastName', 'profilePicture', 'balance');
            }, 'UserTaxi.Driver' => function ($query) {
                $query->select('id', 'firstName', 'lastName', 'profilePicture', 'balance');
            }, 'Coupon'])
            ->orderBy('id', 'desc')
            ->paginate($per_page);
        return view('admin.rides.rejected_ride_list', compact($resource_pl, 'resource', 'resource_pl', 'startDate', 'endDate'));
    }
}
