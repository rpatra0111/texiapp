<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\EmailTemplate;
use App\Mail\DocumentVerification;
use App\Mail\RegistrationNotification;
use App\Mail\WalletReset;
use App\Mail\WithdrawalStatus;
use App\TaxiRide;
use App\Transaction;
use App\UserCashout;
use App\UserDocument;
use App\UserRating;
use App\UserTransaction;
use App\VerificationDocument;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\TaxiType;
use App\UserCarColour;
use App\UserCar;
use App\UserTaxi;
use File;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use View;
use App\UserBankAccount;

class UserAccess extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $type
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request->segment(2);
        $keyword = $request->has('keyword') ? $request->input('keyword') : '';
        $statussort = $request->has('statussort') ? $request->input('statussort') : '';
        $carsort = $request->has('carsort') ? $request->input('carsort') : '';
        $losort = $request->has('losort') ? $request->input('losort') : '';

        $currlatitude = $request->has('curr_latitude') ? $request->input('curr_latitude') : '';
        $currlongitude = $request->has('curr_longitude') ? $request->input('curr_longitude') : '';
        $distance = $request->has('distance') ? $request->input('distance') : '';
        $resource = $type;
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $userType = "P";
        if ($type == 'driver') {
            $userType = "D";
        }
        // \DB::connection()->enableQueryLog();
        $userData = User::where(['userType' => $userType])
            ->where(function ($qwery) use ($keyword) {
                if ($keyword != '') {
                    $qwery->orWhere(function ($query) use ($keyword) {
                        $query->where('firstName', 'like', '%' . $keyword . '%')
                            ->where('firstName', '!=', null);
                    })->orWhere(function ($query) use ($keyword) {
                        $query->where('lastName', 'like', '%' . $keyword . '%')
                            ->where('lastName', '!=', null);
                    })->orWhere(function ($query) use ($keyword) {
                        $query->where('email', 'like', '%' . $keyword . '%')
                            ->where('email', '!=', null);
                    });
                }
            });
        if ($userType == 'D') {
            if($currlatitude!='' && $currlongitude!='' && $distance!=''){
                $userData->where('is_online','Y')->where( function($q) use ($distance, $currlatitude, $currlongitude) { 
                    $q->isWithinMaxDistance($currlatitude, $currlongitude, $distance);
                });

            }
            
            $userData->with(['UserCars.TaxiTypes','UserTaxis'])->withCount(['UserCars'])->withCount(['DriverRides' => function ($query) {
                $query->where('taxi_rides.created_at', Carbon::today());
            }]);
        }
        if ($statussort == 'desc') {
            $userData = $userData->orderBy('driverApproval', 'desc');
            $userData = $userData->orderBy('completion', 'desc');
        } elseif ($statussort == 'asc') {
            $userData = $userData->orderBy('driverApproval', 'asc');
            $userData = $userData->orderBy('completion', 'asc');
        }
        if ($carsort == 'desc') {
            $userData = $userData->orderBy('user_cars_count', 'desc');
        } elseif ($carsort == 'asc') {
            $userData = $userData->orderBy('user_cars_count', 'asc');
        }
        if ($losort == 'desc') {
            $userData = $userData->orderBy('updated_at', 'desc');
        } elseif ($losort == 'asc') {
            $userData = $userData->orderBy('updated_at', 'asc');
        }
        $$resource_pl = $userData->orderBy('id', 'desc')->paginate($per_page);
                // $queries = \DB::getQueryLog();
                // return dd($queries);
        //dd($$resource_pl->toArray());
        return view('admin.user.list', compact($resource_pl, 'resource', 'resource_pl', 'keyword', 'statussort', 'carsort', 'losort','currlatitude','currlongitude','distance'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $type = $request->segment(2);
        $resource = $type;
        $nat = 'Register';
        $meth = 'POST';
        $saveLink = admin_url($type);
        $resource_pl = str_plural($resource);
        $countries = Country::orderBy('name', 'asc')->get();
        $$resource = '';
        return view('admin.user.edit', compact($resource, 'resource', 'resource_pl',
            'countries', 'saveLink', 'nat', 'meth'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = $request->segment(2);
        $input = $request->all();
        $rules = [
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'mobile' => 'required|numeric',
            'addressLine1' => 'nullable',
            'addressLine2' => 'nullable',
            'city' => 'nullable',
            'state' => 'nullable',
            'zipCode' => 'nullable',
            'userType' => [
                'required',
                Rule::in(["D", "P"])
            ],
            'profilePicture' => 'mimes:jpeg,png,jpg,gif,svg|max:10240'
        ];
        $this->validate($request, $rules, [
            'profilePicture.mimes' => 'Please upload a picture of type jpeg, png, jpg, gif, svg',
            'profilePicture.max' => 'Maximum of 10 MB is allowed for profile picture',
            'unique' => 'This :attribute is already registered'
        ]);
        if (User::where('userType', '=', $input['userType'])
                ->where(function ($query) use ($input) {
                    $query->where('email', '=', $input['email'])
                        ->orWhere('mobile', '=', $input['mobile']);
                })->count() > 0) {
            return redirect()->back()->with('error', 'Email/Mobile already in use!');
        }
        /* profile picture Image upload */
        if ($request->hasFile('profilePicture')) {
            if ($request->file('profilePicture')->isValid()) {
                $path = 'uploads/profilePictures/';
                $filePath = $request->profilePicture->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $imageName = $imagePath[count($imagePath) - 1];
                $input['profilePicture'] = $imageName;
            }
        }
        $pass = str_random(6);
        $input['password'] = Hash::make($pass);
        $input['status'] = 'Y';
        $user = User::create($input);
        $body = EmailTemplate::where(['id' => 2])->first();
        $body->description = str_replace('{EMAIL}', $user->email, $body->description);
        $body->description = str_replace('{MOBILE}', $user->mobileNumber, $body->description);
        $body->description = str_replace('{PASSWORD}', $pass, $body->description);
        $body->firstName = $user->firstName;
        Mail::to($user->email)->send(new RegistrationNotification($body));
        return redirect()->back()->with('success', 'Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $type = $request->segment(2);
        $user = User::find($id);
        $resource = $type;
        $nat = 'Edit';
        $meth = 'PATCH';
        $saveLink = admin_url($type . '/' . $id);
        $resource_pl = str_plural($resource);
        $$resource = $user;
        $countries = Country::orderBy('name', 'asc')->get();
        return view('admin.user.edit', compact($resource, 'resource', 'resource_pl',
            'countries', 'saveLink', 'nat', 'meth'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = $request->segment(2);
        $input = $request->all();
        $user = User::find($id);
        $rules = ['firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'address' => 'nullable',
            'city' => 'nullable',
            'state' => 'nullable',
            'zipCode' => 'nullable',
            'userType' => [
                'required',
                Rule::in(["D", "P"])
            ],
            'profilePicture' => 'mimes:jpeg,png,jpg,gif,svg|max:10240'
        ];
        if ($user->email != $input['email']) {
            $rules['email'] = 'required|string|email|max:255|unique:users';
        } else {
            $rules['email'] = 'required|string|email|max:255';
        }
        if ($user->mobile != $input['mobile']) {
            $rules['mobile'] = 'required|numeric|unique:users';
        } else {
            $rules['mobile'] = 'required|numeric';
        }
        $this->validate($request, $rules, [
            'profilePicture.mimes' => 'Please upload a picture of type jpeg, png, jpg, gif, svg',
            'profilePicture.max' => 'Maximum of 10 MB is allowed for profile picture',
        ]);
        /* profile picture Image upload */
        if ($request->hasFile('profilePicture')) {
            if ($request->file('profilePicture')->isValid()) {
                $path = 'uploads/profilePictures/';
                if ($user->logo != null) {
                    if (Storage::disk('s3')->exists($path . $user->profilePicture)) {
                        Storage::disk('s3')->delete($path . $user->profilePicture);
                    }
                }
                $filePath = $request->profilePicture->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $imageName = $imagePath[count($imagePath) - 1];
                $user->profilePicture = $imageName;
            }
        }
        $user->firstName = isset($input['firstName']) ? $input['firstName'] : "";
        $user->lastName = isset($input['lastName']) ? $input['lastName'] : "";
        $user->gender = isset($input['gender']) ? $input['gender'] : "";
        $user->balance = isset($input['balance']) ? $input['balance'] : "";
        $user->mobile = isset($input['mobile']) ? $input['mobile'] : "";
        $user->email = isset($input['email']) ? $input['email'] : "";
        $user->password = (isset($input['password']) && $input['password'] != '') ? Hash::make($input['password']) : "";
        $user->addressLine1 = isset($input['addressLine1']) ? $input['addressLine1'] : "";
        $user->addressLine2 = isset($input['addressLine2']) ? $input['addressLine2'] : "";
        $user->city = isset($input['city']) ? $input['city'] : "";
        $user->state = isset($input['state']) ? $input['state'] : "";
        $user->zipCode = isset($input['zipCode']) ? $input['zipCode'] : "";
        $user->userType = isset($input['userType']) ? $input['userType'] : "";
        $user->loginType = isset($input['loginType']) ? $input['loginType'] : "";
        $user->status = isset($input['status']) ? $input['status'] : "";
        $user->save();
        return redirect()->back()->with('success', 'Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $type = $request->segment(2);
        $user = User::find($id);
        /*if ($user->profilePicture) {
            $path = 'uploads/profilePictures/';
            if (Storage::disk('s3')->exists($path . $user->profilePicture)) {
                Storage::disk('s3')->delete($path . $user->profilePicture);
            }
        }
        User::destroy($id);*/
        $user->status = 'D';
        $user->save();
        return redirect()->back()->with('success', ucfirst($type) . ' successfully removed!');
    }

    public function changeStatus(Request $request, $id)
    {
        $type = $request->segment(2);
        if (!$request->has('current_status')) {
            return redirect()->back()->with('error', 'Sorry! something went wrong.');
        }

        if ($request->input('current_status') == 'Y') {
            $is_active = 'N';
            $msg = 'disabled';
        } else {
            $is_active = 'Y';
            $msg = 'enabled';
        }

        User::where('id', $id)->update(['status' => $is_active]);
        return redirect()->back()->with('success', 'Account is ' . $msg . '!');
    }


    public function listDocs(Request $request)
    {
        $keyword = $request->input('keyword');
        $resource = 'document';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $$resource_pl = UserDocument::whereHas('user', function ($query) use ($keyword) {
            if ($keyword != '') {
                $query->where(function ($query) use ($keyword) {
                    $query->where('firstName', 'like', '%' . $keyword . '%')
                        ->where('firstName', '!=', null);
                })->orWhere(function ($query) use ($keyword) {
                    $query->where('lastName', 'like', '%' . $keyword . '%')
                        ->where('lastName', '!=', null);
                })->orWhere(function ($query) use ($keyword) {
                    $query->where('email', 'like', '%' . $keyword . '%')
                        ->where('email', '!=', null);
                });
            }
        })->orderBy('id', 'desc')
            ->with(['user' => function ($query) {
                $query->select('id', 'firstName', 'lastName');
            }, 'docs' => function ($query) {
                $query->select('id', 'name');
            }])
            ->paginate($per_page);
        $keybox = true;
        return view('admin.user.docs.list', compact($resource_pl, 'resource', 'resource_pl', 'keyword', 'keybox'));
    }

    public function listUserDocs(Request $request, $id)
    {
        $keyword = $request->input('keyword');
        $resource = 'document';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $imageExtensions = ["jpg","png","jpeg"];
        $$resource_pl = UserDocument::where('userId', '=', $id)->orderBy('id', 'desc')
            ->with(['user' => function ($query) {
                $query->select('id', 'firstName', 'lastName');
            }, 'docs' => function ($query) {
                $query->select('id', 'name');
            }])
            ->paginate($per_page);
        $keybox = false;
        foreach ($$resource_pl as $key => $data) {
            $getExtension = getExtension($data['userDocument']);
            if (in_array(getExtension($data['userDocument']), $imageExtensions)){
                $$resource_pl[$key]->typeImage = 'Y';
            }else{
                $$resource_pl[$key]->typeImage = 'N';
            }
        }
        //dd($$resource_pl);
        $verificationDocs = VerificationDocument::all();
        return view('admin.user.docs.list', compact($resource_pl, 'resource', 'resource_pl', 'keyword', 'keybox', 'verificationDocs', 'id'));
    }

    public function docUpdate(Request $request, $id)
    {
        $input = $request->all();
        $rules = [
            'verifyStatus' => [
                'required',
                Rule::in(["Y", "N"])
            ]
        ];
        $this->validate($request, $rules, []);
        $userDcmnt = UserDocument::find($id);
        $usr = User::find($userDcmnt->userId);
        $userDcmnt->verifyStatus = $input['verifyStatus'];
        $userDcmnt->save();
        $verificationDocs = VerificationDocument::find($userDcmnt->docType);

        if ($input['verifyStatus'] == 'Y') {
            $msg = 'Document has been Approved Successfully';
            $template_id = 3;
        } else {
            $msg = 'Document has been Rejected Successfully';
            $template_id = 7;
            $usr->driverApproval = 'N';
            $usr->save();
        }

        $body = EmailTemplate::find($template_id);
        $body->description = str_replace('{DOCUMENT}', $verificationDocs->name, $body->description);
        $body->firstName = $usr->firstName;
        if (!empty($usr->email)) {
            Mail::to($usr->email)->send(new DocumentVerification($body));
        }
        $tokens = collect($usr->UserTokens()->get())->toArray();
        if (!empty($tokens)) {
            $fcmTokensI = [];
            $fcmTokensA = [];
            foreach ($tokens as $key => $values) {
                if ($values['flag'] == 'A')
                    $fcmTokensA[] = $values['fcmToken'];
                if ($values['flag'] == 'I')
                    $fcmTokensI[] = $values['fcmToken'];
            }
            if (!empty($fcmTokensA))
                sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'DVS', 'title' => 'Desty', 'body' => $verificationDocs->name . $msg]]);
            if (!empty($fcmTokensI))
                sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'DVS']], ['title' => 'Desty', 'body' => $verificationDocs->name . $msg]);
        }
        if ($usr->UserDocs()->where('verifyStatus', '=', 'Y')->count() == 3)
		{
            $usr->driverApproval = 'Y';
            /*Temporary SSN Update*/
            $usr->completion = 'Y';
            /*Temporary SSN Update*/
            $usr->save();
			
            $body = EmailTemplate::find(8);
            $body->firstName = $usr->firstName;
            if (!empty($usr->email)) {
                Mail::to($usr->email)->send(new DocumentVerification($body));
            }
            $tokens = collect($usr->UserTokens()->get())->toArray();
            if (!empty($tokens)) {
                $fcmTokensI = [];
                $fcmTokensA = [];
                foreach ($tokens as $key => $values) {
                    if ($values['flag'] == 'A')
                        $fcmTokensA[] = $values['fcmToken'];
                    if ($values['flag'] == 'I')
                        $fcmTokensI[] = $values['fcmToken'];
                }
                if (!empty($fcmTokensA))
                    sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'DVS', 'title' => 'Desty', 'body' => "All your Documents have been verified successfully. You are now eligible for getting ride requests."]]);
                if (!empty($fcmTokensI))
                    sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'DVS']], ['title' => 'Desty', 'body' => "All your Documents have been verified successfully. You are now eligible for getting ride requests."]);
            }
        }
        return redirect()->back()->with('success', $msg);
    }

    public function docUpsert(Request $request, $id)
    {
        $input = $request->all();
        $rules = [
            'verifyDoc' => 'required|file',
            'docType' => 'required|exists:verification_documents,id'
        ];
        $this->validate($request, $rules, []);
        $user = User::find($id);
        $userDoc = $user->UserDocs()->where(['docType' => $input['docType']])->first();
        if ($request->hasFile('verifyDoc') && $request->file('verifyDoc')->isValid()) {
            $path = 'uploads/driverVerification/';
            if ($userDoc) {
                if ($userDoc->userDocument != null) {
                    if (Storage::disk('s3')->exists($path . $userDoc->userDocument)) {
                        Storage::disk('s3')->delete($path . $userDoc->userDocument);
                    }
                }
            }
            $filePath = $request->file('verifyDoc')->store($path, 's3');
            $imagePath = explode('/', $filePath);
            $fileName = $imagePath[count($imagePath)-1];
        }
        else {
            $msg = 'Invalid File';
            return redirect()->back()->with('error', $msg);
        }
        $user->UserDocs()->updateOrCreate(
			[
				'docType' => $input['docType']
			],
			[
				'userDocument' => $fileName, 'docType' => $input['docType'], 'verifyStatus' => 'Y'
			]
		);
		if ($user->UserDocs()->where('verifyStatus', '=', 'Y')->count() == 3)
		{
			$user->driverApproval = 'Y';
            /*Temporary SSN Update*/
            $user->completion = 'Y';
            /*Temporary SSN Update*/
            $user->save();
		}
        $msg = 'Verified Document Uploaded Successfully!';
        return redirect()->back()->with('success', $msg);
    }


    public function listCashout(Request $request)
    {
        $keyword = $request->input('keyword');
        $resource = 'withdrawal';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $saveUrl = admin_url('driver/withdrawal/');
        $$resource_pl = User::/*whereHas('UserBankAccounts', function ($query) {
            $query->where('is_default', '1');
        })->*/
        where('balance', '>', 0)
            ->where(['userType' => 'D', 'status' => 'Y', 'driverApproval' => 'Y'])
            ->where(function ($query) use ($keyword) {
                if ($keyword != '') {
                    $query->where(function ($query) use ($keyword) {
                        $query->where('firstName', 'like', '%' . $keyword . '%')
                            ->where('firstName', '!=', null);
                    })->orWhere(function ($query) use ($keyword) {
                        $query->where('lastName', 'like', '%' . $keyword . '%')
                            ->where('lastName', '!=', null);
                    });
                }
            })->select('id', 'firstName', 'lastName', 'balance')
            ->orderBy('created_at', 'asc')
            ->with(['UserBankAccounts' => function ($query) {
                $query->where('is_default', '1')
                    ->select('user_id', 'bank_name', 'user_name_same_as_bank_ac', 'account_number', 'ifsc_code', 'is_default');
            }])
            ->paginate($per_page);
        return view('admin.user.cashout.list', compact($resource_pl, 'resource', 'resource_pl', 'saveUrl', 'keyword'));
    }

    public function cashoutUpdate($id)
    {
        $admin = Auth::user();
        $user = User::with(['UserTokens'])->find($id);
        if ($user) {
            $description = 'Withdrawal of $ ' . $user->balance;
            Transaction::create([
                'userId' => $user->id,
                'amount' => $user->balance,
                'transactionType' => 'C',
                'description' => $description
            ]);
            Transaction::create([
                'userId' => 1,
                'amount' => $user->balance,
                'transactionType' => 'D',
                'description' => $description . ' by user'
            ]);
            UserTransaction::create([
                'userId' => 1,
                'amount' => $user->balance,
                'transactionType' => 'D',
                'description' => $description . ' by user'
            ]);
            UserTransaction::create([
                'userId' => $user->id,
                'amount' => $user->balance,
                'transactionType' => 'D',
                'description' => $description
            ]);
            $admin->balance = $admin->balance - $user->balance;
            $admin->save();
            $user->balance = 0;
            $user->save();
            $userArr = collect($user)->toArray();
            $body = EmailTemplate::where(['id' => 4])->first();
            $body->description = str_replace('{CURRENCY}', '$', $body->description);
            $body->description = str_replace('{AMOUNT}', $user->balance, $body->description);
            $body->firstName = $user->firstName;
            if (!empty($user->email)) {
                Mail::to($user->email)->send(new WithdrawalStatus($body));
            }
            if (!empty($userArr['user_tokens'])) {
                $tokens = $userArr['user_tokens'];
                $fcmTokensI = [];
                $fcmTokensA = [];
                foreach ($tokens as $key => $values) {
                    if ($values['flag'] == 'A')
                        $fcmTokensA[] = $values['fcmToken'];
                    if ($values['flag'] == 'I')
                        $fcmTokensI[] = $values['fcmToken'];
                }
                if (!empty($fcmTokensA))
                    sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'CRA', 'amount' => $user->balance, 'title' => 'Desty', 'body' => 'Your Withdrawal request has been accepted!']]);
                if (!empty($fcmTokensI))
                    sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'CRA', 'amount' => $user->balance]], ['title' => 'Desty', 'body' => 'Your Withdrawal request has been accepted!']);
            }
            return redirect()->back()->with('success', 'Paid Successfully.');
        } else {
            return redirect()->back()->with('success', 'Payment Failed.');
        }
    }


    public function listNegBalance(Request $request)
    {
        $keyword = $request->input('keyword');
        $resource = 'walletadjust';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $saveUrl = admin_url('driver/walletadjust/');
        $$resource_pl = User::where('userType', '=', 'D')
            ->where('balance', '<', 0.00)
            ->where(function ($query) use ($keyword) {
                if ($keyword != '') {
                    $query->where(function ($query) use ($keyword) {
                        $query->where('firstName', 'like', '%' . $keyword . '%')
                            ->where('firstName', '!=', null);
                    })->orWhere(function ($query) use ($keyword) {
                        $query->where('lastName', 'like', '%' . $keyword . '%')
                            ->where('lastName', '!=', null);
                    })->orWhere(function ($query) use ($keyword) {
                        $query->where('email', 'like', '%' . $keyword . '%')
                            ->where('email', '!=', null);
                    });
                }
            })->orderBy('id', 'desc')->paginate($per_page);

        // Rahul changes start
        foreach ($$resource_pl as $key => $data) {
            $userId = $data->id;
            $lastAdjustment =  UserTransaction::whereNull('taxiRideId')->where('userId',$userId)->orderBy("id", "DESC")->first();

            $totalAmountByCash = UserTransaction::select(DB::raw('SUM(COALESCE(CASE WHEN transactionType = "C" THEN amount END,0)) AS totalAmount'))
            ->whereHas('TaxiRide', function($q){
                $q->where('paidBy', 'S');
            })->where('userId', '=', $userId)
            ->where(['paymentStatus' => "Y"])
            ->where('description','like','%'.'Received for the ride'.'%');

            $totalAmountByCard = UserTransaction::select(DB::raw('SUM(COALESCE(CASE WHEN transactionType = "C" THEN amount END,0)) AS totalAmount'))
            ->whereHas('TaxiRide', function($q){
                $q->where('paidBy', 'R');
            })->where('userId', '=', $userId)
            ->where(['paymentStatus' => "Y"])
            ->where('description','like','%'.'Received for the ride'.'%');

            if(!empty($lastAdjustment)){
                $totalAmountByCash =$totalAmountByCash->where('id', '>' , $lastAdjustment->id);
                $totalAmountByCard =$totalAmountByCard->where('id', '>' , $lastAdjustment->id);;
            }
            $totalAmountByCash =$totalAmountByCash->pluck('totalAmount')[0];
            $totalAmountByCard =$totalAmountByCard->pluck('totalAmount')[0];
            $adminCommissionCharge = (float)((get_general_settings('commission')) / 100);

            $commission = $totalAmountByCash * $adminCommissionCharge;

            $commissionByCard = $totalAmountByCard * $adminCommissionCharge;
            $haveRecivedFromAdmin = $totalAmountByCard - $commissionByCard ;
            $fareCommission = $commission - $haveRecivedFromAdmin;
            
            

            $adjustmanet = "";
            if($totalAmountByCash){
                $adjustmanet = $adjustmanet . " ( ". $totalAmountByCash ." * ". $adminCommissionCharge ." ) ";
            }

            $ChargeForRideCantalation = (- $data['balance'] ) - $fareCommission;
            
            if($totalAmountByCard){
                $adjustmanet = $adjustmanet . " - ( ". $totalAmountByCard ." - ". $commissionByCard. " ) ";
            }
            $adjustmanet = $adjustmanet ." = ". $fareCommission;
            
            $$resource_pl[$key]->totalAmountByCash = $totalAmountByCash;
            $$resource_pl[$key]->totalAmountByCard = $totalAmountByCard;
            $$resource_pl[$key]->ChargeForRideCantalation = $ChargeForRideCantalation;
            $$resource_pl[$key]->adjustmanet = $adjustmanet;
            //$$resource_pl[$key]->totalAmount = $totalAmountByCash;
        }
        // Rahul changes end
        return view('admin.user.walletAdjust.list', compact($resource_pl, 'resource', 'resource_pl', 'saveUrl', 'keyword'));
    }

    public function resetWallet($id)
    {
        $admin = Auth::user();
        $user = User::with(['UserTokens'])->find($id);
        $description = "Adjustment of $ " . $user->balance;
        Transaction::create([
            'userId' => $user->id,
            'amount' => $user->balance,
            'transactionType' => 'D',
            'description' => $description
        ]);
        Transaction::create([
            'userId' => 1,
            'amount' => $user->balance,
            'transactionType' => 'C',
            'description' => $description . " by user"
        ]);
        UserTransaction::create([
            'userId' => 1,
            'amount' => $user->balance,
            'transactionType' => 'C',
            'description' => $description . " by user"
        ]);
        UserTransaction::create([
            'userId' => $user->id,
            'amount' => $user->balance,
            'transactionType' => 'C',
            'description' => $description
        ]);
        $body = EmailTemplate::where(['id' => 6])->first();
        $body->description = str_replace('{AMOUNT}', $user->balance, $body->description);
        $body->firstName = $user->firstName;
        if (!empty($user->email)) {
            Mail::to($user->email)->send(new WalletReset($body));
        }
        if (!empty($user->user_tokens)) {
            $tokens = $user->user_tokens;
            $fcmTokensI = [];
            $fcmTokensA = [];
            foreach ($tokens as $key => $values) {
                if ($values['flag'] == 'A')
                    $fcmTokensA[] = $values['fcmToken'];
                if ($values['flag'] == 'I')
                    $fcmTokensI[] = $values['fcmToken'];
            }
            if (!empty($fcmTokensA))
                sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'WA', 'amount' => $user->balance, 'title' => config('constant.APP_NAME'), 'body' => 'Your wallet has been adjusted!']]);
            if (!empty($fcmTokensI))
                sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'WA', 'amount' => $user->balance]], ['title' => config('constant.APP_NAME'), 'body' => 'Your wallet has been adjusted!']);
        }
        $admin->balance = $admin->balance + $user->balance;
        $admin->save();
        $user->balance = 0.00;
        $user->save();
        return redirect()->back()->with('success', 'Wallet Adjusted Successfully.');
    }


    public function listRides(Request $request, $id)
    {
        $type = $request->segment(2);
        $startDate = $request->input('start');
        $endDate = $request->input('end');
        $user = User::with(['UserTaxis'])->find($id);
        $resource = 'ride';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $saveUrl = admin_url('driver/ride/');
        $$resource_pl = TaxiRide::where(function ($query) use ($user, $startDate, $endDate) {
            $query->where('passengerId', '=', $user->id)
                ->orWhere(function ($qwery) use ($user) {
                    $qwery->whereHas('UserTaxi', function ($query2) use ($user) {
                        $query2->where('userId', '=', $user->id);
                    });
                });
            if (isset($startDate) && isset($endDate)) {
                $query->where('created_at', '>=', date('Y-m-d H:i:s', strtotime($startDate)))
                    ->where('created_at', '<=', date('Y-m-d H:i:s', strtotime($endDate . " 23:59:59")));
            }
        })->with(['Passenger' => function ($query) {
            $query->select('id', 'firstName', 'lastName', 'profilePicture', 'balance');
        }, 'UserTaxi.Driver' => function ($query) {
            $query->select('id', 'firstName', 'lastName', 'profilePicture', 'balance');
        }, 'Coupon'])
            ->orderBy('id', 'desc')
            ->paginate($per_page);
        return view('admin.user.rides.list', compact($resource_pl, 'resource', 'resource_pl', 'saveUrl', 'user', 'startDate', 'endDate'));
    }


    public function ssnPendingList(Request $request)
    {
        $type = $request->segment(2);
        $keyword = $request->input('keyword');
        $resource = $type;
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        if ($type == 'driver') {
            $userType = "D";
        } else {
            return redirect(admin_url());
        }
        $$resource_pl = User::where(['userType' => $userType])
            ->whereNotNull('ssn')
            ->where('ssn', '<>', '')
            ->where('completion', '=', 'P')
            ->where(function ($qwery) use ($keyword) {
                if ($keyword != '') {
                    $qwery->orWhere(function ($query) use ($keyword) {
                        $query->where('firstName', 'like', '%' . $keyword . '%')
                            ->where('firstName', '!=', null);
                    })->orWhere(function ($query) use ($keyword) {
                        $query->where('lastName', 'like', '%' . $keyword . '%')
                            ->where('lastName', '!=', null);
                    })->orWhere(function ($query) use ($keyword) {
                        $query->where('email', 'like', '%' . $keyword . '%')
                            ->where('email', '!=', null);
                    });
                }
            })->orderBy('id', 'desc')->paginate($per_page);
        return view('admin.user.ssn.list', compact($resource_pl, 'resource', 'resource_pl', 'keyword'));
    }

    public function ssnUpdate(Request $request, $id)
    {
        $admin = Auth::user();
        $user = User::with(['UserTokens'])->find($id);
        if ($user && $request->has('current_status') && ($request->input('current_status') == 'Y' || $request->input('current_status') == 'N')) {
            $current_status = $request->input('current_status') == 'Y' ? "Approved" : "Rejected";
            $user->completion = $request->input('current_status');
            $user->save();
            $userArr = collect($user)->toArray();
            /*$body = EmailTemplate::where(['id' => 4])->first();
            $body->description = str_replace('{CURRENCY}', '$', $body->description);
            $body->description = str_replace('{AMOUNT}', $user->balance, $body->description);
            $body->firstName = $user->firstName;
            if (!empty($user->email)) {
                Mail::to($user->email)->send(new WithdrawalStatus($body));
            }*/
            if (!empty($userArr['user_tokens'])) {
                $tokens = $userArr['user_tokens'];
                $fcmTokensI = [];
                $fcmTokensA = [];
                foreach ($tokens as $key => $values) {
                    if ($values['flag'] == 'A')
                        $fcmTokensA[] = $values['fcmToken'];
                    if ($values['flag'] == 'I')
                        $fcmTokensI[] = $values['fcmToken'];
                }
                if (!empty($fcmTokensA))
                    sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'SSNU', 'title' => 'Desty', 'body' => 'Your SSN Number has been ' . $current_status . '!']]);
                if (!empty($fcmTokensI))
                    sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'SSNU']], ['title' => 'Desty', 'body' => 'Your SSN Number has been ' . $current_status . '!']);
            }
            return redirect()->back()->with('success', $current_status . ' Successfully.');
        } else {
            return redirect()->back()->with('success', 'Change Failed.');
        }
    }


    public function ratingList(Request $request)
    {
        $type = $request->segment(2);
        $keyword = $request->input('keyword');
        $resource = $type;
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $userType = "P";
        if ($type == 'driver') {
            $userType = "D";
        }
        $$resource_pl = UserRating::select(DB::raw('COUNT(`user_ratings`.`raterId`) AS totalRating, `user_ratings`.`rateeId`, AVG(`user_ratings`.`driverRating`) AS avg_driverRating, AVG(`user_ratings`.`carRating`) AS avg_carRating, AVG(`user_ratings`.`drivingRating`) as avg_drivingRating, AVG(`user_ratings`.`rating`) AS avg_rating'))
            ->whereHas('Ratee', function ($qwery) use ($keyword, $userType) {
                $qwery->where(['userType' => $userType]);
                if ($keyword != '') {
                    $qwery->where(function ($qery) use ($keyword) {
                        $qery->orWhere(function ($query) use ($keyword) {
                            $query->where('firstName', 'like', '%' . $keyword . '%')
                                ->where('firstName', '!=', null);
                        })->orWhere(function ($query) use ($keyword) {
                            $query->where('lastName', 'like', '%' . $keyword . '%')
                                ->where('lastName', '!=', null);
                        })->orWhere(function ($query) use ($keyword) {
                            $query->where('email', 'like', '%' . $keyword . '%')
                                ->where('email', '!=', null);
                        });
                    });
                }
            })
            ->with([
                'Ratee' => function ($query) {
                    $query->select('id', 'firstName', 'lastName', 'profilePicture', 'status');
                }
            ])
            ->groupBy('rateeId')->paginate($per_page);
        return view('admin.user.rating.list', compact($resource_pl, 'resource', 'resource_pl', 'keyword'));
    }

    public function getTaxiDetails(Request $request)
    {
        $selectedId = config('constant.SELECTED_COLOR');

        $response['has_error'] = 0;
        $user_id = $request->has('user_id') ? $request->input('user_id') : '';
        $user = User::where('id', $user_id)
                ->with('UserCars')
                ->with('UserCars.TaxiTypes')
                ->first();
        $taxiTypes = TaxiType::get();
        $taxiColors = UserCarColour::whereIn('id', $selectedId)->get();
        
        $formHTML = View::make('admin.user.car.add_edit_car')
                ->with('user', $user)
                ->with('taxiTypes',$taxiTypes)
                ->with('taxiColors', $taxiColors)
                ->render();
        $response['form'] = $formHTML;

        return response()->json($response);
    }

    public function addEditCar(Request $request)
    {
        $response['has_error'] = 1;
        $response['errors'] = [];
        $validator = Validator::make($request->all(), [
            'numberPlate' => 'required|string',
            'make' => 'required|string',
            'model' => 'required|string',
            'colour' => 'required|string',
            'year' => 'required|numeric',
            'taxiTypes' => 'required|array|between:1,2',
        ]);
        if ($validator->fails())
        {
            $response['errors'] = $validator->errors();
            //return response()->json($response);
        }
        else
        {
            $response['has_error'] = 0;
            $taxyTypeChecked = $request->input('taxiTypes');
            $carId = $request->input('carId');
            $userId = $request->input('userId');
            $numberPlate = $request->input('numberPlate');
            $make = $request->input('make');
            $model = $request->input('model');
            $colour = $request->input('colour');
            $year = $request->input('year');
            
            $user_cars = UserCar::find($carId);
            //dd($user_cars);
            if(!$user_cars)
            {
                $user_cars = new UserCar();
            }
            $user_cars->userId = $userId;
            $user_cars->numberPlate = $numberPlate;
            $user_cars->make = $make;
            $user_cars->model = $model;
            $user_cars->colour = $colour;
            $user_cars->year = $year;
            $user_cars->created_at = date('Y-m-d H:i:s');
            $user_cars->save();
            $carId = $user_cars->id;
            //dd($carId);

            UserTaxi::where('userId', $userId)->whereNotIn('taxiTypeId', $taxyTypeChecked)->delete();
            foreach($taxyTypeChecked as $key => $taxyType)
            {
                $findCar = UserTaxi::where(['userId'=>$userId, 'carId'=>$carId, 'taxiTypeId'=>$taxyType])->first();
                if(!$findCar)
                {
                    //dd($findCar);
                    $user_taxi_type = new UserTaxi();
                    $user_taxi_type->userId = $userId;
                    $user_taxi_type->carId = $carId;
                    $user_taxi_type->taxiTypeId = $taxyType;
                    $user_taxi_type->forHire = 'N';
                    $user_taxi_type->available = 'N';
                    $user_taxi_type->requested = 'N';
                    $user_taxi_type->created_at = date('Y-m-d H:i:s');
                    $user_taxi_type->save();
                }
            }
        }
        return response()->json($response);
    }

    // rahul changes start 11/18/19
    public function bankInfo(Request $request,$userId)
    {
        $type = $request->segment(2);
        $resource_pl = str_plural($type);
        $userType = "D"; // as bank information only for driver
        $per_page = config('constant.ADMIN_PER_PAGE');
        $keyword=$request->has('keyword') ? $request->input('keyword') : null;
        $userDetails = User::where('userType' , $userType)->where('id' , $userId)->first();
        if ($type == 'driver' && !empty($userDetails)) {
            $userBankAccounts = UserBankAccount::with(['countryDetails','stateDetails']);
            if (!empty($keyword)) {
                $userBankAccounts = $userBankAccounts->where('bank_name', 'like' , '%'.$keyword.'%')
                                    ->orWhere('user_name_same_as_bank_ac', 'like' , '%'.$keyword.'%')
                                    ->orWhere('account_number', 'like' , '%'.$keyword.'%')
                                    ->orWhere('ifsc_code', 'like' , '%'.$keyword.'%');
            }
            $userBankAccounts = $userBankAccounts
            ->where('user_id',$userDetails->id)
            ->orderBy('id', 'desc')
            ->paginate($per_page);
            //dd($userBankAccounts->toArray());
            return view('admin.user.bankInfo',[
                'userDetails' => $userDetails->toArray(),
                'userBankAccounts' => $userBankAccounts,
                'resource_pl' => $resource_pl,
                'keyword' => $keyword
            ]);
        }else{
            return redirect()->back();
        }
    }

    public function listPosBalance(Request $request)
    {
        //dd('test');
        $keyword = $request->input('keyword');
        $resource = 'paymentadjust';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $saveUrl = admin_url('driver/paymentadjust/');

        $$resource_pl = User::where('userType', '=', 'D')
            ->where('balance', '>', 0.00)
            ->where(function ($query) use ($keyword) {
                if ($keyword != '') {
                    $query->where(function ($query) use ($keyword) {
                        $query->where('firstName', 'like', '%' . $keyword . '%')
                            ->where('firstName', '!=', null);
                    })->orWhere(function ($query) use ($keyword) {
                        $query->where('lastName', 'like', '%' . $keyword . '%')
                            ->where('lastName', '!=', null);
                    })->orWhere(function ($query) use ($keyword) {
                        $query->where('email', 'like', '%' . $keyword . '%')
                            ->where('email', '!=', null);
                    });
                }
            })->orderBy('id', 'desc')->paginate($per_page);

        foreach ($$resource_pl as $key => $data) {
            $userId = $data->id;
            $lastAdjustment =  UserTransaction::whereNull('taxiRideId')->where('userId',$userId)->orderBy("id", "DESC")->first();

            $totalAmountByCash = UserTransaction::select(DB::raw('SUM(COALESCE(CASE WHEN transactionType = "C" THEN amount END,0)) AS totalAmount'))
            ->whereHas('TaxiRide', function($q){
                $q->where('paidBy', 'S');
            })->where('userId', '=', $userId)
            ->where(['paymentStatus'=> "Y"])
            ->where('description','like','%'.'Received for the ride'.'%');

            $totalAmountByCard = UserTransaction::select(DB::raw('SUM(COALESCE(CASE WHEN transactionType = "C" THEN amount END,0)) AS totalAmount'))
            ->whereHas('TaxiRide', function($q){
                $q->where('paidBy', 'R');
            })->where('userId', '=', $userId)
            ->where(['paymentStatus'=> "Y"])
            ->where('description','like','%'.'Received for the ride'.'%');

            if(!empty($lastAdjustment)){
                $totalAmountByCash =$totalAmountByCash->where('id', '>' , $lastAdjustment->id);
                $totalAmountByCard =$totalAmountByCard->where('id', '>' , $lastAdjustment->id);;
            }
            $totalAmountByCash =$totalAmountByCash->pluck('totalAmount')[0];
            $totalAmountByCard =$totalAmountByCard->pluck('totalAmount')[0];
            $adminCommissionCharge = (float)((get_general_settings('commission')) / 100);

            $commission = $totalAmountByCash * $adminCommissionCharge;

            $commissionByCard = $totalAmountByCard * $adminCommissionCharge;
            $haveRecivedFromAdmin = $totalAmountByCard - $commissionByCard ;
            $fareCommission = $commission - $haveRecivedFromAdmin;
            
            

            $adjustmanet = "";
            if($totalAmountByCash){
                $adjustmanet = $adjustmanet . " ( ". $totalAmountByCash ." * ". $adminCommissionCharge ." ) ";
            }

            $ChargeForRideCantalation = (- $data['balance'] ) - $fareCommission;
            
            if($totalAmountByCard){
                $adjustmanet = $adjustmanet . " - ( ". $totalAmountByCard ." - ". $commissionByCard. " ) ";
            }
            $adjustmanet = $adjustmanet ." = ". $fareCommission;
            
            $$resource_pl[$key]->totalAmountByCash = $totalAmountByCash;
            $$resource_pl[$key]->totalAmountByCard = $totalAmountByCard;
            $$resource_pl[$key]->ChargeForRideCantalation = $ChargeForRideCantalation;
            $$resource_pl[$key]->adjustmanet = $adjustmanet;
            //$$resource_pl[$key]->totalAmount = $totalAmountByCash;
        }

        return view('admin.user.paymentadjust.list', compact($resource_pl, 'resource', 'resource_pl', 'saveUrl', 'keyword'));
    }

    public function resetWalletbyPayment($id)
    {
        $admin = Auth::user();
        $user = User::with(['UserTokens'])->find($id);
        $description = "Adjustment of $ " . $user->balance;

        Transaction::create([
            'userId' => $user->id,
            'amount' => $user->balance,
            'transactionType' => 'C',
            'description' => $description
        ]);
        Transaction::create([
            'userId' => 1,
            'amount' => $user->balance,
            'transactionType' => 'D',
            'description' => $description . " by admin"
        ]);
        UserTransaction::create([
            'userId' => 1,
            'amount' => $user->balance,
            'transactionType' => 'D',
            'description' => $description . " by admin"
        ]);
        UserTransaction::create([
            'userId' => $user->id,
            'amount' => $user->balance,
            'transactionType' => 'C',
            'description' => $description
        ]);
        $body = EmailTemplate::where(['id' => 6])->first();
        $body->description = str_replace('{AMOUNT}', $user->balance, $body->description);
        $body->firstName = $user->firstName;
        if (!empty($user->email)) {
            Mail::to($user->email)->send(new WalletReset($body));
        }
        if (!empty($user->user_tokens)) {
            $tokens = $user->user_tokens;
            $fcmTokensI = [];
            $fcmTokensA = [];
            foreach ($tokens as $key => $values) {
                if ($values['flag'] == 'A')
                    $fcmTokensA[] = $values['fcmToken'];
                if ($values['flag'] == 'I')
                    $fcmTokensI[] = $values['fcmToken'];
            }
            if (!empty($fcmTokensA))
                sendNotification($fcmTokensA, ['dataContent' => ['notiType' => 'WA', 'amount' => $user->balance, 'title' => config('constant.APP_NAME'), 'body' => 'Your payment has been adjusted!']]);
            if (!empty($fcmTokensI))
                sendNotification($fcmTokensI, ['dataContent' => ['notiType' => 'WA', 'amount' => $user->balance]], ['title' => config('constant.APP_NAME'), 'body' => 'Your payment has been adjusted!']);
        }
        $admin->balance = $admin->balance - $user->balance;
        $admin->save();
        $user->balance = 0.00;
        $user->save();
        return redirect()->back()->with('success', 'Payment Adjusted Successfully.');
    }
    //rahul changes end
}
