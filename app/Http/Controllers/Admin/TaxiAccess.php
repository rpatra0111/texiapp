<?php

namespace App\Http\Controllers\Admin;

use App\TaxiType;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class TaxiAccess extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resource = 'taxiType';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $$resource_pl = TaxiType::orderBy('id', 'asc')->paginate($per_page);
        return view('admin.taxiType.list', compact($resource_pl, 'resource', 'resource_pl'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nat = 'Add';
        $meth = 'POST';
        $resource = 'taxiType';
        $saveLink = admin_url('taxitype');
        $resource_pl = str_plural($resource);
        $$resource = '';
        return view('admin.taxiType.edit', compact($resource, 'resource', 'resource_pl',
            'saveLink', 'nat', 'meth'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $rules = ['name' => 'required|string|max:255',
            'picture' => 'required|mimes:jpeg,png,jpg,gif,svg|max:10240',
            'picturegrey' => 'required|mimes:jpeg,png,jpg,gif,svg|max:10240',
            'fenceDistance' => 'required',
            'baseCost' => 'required',
            'perKmCost' => 'required',
            'cancelCharge' => 'required',
            'timeCharge' => 'required',
            'minCharge' => 'required'
        ];
        $this->validate($request, $rules, [
            'mimes' => 'Please upload a picture of type jpeg, png, jpg, gif, svg',
            'max' => 'Maximum of 10 MB is allowed for :attribute',
            'unique' => 'This :attribute is already registered'
        ]);
        /* profile picture Image upload */
        if ($request->hasFile('picture')) {
            if ($request->file('picture')->isValid()) {
                $path = 'uploads/taxiPictures/';
                $filePath = $request->picture->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $imageName = $imagePath[count($imagePath)-1];
                $input['picture'] = $imageName;
            }
        }
        if ($request->hasFile('picturegrey')) {
            if ($request->file('picturegrey')->isValid()) {
                $path = 'uploads/taxiPictures/';
                $filePath = $request->picturegrey->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $imageName = $imagePath[count($imagePath)-1];
                $input['picturegrey'] = $imageName;
            }
        }
        $taxi = TaxiType::create($input);
        return redirect()->back()->with('success', 'Created successfully!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $taxi = TaxiType::find($id);
        $nat = 'Edit';
        $meth = 'PATCH';
        $resource = 'taxi';
        $saveLink = admin_url('taxitype/' . $id);
        $resource_pl = str_plural($resource);
        $$resource = $taxi;
        return view('admin.taxiType.edit', compact($resource, 'resource', 'resource_pl',
            'saveLink', 'nat', 'meth'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $taxi = TaxiType::find($id);
        $rules = ['name' => 'required|string|max:255',
            'picture' => 'mimes:jpeg,png,jpg,gif,svg|max:10240',
            'picturegrey' => 'mimes:jpeg,png,jpg,gif,svg|max:10240',
            'fenceDistance' => 'required',
            'baseCost' => 'required',
            'perKmCost' => 'required',
            'cancelCharge' => 'required',
            'timeCharge' => 'required',
            'minCharge' => 'required'
        ];

        $this->validate($request, $rules, [
            'picture.mimes' => 'Please upload a picture of type jpeg, png, jpg, gif, svg',
            'picture.max' => 'Maximum of 10 MB is allowed for profile picture',
            'picturegrey.mimes' => 'Please upload a picture of type jpeg, png, jpg, gif, svg',
            'picturegrey.max' => 'Maximum of 10 MB is allowed for profile picture',
        ]);
        /* profile picture Image upload */
        if ($request->hasFile('picture')) {
            if ($request->file('picture')->isValid()) {
                $path = 'uploads/taxiPictures/';
                if ($taxi->picture != null) {
                    if (Storage::disk('s3')->exists($path . $taxi->picture)) {
                        Storage::disk('s3')->delete($path . $taxi->picture);
                    }
                }
                $filePath = $request->picture->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $imageName = $imagePath[count($imagePath)-1];
                $taxi->picture = $imageName;
            }
        }
        if ($request->hasFile('picturegrey')) {
            if ($request->file('picturegrey')->isValid()) {
                $path = 'uploads/taxiPictures/';
                if ($taxi->picturegrey != null) {
                    if (Storage::disk('s3')->exists($path . $taxi->picturegrey)) {
                        Storage::disk('s3')->delete($path . $taxi->picturegrey);
                    }
                }
                $filePath = $request->picturegrey->store($path, 's3');
                $imagePath = explode('/', $filePath);
                $imageName = $imagePath[count($imagePath)-1];
                $taxi->picturegrey = $imageName;
            }
            $path = public_path() . '/assets/uploads/taxiPictures/';
            $image = $request->file('picturegrey');
            if ($taxi->pictureGrey != null) {
                File::delete($path . $taxi->pictureGrey);
            }
            $save_name = time() . str_random(10) . '.' . $image->getClientOriginalExtension();
            $input['picturegrey']->move($path, $save_name);
            $taxi->pictureGrey = $save_name;
        }
        $taxi->name = isset($input['name']) ? $input['name'] : "";
        $taxi->fenceDistance = isset($input['fenceDistance']) ? $input['fenceDistance'] : "";
        $taxi->baseCost = isset($input['baseCost']) ? $input['baseCost'] : "";
        $taxi->perKmCost = isset($input['perKmCost']) ? $input['perKmCost'] : "";
        $taxi->cancelCharge = isset($input['cancelCharge']) ? $input['cancelCharge'] : "";
        $taxi->timeCharge = isset($input['timeCharge']) ? $input['timeCharge'] : "";
        $taxi->minCharge = isset($input['minCharge']) ? $input['minCharge'] : "";
        $taxi->save();
        return redirect()->back()->with('success', 'Updated successfully.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $taxi = TaxiType::find($id);
        $path = public_path() . '/assets/uploads/taxiPictures/';
        if ($taxi->picture != null) {
            File::delete($path . $taxi->picture);
        }
        if ($taxi->pictureGrey != null) {
            File::delete($path . $taxi->pictureGrey);
        }
        TaxiType::destroy($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
}
