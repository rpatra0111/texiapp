<?php

namespace App\Http\Controllers\Admin;

use App\EmailTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resource = 'email';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $$resource_pl = EmailTemplate::orderBy('id', 'asc')->paginate($per_page);
        return view('admin.emails.list', compact($resource_pl, 'resource', 'resource_pl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nat = 'Add';
        $meth = 'POST';
        $resource = 'email';
        $saveLink = admin_url('email');
        $resource_pl = str_plural($resource);
        $$resource = '';
        return view('admin.emails.edit', compact($resource, 'resource', 'resource_pl', 'saveLink', 'nat', 'meth'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $rules = [
            'emailTitle' => 'required',
            'description' => 'required'
        ];
        $this->validate($request, $rules, []);
        $email = EmailTemplate::create($input);
        return redirect()->back()->with('success', 'Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmailTemplate $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(EmailTemplate $emailTemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $email = EmailTemplate::find($id);
        $nat = 'Edit';
        $meth = 'PATCH';
        $resource = 'email';
        $saveLink = admin_url('email/' . $id);
        $resource_pl = str_plural($resource);
        $$resource = $email;
        return view('admin.emails.edit', compact($resource, 'resource', 'resource_pl', 'saveLink', 'nat', 'meth'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $email = EmailTemplate::find($id);
        $rules = [
            'emailTitle' => 'required',
            'description' => 'required'
        ];

        $this->validate($request, $rules, []);

        $email->emailTitle = isset($input['emailTitle']) ? $input['emailTitle'] : "";
        $email->description = isset($input['description']) ? $input['description'] : "";
        $email->save();
        return redirect()->back()->with('success', 'Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EmailTemplate::destroy($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
}
