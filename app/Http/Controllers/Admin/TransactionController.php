<?php

namespace App\Http\Controllers\Admin;

use App\Transaction;
use App\UserTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\TaxiRide;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $type)
    {
        $startDate =  $request->input('start');
        $endDate =  $request->input('end');
        $userId =  $request->input('id');
        $resource = 'transaction';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        if (isset($userId)) {
            if (isset($startDate) && isset($endDate)) {
                $$resource_pl = UserTransaction::where('userId', '=', $userId)->where('created_at', '>=', date('Y-m-d H:i:s', strtotime($startDate)))->where('created_at', '<=', date('Y-m-d H:i:s', strtotime($endDate." 23:59:59")))->orderBy('id', 'desc')->paginate($per_page);
                $totalAmount = UserTransaction::select(DB::raw('SUM(COALESCE(CASE WHEN transactionType = "C" THEN amount END,0)) AS totalAmount'))
                    ->where('userId', '=', $userId)
                    ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime($startDate)))
                    ->where('created_at', '<=', date('Y-m-d H:i:s', strtotime($endDate." 23:59:59")))
                    ->pluck('totalAmount')[0];
            }
            else {
                $$resource_pl = UserTransaction::where('userId', '=', $userId)->orderBy('id', 'desc')->paginate($per_page);
                $totalAmount = UserTransaction::select(DB::raw('SUM(COALESCE(CASE WHEN transactionType = "C" THEN amount END,0)) AS totalAmount'))
                    ->where('userId', '=', $userId)
                    ->pluck('totalAmount')[0];
            }
            return view('admin.transaction.list', compact($resource_pl, 'resource', 'resource_pl', 'startDate', 'endDate', 'userId', 'totalAmount'));
        }
        else {
            switch (strtolower($type)) {
                case 'administrator':
                    if (isset($startDate) && isset($endDate)) {
                        $$resource_pl = UserTransaction::where('userId', '=', Auth::id())->where('created_at', '>=', date('Y-m-d H:i:s', strtotime($startDate)))->where('created_at', '<=', date('Y-m-d H:i:s', strtotime($endDate . " 23:59:59")))->orderBy('id', 'desc')->paginate($per_page);
                        $totalAmount = UserTransaction::select(DB::raw('SUM(COALESCE(CASE WHEN transactionType = "C" THEN amount END,0)) - SUM(COALESCE(CASE WHEN transactionType = "D" THEN amount END,0)) AS totalAmount'))
                            ->where('userId', '=', Auth::id())->where('created_at', '>=', date('Y-m-d H:i:s', strtotime($startDate)))
                            ->where('created_at', '<=', date('Y-m-d H:i:s', strtotime($endDate . " 23:59:59")))
                            ->pluck('totalAmount')[0];
                    } else {
                        $$resource_pl = UserTransaction::where('userId', '=', Auth::id())->orderBy('id', 'desc')->paginate($per_page);
                        $totalAmount = UserTransaction::select(DB::raw('SUM(COALESCE(CASE WHEN transactionType = "C" THEN amount END,0)) - SUM(COALESCE(CASE WHEN transactionType = "D" THEN amount END,0)) AS totalAmount'))
                            ->where('userId', '=', Auth::id())
                            ->pluck('totalAmount')[0];
                    }
                    break;
                case 'company':
                    if (isset($startDate) && isset($endDate)) {
                        $$resource_pl = Transaction::where('created_at', '>=', date('Y-m-d H:i:s', strtotime($startDate)))->where('created_at', '<=', date('Y-m-d H:i:s', strtotime($endDate . " 23:59:59")))->orderBy('id', 'desc')->paginate($per_page);
                        $totalAmount = Transaction::select(DB::raw('SUM(COALESCE(CASE WHEN transactionType = "C" THEN amount END,0)) - SUM(COALESCE(CASE WHEN transactionType = "D" THEN amount END,0)) AS totalAmount'))
                            ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime($startDate)))
                            ->where('created_at', '<=', date('Y-m-d H:i:s', strtotime($endDate . " 23:59:59")))
                            ->pluck('totalAmount')[0];
                    } else {
                        $$resource_pl = Transaction::orderBy('id', 'desc')->paginate($per_page);
                        $totalAmount = Transaction::select(DB::raw('SUM(COALESCE(CASE WHEN transactionType = "C" THEN amount END,0)) - SUM(COALESCE(CASE WHEN transactionType = "D" THEN amount END,0)) AS totalAmount'))
                            ->pluck('totalAmount')[0];
                    }
                    break;
                default:
                    redirect(admin_url());
                    break;
            }
            return view('admin.transaction.list', compact($resource_pl, 'resource', 'resource_pl', 'startDate', 'endDate', 'totalAmount'));
        }
    }

    public function rideTransactions(Request $request, $type)
    {
        $startDate =  $request->input('start');
        $endDate =  $request->input('end');
        $userId =  $request->input('id');
        $resource = 'transaction';
        $resource_pl = str_plural($resource);
        $per_page = config('constant.ADMIN_PER_PAGE');
        $byCard = 0;
        $byCash = 0;
        //dd('work on progress');
        $userDetails = User::find($userId);
        if ($userDetails) {
            $lastAdjustment =  UserTransaction::whereNull('taxiRideId')->where('userId',$userId)->orderBy("id", "DESC")->first();

            //dd($lastAdjustment);
            //$taxiRides = TaxiRide::with(['Passenger'])->where('userTaxiId',$userDetails->id)->orderBy('id', 'DESC')->paginate($per_page);
            $totalAmount = UserTransaction::select(DB::raw('SUM(COALESCE(CASE WHEN transactionType = "C" THEN amount END,0)) AS totalAmount'))
                    ->where('userId', '=', $userId);

            $byCard = UserTransaction::whereHas('TaxiRide', function($q){
                $q->where('paidBy', 'R');
            })->where(['userId'=> $userId,'transactionType' => 'C','paymentStatus'=> "Y"])
            ->where('description','like','%'.'Received for the ride'.'%');

            $byCash = UserTransaction::whereHas('TaxiRide', function($q){
                $q->where('paidBy', 'S');
            })->where(['userId'=> $userId,'transactionType' => 'C','paymentStatus'=> "Y"])
            ->where('description','like','%'.'Received for the ride'.'%');

            $transactionHistories = UserTransaction::with('TaxiRide')
            ->where(['userId'=> $userId])
            ->orderBy('id', 'desc');

            if (!empty($lastAdjustment)) {
                $totalAmount = $totalAmount->where('id', '>' , $lastAdjustment->id);
                $byCard = $byCard->where('id', '>' , $lastAdjustment->id);
                $byCash = $byCash->where('id', '>' , $lastAdjustment->id);
                $transactionHistories = $transactionHistories->where('id', '>' , $lastAdjustment->id);
            }
            $totalAmount =  $totalAmount->pluck('totalAmount')[0];
            $byCard = $byCard->sum('amount');
            $byCash = $byCash->sum('amount');
            $transactionHistories = $transactionHistories->paginate($per_page);
            return view('admin.transaction.detailsList',[
                'resource_pl' => $resource_pl,
                'totalAmount' => $totalAmount,
                'transactionHistories' => $transactionHistories,
                'byCard' => $byCard,
                'byCash' => $byCash,
                'lastAdjustment' => $lastAdjustment
            ]);
        } else {
            return redirect()->back();
        }
    }
}
