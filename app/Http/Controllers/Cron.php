<?php

namespace App\Http\Controllers;

use App\UserTaxi;
use Illuminate\Http\Request;

class Cron extends Controller
{
    public function resetDriverStatus()
    {
        UserTaxi::where('updated_at', '<=', date('Y-m-d h:i:s', (time() - 3600)))
            ->where(function ($query) {
                $query->where('available', '=', 'Y')
                    ->orWhere('forHire', '=', 'Y')
                    ->orWhere('requested', '=', 'Y');
            })->update([
                'available' => 'N',
                'forHire' => 'N',
                'requested' => 'N'
            ]);
    }
}
