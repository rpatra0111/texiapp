<?php

namespace App\Http\Controllers\Site;

use App\DriverReview;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use DB;

class HomeController extends Controller
{
    public function index ()
    {
//        $reviews = DriverReview::all();
//        return view('site.welcome', compact('reviews'));
        $pageTitle = 'Home';
        return view('site.landing', compact('pageTitle'));
    }

    public function terms ()
    {
        return view('site.terms');
    }

    public function cc ()
    {
        // $drivers = User::with('UserTokens')->where('email','pabitra@technoexponent.com')->get();
        // DB::enableQueryLog();
        $drivers = User::with('UserTokens')->where(['userType'=>'D','is_online'=>'N'])->where('updated_at', '<=', date('Y-m-d H:i:s', (time() - 1800)))->get();
        // dd(DB::getQueryLog());
        $fcmTokensI = [];
        $fcmTokensA = [];
        foreach ($drivers as $k => $val) {
            if (isset($val->UserTokens) && $val->UserTokens!=null)
            {
              foreach ($val->UserTokens as $key => $values) {
                    if ($values->flag == 'A')
                    {   
                        $fcmTokensA[] = $values->fcmToken;
                        echo sendNotification([$values->fcmToken], ['dataContent' => ['notiType' => 'IDLE', 'title' => config('constant.APP_NAME'), 'body' => 'Hello '.$val->firstName.' your app is close please open the app']]);
                    }
                    if ($values->flag == 'I'){
                        $fcmTokensI[] = $values->fcmToken;

                       echo sendNotification([$values->fcmToken], ['dataContent' => ['notiType' => 'IDLE']], ['title' => config('constant.APP_NAME'), 'body' => 'Hello '.$val->firstName.' your app is close please open the app']);
                    }
                }
            }
        }
        echo "<br>";
        print_r($fcmTokensA);
        echo "<br>";
        print_r($fcmTokensI);
        echo "<br>";
        exit;
    }
}
