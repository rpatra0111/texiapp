<?php

namespace App\Http\Controllers\Site;

use App\User;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function unsubscribe ($token)
    {
        try {
            $decrypted = decrypt($token);
            User::where('id', $decrypted)->update(['subscribed' => 'N']);
            return view('site.response.success');
        } catch (DecryptException $e) {
            return abort(400);
        }
    }
}
