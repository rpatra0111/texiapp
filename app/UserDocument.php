<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDocument extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId',
        'docType',
        'userDocument',
        'verifyStatus'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function user()
    {
        return $this->belongsTo('App\User', 'userId', 'id');
    }

    public function docs()
    {
        return $this->belongsTo('App\VerificationDocument', 'docType', 'id');
    }
}
