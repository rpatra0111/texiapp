<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryState extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['countryId', 'code', 'name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function Cities()
    {
        return $this->hasMany('App\CountryCity', 'stateId', 'id');
    }

    public function Country()
    {
        return $this->belongsTo('App\Country', 'countryId', 'id');
    }
}
