<?php

namespace App;

use App\Notifications\MailResetPasswordToken;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'language_id', 'firstName', 'lastName', 'gender',
        'addressLine1', 'addressLine2', 'city', 'state',
        'zipcode', 'email', 'countryCode', 'mobile',
        'profilePicture', 'password', 'balance', 'ssn',
        'stripeCustomerId', 'remember_token', 'reset_token',
        'userType', 'driverApproval', 'loginType',
        'completion', 'status', 'subscribed','reference_code','reword_point'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function UserVerify()
    {
        return $this->hasOne('App\UserVerification', 'userId', 'id');
    }

    public function UserTokens()
    {
        return $this->hasMany('App\UserToken', 'userId', 'id');
    }

    public function UserBankAccounts()
    {
        return $this->hasMany('App\UserBankAccount', 'user_id', 'id');
    }

    public function UserTaxis()
    {
        return $this->hasMany('App\UserTaxi', 'userId', 'id');
    }

    public function UserDocs()
    {
        return $this->hasMany('App\UserDocument', 'userId', 'id');
    }

    public function UserTransactions()
    {
        return $this->hasMany('App\UserTransaction', 'userId', 'id');
    }

    public function UserCashouts()
    {
        return $this->hasMany('App\UserCashout', 'userId', 'id');
    }

    public function UserRatings()
    {
        return $this->hasMany('App\UserRating', 'raterId', 'id');
    }

    public function Transactions()
    {
        return $this->hasMany('App\Transaction', 'userId', 'id');
    }

    public function Notifications()
    {
        return $this->hasMany('App\Notification', 'userId', 'id');
    }

    public function TaxiRides()
    {
        return $this->hasMany('App\TaxiRide', 'passengerId', 'id');
    }

    public function UserMenuPermits()
    {
        return $this->hasMany('App\UserMenuPermit', 'userId', 'id');
    }

    public function UserCars()
    {
        return $this->hasMany('App\UserCar', 'userId', 'id');
    }

    public function DriverRides()
    {
        return $this->hasManyThrough('App\TaxiRide', 'App\UserTaxi', 'userId', 'userTaxiId', 'id', 'id');
    }

    /**
     * Send a password reset email to the user
     */
    public function sendPasswordResetNotification($token)
    {
        $this->reset_token = $token;
        $this->save();
        $this->notify(new MailResetPasswordToken($token));
    }

    public function isAdmin()
    {
        $f = false;
        if ($this->userType == 'A' || $this->userType == 'SU') {
            $f = true;
        }
        return $f; // this looks for an admin column in your users table
    }

    public function scopeIsWithinMaxDistance($query, $latitude, $longitude, $radius = 5) {

        $haversine = "(6371 * acos(cos(radians(" . $latitude . ")) 
                        * cos(radians(`curr_lat`)) 
                        * cos(radians(`curr_lng`) 
                        - radians(" . $longitude . ")) 
                        + sin(radians(" . $latitude . ")) 
                        * sin(radians(`curr_lat`))))";
    
        return $query->select('*')
                     ->selectRaw("{$haversine} AS distance")
                     ->whereRaw("{$haversine} < ?", [$radius*111.045]);
    }

    public function referenceUsers()
    {
        return $this->hasMany('App\RefererUser', 'user_id', 'id');
    }

}
