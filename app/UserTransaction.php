<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTransaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId', 'taxiRideId', 'gatewayTransactionId',
        'amount', 'transactionType', 'description',
        'paymentStatus', 'gatewayResponse'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function User()
    {
        return $this->belongsTo('App\User', 'userId', 'id');
    }

    public function TaxiRide()
    {
        return $this->belongsTo('App\TaxiRide', 'taxiRideId', 'id');
    }
}
