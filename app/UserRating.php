<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRating extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'taxiRideId', 'rateeId', 'raterId', 'driverRating',
        'carRating', 'drivingRating', 'rating', 'message'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function TaxiRide()
    {
        return $this->belongsTo('App\TaxiRide', 'taxiRideId', 'id');
    }

    public function Rater()
    {
        return $this->belongsTo('App\User', 'raterId', 'id');
    }

    public function Ratee()
    {
        return $this->belongsTo('App\User', 'rateeId', 'id');
    }
}
