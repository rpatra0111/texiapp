<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBackup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'editedBy', 'userId', 'language_id',
        'firstName', 'lastName', 'gender',
        'addressLine1', 'addressLine2', 'city',
        'state', 'zipcode', 'email', 'countryCode',
        'mobile', 'profilePicture', 'balance',
        'driverApproval', 'status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
