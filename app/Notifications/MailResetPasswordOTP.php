<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MailResetPasswordOTP extends Notification
{
    use Queueable;
	
	protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($toUser)
    {
        $this->user = $toUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    //->from('no-reply@no-reply.com')
					//->to($this->user->email)
                    ->line('You are receiving this email because we received a password reset request for your account.')
                    ->line('Please use the OTP ' . $this->user->otp_to_reset_password . ' to reset your account password.')
                    //->action('Reset Password', url(config('app.url') . route('password.reset', $this->token, false)))
                    ->line('If you did not request a password reset, no further action is required.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
