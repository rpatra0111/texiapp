<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxiRide extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'passengerId', 'userTaxiId', 'taxiTypeId', 'couponId',
        'fromAddress', 'fromLat', 'fromLng', 'toAddress',
        'toLat', 'toLng', 'estimatedDistance', 'totalDistance',
        'estimatedTime', 'timeTaken', 'estimatedFare', 'fare',
        'discounted', 'tips', 'paidBy', 'polylineImage',
        'paidStatus', 'cancel', 'cancelReason', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function Passenger()
    {
        return $this->belongsTo('App\User', 'passengerId', 'id');
    }

    public function UserTaxi()
    {
        return $this->belongsTo('App\UserTaxi', 'userTaxiId', 'id');
    }

    public function Coupon()
    {
        return $this->belongsTo('App\Coupon', 'couponId', 'id');
    }
}
