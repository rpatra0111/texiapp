<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtpToResetPasswordToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
			$table->string('otp_to_reset_password', 6)->after('email')->nullable()->charset('utf8');
			$table->text('token_to_reset_password')->after('otp_to_reset_password')->nullable()->charset('utf8');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
			$table->dropColumn('token_to_reset_password');
		});
    }
}
