var https = require('https');
var fs = require('fs');
var randomstring = require("randomstring");

exports.driverRequest = function (io, socket, data, seqdb, sequelyze) {

};


exports.getStaticMap = function (fromLatLng, toLatLng, callback) {
    let startMarker = "https://goo.gl/142nZz";
    let endMarker = "https://goo.gl/BRN8Ac";
    var buffer = '';
    var request = https.get('https://maps.googleapis.com/maps/api/directions/json?key=' + process.env.GOOGLE_MAPS_KEY + '&origin=' + fromLatLng + '&destination=' + toLatLng + '&mode=driving', function (result) {
        result.setEncoding('utf8');
        result.on('data', function (chunk) {
            buffer += chunk;
        });

        result.on('end', function () {
            let b = JSON.parse(buffer);
            let imgUrl = "https://maps.googleapis.com/maps/api/staticmap?mode=driving&size=600x400&markers=anchor:top%7Cicon:" + startMarker + "%7C" + fromLatLng + "&markers=anchor:top%7Cicon:" + endMarker + "%7C" + toLatLng + "&path=weight:4|color:0x000000ff|enc:" + b.routes[0].overview_polyline.points + "&key=" + process.env.GOOGLE_MAPS_KEY;
            let filename = randomstring.generate(18) + ".jpg";
            https.get(imgUrl, function (response) {
                response.pipe(fs.createWriteStream("/home/slandaise/public_html/assets/uploads/rideHistory/" + filename));
            });
            callback(filename);
        });
    });

    request.on('error', function (e) {
        console.log('error from google.getMapData: ' + e.message)
    });

    request.end();
};