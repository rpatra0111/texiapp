let mysql = require('mysql');

let con = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    charset: 'utf8mb4',
    timezone: 'utc'
});

con.getConnection(function (err, connection) {
    // connected! (unless `err` is set)
    if (err) throw err;
});

exports.connection = con;

exports.fetching = function (table, conditions, parameters, orderBy, limit, offset, callback) {
    let res = "";
    let cond = " ";
    if (typeof conditions !== 'undefined' && conditions !== '') {
        cond += "WHERE " + conditions;
    }
    let param = "*";
    if (typeof parameters !== 'undefined' && parameters !== '') {
        param = parameters;
    }
    let order = " ";
    if (typeof orderBy !== 'undefined' && orderBy !== '') {
        order += "ORDER BY " + orderBy;
    }
    let lim = " ";
    if (typeof limit !== 'undefined' && limit !== '') {
        lim += "LIMIT " + limit;
    }
    let offs = " ";
    if (typeof offset !== 'undefined' && offset !== '') {
        offs += "OFFSET " + offset;
    }
    let sql = "SELECT " + param + " FROM " + table + cond + order + lim + offs;
    con.query(sql, callback);
};


exports.inserting = function (table, values) {
    let sql = "INSERT INTO " + table + " SET ?";
    con.query(sql, values, function (err, result) {
        if (err) throw err;
    });
};


exports.updating = function (table, conditions, values) {
    let cond = " ";
    if (typeof conditions !== 'undefined') {
        cond += "WHERE " + conditions;
    }
    let sql = "UPDATE " + table + " SET ?" + cond;
    con.query(sql, values, function (err, result) {
        if (err) throw err;
    });
};


exports.deleting = function (table, conditions) {
    let cond = " ";
    if (typeof conditions !== 'undefined') {
        cond += "WHERE " + conditions;
    }
    let sql = "DELETE FROM " + table + cond;
    con.query(sql, function (err, result) {
        if (err) throw err;
    });
};


exports.querying = function (query, callback) {
    if (typeof query !== 'undefined' && query !== '') {
        con.query(query, callback);
    }
};