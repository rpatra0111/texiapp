//CONSTANTS
process.env.TZ = 'utc';
process.env.NODE_ENV = 'production';

const USER_TABLE = 'users';
const USER_CAR_TABLE = 'user_cars';
const USER_CAR_COLOUR_TABLE = 'user_car_colours';
const USER_TAXI_TABLE = 'user_taxis';
const TAXI_RIDE_TABLE = 'taxi_rides';
const REJECTED_RIDE_TABLE = 'rejected_rides';
const USER_TOKEN_TABLE = 'user_tokens';
const NOTIFICATION_TABLE = 'notifications';

//const defaultSearchTimeout = 15000;
const defaultSearchTimeout = 90000;

// Setup basic express server
require('dotenv').config({path: __dirname + '/../.env'});
const express = require('express');
const http = require('http');
const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server);
const port = 55916;
const Redis = require('ioredis');
var admin = require("firebase-admin");

let mysqlDB = require('./database');
let booking = require("./controllers/booking");


server.listen(port, function () {
    console.log('Server listening at port %d', port);
});

let numUsers = 0;
let onLine = {};
let userData = {};

admin.initializeApp({
	credential: admin.credential.cert({
        "type": "service_account",
        "project_id": "taxiapp-40650",
        "private_key_id": "92578770cef19f120630823c0fb17bc296a7dda7",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDM4pBLZ8CGJYCA\nUIb6ar9wo9AI8eL7VyJsSSHmHT1QahLvnVT61+FRNHU24lYgjWe8Go5XNY6n7ENR\nl//H5T1ypF7ziNdxRizU2PTsMB3s6pcC0f39tp5yibYOqSzbA15Ib2+o7qs1ADFo\nDbSwxRCiPcWZrPUGAxg/WVfaD2wJVKpNGfNKQvj/3BSaSPssEt/0xELb9PbQRsS/\n4jzYDTDNxzPYkxfNpM4M9FmUC1Rtm6KRNs+ih/6a1RUTNizmCT25nUSyfB2p8O9c\nCytCfF7YWnJ/cAlLNXUQ9ikrMa4alpjBQCSs1X+yp7MQEfSWh2+ekSuiysNXdInq\nWJhyFCMjAgMBAAECggEACx684CrYay974vr76HmEvHNXpjSisiuPhbsOaPPrUQMA\n1hp+80JmS0cCmaD9NbUXDUGPujIA9SJAoRwT2OCbTi6XrLYfo1lAs6i22l7eMO6m\nlZOaKnwP632H8RHDinuLUXki4VNa3XyOMeyUgwefGH8ry2R0xAfE+CM8PmyZ24Ow\n+oCkvmCK4sL6r+uV7pu+BgzsGMahwIQll0zPgH6ZrxfQGYP/tgAQ0rCKc3U14nf5\nVgyGrFXnXTX7eSFO7VRkasVO8Mp6n2+0OgD2byg3qKuAd6xaHN7OIHiT0mKZlYZ7\nhhH2lkTvw6LFZl+VK/UN0oO77AGmVFyQLR9s8VpiWQKBgQDwofxdVZ2M511YuR8b\nMx9WqKv1C2OY2pxJHQffUZcVi/U1yQwbPSyzvtTsiY+OjgOTpjGNXB85z2CEL150\nQujr1zOnSDOmlrdAtflMfNdmgXazOLHnYBGq2R++BJl6FdFqgNBGM+nWXug9BINJ\nKGObqISZKrAb6zeyxkQlvUDX9QKBgQDZ+CbHwge3I6kFOZiTmPc7ukk7xZNqizp9\nHYjDxhN+YgcFYMZyz/aj2+EEZUNSvkgc4PRdokY+j59pTEm4A/nVb2xMcN/KNRmb\nhohyTbG43or/UVXcvRWcpAfp0m4sqylusp6aTJ7wLVlVAAsRX7rEOwduSVqcoFoq\nbwe0XA/XtwKBgQDuCmA6+FnNrIaEIUUrRAxJTrwkwWp6KDAYh5ZlDTJXR0eAOG04\nePDCsxGxGHehvsy8/9ffDfV527KPsMhQXGxL+WwInH0z275WTm3ag1kV9xY0Pu6G\ng1uW1gj3kVc+FV0/bUQbXnmA7gvxaIhzE3hN4IujdT4XsD0+JbH+X1RJDQKBgQCQ\nMpaVOEuaYMEEb+H8YwnMPaUNIdwwdAMxmkRMH9HtnjQ+3pLqiaIFLQVjvwoWx0M1\nt6xx9VS7NpCjPJDYZ+jzxk7DEzk5sH9MYGgMFlIolzElT02aRozxn6KMkWXJr0It\ntYUvVtke0yFyFiE8UTRhdRupXmXPkFWWFPXn36+raQKBgHDucNkvOdTL4AEkS/CU\nwGzzZqDUdwhuqJpAxtdu4m022uxAlpwQeCfjkgqvztqmyJMOA+w8+1xD1HAGQzyS\nViEPv77XPnWDja0HcwDd4kdvKu1MKUFFF1ZmllF2r/E045OzdNuSkk87zxFUMIHw\nnjIsXM4uDEXpJSFEKC7RdSn6\n-----END PRIVATE KEY-----\n",
        "client_email": "firebase-adminsdk-gp7kc@taxiapp-40650.iam.gserviceaccount.com",
        "client_id": "117227414473772889801",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://accounts.google.com/o/oauth2/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-gp7kc%40taxiapp-40650.iam.gserviceaccount.com"
      }),
	databaseURL: "https://taxiapp-161806.firebaseio.com"
});

io.on('connection', function (socket) {
    let redisClient = new Redis({
        port: 6379,          // Redis port
        host: '127.0.0.1',   // Redis host
        family: 4,           // 4 (IPv4) or 6 (IPv6)
        password: process.env.REDIS_PASSWORD,
        db: 0
    });
    console.log('Connection');
    let addedUser = false, addingUser = false, lat, lng, locality, reqDrivers = [],
        userTaxiDetails, myRoom = null, searchDriver = false, reqFuncTymOut,
        funcTimeout, driverReqData = {}, taxiReqId = 0, taxiType = [],
        disconnected = false, first = true, onTaxiRide, pendingTaxiRide=[], pushArrivedDriver = true;

    // add user
    socket.on('add user', function (data) {
        if (addedUser) return;
        if (!addingUser) {
            addingUser = true;
            // we store the username in the socket session for this client
            redisClient.subscribe(process.env.CHANNEL_NAME, function (err, count) {
                if (err)
                    console.log('Error: ', err);
                console.log('Successfully connected: ', count)
            });
            data = JSON.parse(data);
            console.log('add user', data);
            if (typeof data.userId !== 'undefined' && typeof data.userType !== 'undefined' && data.userId !== '' && data.userType !== '') {
                let usrid = parseInt(data.userId);
                let usrtype = data.userType.toString();
                socket.username = usrid;
                socket.usertype = usrtype;
                userData[usrid] = {};
                onLine[usrid] = socket.id;
                userData[usrid].onRide = false;
                ++numUsers;
                console.log("UserIds",userData);
                mysqlDB.fetching(USER_TABLE, 'id=' + parseInt(socket.username), 'id,firstName,lastName,gender,countryCode,mobile,profilePicture', '', '', '', function (userErr, user) {
                    if (userErr) {
                        console.log(userErr);
                    }
                    else {
                        if (user.length > 0) {
                            userData[usrid].id = user[0].id;
                            userData[usrid].firstName = user[0].firstName;
                            userData[usrid].lastName = user[0].lastName;
                            userData[usrid].gender = user[0].gender;
                            userData[usrid].countryCode = user[0].countryCode;
                            userData[usrid].mobile = user[0].mobile;
                            userData[usrid].profilePicture = user[0].profilePicture;
                            addedUser = true;
                            addingUser = false;
                        }
                    }

                });
                if (usrtype === 'D') {
                    mysqlDB.fetching(USER_TAXI_TABLE, "userId=" + usrid, "", "", "", "", function (uTaxiErr, uTaxiData) {
                        if (uTaxiErr) {
                            console.log(uTaxiErr);
                        }
                        if (uTaxiData.length > 0) {
                            userData[usrid].taxiType = [];
                            uTaxiData.forEach(function (elem) {
                                userData[usrid].taxiType.push(parseInt(elem.taxiTypeId));
                                taxiType.push(parseInt(elem.taxiTypeId));
                            });
                            let query = "SELECT uc.id, uc.userId, uc.numberPlate, uc.make, uc.model, uc.colour, uc.year, ucc.colour, ucc.hexCode FROM " + USER_CAR_TABLE + " AS uc LEFT JOIN " + USER_CAR_COLOUR_TABLE + " AS ucc ON uc.colour = ucc.id WHERE uc.userId=" + usrid;
                            mysqlDB.querying(query, function (uCarErr, uCarData) {
                                if (uCarErr) {
                                    console.log(uCarErr);
                                }
                                userTaxiDetails = uCarData;
                                let query = "SELECT tr.*, ut.userId FROM " + TAXI_RIDE_TABLE + " AS tr LEFT JOIN " + USER_TAXI_TABLE + " AS ut ON tr.userTaxiId = ut.id WHERE ut.userId=" + usrid + " AND tr.cancel='N' AND tr.status IN ('S', 'A', 'R')";
                                mysqlDB.querying(query, function (tRideErr, onTRide) {
                                    if (tRideErr) {
                                        console.log(tRideErr);
                                    }
                                    else {
                                        if (onTRide.length > 0) {
                                            userData[usrid].onRide = true;
                                            onTaxiRide = onTRide[0];
                                            myRoom = parseInt(onTaxiRide.id);
                                            socket.join(myRoom);
                                            socket.room = myRoom;
                                            mysqlDB.updating(USER_TAXI_TABLE, "userId=" + usrid, {
                                                available: "Y",
                                                requested: "N",
                                                forHire: "N",
                                                updated_at: new Date()
                                            });
                                        }
                                        else {
                                            mysqlDB.updating(USER_TAXI_TABLE, "userId=" + usrid, {
                                                available: "Y",
                                                requested: "N",
                                                forHire: "Y",
                                                updated_at: new Date()
                                            });
                                        }
                                    }
                                });
                            });
                        }
                        else {
                            socket.emit('appError', {
                                status: "IU"
                            });
                        }
                    });
                }
                if (usrtype === 'P') {
                    mysqlDB.fetching(TAXI_RIDE_TABLE, "passengerId=" + usrid + " AND status IN ('S', 'A', 'R')", "", "", "", "", function (tRideErr, onTRide) {
                        if (tRideErr) {
                            console.log(tRideErr);
                        }
                        else {
                            if (onTRide.length > 0) {
                                userData[usrid].onRide = true;
                                onTaxiRide = onTRide[0];
                                myRoom = parseInt(onTaxiRide.id);
                                socket.join(myRoom);
                                socket.room = myRoom;
                            }
                        }
                    });
                }
            }
            else {
                socket.emit('appError', {
                    status: "MP"
                });
            }
        }
    });

    //init user
    socket.on('init', function (data) {
        data = JSON.parse(data);
        if (typeof data.locality !== 'undefined' && data.locality !== '' && typeof data.lat !== 'undefined' && data.lat !== '' && typeof data.lng !== 'undefined' && data.lng !== '') {
            lat = parseFloat(data.lat);
            lng = parseFloat(data.lng);
            locality = data.locality.toString();
            if (addedUser && !disconnected) {
                if (userData[socket.username].onRide) {
                    mysqlDB.fetching(TAXI_RIDE_TABLE, "passengerId=" + socket.username + " AND status IN ('S', 'A', 'R')", "", "", "", "", function (tRideErr, onTRide) {
                        if (tRideErr) {
                            console.log(tRideErr);
                        }
                        else {
                            if (onTRide.length > 0) {
                                onTaxiRide = onTRide[0];
                                let query = "SELECT ut.userId, ut.taxiTypeId, ut.currLat, ut.currLng, ut.currLocality, ut.available, ut.forHire, u.driverApproval, (3959 * acos(cos(radians('" + lat + "')) * cos(radians(currLat)) * cos( radians(currLng) - radians('" + lng + "')) + sin(radians('" + lat + "')) * sin(radians(currLat)))) AS distance FROM " + USER_TAXI_TABLE + " AS ut JOIN "+USER_TABLE+" AS u ON ut.userId = u.id WHERE ut.id="+onTaxiRide.userTaxiId+" GROUP BY ut.userId";
                                mysqlDB.querying(query, function (err, driverData) {
                                    if (err) {
                                        console.log(err);
                                    }
                                    else {
                                        socket.emit('drivers available', {
                                            drivers: driverData
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
                else {
                    let query = "SELECT ut.userId, ut.taxiTypeId, ut.currLat, ut.currLng, ut.currLocality, ut.available, ut.forHire, u.driverApproval, (3959 * acos(cos(radians('" + lat + "')) * cos(radians(currLat)) * cos( radians(currLng) - radians('" + lng + "')) + sin(radians('" + lat + "')) * sin(radians(currLat)))) AS distance FROM " + USER_TAXI_TABLE + " AS ut JOIN "+USER_TABLE+" AS u ON ut.userId = u.id WHERE ut.available='Y' AND ut.forHire='Y' AND u.driverApproval = 'Y' GROUP BY ut.userId HAVING distance<=10";
                    mysqlDB.querying(query, function (err, driverData) {
                        if (err) {
                            console.log(err);
                        }
                        else {
                            socket.emit('drivers available', {
                                drivers: driverData
                            });
                        }
                    });
                }
            }
            else {
                socket.emit('appError', {
                    status: "RAU"
                });
            }
        }
        else {
            socket.emit('appError', {
                status: "MP"
            });
        }
    });

    redisClient.on('message', function (channel, message) {
        message = JSON.parse(message);
        if (message.event) {
            if (addedUser && !disconnected) {
                let data = message.data.eventData;
                if (data.userId) {
                    if (parseInt(socket.username) === parseInt(data.userId)) {
                        if (onLine[socket.username]) {
                            console.log("message.event if",message.event);
                            switch (message.event) {
                                case 'CancelRide':
                                    if (onLine[socket.username]) {
                                        if (userData[socket.username].onRide) {
                                            switch (socket.usertype.toString()) {
                                                case 'D':
                                                    socket.to(myRoom).emit('cancel ride', {
                                                        taxiRequestId: data.taxiRequestId,
                                                        message: "Ride Cancelled by Driver"
                                                    });
                                                    socket.to(myRoom).emit('driver left', {
                                                        userId: socket.username,
                                                        currLat: lat,
                                                        currLng: lng,
                                                        currLocality: myRoom
                                                    });
                                                    userData[parseInt(socket.username)].onRide = false;
                                                    if (onLine[parseInt(data.passengerId)]) {
                                                        userData[parseInt(data.passengerId)].onRide = false;
                                                    }
                                                    break;
                                                case 'P':
                                                    socket.to(myRoom).emit('cancel ride', {
                                                        message: "Ride Cancelled by Passenger",
                                                        taxiRequestId: data.taxiRequestId,
                                                        paidBy: data.paidBy,
                                                        fare: data.discounted
                                                    });
                                                    userData[parseInt(socket.username)].onRide = false;
                                                    if (onLine[parseInt(data.driverId)]) {
                                                        userData[parseInt(data.driverId)].onRide = false;
                                                    }
                                                    break;
                                            }
                                        }
                                        else {
                                            socket.emit('appError', {
                                                status: "NOR"
                                            });
                                        }
                                    }
                                    break;
                                case 'StartRide':
                                    if (onLine[socket.username]) {
                                        if (userData[socket.username].onRide) {
                                            if (socket.usertype.toString() === 'D') {
                                                socket.to(myRoom).emit('start ride', {
                                                    message: "Ride Started"
                                                });
                                            }
                                        }
                                        else {
                                            socket.emit('appError', {
                                                status: "NOR"
                                            });
                                        }
                                    }
                                    break;
                                case 'FinishRide':
                                    if (onLine[socket.username]) {
                                        if (userData[socket.username].onRide) {
                                            if (socket.usertype.toString() === 'D') {
                                                socket.to(myRoom).emit('finish ride', {
                                                    message: "Ride Finished",
                                                    driver: {
                                                        id: data.driver.id,
                                                        firstName: data.driver.firstName,
                                                        lastName: data.driver.lastName,
                                                        gender: data.driver.gender,
                                                        countryCode: data.driver.countryCode,
                                                        mobile: data.driver.mobile,
                                                        profilePicture: data.driver.profilePicture
                                                    },
                                                    fare: data.fare,
                                                    paidBY: data.paidBy,
                                                    taxiRequestId: data.taxiRequestId,
                                                    finishedOn: data.finishedOn
                                                });
                                                if (data.paidBy.toString() === 'R') {
                                                    socket.to(myRoom).emit('end ride', {
                                                        userId: socket.username,
                                                        currLat: lat,
                                                        currLng: lng,
                                                        currLocality: myRoom
                                                    });
                                                    userData[socket.username].onRide = false;
                                                    if (onLine[data.passengerId])
                                                        userData[data.passengerId].onRide = false;
                                                }
                                            }
                                        }
                                        else {
                                            socket.emit('appError', {
                                                status: "NOR"
                                            });
                                        }
                                    }
                                    break;
                                case 'CashPaid':
                                    if (onLine[socket.username]) {
                                        if (userData[socket.username].onRide) {
                                            if (socket.usertype.toString() === 'D') {
                                                socket.to(myRoom).emit('end ride', {
                                                    userId: socket.username,
                                                    currLat: lat,
                                                    currLng: lng,
                                                    currLocality: myRoom
                                                });
                                                userData[socket.username].onRide = false;
                                                if (onLine[data.passengerId])
                                                    userData[data.passengerId].onRide = false;
                                            }
                                        }
                                    }
                                    break;
                                case 'DriverResponse':
                                    if (onLine[socket.username]) {
                                        if (!userData[socket.username].onRide) {
                                            if (socket.usertype.toString() === 'D') {
                                                if (data.dResponse.toString() === 'Y') {
                                                    clearTimeout(reqFuncTymOut);
                                                    socket.to(myRoom).emit('driver left', {
                                                        userId: socket.username,
                                                        currLat: lat,
                                                        currLng: lng,
                                                        currLocality: myRoom
                                                    });
                                                    searchDriver = false;
                                                    userData[socket.username].onRide = true;
                                                    socket.leave(myRoom);
                                                    myRoom = parseInt(data.taxiRequestId);
                                                    socket.join(myRoom);
                                                    socket.room = myRoom;
                                                    if(userData[parseInt(data.passengerId)])
                                                        userData[parseInt(data.passengerId)].onRide = true;
                                                    socket.to(myRoom).emit('driver request', {
                                                        status: "success",
                                                        message: "The driver will arrive soon!",
                                                        driver: data.driver,
                                                        taxiRequestId: parseInt(data.taxiRequestId),
                                                        carDetails: userTaxiDetails
                                                    });
                                                }
                                            }
                                            if (socket.usertype.toString() === 'P') {
                                                if (data.dResponse.toString() === 'Y') {
                                                    clearTimeout(reqFuncTymOut);
                                                }
                                                //start one by one req send driver
                                                // this besically working id a driver reject then the request goes to another driver
                                                //console.log("Passenger Driver Response: ", message);
                                                clearTimeout(funcTimeout);
                                                taxiRequest();
                                                //end one by one req send driver
                                            }
                                        }
                                    }
                                    break;
                                case 'DriverRequest':
                                    if (!userData[socket.username].onRide) {
                                        if (socket.usertype.toString() === 'P' && !searchDriver) {
                                            searchDriver = true;
                                            taxiReqId = parseInt(data.taxiRideId);
                                            socket.leave(myRoom);
                                            myRoom = taxiReqId;
                                            socket.join(myRoom);
                                            socket.room = myRoom;
                                            //start one by one req send driver
                                            // hide this section if want to send the req all the driver at a time
                                            driverReqData = data.requestData;
                                            clearTimeout(funcTimeout);
                                            taxiRequest();
                                            //end one by one req send driver
                                            reqFuncTymOut = setTimeout(function () {
                                                if (onLine[socket.username]) {
                                                    if (userData[socket.username].onRide) {
                                                        searchDriver = false;
                                                    } else {
                                                        mysqlDB.updating(TAXI_RIDE_TABLE, "id=" + taxiReqId + " AND status='P'", {
                                                            status: "V",
                                                            updated_at: new Date()
                                                        });
                                                        socket.emit('driver request', {
                                                            status: "fail",
                                                            message: "No driver available!",
                                                            taxiRequestId: taxiReqId
                                                        });
                                                        searchDriver = false;
                                                        socket.leave(socket.room);
                                                        myRoom = locality;
                                                        socket.join(myRoom);
                                                        socket.room = myRoom;
                                                    }
                                                }
                                            }, defaultSearchTimeout);
                                        }
                                    }
                                    else {
                                        socket.emit('driver request', {
                                            status: "fail",
                                            message: "Already on ride!"
                                        });
                                    }
                                    break;
                            }
                        }
                    }
                } else {
                    if (data.userType) {
                        if (socket.usertype.toString() === data.userType.toString()) {
                            console.log("message.event else",message.event);
                            switch (message.event) {
                                // this section commited against send the ride request driver one by one, open this section to send request at a time to all the driver
                                /*case 'DriverRequest':
                                    console.log("2nd DriverRequest");
                                    if (onLine[socket.username] && !searchDriver) {
                                        if (!userData[socket.username].onRide && taxiType.indexOf(parseInt(data.requestData.taxiType)) > -1) {
                                            let radlat1 = Math.PI * parseFloat(lat) / 180;
                                            let radlat2 = Math.PI * parseFloat(data.requestData.fromLat) / 180;
                                            let theta = parseFloat(lng) - parseFloat(data.requestData.fromLng);
                                            let radtheta = Math.PI * theta / 180;
                                            let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                                            if (dist > 1) {
                                                dist = 1;
                                            }
                                            dist = Math.acos(dist);
                                            dist = dist * 180 / Math.PI;
                                            dist = dist * 60 * 1.1515;
                                            if (dist < 5) {
                                                mysqlDB.updating(USER_TAXI_TABLE, "userId=" + socket.username, {
                                                    requested: "Y",
                                                    updated_at: new Date()
                                                });
                                                taxiReqId = data.taxiRideId;
                                                searchDriver = true;
                                                let responseData = {
                                                    passengerId: data.requestData.passengerId,
                                                    passengerFirstName: data.requestData.passengerFirstName,
                                                    passengerLastName: data.requestData.passengerLastName,
                                                    passengerProfilePicture: data.requestData.passengerProfilePicture,
                                                    passengerCountryCode: data.requestData.passengerCountryCode,
                                                    passengerMobile: data.requestData.passengerMobile,
                                                    taxiRequestId: data.taxiRideId,
                                                    fromAddress: data.requestData.fromAddress,
                                                    fromLat: data.requestData.fromLat,
                                                    fromLng: data.requestData.fromLng,
                                                    toAddress: data.requestData.toAddress,
                                                    toLat: data.requestData.toLat,
                                                    toLng: data.requestData.toLng,
                                                    estimatedDistance: data.requestData.estimatedDistance,
                                                    estimatedTime: data.requestData.estimatedTime,
                                                    fare: data.requestData.fare,
                                                    discounted: data.requestData.discounted,
                                                    estimatedFare: data.requestData.estimatedFare,
                                                    paidBy: data.requestData.paidBy
                                                };
                                                pendingTaxiRide.push(responseData);
                                                socket.volatile.emit('request taxi', responseData);
                                                reqFuncTymOut = setTimeout(function () {
                                                    if (!userData[socket.username].onRide) {
                                                        mysqlDB.updating(USER_TAXI_TABLE, "userId=" + socket.username, {
                                                            requested: "N",
                                                            updated_at: new Date()
                                                        });
                                                        socket.emit('free driver', {
                                                            status: "success",
                                                            taxiRequestId: taxiReqId,
                                                            message: "Request Timeout!"
                                                        });
                                                        taxiReqId = 0;
                                                        searchDriver = false;
                                                    }
                                                }, defaultSearchTimeout);
                                            }
                                        }
                                        else {
                                            socket.emit('driver request', {
                                                status: "fail",
                                                message: "Already on ride!"
                                            });
                                        }
                                    }
                                    break;
                                    */
                                case 'ReleaseDrivers':
                                    if (parseInt(taxiReqId) === parseInt(data.taxiRequestId) && !userData[socket.username].onRide) {
                                        mysqlDB.updating(USER_TAXI_TABLE, "userId=" + socket.username, {
                                            requested: "N",
                                            updated_at: new Date()
                                        });
                                        taxiReqId = 0;
                                        searchDriver = false;
                                        socket.emit('free driver', {
                                            status: "success",
                                            taxiRequestId: data.taxiRequestId,
                                            message: "Ride accepted by someone!"
                                        });
                                    }
                            }
                        }
                    }
                }
            }
            else {
                socket.emit('appError', {
                    status: "RAU"
                });
            }
        }
    });

    // current location
    socket.on('location', function (data) {
        data = JSON.parse(data);
        //console.log("location :",data);
        if (typeof data.locality !== 'undefined' && data.locality !== '' && typeof data.userId !== 'undefined' && data.userId !== '' && typeof data.lat !== 'undefined' && data.lat !== '' && typeof data.lng !== 'undefined' && data.lng !== '') {
            lat = parseFloat(data.lat);
            lng = parseFloat(data.lng);
            locality = data.locality.toString();
            if (addedUser && !disconnected) {
                if (onLine[socket.username]) {
                    if (!userData[socket.username].onRide && !searchDriver) {
                        if (myRoom === null) {
                            myRoom = locality;
                            socket.join(myRoom);
                            socket.room = myRoom;
                        }
                        else {
                            if (socket.room !== locality) {
                                if (socket.usertype.toString() === 'D') {
                                    socket.to(socket.room).emit('driver left', {
                                        userId: socket.username,
                                        currLat: lat,
                                        currLng: lng,
                                        currLocality: myRoom
                                    });
                                }
                                socket.leave(socket.room);
                                myRoom = locality;
                                socket.join(myRoom);
                                socket.room = myRoom;
                                if (socket.usertype.toString() === 'D') {
                                    socket.to(myRoom).emit('driver joined', {
                                        currLat: lat,
                                        currLng: lng,
                                        currLocality: myRoom,
                                        userId: socket.username
                                    });
                                }
                            }
                        }
                    }
                    if (socket.usertype.toString() === 'D' && myRoom !== null) {
                        mysqlDB.updating(USER_TAXI_TABLE, "userId=" + socket.username, {
                            currLat: lat,
                            currLng: lng,
                            currLocality: locality,
                            updated_at: new Date()
                        });
                        if (first) {
                            socket.to(myRoom).emit('driver joined', {
                                currLat: lat,
                                currLng: lng,
                                currLocality: myRoom,
                                userId: socket.username
                            });
                            first = false;
                        }
                        else {
                            socket.to(myRoom).emit('driver location', {
                                currLat: lat,
                                currLng: lng,
                                currLocality: myRoom,
                                userId: socket.username
                            });
                        }

                        if (userData[socket.username].onRide && pushArrivedDriver) {
                            let taxiRequestId = socket.room;
                            if (parseInt(taxiRequestId)>0) {
                                console.log('taxiRequestId', taxiRequestId);
                                console.log("100.00/1609",100.00/1609);
                                let condition = "id=" + taxiRequestId + " AND status IN ('S') AND SQRT( POW(69.1 * (fromLat - " + lat + "), 2) + POW(69.1 * (" + lng + "- fromLng) * COS(fromLat / 57.3), 2)) <=" + (100.00/1609) ;
                                mysqlDB.fetching(TAXI_RIDE_TABLE, condition , '', '', '', '', function (rideErr, userTexiRide) {
                                    if (rideErr) {
                                        console.log(rideErr);
                                    }else{
                                        if (userTexiRide && userTexiRide[0].id == taxiRequestId) {
                                            let passengerId = userTexiRide[0].passengerId;
                                            pushArrivedDriver = false;
                                            $data = {
                                                'passengerId': passengerId,
                                                'taxiRequestId': taxiRequestId,
                                            }
                                            driverArrived($data);
                                            console.log("push notification sent Successfully");
                                        }
                                        console.log("userTexiRide",userTexiRide[0].id);
                                    }
                                });
                            }
                        }
                    }
                    if (socket.usertype.toString() === 'P' && myRoom !== null) {
                        if (userData[socket.username].onRide) {
                            socket.to(myRoom).emit('passenger location', {
                                currLat: lat,
                                currLng: lng,
                                taxiRideId: myRoom,
                                userId: socket.username
                            });
                        }
                        else {
                            /*if (first) {
                                let query = "SELECT ut.userId, ut.taxiTypeId, ut.currLat, ut.currLng, ut.currLocality, ut.available, ut.forHire, u.driverApproval, (3959 * acos(cos(radians('" + lat + "')) * cos(radians(currLat)) * cos( radians(currLng) - radians('" + lng + "')) + sin(radians('" + lat + "')) * sin(radians(currLat)))) AS distance FROM " + USER_TAXI_TABLE + " AS ut JOIN "+USER_TABLE+" AS u ON ut.userId = u.id HAVING ut.available='Y' AND ut.forHire='Y' AND distance<=10"/!* AND u.driverApproval = 'A'"*!/;
                                mysqlDB.querying(query, function (err, driverData) {
                                    if (err) {
                                        console.log(err);
                                    }
                                    else {

                                        socket.emit('drivers available', {
                                            drivers: driverData
                                        });
                                    }
                                });
                                first = false;
                            }*/
                        }
                    }

                    mysqlDB.updating(USER_TABLE, "id=" + socket.username, {
                        curr_lat: lat,
                        curr_lng: lng,
                        location: locality,
                        is_online: 'Y',
                        updated_at: new Date()
                    });

                }

            } else {
                socket.emit('appError', {
                    status: "RAU"
                });
            }
        }
        else {
            socket.emit('appError', {
                status: "MP"
            });
        }
    });


    let taxiRequestQueue = function () {

    };


    // send drivers request turn by turn
    let taxiRequest = function () {
        clearTimeout(funcTimeout);
        if (reqDrivers.length > 0) {
            mysqlDB.updating(USER_TAXI_TABLE, "userId=" + reqDrivers[reqDrivers.length - 1], {
                requested: "N",
                updated_at: new Date()
            });
        }
        if (addedUser && !disconnected) {
            if (onLine[socket.username] && socket.usertype.toString() === 'P') {
                if (!userData[socket.username].onRide && reqDrivers.length < 8) {
                    let query = "SELECT ut.userId, ut.taxiTypeId, ut.available, ut.forHire, ut.requested, u.driverApproval, (3959 * acos(cos(radians('" + driverReqData.fromLat + "')) * cos(radians(ut.currLat)) * cos( radians(ut.currLng) - radians('" + driverReqData.fromLng + "')) + sin(radians('" + driverReqData.fromLat + "')) * sin(radians(ut.currLat)))) AS distance FROM "+USER_TAXI_TABLE+" AS ut JOIN "+USER_TABLE+" AS u ON ut.userId = u.id HAVING ut.available='Y' AND ut.forHire='Y' AND ut.requested='N' AND ut.taxiTypeId=" + driverReqData.taxiType + " AND distance<=5 AND u.driverApproval = 'Y'";
                    if (reqDrivers.length > 0)
                        query += " AND ut.userId NOT IN (" + reqDrivers.join() + ")";
                    query += " ORDER BY RAND() LIMIT 1";
                    mysqlDB.querying(query, function (uTErr, userTaxi) {
                        if (uTErr) {
                            console.log(uTErr);
                        }
                        else {
                            if (userTaxi.length > 0) {
                                let userTaxis = userTaxi[0];
                                reqDrivers.push(userTaxis.userId);
                                if (onLine[userTaxis.userId]) {
                                    mysqlDB.updating(USER_TAXI_TABLE, "userId=" + reqDrivers[reqDrivers.length - 1], {
                                        requested: "Y",
                                        updated_at: new Date()
                                    });
                                    let responseData = {
                                        passengerId: socket.username,
                                        passengerFirstName: userData[socket.username].firstName,
                                        passengerLastName: userData[socket.username].lastName,
                                        passengerProfilePicture: userData[socket.username].profilePicture,
                                        taxiRequestId: taxiReqId,
                                        passengerMobile: userData[socket.username].mobile,
                                        fromAddress: driverReqData.fromAddress,
                                        fromLat: driverReqData.fromLat,
                                        fromLng: driverReqData.fromLng,
                                        toAddress: driverReqData.toAddress,
                                        toLat: driverReqData.toLat,
                                        toLng: driverReqData.toLng,
                                        estimatedDistance: driverReqData.estimatedDistance,
                                        estimatedTime: driverReqData.estimatedTime,
                                        fare: driverReqData.fare,
                                        discounted: driverReqData.discounted,
                                        estimatedFare: driverReqData.estimatedFare,
                                        paidBy: driverReqData.paidBy
                                    };
                                    console.log("sending request to", onLine[userTaxis.userId]);
                                    socket.to(onLine[userTaxis.userId]).volatile.emit('request taxi', responseData);
                                    funcTimeout = setTimeout(function () {
                                        taxiRequest();
                                    }, defaultSearchTimeout);
                                }
                                else {
                                    taxiRequest();
                                }
                            }
                            else {
                                reqDrivers = [];
                                mysqlDB.updating(TAXI_RIDE_TABLE, "id=" + taxiReqId + " AND status='P'", {
                                    status: "V",
                                    updated_at: new Date()
                                });
                                clearTimeout(reqFuncTymOut);
                                socket.emit('driver request', {
                                    status: "fail",
                                    message: "No driver available in this location!",
                                    taxiRequestId: taxiReqId
                                });
                                searchDriver = false;
                                socket.leave(socket.room);
                                myRoom = locality;
                                socket.join(myRoom);
                                socket.room = myRoom;
                            }
                        }
                    });
                }
                else {
                    if ((!userData[socket.username].onRide && reqDrivers.length === 8)) {
                        clearTimeout(funcTimeout);
                        reqDrivers = [];
                        mysqlDB.updating(TAXI_RIDE_TABLE, "id=" + taxiReqId + " AND status='P'", {
                            status: "V",
                            updated_at: new Date()
                        });
                        clearTimeout(reqFuncTymOut);
                        socket.emit('driver request', {
                            status: "fail",
                            message: "None of our drivers agreed with your offer!",
                            taxiRequestId: taxiReqId
                        });
                        searchDriver = false;
                        socket.leave(socket.room);
                        myRoom = locality;
                        socket.join(myRoom);
                        socket.room = myRoom;
                    }
                    else {
                        if (userData[socket.username].onRide) {
                            searchDriver = false;
                            reqDrivers = [];
                            clearTimeout(funcTimeout);
                        }
                    }
                }
            }
        }
        else {
            socket.emit("appError", {
                status: "RAU"
            });
        }
    };

    // driver reject by socket
    socket.on('driver reject', function (data){
        //console.log("driver reject");
        if (onLine[socket.username]) {
            if (!userData[socket.username].onRide) {
                if (socket.usertype.toString() === 'D') {
                    if (data.dResponse.toString() === 'Y') {
                        clearTimeout(reqFuncTymOut);
                        socket.to(myRoom).emit('driver left', {
                            userId: socket.username,
                            currLat: lat,
                            currLng: lng,
                            currLocality: myRoom
                        });
                        searchDriver = false;
                        userData[socket.username].onRide = true;
                        socket.leave(myRoom);
                        myRoom = parseInt(data.taxiRequestId);
                        socket.join(myRoom);
                        socket.room = myRoom;
                        if(userData[parseInt(data.passengerId)])
                            userData[parseInt(data.passengerId)].onRide = true;
                        socket.to(myRoom).emit('driver request', {
                            status: "success",
                            message: "The driver will reach you soon!",
                            driver: data.driver,
                            taxiRequestId: parseInt(data.taxiRequestId),
                            carDetails: userTaxiDetails
                        });
                    }
                }
                if (socket.usertype.toString() === 'P') {
                    if (data.dResponse.toString() === 'Y') {
                        clearTimeout(reqFuncTymOut);
                        //console.log('driver response passenger', data);
                    }
                    //start one by one req send driver
                    // this besically working id a driver reject then the request goes to another driver
                    //console.log("Passenger Driver Response: ", message);
                    clearTimeout(funcTimeout);
                    taxiRequest();
                    //end one by one req send driver
                }
            }
        }
    });


    // disconnecting user
    socket.on('disconnect', function () {
        console.log('disconnection ' + socket.username);
        if (addedUser && !disconnected) {
            if (onLine[socket.username]) {
                if (myRoom !== null) {
                    if (socket.usertype.toString() === 'D') {
                        mysqlDB.updating(USER_TAXI_TABLE, "userId=" + socket.username, {
                            available: "N",
                            requested: "N",
                            forHire: "N",
                            updated_at: new Date()
                        });
                        socket.to(myRoom).emit('driver left', {
                            userId: socket.username,
                            currLat: lat,
                            currLng: lng,
                            currLocality: myRoom
                        });
                    }
                    if (socket.usertype.toString() === 'P') {
                        //console.log("Passenger");
                        mysqlDB.fetching(TAXI_RIDE_TABLE, "passengerId=" + socket.username + " AND status IN ('P','V')", '', '', '', '', function (rideErr, ride) {
                            if (rideErr) {
                                console.log(rideErr);
                            }
                            else {
                                // console.log("Rejected or void ride");
                                // console.log(ride);
                                if (ride.length > 0) {
                                    for (let ij = 0; ij < ride.length; ij++)
                                    {
                                        let rejectRideData = ride[ij];
                                        delete rejectRideData.id;
                                        console.log("Requested rejected!");
                                        //console.log(rejectRideData);
                                        mysqlDB.inserting(REJECTED_RIDE_TABLE, rejectRideData);
                                    }
                                }
                            }
                            mysqlDB.deleting(TAXI_RIDE_TABLE, "passengerId=" + socket.username + " AND status IN ('P','V')");
                        });
                    }
                    socket.leave(myRoom);
                    myRoom = null;
                    mysqlDB.updating(USER_TABLE, "id=" + socket.username, {
                        is_online: 'N',
                        updated_at: new Date()
                    });
                }
                delete onLine[socket.username];
                delete userData[socket.username];
                --numUsers;
            }
        }
        redisClient.disconnect();
        disconnected = true;
        clearTimeout(funcTimeout);
        clearTimeout(reqFuncTymOut);
        socket.disconnect(true);
    });


    socket.on('driver arrived', function (data){
        data = JSON.parse(data);
    });

    let driverArrived = function (data) {
        console.log("driver arrive",data);
        if (addedUser && !disconnected) {
            let driverDetails = {
                id: userData[socket.username].id,
                firstName: userData[socket.username].firstName,
                lastName: userData[socket.username].lastName,
                gender: userData[socket.username].gender,
                countryCode: userData[socket.username].countryCode,
                mobile: userData[socket.username].mobile,
                profilePicture: userData[socket.username].profilePicture
            }
            let condition = "userId=" + data.passengerId;
            mysqlDB.fetching(USER_TOKEN_TABLE, condition , "", "", "", "", function (resErrs, resData) {
                if (resErrs) {
                    console.log("resErrs",resErrs);
                }else{
                    if (resData.length > 0) {
                        let message = "Driver " + driverDetails.firstName + " arrived at your location.";
                        console.log("message", message);
                        let fcmTokensD = resData[0].fcmToken;
                        let deviceType = resData[0].flag;
                        if (fcmTokensD != null && fcmTokensD != "") {
                            var payload_push = 
                            {
                                data: ({
                                    notiType: 'TDA',
                                    message: JSON.stringify({
                                        title: "Desty",
                                        msg: message,
                                        taxiRequestId: data.taxiRequestId,
                                        driverDetails: driverDetails
                                    }),
                                    badge: '1',
                                    clickAction: ".HomeActivity"
                                }),
                                notification: {
                                    message: JSON.stringify({
                                        notiType: 'TDA',
                                        title: "Desty",
                                        msg: message,
                                        taxiRequestId: data.taxiRequestId,
                                        driverDetails: driverDetails
                                    }),
                                    alert: "Desty",
                                    clickAction: ".HomeActivity",
                                    title: "Desty",
                                    body: message,
                                    sound: 'default'
                                }
                            };
                            console.log("payload_push",payload_push);
                            admin.messaging().sendToDevice(fcmTokensD, payload_push).then((response) => {
                                console.log('Successfully sent driver arrival message to rider:', response);
                            }).catch((error) => {
                                console.log('Error sending driver arrival message to rider:', error);
                            });
                        }
                    }
                }
            });
        }
    };
});
