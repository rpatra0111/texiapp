<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Non-Auth routes
Route::group(['namespace' => 'Api', 'prefix' => '/'], function () {
    Route::get('taxiType', 'ApiController@taxiTypeList');
    Route::get('languages', 'ApiController@languageList');
    Route::get('docType', 'ApiController@docTypeList');
    Route::get('bankList', 'ApiController@bankList');
    Route::get('topicList', 'ApiController@topicList');
    Route::get('couponList', 'ApiController@couponList');
    Route::get('carColourList', 'ApiController@carColourList');
    Route::get('test', 'ApiController@test');
    
    Route::get('appdl', 'ApiController@appDeepLink');

    Route::post('sendOTP', 'Auth\RegisterController@sendOTP');
    Route::post('register', 'Auth\RegisterController@register');

    Route::post('login', 'Auth\LoginController@login');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/verifyOTP', 'Auth\ForgotPasswordController@verifyOTP')->name('password.verifyotp');
    Route::post('password/mobile', 'Auth\ForgotPasswordController@sendResetOTP');
    Route::post('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');

    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::post('password/validateOTP', 'Auth\ResetPasswordController@validateOTP');

    //Auth Routes
    Route::group(['middleware' => ['auth:api']], function () {
        Route::post('suggestPrice', 'ApiController@recommendedPrice');
        Route::post('couponValidate', 'ApiController@couponValidate');

        Route::post('password/mReset', 'Auth\ResetPasswordController@otpReset')->name('password.mReset');

        Route::post('logout', 'Auth\LoginController@logout');

        Route::post('confirmNumber', 'Auth\RegisterController@verifyPhone');
        Route::get('resendOTP', 'Auth\RegisterController@resendOTP');
        Route::post('registerPassenger', 'Auth\RegisterController@registerPassenger');

        Route::post('editProfile', 'UserController@editProfile');
        Route::post('updateProfilePicture', 'UserController@saveProfilePicture');
        Route::post('updatePassword', 'UserController@changePassword');
        //Route::post('updateNumber', 'UserController@changeNumber');
        Route::get('userDetails', 'UserController@userDetails');
        Route::post('verifyDocs', 'UserController@verifyDocs');
        Route::get('verifyDocs', 'UserController@verifyDocs');
        Route::post('showRating', 'UserController@userRating');
        Route::get('notificationList', 'UserController@notificationList');
        Route::post('keepLoggedIn', 'UserController@keepLoggedIn');

        Route::post('add-car', 'UserCarController@store');
        Route::post('update-car', 'UserCarController@update');
        Route::get('car-details', 'UserCarController@show');

        Route::get('bankAccountDetails', 'UserController@bankAccountDetails');
        Route::get('bankAccountDetails/{account_id}', 'UserController@bankAccountDetails');
        Route::post('updateBankAccount', 'UserController@updateBankAccount');
        Route::post('queryForHelp', 'UserController@queryForHelp');

        Route::post('rateUser', 'RideController@rateUser');
        Route::get('rideHistory', 'RideController@rideHistory');
        Route::get('rideData', 'RideController@onRideData');
        Route::post('startRide', 'RideController@startRide');
        Route::post('finishRide', 'RideController@finishRide');
        Route::post('cashPaid', 'RideController@cashPaid');
        Route::post('cancelRide', 'RideController@cancelRide');
        Route::post('driverRequest', 'RideController@driverRequest');
        Route::post('driverResponse', 'RideController@driverResponse');

        Route::post('cardPay', 'TransactionController@stripePay');
        Route::get('transactionHistory', 'TransactionController@transactionHistory');
        Route::get('cashOut', 'TransactionController@cashOut');
        Route::post('payTips', 'TransactionController@payTips');
        Route::get('userCards', 'TransactionController@cardListing');

    });
});
