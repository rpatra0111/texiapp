<?php
// use DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['namespace' => 'Site'], function () {
    Route::get('/', 'HomeController@index');
    // Route::get('/cc', 'HomeController@cc');
    Route::get('terms', 'HomeController@terms');
    Route::get('unsubscribe/{token}', 'UserController@unsubscribe');
    // Route::get('/cc', function(){
    //     DB::table('users')->where('email','pabitra@technoexponent.com')
    //     ->update([
    //         'zipcode' =>  rand()
    //     ]);
    // });
});


Route::group(['prefix' => 'cron/'], function () {
    Route::get('driverReset', 'Cron@resetDriverStatus');
});


/**
 * Admin Authenticate control routes.
 */

Route::group(['namespace' => 'Admin', 'prefix' => config('constant.ADMIN_ROUTE_PREFIX') . '/'], function () {
    Route::get('/', 'AdminController@index');

    Route::get('getCities/{id}', 'Ajax@fetchAllCities');
    Route::get('getStates/{id}', 'Ajax@fetchAllStates');

    Route::get('login', 'Auth\LoginController@showLoginForm');
    Route::post('loginProcess', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout');
    /* Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');*/

    Route::get('dashboard', 'HomeController@index');
    Route::get('profile', 'HomeController@profile');
    Route::post('profile', 'HomeController@updateProfile');

    Route::get('passenger', 'UserAccess@index');
    Route::get('passenger/create', 'UserAccess@create');
    Route::post('passenger', 'UserAccess@store');
    Route::get('passenger/{id}/edit', 'UserAccess@edit');
    Route::match(['put', 'patch'], 'passenger/{id}', 'UserAccess@update');
    Route::delete('passenger/{id}', 'UserAccess@destroy');
    Route::post('passenger/{id}/change-status', 'UserAccess@changeStatus');
    Route::get('passenger/rides/{id}', 'UserAccess@listRides');

    Route::get('driver', 'UserAccess@index');
    Route::get('driver/create', 'UserAccess@create');
    Route::post('driver', 'UserAccess@store');
    Route::get('driver/{id}/edit', 'UserAccess@edit');
    //rahul changes start 11/18/19
    Route::get('driver/{id}/bank-details', 'UserAccess@bankInfo');
    //rahul changes end
    Route::match(['put', 'patch'], 'driver/{id}', 'UserAccess@update');
    Route::delete('driver/{id}', 'UserAccess@destroy');
    Route::post('driver/{id}/change-status', 'UserAccess@changeStatus');
    Route::get('driver/document', 'UserAccess@listDocs');
    Route::get('driver/{id}/document', 'UserAccess@listUserDocs');
    Route::post('driver/document/{id}/update', 'UserAccess@docUpdate');
    Route::post('driver/document/{id}/upsert', 'UserAccess@docUpsert');
    Route::get('driver/withdrawal', 'UserAccess@listCashout');
    Route::get('driver/withdrawal/{id}/update', 'UserAccess@cashoutUpdate');
    Route::get('driver/withdrawal', 'UserAccess@listCashout');
    Route::get('driver/withdrawal/{id}/update', 'UserAccess@cashoutUpdate');
    Route::get('driver/walletadjust', 'UserAccess@listNegBalance');
    Route::get('driver/paymentadjust', 'UserAccess@listPosBalance');
    Route::get('driver/walletadjust/{id}/reset', 'UserAccess@resetWallet');
    Route::get('driver/paymentadjust/{id}/resetPayment', 'UserAccess@resetWalletbyPayment');
    Route::get('driver/rides/{id}', 'UserAccess@listRides');
    Route::get('driver/ssn-pending', 'UserAccess@ssnPendingList');
    Route::post('driver/{id}/change-ssn-status', 'UserAccess@ssnUpdate');
    Route::get('driver/rating', 'UserAccess@ratingList');
    Route::post('get-taxi-details', 'UserAccess@getTaxiDetails');
    Route::post('user/add-edit-car', 'UserAccess@addEditCar');

    Route::get('subAdmin', 'AdminAccess@index');
    Route::get('subAdmin/create', 'AdminAccess@create');
    Route::post('subAdmin', 'AdminAccess@store');
    Route::get('subAdmin/{id}/edit', 'AdminAccess@edit');
    Route::match(['put', 'patch'], 'subAdmin/{id}', 'AdminAccess@update');
    Route::delete('subAdmin/{id}', 'AdminAccess@destroy');
    Route::post('subAdmin/updateMenuPermit/{id}', 'AdminAccess@updatePermission');
    Route::post('subAdmin/{id}/change-status', 'AdminAccess@changeStatus');

    Route::get('rides/user-rides', 'RideController@index');
    Route::get('rides/rejected-rides', 'RideController@rejectedRides');

    /*DO NOT CHANGE THE ORDER*/
    Route::group(['prefix' => 'taxitype'], function () {
        Route::resource('rate', 'CitySpecificCost');
    });

    Route::resource('taxitype', 'TaxiAccess');
    /*DO NOT CHANGE THE ORDER*/

    Route::resource('coupon', 'CouponController');

    Route::get('location/{type}', 'LocationController@index');
    Route::get('location/{type}/create', 'LocationController@create');
    Route::post('location/{type}', 'LocationController@store');
    Route::get('location/{type}/{id}/edit', 'LocationController@edit');
    Route::match(['put', 'patch'], 'location/{type}/{id}', 'LocationController@update');
    Route::delete('location/{type}/{id}', 'LocationController@destroy');

    Route::resource('bank', 'BankController');

    Route::resource('email', 'EmailController');

    Route::resource('helptopic', 'HelpTopic');

    Route::group(['prefix' => 'site/'], function () {
        Route::resource('review', 'ReviewController');
        Route::get('settings', 'SiteController@settings');
        Route::post('settings', 'SiteController@updateSettings');
    });

    Route::get('transaction/{type}', 'TransactionController@index');
    Route::get('{type}/transaction', 'TransactionController@index');
    Route::get('{type}/ride-transaction', 'TransactionController@rideTransactions');
});